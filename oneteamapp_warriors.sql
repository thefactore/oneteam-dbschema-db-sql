-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: oneteam-test-cluster.cluster-cb9popnocd21.ap-south-1.rds.amazonaws.com
-- Generation Time: May 17, 2021 at 12:32 PM
-- Server version: 5.6.10-log
-- PHP Version: 7.2.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oneteamapp_warriors`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE `activity` (
  `id` int(11) NOT NULL,
  `activity_global_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `companybadge_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` longtext COLLATE utf8_bin NOT NULL,
  `image` varchar(255) COLLATE utf8_bin NOT NULL,
  `dashboard_image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `start_time` time NOT NULL,
  `points` int(11) NOT NULL,
  `rules` longtext COLLATE utf8_bin NOT NULL,
  `rounds` int(11) NOT NULL,
  `activity_type` int(11) NOT NULL,
  `icons` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 - active, 2 - inactive',
  `activity_cost` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `notification_flag` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `activity`
--

INSERT INTO `activity` (`id`, `activity_global_id`, `created_at`, `company_id`, `companybadge_id`, `title`, `description`, `image`, `dashboard_image`, `start_date`, `start_time`, `points`, `rules`, `rounds`, `activity_type`, `icons`, `status`, `activity_cost`, `duration`, `notification_flag`) VALUES
(1, 'titlefight01', '2018-08-14 04:39:48', 2, 1, 'Title Fight', '<p>Description....</p>', '153417658410704685835b71ad482da23.png', '153417658415514710315b71ad48329e1.jpg', '2018-09-24 13:00:00', '00:00:00', 30, '<p>Rules...</p>', 2, 1, '1,4', 1, 30, 80, 4);

-- --------------------------------------------------------

--
-- Table structure for table `activity_icons`
--

CREATE TABLE `activity_icons` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `image` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `activity_icons`
--

INSERT INTO `activity_icons` (`id`, `created_at`, `title`, `image`) VALUES
(1, '2018-08-01 01:55:37', 'Difficulty Level', '15330543378302418485b608d8191317.png'),
(2, '2018-08-01 01:55:55', 'Connectivity', '15330543557577489195b608d931afc4.png'),
(3, '2018-08-01 01:56:47', 'Type', '15330544073866797375b608dc776555.png'),
(4, '2018-08-01 01:56:56', 'Language', '153305441617524317955b608dd0a48dd.png');

-- --------------------------------------------------------

--
-- Table structure for table `activity_tasks`
--

CREATE TABLE `activity_tasks` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `task_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 - QnA',
  `duration` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `question` varchar(255) COLLATE utf8_bin NOT NULL,
  `answer_type` tinyint(4) NOT NULL COMMENT '1 - self group, 2 - others',
  `round` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `activity_tasks`
--

INSERT INTO `activity_tasks` (`id`, `created_at`, `title`, `task_type`, `duration`, `activity_id`, `question`, `answer_type`, `round`, `image`) VALUES
(1, '2018-08-13 16:09:48', 'Round 1', 1, 40, 1, 'Who is funniest ?', 0, 1, '15341765884762843905b71ad4c17bc1.png'),
(2, '2018-08-13 16:09:48', 'Round 2', 1, 40, 1, 'Who is stronest ?', 0, 2, '153417658817403674545b71ad4c199a9.jpg'),
(3, '2018-09-06 06:34:40', 'FDSAFA', 1, 5, 2, 'FSAFA', 0, 1, '153621568010851839945b90ca8074877.png'),
(4, '2018-09-06 06:34:40', 'FDSAF', 1, 5, 2, 'DSAD', 0, 2, '15362156802034576805b90ca8075887.png');

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `image` varchar(255) COLLATE utf8_bin NOT NULL,
  `dashboard_image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1 - active, 2 - inactive',
  `publish_date` datetime DEFAULT NULL,
  `likes` int(11) NOT NULL DEFAULT '0',
  `notification_flag` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `announcement_likes`
--

CREATE TABLE `announcement_likes` (
  `announcement_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `announcement_likes`
--

INSERT INTO `announcement_likes` (`announcement_id`, `employee_id`, `created_at`) VALUES
(1, 1, '2018-08-13 11:28:41'),
(3, 10, '2018-08-16 09:58:55'),
(4, 10, '2018-08-16 11:35:09'),
(5, 10, '2018-08-16 11:35:10'),
(6, 10, '2018-08-16 10:02:10'),
(7, 17, '2018-08-18 08:59:56'),
(2, 27, '2018-08-20 17:33:09'),
(7, 18, '2018-08-24 09:48:34');

-- --------------------------------------------------------

--
-- Table structure for table `autoreplymessage`
--

CREATE TABLE `autoreplymessage` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `platform_id` int(11) NOT NULL,
  `platform` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `autoreplymessage`
--

INSERT INTO `autoreplymessage` (`id`, `created_at`, `message`, `platform_id`, `platform`) VALUES
(1, '2018-11-28 08:08:47', 'Thank You from sa-group. We will received your sms.', 1, 'Shortcode Reply'),
(2, '2018-11-28 08:08:55', 'Your activation code is \"{activation_code}\". Thank You for choosing Oneteam.', 3, 'Activation Send'),
(3, '2018-11-28 08:09:04', 'Your request has been submitted. We will revert back shortly.', 2, 'Activation Request');

-- --------------------------------------------------------

--
-- Table structure for table `avatars`
--

CREATE TABLE `avatars` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `image` varchar(255) COLLATE utf8_bin NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `avatars`
--

INSERT INTO `avatars` (`id`, `created_at`, `image`, `title`, `position`) VALUES
(2, '2018-07-16 21:26:40', '153491438012863286785b7cef4cdd517.png', 'First', 1),
(3, '2018-07-20 22:06:53', '15349143425783960565b7cef268f538.png', 'Funky Bunny', 2),
(4, '2018-07-21 10:52:44', '15321199645804773145b524b9c6e304.png', 'Employee Male', 3),
(5, '2018-07-21 10:52:57', '15321199772908182875b524ba9c53f8.png', 'Employee Female', 4),
(6, '2018-07-21 10:53:19', '15321199992299951935b524bbf47956.png', 'Orange Camera Placeholder', 7),
(7, '2018-07-21 10:53:33', '1532120013383357895b524bcd97b15.png', 'Green Gallery Placeholder', 6),
(8, '2018-07-21 10:53:49', '153491430914719153735b7cef051b2e1.png', 'Default Avatar', 5),
(9, '2018-08-22 17:32:01', '15349141218368914425b7cee4952757.png', 'male corporate with phone', 0),
(10, '2018-08-22 17:32:23', '153491414317146646415b7cee5f8a8d2.png', 'female corporate with mac', 0),
(11, '2018-08-22 17:32:40', '15349141604338751645b7cee7037ff1.png', 'male corporate without suit', 0),
(12, '2018-08-22 17:32:56', '153491417612182028435b7cee8047e72.png', 'female corporate', 0),
(13, '2018-08-22 17:33:22', '153491420210728024615b7cee9a8782e.png', 'dog with cap', 0),
(14, '2018-08-22 17:33:44', '153491422419582469455b7ceeb02ebe7.png', 'fox with specs', 0),
(15, '2018-08-22 17:34:01', '15349142411399527055b7ceec1027f5.png', 'cat', 0);

-- --------------------------------------------------------

--
-- Table structure for table `badges`
--

CREATE TABLE `badges` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `barcodescanner`
--

CREATE TABLE `barcodescanner` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gate_id` int(11) NOT NULL,
  `gate_no` int(11) NOT NULL,
  `barcode_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `scan_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `scan_count` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `barcode_no`
--

CREATE TABLE `barcode_no` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `ticket_category_id` int(11) NOT NULL,
  `prefix` varchar(255) COLLATE utf8_bin NOT NULL,
  `random_code` varchar(255) COLLATE utf8_bin NOT NULL,
  `final_barcode_no` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `barcode_ticket_category`
--

CREATE TABLE `barcode_ticket_category` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `event_date` date DEFAULT NULL,
  `category_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `prefix` varchar(255) COLLATE utf8_bin NOT NULL,
  `random_code_length` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `barcode_ticket_category`
--

INSERT INTO `barcode_ticket_category` (`id`, `created_at`, `event_date`, `category_name`, `prefix`, `random_code_length`) VALUES
(8, '2020-02-06 18:22:13', '2020-03-15', 'VVIP  MORNING', 'VVP15SAG03MN20', 8),
(9, '2020-02-06 18:45:34', '2020-03-15', 'VIP MORNING', 'VIP15SAG03MN20', 8),
(10, '2020-02-06 18:48:12', '2020-03-15', 'GENERAL MORNING', 'GEN15SAG03MN20', 8),
(11, '2020-02-06 18:52:11', '2020-02-15', 'VVIP EVENING', 'VVP15SAG03EV20', 8),
(12, '2020-02-06 19:01:11', '2020-03-15', 'VIP EVENING', 'VIP15SAG03EV20', 8),
(13, '2020-02-06 19:01:53', '2020-03-15', 'GENERAL EVENING', 'GEN15SAG03EV20', 8),
(14, '2020-02-11 18:00:09', '2020-02-23', 'EXECUTIVE', 'ZE20', 6),
(15, '2020-02-11 18:01:31', '2020-02-23', 'VIP  MN20', 'ZV20', 6);

-- --------------------------------------------------------

--
-- Table structure for table `chatcamp_data`
--

CREATE TABLE `chatcamp_data` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `channel_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `message_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL,
  `message_poster_role` int(11) NOT NULL,
  `message_type` varchar(255) NOT NULL,
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `total_mention` int(11) NOT NULL,
  `mention_users` longtext NOT NULL,
  `like` int(11) NOT NULL,
  `like_members` longtext NOT NULL,
  `comment` int(11) NOT NULL,
  `comment_by` longtext NOT NULL,
  `is_pinned` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chatcamp_data`
--

INSERT INTO `chatcamp_data` (`id`, `created_at`, `channel_id`, `message_id`, `user_id`, `message_poster_role`, `message_type`, `message`, `total_mention`, `mention_users`, `like`, `like_members`, `comment`, `comment_by`, `is_pinned`) VALUES
(12, '2019-01-04 10:24:43', '5c068c9748bdc64af600c02b', '6475741388725022720', 10, 2, 'text', '', 0, '', 1, '[{\"user_id\":\"4\"}]', 5, '[{\"user_id\":\"55\",\"comment\":\"hi mr amit\"},{\"user_id\":\"55\",\"comment\":\"hi mr amit\"},{\"user_id\":\"55\",\"comment\":\"hi mr amit\"},{\"user_id\":\"55\",\"comment\":\"hi mr amit\"},{\"user_id\":\"55\",\"comment\":\"hi mr amit\"}]', 0),
(21, '2018-12-03 14:38:45', '5c00328448bdc62857c4da9d', '6462623284100853760', 46, 2, 'text', '', 1, '[{\"user_id\":\"4\"}]', 2, '[{\"user_id\":\"4\"},{\"user_id\":\"45\"}]', 1, '[{\"user_id\":\"45\",\"comment\":\"amit kumar yadav test\"}]', 0),
(22, '2018-11-15 15:31:41', '5bacd2e248bdc632119062d9', '6468469713285410816', 80, 2, 'text', '', 3, '[{\"user_id\":\"52\"},{\"user_id\":\"53\"},{\"user_id\":\"80\"}]', 6, '[{\"user_id\":\"52\"},{\"user_id\":\"52\"},{\"user_id\":\"52\"},{\"user_id\":\"52\"},{\"user_id\":\"52\"},{\"user_id\":\"52\"}]', 0, '[]', 0),
(23, '2018-11-15 15:56:19', '5bacd2e248bdc632119062d9', '6468854382912860160', 52, 2, 'text', '', 0, '[]', 5, '[{\"user_id\":\"52\"},{\"user_id\":\"52\"},{\"user_id\":\"52\"},{\"user_id\":\"52\"},{\"user_id\":\"52\"}]', 8, '[{\"user_id\":\"52\",\"comment\":\"own comment\"},{\"user_id\":\"53\",\"comment\":\"ps comment\"},{\"user_id\":\"80\",\"comment\":\"amit comment\"},{\"user_id\":\"52\",\"comment\":\"ankesh comment\"},{\"user_id\":\"53\",\"comment\":\"ps comment\"},{\"user_id\":\"80\",\"comment\":\"amit kumar yadav comment\"},{\"user_id\":\"80\",\"comment\":\"amit kumar comment\"},{\"user_id\":\"80\",\"comment\":\"amit kumar comment\"}]', 0),
(25, '2018-12-03 14:44:55', '5c00328448bdc62857c4da9d', '6474143286054350848', 52, 2, 'text', '', 0, '[]', 0, '[]', 0, '[]', 1),
(26, '2019-01-04 11:27:23', '5c068c9748bdc64af600c02b', '64757413887250227204', 10, 2, 'TEXT', '', 0, '[]', 0, '[]', 0, '[]', 1),
(27, '2019-05-20 13:32:45', '5ce292eeb438aa000112cb1e', '6536224217073971200', 5, 2, 'text', '', 0, '[]', 0, '[]', 0, '[]', 1),
(28, '2019-08-21 07:37:35', '5d5bf6493cec8400015cfd87', '6569843668843229184', 10, 2, 'text', '', 0, '[]', 1, '[{\"user_id\":\"10\"}]', 0, '[]', 0),
(29, '2019-08-21 07:39:21', '5d5bf6493cec8400015cfd87', '6569845221813645312', 10, 2, 'text', '', 1, '[{\"user_id\":\"10\"}]', 0, '[]', 0, '[]', 0),
(30, '2019-08-22 12:43:13', '5d5bf6493cec8400015cfd87', '6570283124553412608', 10, 2, 'attachment', '', 0, '[]', 0, '[]', 0, '[]', 0),
(31, '2019-09-02 07:48:17', '5d6a3bd148bdc619aade0a86', '6573523343545200640', 7, 2, 'text', '', 0, '[]', 2, '[{\"user_id\":\"4\"},{\"user_id\":\"3\"}]', 3, '[{\"user_id\":\"3\",\"comment\":\"test\"},{\"user_id\":\"3\",\"comment\":\"Test\"},{\"user_id\":\"3\",\"comment\":\"test comment\"}]', 0),
(32, '2019-08-31 14:40:49', '5d6a3bd148bdc619aade0a86', '6573500574770130944', 11, 2, 'text', '', 0, '[]', 1, '[{\"user_id\":\"3\"}]', 0, '[]', 0),
(33, '2019-09-01 11:01:33', '5d6a3bd148bdc619aade0a86', '6573495695813111808', 7, 2, 'text', '', 0, '[]', 1, '[{\"user_id\":\"3\"}]', 0, '[]', 0),
(34, '2019-09-02 07:31:24', '5d6a3bd148bdc619aade0a86', '6573501433126055936', 12, 2, 'text', '', 0, '[]', 1, '[{\"user_id\":\"3\"}]', 1, '[{\"user_id\":\"3\",\"comment\":\"Test\"}]', 0),
(35, '2019-09-03 10:32:49', '5d6d0c4d68d2430001664808', '6574270593053487104', 1, 2, 'text', '', 0, '[]', 2, '[{\"user_id\":\"1\"},{\"user_id\":\"4\"}]', 9, '[{\"user_id\":\"1\",\"comment\":\"how are your all\"},{\"user_id\":\"1\",\"comment\":\"fine\"},{\"user_id\":\"1\",\"comment\":\"hi fdsafdasfasfas\"},{\"user_id\":\"1\",\"comment\":\"amit kumar yadav\"},{\"user_id\":\"1\",\"comment\":\"hi brother\"},{\"user_id\":\"1\",\"comment\":\"how are you\"},{\"user_id\":\"1\",\"comment\":\"kune jila ke jalebi\"},{\"user_id\":\"1\",\"comment\":\"dekh lo ni fir mare jaoge\"},{\"user_id\":\"1\",\"comment\":\"super\"}]', 0),
(36, '2019-09-03 10:48:54', '5d6d0c4d68d2430001664808', '6574271133216927744', 1, 2, 'attachment', '', 0, '[]', 3, '[{\"user_id\":\"1\"},{\"user_id\":\"9\"},{\"user_id\":\"6\"}]', 5, '[{\"user_id\":\"1\",\"comment\":\"hi\"},{\"user_id\":\"1\",\"comment\":\"nice pic\"},{\"user_id\":\"1\",\"comment\":\"superb\"},{\"user_id\":\"1\",\"comment\":\"test\"},{\"user_id\":\"9\",\"comment\":\"Test.\"}]', 0),
(37, '2019-09-03 10:18:42', '5d6d0c4d68d2430001664808', '6574274171100983296', 1, 2, 'attachment', '', 0, '[]', 3, '[{\"user_id\":\"1\"},{\"user_id\":\"9\"},{\"user_id\":\"4\"}]', 1, '[{\"user_id\":\"9\",\"comment\":\"fantastic babua\"}]', 0),
(38, '2019-09-03 10:20:17', '5d6d0c4d68d2430001664808', '6574592252520165376', 9, 2, 'text', '', 0, '[]', 2, '[{\"user_id\":\"9\"},{\"user_id\":\"4\"}]', 1, '[{\"user_id\":\"9\",\"comment\":\"test\"}]', 0),
(39, '2019-09-03 11:16:33', '5d6d0c4d68d2430001664808', '6574592692427157504', 9, 2, 'attachment', '', 0, '[]', 4, '[{\"user_id\":\"9\"},{\"user_id\":\"4\"},{\"user_id\":\"1\"},{\"user_id\":\"6\"}]', 2, '[{\"user_id\":\"9\",\"comment\":\"Event Engagement Platform\"},{\"user_id\":\"9\",\"comment\":\"test\"}]', 0),
(40, '2019-09-03 10:24:18', '5d6d0c4d68d2430001664808', '6574594847871594496', 9, 2, 'text', '', 1, '[{\"user_id\":\"1\"}]', 1, '[{\"user_id\":\"4\"}]', 0, '[]', 0),
(41, '2019-09-03 10:25:21', '5d6d0c4d68d2430001664808', '6574593628230905856', 9, 2, 'attachment', '', 0, '[]', 2, '[{\"user_id\":\"4\"},{\"user_id\":\"9\"}]', 0, '[]', 0),
(42, '2019-09-03 10:35:10', '5d6d0c4d68d2430001664808', '6574600282871689216', 9, 2, 'text', '', 0, '[]', 1, '[{\"user_id\":\"4\"}]', 0, '[]', 0),
(43, '2019-09-03 10:47:59', '5d6d0c4d68d2430001664808', '6574600103368060928', 9, 2, 'attachment', '', 0, '[]', 1, '[{\"user_id\":\"6\"}]', 0, '[]', 0),
(44, '2019-09-03 10:44:31', '5d6d0c4d68d2430001664808', '6574601005357330432', 4, 2, 'text', '', 0, '[]', 2, '[{\"user_id\":\"4\"},{\"user_id\":\"6\"}]', 0, '[]', 0),
(45, '2019-09-03 10:46:56', '5d6d0c4d68d2430001664808', '6574602320888197120', 6, 2, 'text', '', 0, '[]', 2, '[{\"user_id\":\"6\"},{\"user_id\":\"4\"}]', 0, '[]', 0),
(46, '2019-09-03 10:43:14', '5d6d0c4d68d2430001664808', '6574602317750857728', 6, 2, 'text', '', 0, '[]', 1, '[{\"user_id\":\"6\"}]', 0, '[]', 0),
(47, '2019-09-03 10:46:41', '5d6d0c4d68d2430001664808', '6574602226390528000', 6, 2, 'text', '', 0, '[]', 2, '[{\"user_id\":\"4\"},{\"user_id\":\"6\"}]', 0, '[]', 0),
(48, '2019-09-03 11:11:19', '5d6d0c4d68d2430001664808', '6574608325000818688', 6, 2, 'text', '', 0, '[]', 2, '[{\"user_id\":\"6\"},{\"user_id\":\"4\"}]', 1, '[{\"user_id\":\"6\",\"comment\":\"nice\"}]', 0),
(49, '2019-09-03 11:16:23', '5d6d0c4d68d2430001664808', '6574607491718443008', 6, 2, 'attachment', '', 0, '[]', 2, '[{\"user_id\":\"6\"},{\"user_id\":\"4\"}]', 3, '[{\"user_id\":\"6\",\"comment\":\"nice\"},{\"user_id\":\"6\",\"comment\":\"ache\"},{\"user_id\":\"6\",\"comment\":\"?\"}]', 0),
(50, '2019-09-03 11:10:40', '5d6d0c4d68d2430001664808', '6574609216693071872', 1, 2, 'text', '', 0, '[]', 1, '[{\"user_id\":\"6\"}]', 0, '[]', 0),
(51, '2019-09-03 16:59:52', '5d6d0c4d68d2430001664808', '6574607224016990208', 6, 2, 'text', '', 0, '[]', 2, '[{\"user_id\":\"4\"},{\"user_id\":\"6\"}]', 6, '[{\"user_id\":\"4\",\"comment\":\"Gi\"},{\"user_id\":\"6\",\"comment\":\"thanks\"},{\"user_id\":\"4\",\"comment\":\"Ji\"},{\"user_id\":\"6\",\"comment\":\"Uuuuuuuuu\"},{\"user_id\":\"6\",\"comment\":\"Kuuuuu\"},{\"user_id\":\"6\",\"comment\":\"Hai\"}]', 0),
(52, '2019-09-03 11:18:16', '5d6d0c4d68d2430001664808', '6574610972541972480', 6, 2, 'text', '', 0, '[]', 1, '[{\"user_id\":\"4\"}]', 1, '[{\"user_id\":\"6\",\"comment\":\"hmm\"}]', 0),
(53, '2019-09-03 11:19:44', '5d6d0c4d68d2430001664808', '6574611582087589888', 6, 2, 'text', '', 0, '[]', 0, '[]', 1, '[{\"user_id\":\"6\",\"comment\":\"ache\"}]', 0),
(54, '2019-09-03 11:22:45', '5d6d0c4d68d2430001664808', '6574611968492040192', 6, 2, 'text', '', 0, '[]', 2, '[{\"user_id\":\"4\"},{\"user_id\":\"6\"}]', 0, '[]', 0),
(55, '2019-09-03 11:32:41', '5d6d0c4d68d2430001664808', '6574613931141099520', 6, 2, 'attachment', '', 0, '[]', 1, '[{\"user_id\":\"6\"}]', 1, '[{\"user_id\":\"6\",\"comment\":\"jjj\"}]', 0),
(56, '2019-09-03 11:38:05', '5d6d0c4d68d2430001664808', '6574615043285970944', 6, 2, 'text', '', 0, '[]', 2, '[{\"user_id\":\"4\"},{\"user_id\":\"6\"}]', 0, '[]', 0),
(57, '2019-09-03 11:38:13', '5d6d0c4d68d2430001664808', '6574612559398170624', 6, 2, 'attachment', '', 0, '[]', 1, '[{\"user_id\":\"6\"}]', 0, '[]', 0),
(58, '2019-09-03 11:44:02', '5d6d0c4d68d2430001664808', '6574617313666920448', 6, 2, 'text', '', 0, '[]', 2, '[{\"user_id\":\"4\"},{\"user_id\":\"6\"}]', 1, '[{\"user_id\":\"6\",\"comment\":\"lll\"}]', 0),
(59, '2019-09-03 11:51:34', '5d6d0c4d68d2430001664808', '6574618432006778880', 4, 2, 'text', '', 0, '[]', 2, '[{\"user_id\":\"6\"},{\"user_id\":\"4\"}]', 1, '[{\"user_id\":\"6\",\"comment\":\"testing\"}]', 0),
(60, '2019-09-03 17:21:09', '5d6d0c4d68d2430001664808', '6574619870393004032', 4, 2, 'text', '', 0, '[]', 2, '[{\"user_id\":\"6\"},{\"user_id\":\"4\"}]', 5, '[{\"user_id\":\"6\",\"comment\":\"kkkkkkk\"},{\"user_id\":\"6\",\"comment\":\"yes\"},{\"user_id\":\"6\",\"comment\":\"Fi\"},{\"user_id\":\"6\",\"comment\":\"Hey\"},{\"user_id\":\"6\",\"comment\":\"Himmi\"}]', 0),
(61, '2019-09-03 17:14:31', '5d6d0c4d68d2430001664808', '6574618180587614208', 4, 2, 'text', '', 0, '[]', 2, '[{\"user_id\":\"6\"},{\"user_id\":\"4\"}]', 11, '[{\"user_id\":\"6\",\"comment\":\"j\"},{\"user_id\":\"6\",\"comment\":\"Gm\"},{\"user_id\":\"6\",\"comment\":\"Hmmm\"},{\"user_id\":\"6\",\"comment\":\"Nopa\"},{\"user_id\":\"6\",\"comment\":\"Hi\"},{\"user_id\":\"6\",\"comment\":\"Hehe\"},{\"user_id\":\"4\",\"comment\":\"Hi\"},{\"user_id\":\"6\",\"comment\":\"Sdu\"},{\"user_id\":\"6\",\"comment\":\"Kyu\"},{\"user_id\":\"6\",\"comment\":\"Kha\"},{\"user_id\":\"6\",\"comment\":\"H\"}]', 0),
(62, '2019-09-03 13:02:51', '5d6d0c4d68d2430001664808', '6574624271518134272', 4, 2, 'text', '', 0, '[]', 3, '[{\"user_id\":\"6\"},{\"user_id\":\"9\"},{\"user_id\":\"4\"}]', 12, '[{\"user_id\":\"6\",\"comment\":\"nice\"},{\"user_id\":\"6\",\"comment\":\"j\"},{\"user_id\":\"6\",\"comment\":\"ok\"},{\"user_id\":\"9\",\"comment\":\"okay\"},{\"user_id\":\"6\",\"comment\":\"hhhhhhhhh\"},{\"user_id\":\"6\",\"comment\":\"jjjjjj\"},{\"user_id\":\"6\",\"comment\":\"jjjjjkkk\"},{\"user_id\":\"6\",\"comment\":\"got\"},{\"user_id\":\"6\",\"comment\":\"kkkkk\"},{\"user_id\":\"6\",\"comment\":\"hi\"},{\"user_id\":\"6\",\"comment\":\"hi\"},{\"user_id\":\"6\",\"comment\":\"jjjjjjj\"}]', 0),
(63, '2019-09-03 15:01:59', '5d6d0c4d68d2430001664808', '6574619819457376256', 6, 2, 'text', '', 0, '[]', 2, '[{\"user_id\":\"6\"},{\"user_id\":\"4\"}]', 0, '[]', 0),
(64, '2019-09-03 15:03:19', '5d6d0c4d68d2430001664808', '6574648543905050624', 4, 2, 'text', '', 0, '[]', 2, '[{\"user_id\":\"4\"},{\"user_id\":\"6\"}]', 6, '[{\"user_id\":\"6\",\"comment\":\"shut\"},{\"user_id\":\"6\",\"comment\":\"up\"},{\"user_id\":\"6\",\"comment\":\"ravi\"},{\"user_id\":\"6\",\"comment\":\"?KKKKKK\"},{\"user_id\":\"6\",\"comment\":\"HO GYA\"},{\"user_id\":\"6\",\"comment\":\"Ji\"}]', 0),
(65, '2019-09-03 16:37:30', '5d6d0c4d68d2430001664808', '6574618107254403072', 4, 2, 'text', '', 0, '[]', 3, '[{\"user_id\":\"6\"},{\"user_id\":\"4\"},{\"user_id\":\"2\"}]', 11, '[{\"user_id\":\"6\",\"comment\":\"Hi\"},{\"user_id\":\"6\",\"comment\":\"Hey\"},{\"user_id\":\"6\",\"comment\":\"H\"},{\"user_id\":\"4\",\"comment\":\"Ji\"},{\"user_id\":\"6\",\"comment\":\"My\"},{\"user_id\":\"6\",\"comment\":\"Hhhhh\"},{\"user_id\":\"4\",\"comment\":\"H\"},{\"user_id\":\"6\",\"comment\":\"Kya\"},{\"user_id\":\"6\",\"comment\":\"Huuuuuu\"},{\"user_id\":\"2\",\"comment\":\"Hi\"},{\"user_id\":\"6\",\"comment\":\"Gggdsafh\"}]', 0),
(66, '2019-09-04 08:46:29', '5d6d0c4d68d2430001664808', '6574683804860936192', 4, 2, 'text', '', 0, '[]', 2, '[{\"user_id\":\"6\"},{\"user_id\":\"3\"}]', 10, '[{\"user_id\":\"6\",\"comment\":\"Gi\"},{\"user_id\":\"6\",\"comment\":\"Kya h\"},{\"user_id\":\"6\",\"comment\":\"Han\"},{\"user_id\":\"6\",\"comment\":\"Hiii\"},{\"user_id\":\"6\",\"comment\":\"Hhhhhhh\"},{\"user_id\":\"6\",\"comment\":\"Ravi ji\"},{\"user_id\":\"6\",\"comment\":\"Suni\"},{\"user_id\":\"6\",\"comment\":\"Gggdsafhkjdzfbvvf\"},{\"user_id\":\"6\",\"comment\":\"Hi\"},{\"user_id\":\"6\",\"comment\":\"Hi\"}]', 0),
(67, '2019-09-03 16:41:48', '5d6d0c4d68d2430001664808', '6574691460401065984', 2, 2, 'text', '', 0, '[]', 2, '[{\"user_id\":\"2\"},{\"user_id\":\"6\"}]', 4, '[{\"user_id\":\"2\",\"comment\":\"Fhhhjjhfds\"},{\"user_id\":\"4\",\"comment\":\"Ji\"},{\"user_id\":\"2\",\"comment\":\"Guj\"},{\"user_id\":\"6\",\"comment\":\"How\"}]', 0),
(68, '2019-09-04 08:49:20', '5d6d0c4d68d2430001664808', '6574935519862452224', 3, 2, 'attachment', '', 0, '[]', 1, '[{\"user_id\":\"3\"}]', 1, '[{\"user_id\":\"3\",\"comment\":\"Very Good Morning\"}]', 1),
(69, '2019-10-31 06:06:16', '5d612ec548bdc619aae09085', '6578243876643926016', 10, 2, 'text', '', 0, '[]', 1, '[{\"user_id\":\"12\"}]', 0, '[]', 0),
(70, '2020-06-06 09:40:06', '5eda5d07c7ecde00016f6de1', '6674967990250762240', 16, 2, 'attachment', '', 0, '[]', 1, '[{\"user_id\":\"16\"}]', 0, '[]', 0),
(71, '2020-06-06 10:43:53', '5eda5d07c7ecde00016f6de1', '6674967626415861760', 16, 2, 'text', '', 0, '[]', 1, '[{\"user_id\":\"14\"}]', 1, '[{\"user_id\":\"14\",\"comment\":\"Check\"}]', 0),
(72, '2020-06-06 11:14:27', '5eda5d07c7ecde00016f6de1', '6674983749886668800', 14, 2, 'text', '', 0, '[]', 0, '[]', 3, '[{\"user_id\":\"14\",\"comment\":\"new check\"},{\"user_id\":\"14\",\"comment\":\"Test\"},{\"user_id\":\"16\",\"comment\":\"by dexter\"}]', 0),
(73, '2020-06-15 07:39:05', '5eda5d07c7ecde00016f6de1', '6678185270879186944', 17, 2, 'attachment', '', 0, '[]', 1, '[{\"user_id\":\"14\"}]', 0, '[]', 0),
(74, '2020-06-15 12:57:58', '5eda5d07c7ecde00016f6de1', '6678277041558646784', 18, 2, 'text', '', 0, '[]', 1, '[{\"user_id\":\"18\"}]', 1, '[{\"user_id\":\"18\",\"comment\":\"Android\"}]', 0),
(75, '2020-06-15 13:09:40', '5eda5d07c7ecde00016f6de1', '6678282231602999296', 19, 2, 'text', '', 0, '[]', 0, '[]', 1, '[{\"user_id\":\"19\",\"comment\":\"Ios\"}]', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ci_session`
--

CREATE TABLE `ci_session` (
  `id` varchar(40) COLLATE utf8_bin NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_bin NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `ci_session`
--

INSERT INTO `ci_session` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('c0qt3tecafe9glv8q794nr2pb04vhj55', '10.1.4.21', 1621249524, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632313234393532343b);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `display_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `activation_code` varchar(255) COLLATE utf8_bin NOT NULL,
  `message_platform` tinyint(11) NOT NULL,
  `expiry_date` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 - active, 2 - inactive',
  `configure_status` tinyint(4) NOT NULL DEFAULT '0',
  `logo` varchar(255) COLLATE utf8_bin NOT NULL,
  `splash_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `tagline` text COLLATE utf8_bin NOT NULL,
  `tagline_status` tinyint(4) NOT NULL DEFAULT '0',
  `total_groups` int(11) NOT NULL DEFAULT '0',
  `single_device_login_status` tinyint(4) NOT NULL DEFAULT '0',
  `do_not_allow_new_user_status` tinyint(4) NOT NULL DEFAULT '0',
  `message_sms_notification` tinyint(4) NOT NULL DEFAULT '0',
  `message_sender_name` varchar(7) COLLATE utf8_bin NOT NULL DEFAULT 'OTBUZZ',
  `message_custom_text` text COLLATE utf8_bin NOT NULL,
  `theme_id` int(11) NOT NULL DEFAULT '0',
  `splash_background` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `address` text COLLATE utf8_bin,
  `gst_number` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `splash_logo` tinyint(4) NOT NULL DEFAULT '0',
  `splash_background_status` tinyint(4) NOT NULL DEFAULT '0',
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `show_case_first` varchar(255) COLLATE utf8_bin NOT NULL,
  `show_case_second` varchar(255) COLLATE utf8_bin NOT NULL,
  `show_case_third` varchar(255) COLLATE utf8_bin NOT NULL,
  `show_case_fourth` varchar(255) COLLATE utf8_bin NOT NULL,
  `channel_url` varchar(555) COLLATE utf8_bin NOT NULL,
  `chatcamp_channel_id` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `created_at`, `company_name`, `display_name`, `activation_code`, `message_platform`, `expiry_date`, `status`, `configure_status`, `logo`, `splash_id`, `tagline`, `tagline_status`, `total_groups`, `single_device_login_status`, `do_not_allow_new_user_status`, `message_sms_notification`, `message_sender_name`, `message_custom_text`, `theme_id`, `splash_background`, `city`, `address`, `gst_number`, `splash_logo`, `splash_background_status`, `email`, `password`, `show_case_first`, `show_case_second`, `show_case_third`, `show_case_fourth`, `channel_url`, `chatcamp_channel_id`) VALUES
(2, '2018-08-13 20:55:08', 'New TFE', '', 'tfe5', 1, '2020-07-31', 1, 1, '153511472215051859655b7ffde2131a1.png', '', 'This is only for test', 1, 4, 0, 0, 1, 'OTBUZZ', '\\nThank you \\n OTBUZZ', 1, '153735961712103033655ba23f01ed0f8.gif', 'delhi ncr', '1A jain mandir road canought place delhi', '', 1, 1, 'amit.yadav.yadav@sa-group.in', 'tfe@2018', '', '', '154098870910577266345bd99f2542ed3.png', '', 'sendbird_group_channel_43842_d2dda10d811129e1d2112803a5bcd166b73d5e98', '5eda5d07c7ecde00016f6de1'),
(7, '2018-08-13 20:55:08', 'The Factor E', '', 'tfe2', 1, '2019-06-30', 1, 1, '15409879647191390075bd99c3cbe4d7.png', '', 'This is only for test', 1, 4, 0, 1, 1, 'OTBUZZ', 'thank you otbuzz', 1, '153735961712103033655ba23f01ed0f8.gif', 'delhi ncr', '1A jain mandir road canought place delhi', '', 1, 1, 'amit.yadav@sa-group.in', 'tfe@2018', '', '', '154098870910577266345bd99f2542ed3.png', '', 'sendbird_group_channel_43842_d2dda10d811129e1d2112803a5bcd166b73d5e98', '5bfbaccf48bdc62857dd440c');

-- --------------------------------------------------------

--
-- Table structure for table `companyeventdata`
--

CREATE TABLE `companyeventdata` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `companyevent_id` int(11) NOT NULL,
  `itinerary_id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `start_date_time` datetime NOT NULL,
  `end_date_time` datetime NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `dashboard_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `filter_join_date_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `filter_join_user_base` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `coordinator` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `role_base` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `user_base` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `istobereminded` tinyint(4) NOT NULL DEFAULT '0',
  `duration` int(11) NOT NULL,
  `istobepinned` tinyint(4) NOT NULL DEFAULT '0',
  `pinduration` int(11) NOT NULL,
  `surveyid` int(11) NOT NULL,
  `issurveytobereminded` tinyint(4) NOT NULL DEFAULT '0',
  `frequency` int(11) NOT NULL,
  `paid_ticket_status` tinyint(4) NOT NULL DEFAULT '0',
  `buy_more_status` tinyint(4) NOT NULL DEFAULT '0',
  `per_ticket_count` tinyint(4) NOT NULL DEFAULT '1',
  `paid_ticket_start_date_time` datetime DEFAULT NULL,
  `paid_ticket_expiry_date` datetime DEFAULT NULL,
  `ticketconfig_id` tinyint(4) DEFAULT NULL,
  `live_session_status` tinyint(4) NOT NULL DEFAULT '0',
  `live_session_id` int(11) DEFAULT '0',
  `ticket_sharing_status` tinyint(4) NOT NULL DEFAULT '0',
  `ticket_bank_id` int(11) DEFAULT NULL,
  `registration` tinyint(4) NOT NULL DEFAULT '0',
  `registration_expiry_date` datetime DEFAULT NULL,
  `register_user` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `limited_seats_status` tinyint(4) NOT NULL DEFAULT '0',
  `limited_seats` int(11) NOT NULL DEFAULT '-1',
  `additional_information` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `additional_attachment` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `content` varchar(5000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `transport_ticket` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `menu_option` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ticket_configuration_status` tinyint(4) NOT NULL DEFAULT '1',
  `notification_flag_reminder` tinyint(4) NOT NULL DEFAULT '0',
  `notification_flag_surveyreminder` tinyint(4) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companyeventdata`
--

INSERT INTO `companyeventdata` (`id`, `created_at`, `company_id`, `companyevent_id`, `itinerary_id`, `title`, `start_date_time`, `end_date_time`, `image`, `dashboard_image`, `filter_join_date_time`, `filter_join_user_base`, `coordinator`, `role_base`, `user_base`, `istobereminded`, `duration`, `istobepinned`, `pinduration`, `surveyid`, `issurveytobereminded`, `frequency`, `paid_ticket_status`, `buy_more_status`, `per_ticket_count`, `paid_ticket_start_date_time`, `paid_ticket_expiry_date`, `ticketconfig_id`, `live_session_status`, `live_session_id`, `ticket_sharing_status`, `ticket_bank_id`, `registration`, `registration_expiry_date`, `register_user`, `limited_seats_status`, `limited_seats`, `additional_information`, `additional_attachment`, `content`, `status`, `transport_ticket`, `menu_option`, `ticket_configuration_status`, `notification_flag_reminder`, `notification_flag_surveyreminder`, `rank`) VALUES
(1, '2018-10-10 12:11:28', 2, 2, 1, 'Ticketing Development', '2017-09-13 08:05:00', '2020-04-30 22:25:00', '153923997020403237495bbef02229005.png', '15392400645461688995bbef080d112e.png', NULL, NULL, '12', '2', '1,12', 1, 20, 1, 23, 1, 1, 25, 1, 0, 1, '2017-09-13 08:05:00', '2019-03-13 18:03:00', 1, 0, 2, 1, 1, 0, '2019-04-24 13:20:00', NULL, 0, 0, 'hhhhhhhhhhhh', NULL, '{\"transport_type\":\"5\",\"transport_start_location\":\"ds\",\"transport_start_lat\":\"12.23\",\"transport_start_long\":\"12.124\",\"transport_end_location\":\"gfsd\",\"transport_end_lat\":\"13.23\",\"transport_end_long\":\"233.23\",\"information\":\"hello bhai\",\"other_type\":\"other type\"}', 1, '153934165620967600265bc07d58c9a84.png', '', 1, 1, 0, 1),
(4, '2018-10-12 07:11:38', 2, 1, 1, 'return transport1', '2018-12-28 20:30:00', '2019-10-30 21:50:00', '1539328298501906785bc0492a67413.png', '15393282983069789735bc0492a6f43f.png', NULL, NULL, '12', '1,2,3', '12', 1, 20, 0, 1, 0, 0, 12, 0, 0, 1, NULL, '2019-06-30 11:20:00', 3, 0, NULL, 0, NULL, 1, '2019-04-20 16:40:00', '10,63', 1, 5, 'google', '154763354711537913845c3f038b907d1.png', '{\"transport_type\":\"1\",\"transport_start_location\":\"delhi\",\"transport_start_lat\":\"12.23\",\"transport_start_long\":\"13.23\",\"transport_end_location\":\"jaipur\",\"transport_end_lat\":\"13.5\",\"transport_end_long\":\"18.6\",\"information\":\"hello for plane\",\"other_type\":\"\"}', 1, '15393399474988720665bc076ab7edc5.png', '', 1, 0, 0, 5),
(5, '2018-10-12 13:40:33', 2, 1, 2, 'lunch', '2018-12-28 20:45:00', '2019-12-31 20:25:00', '153935163315946882555bc0a4517c23d.png', '15393516338064366695bc0a4517d4b3.png', NULL, NULL, '63,10,12', '', '10,12', 1, 25, 0, 1, 0, 0, 1, 1, 0, 1, NULL, '2019-03-31 20:25:00', 1, 0, NULL, 0, NULL, 0, '0000-00-00 00:00:00', NULL, 0, 0, '', NULL, '{\"type_of_meal\":\"2\",\"meal_location\":\"delhi\",\"meal_lat\":\"12.4\",\"meal_long\":\"15.5\",\"information\":\"you lunch\"}', 1, '15393516339477041935bc0a4517e49c.png', '', 0, 1, 0, 3),
(6, '2019-03-13 14:41:05', 2, 1, 1, 'test after add paid ticket', '2019-03-27 21:05:00', '2019-11-28 20:05:00', '155248806510344979725c891681b5ebd.jpg', '155248806511477005195c891681c8640.jpg', NULL, NULL, '12', '4', '12', 0, 10, 0, 10, 0, 0, 10, 0, 0, 1, '2019-03-27 21:05:00', '2019-07-12 20:45:00', 1, 0, NULL, 0, NULL, 0, '2019-03-31 21:10:00', NULL, 1, 50, 'test', NULL, '{\"transport_type\":\"1\",\"transport_start_location\":\"delhi\",\"transport_start_lat\":\"12.2\",\"transport_start_long\":\"12.3\",\"transport_end_location\":\"delhi\",\"transport_end_lat\":\"12.2\",\"transport_end_long\":\"12.3\",\"information\":\"hello after paid ticket enable\",\"other_type\":\"\"}', 1, '155248806515988455065c891681c993f.jpg', '', 1, 0, 0, 6),
(7, '2020-01-23 10:20:41', 2, 1, 1, 'Near Holi event', '2020-01-31 15:45:00', '2020-02-29 15:45:00', '15797748419201122365e2973794e717.jpg', '157977484120658359055e29737984bac.png', NULL, NULL, '1', '', '1', 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, NULL, NULL, 0, 0, NULL, 0, NULL, 1, '2020-01-30 15:45:00', NULL, 1, 5000, 'test', '157977484119478201825e2973798ef62.png', '{\"transport_type\":\"5\",\"transport_start_location\":\"holi\",\"transport_start_lat\":\"1.2\",\"transport_start_long\":\"1.21\",\"transport_end_location\":\"holi\",\"transport_end_lat\":\"1.2\",\"transport_end_long\":\"1.3\",\"information\":\"test\",\"other_type\":\"Near Holi event\"}', 1, '157977484116236108645e29737994a2f.png', '', 1, 0, 0, 7);

-- --------------------------------------------------------

--
-- Table structure for table `companyeventtickettransaction`
--

CREATE TABLE `companyeventtickettransaction` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `itinerary_id` int(11) NOT NULL,
  `ticket_category_id` int(11) NOT NULL,
  `unique_user_identifire` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `no_of_ticket` int(11) NOT NULL,
  `session_id` int(11) NOT NULL DEFAULT '0',
  `referral_code_no` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `transaction_amount` varchar(255) COLLATE utf8_bin NOT NULL,
  `credit_amount` varchar(255) COLLATE utf8_bin NOT NULL,
  `gateway_charge` varchar(255) COLLATE utf8_bin NOT NULL,
  `transaction_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `payment_gateway` int(11) NOT NULL,
  `channel_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `payment_status` varchar(255) COLLATE utf8_bin NOT NULL,
  `bank_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `bank_txn_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `payment_mode` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `gateway_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `gateway_txn_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `companyreferralcode`
--

CREATE TABLE `companyreferralcode` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `itinerary_data_id` int(11) DEFAULT NULL,
  `referral_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `referral_code` varchar(255) COLLATE utf8_bin NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `companyreferralcode`
--

INSERT INTO `companyreferralcode` (`id`, `created_at`, `company_id`, `itinerary_data_id`, `referral_name`, `referral_code`, `status`) VALUES
(1, '2019-11-04 13:13:43', 2, 1, 'Test By Amit', 'FDFE', 1),
(2, '2019-11-04 16:01:27', 2, 1, 'Test', 'FDFF', 1),
(3, '2019-11-04 16:07:02', 7, 1, 'Dafasf', 'FDFFDSFF', 1);

-- --------------------------------------------------------

--
-- Table structure for table `companyserverurl`
--

CREATE TABLE `companyserverurl` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `server_name` varchar(255) DEFAULT NULL,
  `server_id` int(11) NOT NULL,
  `frontend_server_url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companyserverurl`
--

INSERT INTO `companyserverurl` (`id`, `company_id`, `created_at`, `server_name`, `server_id`, `frontend_server_url`) VALUES
(2, 2, '2019-02-25 06:39:44', 'http://localhost/appadmin3.6/mobile/', 1, 'http://localhost/buzzlive0.4'),
(3, 7, '2020-04-07 14:39:57', 'http://localhost/appadmin3.6/mobile/', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `companysponsor`
--

CREATE TABLE `companysponsor` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sponsor_title_id` int(11) NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT ' 1 - active, 2 - inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companysponsor`
--

INSERT INTO `companysponsor` (`id`, `company_id`, `created_at`, `sponsor_title_id`, `image`, `status`) VALUES
(6, 2, '2018-12-06 08:02:26', 4, '154408334618351259685c08d792854f8.jpg', 1),
(7, 2, '2018-12-06 08:02:34', 3, '154408335420858140025c08d79ab05d2.jpg', 1),
(8, 2, '2018-12-06 08:02:44', 4, '15440833643264771205c08d7a43c425.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `companysponsortitle`
--

CREATE TABLE `companysponsortitle` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `position` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT ' 1 - active, 2 - inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companysponsortitle`
--

INSERT INTO `companysponsortitle` (`id`, `created_at`, `company_id`, `title`, `position`, `status`) VALUES
(3, '2018-12-06 08:01:49', 2, 'first sponsor', 2, 1),
(4, '2018-12-06 08:02:05', 2, 'second high sponsor', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `companysubscriptiontype`
--

CREATE TABLE `companysubscriptiontype` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `title` text COLLATE utf8_bin NOT NULL,
  `no_of_days` int(11) NOT NULL,
  `recommendation_status` tinyint(4) NOT NULL DEFAULT '0',
  `amount` varchar(255) COLLATE utf8_bin NOT NULL,
  `payment_gateway` tinyint(4) NOT NULL,
  `total_pay_amount` varchar(255) COLLATE utf8_bin NOT NULL,
  `credit_amount` varchar(255) COLLATE utf8_bin NOT NULL,
  `gateway_charge` varchar(255) COLLATE utf8_bin NOT NULL,
  `include_tax_amount_status` tinyint(4) NOT NULL,
  `description` longtext COLLATE utf8_bin NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1- active, 2 - inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `companysubscriptiontype`
--

INSERT INTO `companysubscriptiontype` (`id`, `created_at`, `company_id`, `title`, `no_of_days`, `recommendation_status`, `amount`, `payment_gateway`, `total_pay_amount`, `credit_amount`, `gateway_charge`, `include_tax_amount_status`, `description`, `status`) VALUES
(2, '2020-03-06 18:12:25', 2, 'For 15 Days Subscription', 15, 1, '50', 1, '51.83', '50', '1.83', 0, '<p>platinum people can subscribe</p>', 3),
(3, '2020-03-06 18:12:25', 2, 'For 30 Days Subscription', 30, 0, '100', 1, '100', '96.46', '3.54', 1, '<p>platinum people can subscribe</p>', 1),
(4, '2020-03-06 18:12:25', 2, 'For 45 Days Subscription', 45, 0, '150', 1, '155.50', '150', '5.50', 0, '<p>platinum people can subscribe</p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_activity_icons`
--

CREATE TABLE `company_activity_icons` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `image` varchar(255) COLLATE utf8_bin NOT NULL,
  `company_id` int(11) NOT NULL,
  `activityicon_id` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT ' 1 - active, 2 - inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `company_activity_icons`
--

INSERT INTO `company_activity_icons` (`id`, `created_at`, `title`, `image`, `company_id`, `activityicon_id`, `status`) VALUES
(1, '2018-08-13 08:38:26', 'Difficulty Level', '153414950618825739655b7143825cde6.png', 2, 1, 1),
(2, '2018-08-13 08:38:26', 'Connectivity', '153414950610514158745b714382679f7.png', 2, 2, 1),
(3, '2018-08-13 08:38:26', 'Type', '153414950619733464785b71438269226.png', 2, 3, 1),
(4, '2018-08-13 08:38:26', 'Language', '15341495065718578125b7143826acce.png', 2, 4, 1),
(5, '2018-08-24 12:58:26', 'Difficulty Level', '15351155064118999455b8000f2c2ee5.png', 3, 1, 1),
(6, '2018-08-24 12:58:26', 'Connectivity', '153511550610463102835b8000f2c3261.png', 3, 2, 1),
(7, '2018-08-24 12:58:26', 'Type', '153511550616681002115b8000f2c341b.png', 3, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_avatars`
--

CREATE TABLE `company_avatars` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `avatar_id` int(11) NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8_bin NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT ' 1 - active, 2 - inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `company_avatars`
--

INSERT INTO `company_avatars` (`id`, `created_at`, `company_id`, `avatar_id`, `image`, `title`, `position`, `status`) VALUES
(1, '2018-08-13 07:56:22', 1, 2, '15341469825936151195b7139a6299ce.png', 'First', 0, 1),
(2, '2018-08-13 07:56:22', 1, 3, '153414698220681110545b7139a62bd63.png', 'Funky Bunny', 0, 1),
(3, '2018-08-13 07:56:22', 1, 4, '153414698214702169375b7139a62f6e3.png', 'Employee Male', 0, 1),
(4, '2018-08-13 07:56:22', 1, 5, '15341469827968647465b7139a62fdc4.png', 'Employee Female', 0, 1),
(5, '2018-08-13 07:56:22', 1, 6, '15341469821769306435b7139a630097.png', 'Orange Camera Placeholder', 0, 1),
(6, '2018-08-13 08:27:13', 2, 2, '153414883310522349625b7140e1cd208.png', 'First', 0, 1),
(7, '2018-08-13 08:27:14', 2, 3, '153414883316550389815b7140e1cf092.png', 'Funky Bunny', 0, 1),
(8, '2018-08-13 08:27:14', 2, 4, '153414883410518024035b7140e208817.png', 'Employee Male', 0, 1),
(9, '2018-08-13 08:27:14', 2, 5, '15341488343378067635b7140e2095df.png', 'Employee Female', 0, 1),
(10, '2018-08-13 11:47:03', 4, 2, '15341608238672190235b716fb7bc89f.png', 'First', 0, 1),
(11, '2018-08-13 11:47:03', 4, 3, '153416082320229790345b716fb7bf5d4.png', 'Funky Bunny', 0, 1),
(12, '2018-08-13 11:47:03', 4, 4, '153416082315116944015b716fb7bfae2.png', 'Employee Male', 0, 1),
(13, '2018-08-13 11:47:03', 4, 5, '15341608233285580195b716fb7bfd1c.png', 'Employee Female', 0, 1),
(14, '2018-08-24 12:56:15', 3, 8, '153511537515173188865b80006f2036d.png', 'Default Avatar', 0, 1),
(15, '2018-08-24 12:56:15', 3, 9, '153511537511055288385b80006f20b90.png', 'male corporate with phone', 0, 1),
(16, '2018-08-24 12:56:15', 3, 10, '15351153752074787575b80006f2f268.png', 'female corporate with mac', 0, 1),
(17, '2018-10-30 12:56:47', 2, 10, '15409042073213261975bd8550f7f58b.png', 'female corporate with mac', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_badges`
--

CREATE TABLE `company_badges` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `badge_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `company_badges`
--

INSERT INTO `company_badges` (`id`, `created_at`, `title`, `image`, `company_id`, `badge_id`) VALUES
(1, '2018-08-14 04:36:53', 'Honest Badge', '15341764134160605225b71ac9d7ed96.png', 2, 0),
(2, '2018-08-16 22:22:43', 'Miss Beautiful', '15344131639914383155b75496b8c3fc.png', 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `company_employee_configuration`
--

CREATE TABLE `company_employee_configuration` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `show_mobile_no_status` tinyint(4) NOT NULL DEFAULT '0',
  `custom_field_one_status` tinyint(4) NOT NULL DEFAULT '0',
  `custom_field_one_name` text CHARACTER SET utf8 COLLATE utf8_bin,
  `custom_field_two_status` tinyint(4) NOT NULL DEFAULT '0',
  `custom_field_two_name` text CHARACTER SET utf8 COLLATE utf8_bin,
  `custom_field_three_status` tinyint(4) NOT NULL DEFAULT '0',
  `custom_field_three_name` text CHARACTER SET utf8 COLLATE utf8_bin,
  `skip_profile_status` tinyint(4) DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL COMMENT '1-active,2-inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_employee_configuration`
--

INSERT INTO `company_employee_configuration` (`id`, `company_id`, `show_mobile_no_status`, `custom_field_one_status`, `custom_field_one_name`, `custom_field_two_status`, `custom_field_two_name`, `custom_field_three_status`, `custom_field_three_name`, `skip_profile_status`, `created_at`, `status`) VALUES
(1, 2, 0, 0, 'one', 0, 'two', 0, 'three', 0, '2019-08-07 18:30:21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_event`
--

CREATE TABLE `company_event` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `event_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `status` tinyint(11) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_event`
--

INSERT INTO `company_event` (`id`, `created_at`, `event_name`, `status`, `company_id`) VALUES
(1, '2018-10-09 11:22:29', 'Factor E first event', 1, 2),
(2, '2018-10-12 12:23:06', 'just test for second event', 1, 2),
(3, '2018-12-19 10:53:37', 'test corporate', 1, 2),
(4, '2019-10-16 06:55:52', 'Ticked Itinerary', 1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `company_event_data`
--

CREATE TABLE `company_event_data` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `itinerary_id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `start_date_time` datetime NOT NULL,
  `end_date_time` datetime NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `dashboard_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `istobereminded` tinyint(4) NOT NULL DEFAULT '0',
  `duration` int(11) NOT NULL,
  `istobepinned` tinyint(4) NOT NULL DEFAULT '0',
  `pinduration` int(11) NOT NULL,
  `surveyid` int(11) NOT NULL,
  `issurveytobereminded` tinyint(4) NOT NULL DEFAULT '0',
  `frequency` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_event_data`
--

INSERT INTO `company_event_data` (`id`, `created_at`, `company_id`, `itinerary_id`, `title`, `start_date_time`, `end_date_time`, `image`, `dashboard_image`, `istobereminded`, `duration`, `istobepinned`, `pinduration`, `surveyid`, `issurveytobereminded`, `frequency`, `status`) VALUES
(1, '2018-10-10 12:11:28', 2, 1, 'just test', '2018-10-10 07:16:29', '2018-10-17 00:00:00', 'heloo.jpg', 'heelooo.jpg', 1, 30, 1, 23, 1, 1, 25, 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_mobile_menu`
--

CREATE TABLE `company_mobile_menu` (
  `company_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `image` varchar(255) COLLATE utf8_bin NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `company_mobile_menu`
--

INSERT INTO `company_mobile_menu` (`company_id`, `menu_id`, `name`, `image`, `position`, `status`) VALUES
(1, 10, '', '', 1, 0),
(1, 9, '', '', 1, 0),
(1, 8, '', '', 1, 0),
(1, 4, '', '', 1, 0),
(1, 2, '', '', 1, 0),
(1, 1, '', '', 1, 0),
(4, 2, 'Message', '153305930214158587315b60a0e6bcb7a.png', 2, 0),
(3, 2, 'Message', '153305930214158587315b60a0e6bcb7a.png', 2, 0),
(3, 1, 'Feed', '153305915510808636865b60a053bd254.png', 1, 0),
(4, 1, 'Feed', '153305915510808636865b60a053bd254.png', 1, 0),
(2, 4, 'Survey mast', '153305931313729835795b60a0f17bbf9.png', 4, 1),
(4, 3, 'Itinerary', '153305929512066980065b60a0df62da0.png', 3, 0),
(4, 4, 'Survey', '153305931313729835795b60a0f17bbf9.png', 4, 0),
(3, 3, 'Itinerary', '153305929512066980065b60a0df62da0.png', 3, 0),
(3, 4, 'Survey', '153305931313729835795b60a0f17bbf9.png', 4, 0),
(2, 3, 'Itinerary', '153305929512066980065b60a0df62da0.png', 3, 1),
(2, 2, 'Message', '153305930214158587315b60a0e6bcb7a.png', 2, 0),
(2, 1, 'Feed test', '153305915510808636865b60a053bd254.png', 1, 1),
(2, 5, 'Directory', '153305931313729835795b60a0f17bbf9.png', 5, 0),
(2, 13, 'Live', '15758727706468394515dede902c9cce.png', 6, 1),
(2, 14, 'Buzz', '157587279716151954045dede91d2d517.png', 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_modules`
--

CREATE TABLE `company_modules` (
  `company_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `wheel_module` tinyint(1) NOT NULL DEFAULT '0',
  `wheel_message` text COLLATE utf8_bin NOT NULL,
  `activity_module` tinyint(1) NOT NULL DEFAULT '0',
  `activity_message` text COLLATE utf8_bin NOT NULL,
  `feed_module` tinyint(1) NOT NULL DEFAULT '0',
  `feed_message` text COLLATE utf8_bin NOT NULL,
  `sms_module` tinyint(1) NOT NULL DEFAULT '0',
  `sms_message` text COLLATE utf8_bin NOT NULL,
  `itinerary_module` tinyint(4) NOT NULL DEFAULT '0',
  `itinerary_message` text COLLATE utf8_bin NOT NULL,
  `survey_module` tinyint(1) NOT NULL DEFAULT '0',
  `survey_message` text COLLATE utf8_bin NOT NULL,
  `directory_module` tinyint(1) NOT NULL,
  `directory_message` text COLLATE utf8_bin NOT NULL,
  `live_module` tinyint(4) NOT NULL,
  `live_message` text COLLATE utf8_bin NOT NULL,
  `buzz_module` tinyint(4) NOT NULL,
  `buzz_message` text COLLATE utf8_bin NOT NULL,
  `announcement_module` tinyint(1) NOT NULL DEFAULT '0',
  `announcement_message` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `company_modules`
--

INSERT INTO `company_modules` (`company_id`, `created_at`, `wheel_module`, `wheel_message`, `activity_module`, `activity_message`, `feed_module`, `feed_message`, `sms_module`, `sms_message`, `itinerary_module`, `itinerary_message`, `survey_module`, `survey_message`, `directory_module`, `directory_message`, `live_module`, `live_message`, `buzz_module`, `buzz_message`, `announcement_module`, `announcement_message`) VALUES
(1, '2018-08-13 08:09:52', 0, 'no data available wheel data only for testing', 0, 'no data available activity data only for testing', 0, '', 0, '', 0, '', 0, 'no data available survey data only for testing', 0, '', 0, '', 0, '', 0, 'no data available announcement data only for testing'),
(2, '2018-08-13 08:36:01', 0, 'This is only for pro version', 0, 'This is only for pro version', 1, 'feed', 0, 'hello brother', 1, 'Itinerary for pro version', 1, 'no data availabe survey data it is only for test.', 0, 'No data available', 1, 'live message', 1, 'buzz message', 1, 'no data available announcement data it is only for test.'),
(3, '2018-08-24 12:57:46', 0, 'This is only for pro version', 0, 'This is only for pro version', 1, '', 1, '', 1, '', 1, '', 0, '', 0, '', 0, '', 1, ''),
(4, '2018-08-14 09:35:26', 0, 'This is only for pro version', 0, 'This is only for pro version', 1, '', 1, '', 1, '', 1, 'Your survey is working, wait for more.', 0, '', 0, '', 0, '', 1, 'Discover more with wheel of discovery. Contact your administrator!');

-- --------------------------------------------------------

--
-- Table structure for table `company_notifications`
--

CREATE TABLE `company_notifications` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `notification_suffix` tinyint(4) NOT NULL,
  `user_request_notification_status` tinyint(4) NOT NULL,
  `user_request_message` longtext COLLATE utf8_bin NOT NULL,
  `user_approve_notification_status` tinyint(4) NOT NULL,
  `user_approve_message` longtext COLLATE utf8_bin NOT NULL,
  `user_decline_notification_status` tinyint(4) NOT NULL,
  `user_decline_message` longtext COLLATE utf8_bin NOT NULL,
  `itinerary_update_notification_status` tinyint(4) NOT NULL,
  `itinerary_update_title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `itinerary_update_message` longtext COLLATE utf8_bin NOT NULL,
  `otconfigure_status` tinyint(4) NOT NULL DEFAULT '0',
  `user_approve_notification_online_status` tinyint(4) NOT NULL,
  `user_approve_online_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `user_approve_online_message` longtext COLLATE utf8_bin NOT NULL,
  `user_decline_notification_online_status` tinyint(4) NOT NULL,
  `user_decline_online_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `user_decline_online_message` longtext COLLATE utf8_bin NOT NULL,
  `feed_like_notification_online_status` tinyint(4) NOT NULL DEFAULT '0',
  `feed_like_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `feed_like_message` longtext COLLATE utf8_bin NOT NULL,
  `feed_comment_notification_online_status` tinyint(4) NOT NULL DEFAULT '0',
  `feed_comment_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `feed_comment_message` longtext COLLATE utf8_bin NOT NULL,
  `poll_publish_notification_status` tinyint(4) NOT NULL,
  `poll_publish_offline_message` longtext COLLATE utf8_bin NOT NULL,
  `poll_publish_notification_online_status` tinyint(4) NOT NULL,
  `poll_publish_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `poll_publish_message` longtext COLLATE utf8_bin NOT NULL,
  `poll_reminder_notification_status` tinyint(4) NOT NULL,
  `poll_reminder_offline_message` longtext COLLATE utf8_bin NOT NULL,
  `poll_reminder_notification_online_status` tinyint(4) NOT NULL,
  `poll_reminder_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `poll_reminder_message` longtext COLLATE utf8_bin NOT NULL,
  `itinerary_update_notification_online_status` tinyint(4) NOT NULL,
  `itinerary_update_online_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `itinerary_update_online_message` longtext COLLATE utf8_bin NOT NULL,
  `itinerary_start_notification_status` tinyint(4) NOT NULL,
  `itinerary_start_title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `itinerary_start_message` longtext COLLATE utf8_bin NOT NULL,
  `itinerary_start_notification_online_status` tinyint(4) NOT NULL,
  `itinerary_start_online_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `itinerary_start_online_message` longtext COLLATE utf8_bin NOT NULL,
  `itinerary_survey_notification_status` tinyint(4) NOT NULL,
  `itinerary_survey_title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `itinerary_survey_message` longtext COLLATE utf8_bin NOT NULL,
  `itinerary_survey_notification_online_status` tinyint(4) NOT NULL,
  `itinerary_survey_online_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `itinerary_survey_online_message` longtext COLLATE utf8_bin NOT NULL,
  `survey_not_started_notification_status` tinyint(4) NOT NULL,
  `survey_not_started_title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `survey_not_started_message` longtext COLLATE utf8_bin NOT NULL,
  `survey_not_started_time` time NOT NULL,
  `survey_not_started_notification_online_status` tinyint(4) NOT NULL,
  `survey_not_started_online_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `survey_not_started_online_message` longtext COLLATE utf8_bin NOT NULL,
  `survey_not_started_online_time` time NOT NULL,
  `survey_pending_notification_status` tinyint(4) NOT NULL,
  `survey_pending_title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `survey_pending_message` longtext COLLATE utf8_bin NOT NULL,
  `survey_pending_time` time NOT NULL,
  `survey_pending_notification_online_status` tinyint(4) NOT NULL,
  `survey_pending_online_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `survey_pending_online_message` longtext COLLATE utf8_bin NOT NULL,
  `survey_pending_online_time` time NOT NULL,
  `mention_notification_status` tinyint(4) NOT NULL,
  `mention_title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `mention_message` longtext COLLATE utf8_bin NOT NULL,
  `mention_notification_online_status` tinyint(4) NOT NULL,
  `mention_online_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `mention_online_message` longtext COLLATE utf8_bin NOT NULL,
  `feed_pinned_notification_status` tinyint(4) NOT NULL,
  `feed_pinned_message` longtext COLLATE utf8_bin NOT NULL,
  `feed_pinned_notification_online_status` tinyint(4) NOT NULL,
  `feed_pinned_online_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `feed_pinned_online_message` longtext COLLATE utf8_bin NOT NULL,
  `new_text_message_notification_online_status` tinyint(4) NOT NULL DEFAULT '0',
  `new_text_message_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `new_text_message_message` longtext COLLATE utf8_bin NOT NULL,
  `new_attachment_message_notification_online_status` tinyint(4) NOT NULL DEFAULT '0',
  `new_attachment_message_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `new_attachment_message_message` longtext COLLATE utf8_bin NOT NULL,
  `feed_text_post_notification_online_status` tinyint(4) NOT NULL DEFAULT '0',
  `feed_text_post_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `feed_text_post_message` longtext COLLATE utf8_bin NOT NULL,
  `feed_attachment_post_notification_online_status` tinyint(4) NOT NULL DEFAULT '0',
  `feed_attachment_post_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `feed_attachment_post_message` longtext COLLATE utf8_bin NOT NULL,
  `subscription_expire_notification_status` tinyint(4) NOT NULL DEFAULT '0',
  `subscription_expire_offline_message` longtext COLLATE utf8_bin,
  `subscription_expire_notification_online_status` tinyint(4) NOT NULL DEFAULT '0',
  `subscription_expire_title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `subscription_expire_message` longtext COLLATE utf8_bin,
  `livesession_update_notification_status` tinyint(4) DEFAULT '0',
  `livesession_update_notification_title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `livesession_update_notification_message` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `livesession_update_sms_status` tinyint(4) DEFAULT '0',
  `livesession_update_sms_message` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `livesession_before_reminder_notification_status` tinyint(4) DEFAULT '0',
  `livesession_before_reminder_notification_title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `livesession_before_reminder_notification_message` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `livesession_before_reminder_sms_status` tinyint(4) DEFAULT '0',
  `livesession_before_reminder_sms_message` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `livesession_start_notification_status` tinyint(4) NOT NULL DEFAULT '0',
  `livesession_start_notification_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `livesession_start_notification_message` varchar(255) COLLATE utf8_bin NOT NULL,
  `livesession_start_sms_notification_status` tinyint(4) NOT NULL DEFAULT '0',
  `livesession_start_sms_notification_message` varchar(255) COLLATE utf8_bin NOT NULL,
  `livesession_join_notification_status` tinyint(4) NOT NULL DEFAULT '0',
  `livesession_join_notification_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `livesession_join_notification_status_message` varchar(255) COLLATE utf8_bin NOT NULL,
  `livesession_join_sms_notification_status` tinyint(4) NOT NULL DEFAULT '0',
  `livesession_join_sms_notification_message` varchar(255) COLLATE utf8_bin NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1-active,2-inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `company_notifications`
--

INSERT INTO `company_notifications` (`id`, `created_at`, `company_id`, `notification_suffix`, `user_request_notification_status`, `user_request_message`, `user_approve_notification_status`, `user_approve_message`, `user_decline_notification_status`, `user_decline_message`, `itinerary_update_notification_status`, `itinerary_update_title`, `itinerary_update_message`, `otconfigure_status`, `user_approve_notification_online_status`, `user_approve_online_title`, `user_approve_online_message`, `user_decline_notification_online_status`, `user_decline_online_title`, `user_decline_online_message`, `feed_like_notification_online_status`, `feed_like_title`, `feed_like_message`, `feed_comment_notification_online_status`, `feed_comment_title`, `feed_comment_message`, `poll_publish_notification_status`, `poll_publish_offline_message`, `poll_publish_notification_online_status`, `poll_publish_title`, `poll_publish_message`, `poll_reminder_notification_status`, `poll_reminder_offline_message`, `poll_reminder_notification_online_status`, `poll_reminder_title`, `poll_reminder_message`, `itinerary_update_notification_online_status`, `itinerary_update_online_title`, `itinerary_update_online_message`, `itinerary_start_notification_status`, `itinerary_start_title`, `itinerary_start_message`, `itinerary_start_notification_online_status`, `itinerary_start_online_title`, `itinerary_start_online_message`, `itinerary_survey_notification_status`, `itinerary_survey_title`, `itinerary_survey_message`, `itinerary_survey_notification_online_status`, `itinerary_survey_online_title`, `itinerary_survey_online_message`, `survey_not_started_notification_status`, `survey_not_started_title`, `survey_not_started_message`, `survey_not_started_time`, `survey_not_started_notification_online_status`, `survey_not_started_online_title`, `survey_not_started_online_message`, `survey_not_started_online_time`, `survey_pending_notification_status`, `survey_pending_title`, `survey_pending_message`, `survey_pending_time`, `survey_pending_notification_online_status`, `survey_pending_online_title`, `survey_pending_online_message`, `survey_pending_online_time`, `mention_notification_status`, `mention_title`, `mention_message`, `mention_notification_online_status`, `mention_online_title`, `mention_online_message`, `feed_pinned_notification_status`, `feed_pinned_message`, `feed_pinned_notification_online_status`, `feed_pinned_online_title`, `feed_pinned_online_message`, `new_text_message_notification_online_status`, `new_text_message_title`, `new_text_message_message`, `new_attachment_message_notification_online_status`, `new_attachment_message_title`, `new_attachment_message_message`, `feed_text_post_notification_online_status`, `feed_text_post_title`, `feed_text_post_message`, `feed_attachment_post_notification_online_status`, `feed_attachment_post_title`, `feed_attachment_post_message`, `subscription_expire_notification_status`, `subscription_expire_offline_message`, `subscription_expire_notification_online_status`, `subscription_expire_title`, `subscription_expire_message`, `livesession_update_notification_status`, `livesession_update_notification_title`, `livesession_update_notification_message`, `livesession_update_sms_status`, `livesession_update_sms_message`, `livesession_before_reminder_notification_status`, `livesession_before_reminder_notification_title`, `livesession_before_reminder_notification_message`, `livesession_before_reminder_sms_status`, `livesession_before_reminder_sms_message`, `livesession_start_notification_status`, `livesession_start_notification_title`, `livesession_start_notification_message`, `livesession_start_sms_notification_status`, `livesession_start_sms_notification_message`, `livesession_join_notification_status`, `livesession_join_notification_title`, `livesession_join_notification_status_message`, `livesession_join_sms_notification_status`, `livesession_join_sms_notification_message`, `status`) VALUES
(9, '2018-12-17 12:57:26', 2, 1, 1, '{user_name}, Thank you for the Request. Please wait for Approval.', 1, '{user_name}, You are approved.', 1, '{user_name}, Your Request has been declined.', 1, 'Itinerary Update', 'itinerary For \"{itinerary_title}\".', 0, 1, 'Request Approve', '{user_name}, You are Approved.', 1, 'Request Decline', '{user_name}, Your  Request has been Declined.', 1, 'Feed Post Like', '{user_name} like your post.', 1, 'New Comment on Feed', '{user_name} comment on your post, {comment_message}.', 1, '{poll_title}, company publish new Poll1.', 1, 'Poll publish', '{poll_title}, company publish new Poll1.', 1, '{user_name}, Your input is awaited for \"{poll_title}\" Poll.', 1, 'Reminder for Pending Poll', '{user_name}, Your input is awaited for \"{poll_title}\" Poll.', 1, 'Itinerary Update', 'itinerary For \"{itinerary_title}\".', 1, 'Reminder', 'Reminder For \"{itinerary_title}\".', 1, 'Reminder', 'Reminder For \"{itinerary_title}\".', 1, 'Survey Reminder', 'Reminder For \"{itinerary_title}\".', 1, 'Survey Reminder', 'Reminder For \"{itinerary_title}\".', 1, 'Feedback Pending ', '\"{survey_title}\", Your Feedback is Pending.', '12:00:00', 1, 'Feedback Pending', '\"{survey_title}\", Your Feedback is Pending.', '12:00:00', 1, 'Feedback Pending ', 'Only \"{left_question}\" Question left. \"{survey_title}\", Your Feedback is Pending.', '12:00:00', 1, 'Feedback Pending', 'Only \"{left_question}\" Question left. \"{survey_title}\", Your Feedback is Pending.', '12:00:00', 1, 'Mention', '{user_name}, Mention in a post.', 1, 'Mention', '{user_name}, Mention in a post.', 0, 'Post by {user_name} just got pinned, Watch it now.', 1, 'Pinned Post Notification', 'Post by {user_name} just got pinned, Watch it now.', 1, 'New Text Message Received', '\"{user_name}\" \"{text_message}\"', 1, 'New Attachment Message Received1', '\"{user_name}\";\"{attachment_type}\"', 1, 'Feed Post New Text Message', 'New Feed posted by \"{user_name}\";\"{text_message}\", Watch it now.', 1, 'Feed Post New Attachment Message', 'New Feed posted by \"{user_name}\";\"{attachment_type}\", Watch it now.', 1, 'Your {subscription_type} Expire in {left_days}. Please subscribe new plan.', 1, 'Subscription Expire Alert', 'Your {subscription_type} Expire in {left_days}. Please subscribe new plan.', 1, 'Live Session', 'Live session for \"{livesession_title}\".', 1, 'Live session for \"{livesession_title}\".', 1, 'Reminder', 'Reminder For \"{livesession_title}\".', 1, 'Reminder For \"{livesession_title}\".', 1, 'Live Session Start Notification', '{user_name} has started {livesession_title} session', 1, '{user_name} has started {livesession_title} session', 1, 'Live Session Join Notification', '{user_name} has joined {livesession_title} session.', 1, '{user_name} has joined {livesession_title} session.', 1),
(10, '2020-04-07 14:39:52', 7, 1, 0, '', 0, '', 0, '', 1, 'Itinerary Update', 'itinerary For \"{itinerary_title}\".', 0, 0, '', '', 0, '', '', 1, 'Feed Post Like', '{user_name} like your post.', 1, 'New Comment on Feed', '{user_name} comment on your post, {comment_message}.', 1, '{poll_title}, company publish new Poll1.', 1, 'Poll publish', '{poll_title}, company publish new Poll1.', 1, '{user_name}, Your input is awaited for \"{poll_title}\" Poll.', 1, 'Reminder for Pending Poll', '{user_name}, Your input is awaited for \"{poll_title}\" Poll.', 1, 'Itinerary Update', 'itinerary For \"{itinerary_title}\".', 1, 'Reminder', 'Reminder For \"{itinerary_title}\".', 1, 'Reminder', 'Reminder For \"{itinerary_title}\".', 1, 'Survey Reminder', 'Reminder For \"{itinerary_title}\".', 1, 'Survey Reminder', 'Reminder For \"{itinerary_title}\".', 1, 'Feedback Pending ', '\"{survey_title}\", Your Feedback is Pending.', '12:00:00', 1, 'Feedback Pending ', '\"{survey_title}\", Your Feedback is Pending.', '12:00:00', 1, 'Feedback Pending ', 'Only \"{left_question}\" Question left. \"{survey_title}\", Your Feedback is Pending.', '12:00:00', 1, 'Feedback Pending ', 'Only \"{left_question}\" Question left. \"{survey_title}\", Your Feedback is Pending.', '12:00:00', 1, 'Mention', '{user_name}, Mention in a post.', 1, 'Mention', '{user_name}, Mention in a post.', 1, 'Post by {user_name} just got pinned, Watch it now.', 1, 'Pinned Post Notification', 'Post by {user_name} just got pinned, Watch it now.', 1, 'New Text Message Received', '\"{user_name}\" \"{text_message}\"', 1, 'New Attachment Message Received1', '\"{user_name}\";\"{attachment_type}\"', 1, 'Feed Post New Text Message', 'New Feed posted by \"{user_name}\";\"{text_message}\", Watch it now.', 1, 'Feed Post New Attachment Message', 'New Feed posted by \"{user_name}\";\"{attachment_type}\", Watch it now.', 0, 'Your {subscription_type} Expire in {left_days}. Please subscribe new plan.', 0, 'Subscription Expire Alert', 'Your {subscription_type} Expire in {left_days}. Please subscribe new plan.', 0, NULL, NULL, 0, NULL, 0, NULL, NULL, 0, NULL, 0, '', '', 0, '', 0, '', '', 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_payment_gateway`
--

CREATE TABLE `company_payment_gateway` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `payment_gateway_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `api_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `salt` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `gateway_charge` varchar(255) NOT NULL,
  `website_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `api_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `txn_status_api_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `call_back_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_payment_gateway`
--

INSERT INTO `company_payment_gateway` (`id`, `created_at`, `company_id`, `payment_gateway_name`, `api_key`, `salt`, `gateway_charge`, `website_name`, `api_url`, `txn_status_api_url`, `call_back_url`, `status`) VALUES
(1, '2019-08-26 17:12:16', 2, 'PayTm', 'GrwysB46511255851793', 'dTaEz6Svy0#JzTPL', '3', 'WEBSTAGING', 'https://securegw-stage.paytm.in/theia/processTransaction', 'https://securegw-stage.paytm.in/merchant-status/getTxnStatus', 'http://192.168.1.11/appadmin3.6/mobile/', 1),
(2, '2019-08-26 17:12:16', 7, 'PayU Money', 'GrwysB46511255851793', 'dTaEz6Svy0#JzTPL', '2', 'WEBSTAGING', 'https://securegw-stage.paytm.in/theia/processTransaction', 'https://securegw-stage.paytm.in/merchant-status/getTxnStatus', 'http://localhost/appadmin3.6/mobile/signup_payment_success/', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_question`
--

CREATE TABLE `company_question` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `question` varchar(255) COLLATE utf8_bin NOT NULL,
  `module` int(11) NOT NULL DEFAULT '1',
  `category_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `companyvalue_id` int(11) NOT NULL,
  `answer_type` int(11) NOT NULL COMMENT '1 - text, 2 - multiple, 3 - dropdown',
  `options` text COLLATE utf8_bin,
  `correct_answer` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '1 - active, 2 - inactive 	',
  `answer_validation` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1 - yes',
  `image` varchar(255) COLLATE utf8_bin NOT NULL,
  `question_id` int(11) NOT NULL DEFAULT '0',
  `wheel_self_tagline` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `wheel_others_tagline` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `company_question`
--

INSERT INTO `company_question` (`id`, `created_at`, `question`, `module`, `category_id`, `company_id`, `companyvalue_id`, `answer_type`, `options`, `correct_answer`, `status`, `answer_validation`, `image`, `question_id`, `wheel_self_tagline`, `wheel_others_tagline`) VALUES
(2, '2018-08-13 21:04:35', 'First Prime Ministers of India........................', 1, 2, 2, 3, 2, 'Mahatma Gandhi, Jawaharlal Nehru, Indra Gandhi, None of these.', '', 1, 0, '', 0, '{value} is my answer for wheel.', '{value} is another person answer for wheel.'),
(3, '2018-08-13 22:25:50', 'First president of India.', 2, 3, 2, 3, 4, '1,2,3,4,5', '', 1, 0, '', 0, '{value} is my ans for wheel.', '{value} another person give the ans for you.'),
(35, '2019-01-07 12:48:47', 'test 3', 1, 2, 2, 3, 1, '', '', 1, 0, '', 0, '', ''),
(36, '2019-06-13 10:46:42', 'test with poll and survey module integrated', 2, 2, 2, 3, 4, 'Very Bad,Bad,Good,Very Good,Execellent', '', 1, 0, '', 0, 'test', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `company_question_categories`
--

CREATE TABLE `company_question_categories` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 - active, 2 - inactive',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `company_question_categories`
--

INSERT INTO `company_question_categories` (`id`, `created_at`, `category_name`, `status`, `category_id`, `company_id`) VALUES
(1, '2018-08-13 20:27:09', 'Test-facter', 1, 0, 1),
(2, '2018-08-13 20:58:18', 'Test-Factor', 1, 0, 2),
(3, '2018-08-13 22:16:28', 'Test Factor 2', 1, 0, 2),
(4, '2018-08-14 00:19:07', 'Know', 1, 0, 4),
(25, '2018-09-15 09:05:21', 'Know Your Mates', 1, 2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `company_signupconf`
--

CREATE TABLE `company_signupconf` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `approval_status` tinyint(4) NOT NULL,
  `subscription_charge_status` tinyint(4) NOT NULL,
  `amount` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `payment_gateway` tinyint(4) DEFAULT NULL,
  `total_pay_amount` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `credit_amount` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `gateway_charge` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `include_tax_amount_status` tinyint(4) NOT NULL,
  `free_trial` tinyint(4) NOT NULL,
  `no_of_days_free_trial` int(11) DEFAULT '0',
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_signupconf`
--

INSERT INTO `company_signupconf` (`id`, `created_at`, `company_id`, `approval_status`, `subscription_charge_status`, `amount`, `payment_gateway`, `total_pay_amount`, `credit_amount`, `gateway_charge`, `include_tax_amount_status`, `free_trial`, `no_of_days_free_trial`, `status`) VALUES
(1, '2019-08-23 12:49:46', 2, 0, 0, '255', 1, '255', '248.98', '6.02', 1, 0, 5, 1),
(3, '2020-03-12 14:05:16', 7, 1, 0, '', 0, '0.00', '', '0.00', 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_sponsor`
--

CREATE TABLE `company_sponsor` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `position` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT ' 1 - active, 2 - inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_sponsor`
--

INSERT INTO `company_sponsor` (`id`, `company_id`, `created_at`, `position`, `title`, `image`, `status`) VALUES
(2, 2, '2018-11-20 07:57:40', 2, 'test', '154281118121422740555bf56e2dcfb11.png', 1),
(3, 2, '2018-11-20 08:14:26', 1, 'test second', '15428111679991243345bf56e1fe4c29.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_user_event_shared_ticket`
--

CREATE TABLE `company_user_event_shared_ticket` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `main_ticket_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `shared_by` int(11) NOT NULL,
  `mobile_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ticket_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `company_user_unique_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `itinerary_data_id` int(11) NOT NULL,
  `ticket_category_id` int(11) NOT NULL,
  `ticket_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `final_ticket_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `referral_code_no` varchar(255) NOT NULL,
  `download_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 'No',
  `verify_status` tinyint(4) NOT NULL DEFAULT '0',
  `share_type` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1-active, 2- deactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_user_event_shared_ticket`
--

INSERT INTO `company_user_event_shared_ticket` (`id`, `created_at`, `main_ticket_id`, `user_id`, `shared_by`, `mobile_no`, `ticket_user_name`, `company_user_unique_id`, `company_id`, `itinerary_data_id`, `ticket_category_id`, `ticket_id`, `final_ticket_no`, `transaction_id`, `amount`, `referral_code_no`, `download_status`, `verify_status`, `share_type`, `status`) VALUES
(1, '2020-04-23 10:41:59', 4, 1, 1, '9310072682', 'amit kumar yadav shared ticket', '9596561100', 2, 1, 4, 'abc41', 'abc41', 'txn004', '50', 'cdtr', 'No', 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_user_event_ticket`
--

CREATE TABLE `company_user_event_ticket` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `company_user_unique_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `itinerary_data_id` int(11) NOT NULL,
  `ticket_category_id` int(11) NOT NULL,
  `ticket_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `final_ticket_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `referral_code_no` varchar(255) NOT NULL,
  `download_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 'No',
  `verify_status` tinyint(4) NOT NULL DEFAULT '0',
  `is_shared` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1-active, 2- deactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_user_event_ticket`
--

INSERT INTO `company_user_event_ticket` (`id`, `created_at`, `user_id`, `company_user_unique_id`, `company_id`, `itinerary_data_id`, `ticket_category_id`, `ticket_id`, `final_ticket_no`, `transaction_id`, `amount`, `referral_code_no`, `download_status`, `verify_status`, `is_shared`, `status`) VALUES
(1, '2020-04-23 10:41:59', 1, '9596561100', 2, 1, 1, 'abc', 'abc', 'txn001', '50', 'cdtr', 'No', 1, 0, 1),
(2, '2020-04-23 10:41:59', 1, '9596561100', 2, 1, 1, 'abc2', 'abc2', 'txn002', '50', 'cdtr', 'No', 1, 0, 1),
(3, '2020-04-23 10:41:59', 1, '9596561100', 2, 1, 3, 'abc31', 'abc31', 'txn003', '50', 'cdtr', 'No', 1, 0, 1),
(4, '2020-04-23 10:41:59', 1, '9596561100', 2, 1, 4, 'abc41', 'abc41', 'txn004', '50', 'cdtr', 'No', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_user_role`
--

CREATE TABLE `company_user_role` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `user_role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `can_create_new_text_post` tinyint(4) NOT NULL DEFAULT '1',
  `can_create_new_image_post` tinyint(4) NOT NULL DEFAULT '1',
  `can_create_new_video_post` tinyint(4) NOT NULL DEFAULT '1',
  `can_create_new_new_quick_notification` tinyint(4) NOT NULL DEFAULT '1',
  `can_create_new_poll` tinyint(4) NOT NULL DEFAULT '0',
  `can_start_new_game` tinyint(4) NOT NULL DEFAULT '0',
  `can_comment_on_a_post` tinyint(4) NOT NULL DEFAULT '1',
  `can_like_a_post` tinyint(4) NOT NULL DEFAULT '1',
  `can_pin_a_post` tinyint(4) NOT NULL DEFAULT '1',
  `can_share_a_post` tinyint(4) NOT NULL DEFAULT '1',
  `can_watch_team_showcase_area` tinyint(4) NOT NULL DEFAULT '1',
  `can_watch_profile_of_other_user` tinyint(4) NOT NULL DEFAULT '1',
  `can_search_other_user` tinyint(4) NOT NULL DEFAULT '1',
  `can_communicate_with_other_role` tinyint(4) NOT NULL DEFAULT '1',
  `communication_role` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `can_send_message_to_other_user_from_feed_screen` tinyint(4) NOT NULL DEFAULT '1',
  `can_search_user_in_messaging_screen` int(4) NOT NULL DEFAULT '1',
  `can_add_people_in_group_chat` int(4) NOT NULL DEFAULT '1',
  `status` int(4) NOT NULL COMMENT '1- active and 2 - Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_user_role`
--

INSERT INTO `company_user_role` (`id`, `created_at`, `company_id`, `user_role_id`, `user_role_name`, `can_create_new_text_post`, `can_create_new_image_post`, `can_create_new_video_post`, `can_create_new_new_quick_notification`, `can_create_new_poll`, `can_start_new_game`, `can_comment_on_a_post`, `can_like_a_post`, `can_pin_a_post`, `can_share_a_post`, `can_watch_team_showcase_area`, `can_watch_profile_of_other_user`, `can_search_other_user`, `can_communicate_with_other_role`, `communication_role`, `can_send_message_to_other_user_from_feed_screen`, `can_search_user_in_messaging_screen`, `can_add_people_in_group_chat`, `status`) VALUES
(14, '2019-01-15 10:03:09', '2', 1, 'Viewer', 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, '1,2,3,4', 0, 0, 0, 1),
(15, '2019-01-15 10:03:09', '2', 2, 'Participant', 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, '1,2,3,4', 1, 1, 1, 1),
(16, '2019-01-15 10:03:09', '2', 3, 'Event Manager', 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, '1,2,3,4', 1, 1, 1, 1),
(17, '2019-01-15 10:03:09', '2', 4, 'Spoc', 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, '1,2,3,4', 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_values`
--

CREATE TABLE `company_values` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `attribute` varchar(255) COLLATE utf8_bin NOT NULL,
  `tagline` text COLLATE utf8_bin,
  `company_id` int(11) NOT NULL,
  `corporatevalue_id` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT ' 1 - active, 2 - inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `company_values`
--

INSERT INTO `company_values` (`id`, `created_at`, `attribute`, `tagline`, `company_id`, `corporatevalue_id`, `status`) VALUES
(1, '2018-08-13 20:30:04', 'Facter-Honesty', '{value} this is only for testing for facter company', 1, 0, 1),
(2, '2018-08-13 08:07:41', 'Honesty-Facter', 'This is only for test', 1, 1, 1),
(3, '2018-08-13 08:27:19', 'Honesty', '{value} This is only for test', 2, 1, 1),
(4, '2018-08-14 00:21:15', 'Collaborate', '{Let\'s Collaborate}', 4, 0, 1),
(6, '2018-08-16 08:15:24', 'Engagement', 'engagement', 4, 0, 1),
(31, '2018-09-15 09:05:21', 'Honesty-Factor', NULL, 4, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_wheel_categories`
--

CREATE TABLE `company_wheel_categories` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 - active, 2 - inactive',
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `wheelcategory_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `company_wheel_categories`
--

INSERT INTO `company_wheel_categories` (`id`, `created_at`, `category_name`, `status`, `image`, `company_id`, `wheelcategory_id`) VALUES
(1, '2018-08-13 08:38:09', 'Collaboration', 1, '15341494898047644015b7143716f109.png', 2, 1),
(2, '2018-08-13 08:38:09', 'Commitment', 1, '15341494898709979975b71437173ce8.png', 2, 2),
(3, '2018-08-13 08:38:09', 'Happiness', 1, '15341494893593831075b714371768a3.png', 2, 4),
(4, '2018-08-13 08:38:09', 'Innovation', 1, '153414948919378183185b7143717927a.png', 2, 5),
(5, '2018-08-14 20:01:30', 'Hobbies', 1, '153440619916310221455b752e37404a3.png', 4, 0),
(6, '2018-08-14 20:09:16', 'Specialisation', 1, '153423235620435736585b728724dbec5.png', 4, 0),
(7, '2018-08-14 20:10:54', 'Location', 1, '153423245412818991965b72878653688.jpg', 4, 0),
(8, '2018-08-14 20:15:49', 'Organisation', 1, '15342327493924031745b7288ad73ed1.jpg', 4, 0),
(10, '2018-08-15 20:21:09', 'Fear', 1, '15343644697663377845b748b35240f9.png', 4, 3),
(11, '2018-08-24 12:57:47', 'Fear', 1, '153511546621452252515b8000caf1ff3.png', 3, 3),
(12, '2018-08-24 12:57:47', 'Personal', 1, '153511546714742303445b8000cb00e58.png', 3, 6),
(13, '2018-08-24 12:57:47', 'Social', 1, '15351154673439895035b8000cb013a2.png', 3, 7),
(14, '2018-08-24 12:57:47', 'Strength', 1, '15351154679107269455b8000cb02b4e.png', 3, 8),
(15, '2018-10-04 06:15:56', 'test only', 1, '153863375620855490945bb5b01c5a3c1.jpeg', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `company_wheel_questions`
--

CREATE TABLE `company_wheel_questions` (
  `category_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `company_wheel_questions`
--

INSERT INTO `company_wheel_questions` (`category_id`, `question_id`) VALUES
(1, 3),
(2, 3),
(3, 3),
(4, 2),
(7, 6),
(5, 8),
(8, 7),
(6, 4),
(5, 5),
(15, 2);

-- --------------------------------------------------------

--
-- Table structure for table `company_zoom_accounts`
--

CREATE TABLE `company_zoom_accounts` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `meeting_id` varchar(100) DEFAULT NULL,
  `api_key` varchar(100) DEFAULT NULL,
  `api_secret` varchar(100) DEFAULT NULL,
  `jwt_token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_zoom_accounts`
--

INSERT INTO `company_zoom_accounts` (`id`, `username`, `company_id`, `password`, `meeting_id`, `api_key`, `api_secret`, `jwt_token`, `created_at`, `status`) VALUES
(1, 'prabhakar.sharma@sa-group.in', 5, 'Tfe@2018', '3429674264', 'zId3r76LTNOFS_oLazpHrw', 'nmaNQ9AjAoR4PWpZshsN4Dt8qfNsX4PCJfbl', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6InpJZDNyNzZMVE5PRlNfb0xhenBIcnciLCJleHAiOjE2NzA4MjY2MDAsImlhdCI6MTU4OTA0MjEwNX0.w2RnJfnSVQF32vO9jJyOa4dXelQEitdOUq8Vz77eXTQ', '2020-05-09 16:35:56', 1),
(3, 'technicalheaven01@gmail.com', 5, '22@Sep@1995', '81121290988', 'bHYtOk61T5-uyOVo2biHaA', 'AuuDdXd0cn0XLD4P0mMFHdd9Huk9ylALyYtl', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6ImJIWXRPazYxVDUtdXlPVm8yYmlIYUEiLCJleHAiOjE2MzkyOTA2MDAsImlhdCI6MTU5MTA5MzA3NH0.N684qe5QkBuX--94hvBCkdf-122tm1NLMIwxezj0D2k', '2020-06-02 10:19:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` longtext COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `created_at`, `title`, `description`) VALUES
(1, '2017-08-16 18:07:38', 'About Us', '<p> </p>\r\n\r\n<p>I am re</p>\r\n\r\n<p>I am blu</p>\r\n\r\n<p> </p>\r\n\r\n<p>I am big</p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>'),
(2, '2017-08-16 18:07:38', 'Privacy', '<p>Privacy Policy</p>'),
(3, '2017-09-06 11:36:45', 'Contact Us', 'Contact Us');

-- --------------------------------------------------------

--
-- Table structure for table `corporate_menu`
--

CREATE TABLE `corporate_menu` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `alias` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `corporate_menu`
--

INSERT INTO `corporate_menu` (`id`, `created_at`, `title`, `parent_id`, `alias`, `position`) VALUES
(1, '2018-08-15 09:38:18', 'Employee', 0, '', 1),
(2, '2018-08-15 09:38:18', 'Announcement', 0, 'announcement', 9),
(3, '2018-08-15 09:38:18', 'Avatars', 0, 'companyavatars', 2),
(4, '2018-08-15 09:38:18', 'User Role', 0, 'companymobilemenu', 4),
(5, '2018-08-15 09:38:18', 'Question Categories', 0, 'companyquestioncategory', 3),
(7, '2018-08-15 09:39:59', 'Question Bank', 0, 'companyquestion', 5),
(8, '2018-08-15 09:39:59', 'Notification', 0, 'companynotifications', 6),
(11, '2018-08-15 09:41:33', 'Survey List', 17, 'survey', 1),
(12, '2018-08-15 09:41:33', 'Survey Response', 17, 'surveyresponse', 2),
(17, '2018-08-15 10:21:25', 'Survey', 0, NULL, 8),
(18, '2018-08-15 11:29:07', 'Groups', 0, 'companygroups', 2),
(22, '2018-12-19 10:50:03', 'Itinerary', 0, NULL, 0),
(23, '2018-12-19 10:52:54', 'Event', 22, 'companyevent', 1),
(24, '2018-12-19 10:52:54', 'Event Information ', 22, 'companyeventdata', 2),
(25, '2018-12-19 10:55:49', 'Sponsor', 0, NULL, 0),
(26, '2018-12-19 10:57:37', 'sponsor title', 25, 'companysponsortitle', 0),
(27, '2018-12-19 10:57:37', 'sponsor', 25, 'companysponsor', 0),
(28, '2019-04-29 14:23:00', 'Poll', 0, NULL, 8),
(29, '2019-04-29 14:23:00', 'Poll list', 28, 'poll', 1),
(30, '2019-08-02 07:14:49', 'Employee', 1, 'employee', 1),
(31, '2019-08-02 07:14:49', 'Employee Configuration', 1, 'companyemployeeconfiguration', 2);

-- --------------------------------------------------------

--
-- Table structure for table `corporate_values`
--

CREATE TABLE `corporate_values` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `attribute` varchar(255) COLLATE utf8_bin NOT NULL,
  `tagline` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `corporate_values`
--

INSERT INTO `corporate_values` (`id`, `created_at`, `attribute`, `tagline`) VALUES
(1, '2018-08-13 20:29:06', 'Honesty-Factor', 'This is only for test');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263);

-- --------------------------------------------------------

--
-- Table structure for table `device_version`
--

CREATE TABLE `device_version` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `platform` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `package_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `build_version` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `mandatory_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `device_version`
--

INSERT INTO `device_version` (`id`, `created_at`, `platform`, `package_name`, `build_version`, `mandatory_status`) VALUES
(1, '2018-09-17 09:17:02', 'Android', 'android4.1', '21', 1),
(2, '2018-12-06 10:19:04', 'Iphone', 'Iphone', '1.0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `user_role` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `avatar` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `country_code` int(5) NOT NULL DEFAULT '91',
  `employee_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `leader_id` int(11) DEFAULT NULL,
  `custom_field_one` longtext COLLATE utf8_bin,
  `custom_field_two` longtext COLLATE utf8_bin,
  `custom_field_three` longtext COLLATE utf8_bin,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 - active, 2 - inactive',
  `device_type` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `designation` text COLLATE utf8_bin,
  `points` int(11) NOT NULL DEFAULT '0',
  `profile_flag` int(11) NOT NULL DEFAULT '0',
  `device_token` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `login_token` varchar(255) COLLATE utf8_bin DEFAULT 'new_token',
  `is_logined` varchar(255) COLLATE utf8_bin DEFAULT 'false',
  `is_chatcamp_user_created` tinyint(4) NOT NULL DEFAULT '0',
  `is_channel_join` tinyint(4) NOT NULL DEFAULT '0',
  `login_date_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `notification_count` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `created_at`, `company_id`, `user_role`, `group_id`, `name`, `avatar`, `country_code`, `employee_id`, `date_of_birth`, `leader_id`, `custom_field_one`, `custom_field_two`, `custom_field_three`, `status`, `device_type`, `designation`, `points`, `profile_flag`, `device_token`, `login_token`, `is_logined`, `is_chatcamp_user_created`, `is_channel_join`, `login_date_time`, `notification_count`) VALUES
(1, '2019-09-01 01:37:13', 2, 1, 2, 'Amit Kumar Yadav M', '159419702213931735395f05841e7602b.', 91, '5stSAbUysNPFYanVbtd7dA', '2019-08-28', 13, '', '', '9596561100', 1, 'Web Browser', 'learn', 0, 1, '', '', 'false', 0, 0, '2020-04-09 12:42:48', 0),
(12, '2019-10-21 12:08:35', 2, 2, 2, 'amit93100', '153383081913743055535b6c66a34c3ac.jpg', 91, 'kxm3BxmyjyKU2KPXWCtvCQ==', '2019-08-20', 13, '', '', '9310072682', 1, 'Web Browser', 'play', 0, 1, '', '', 'false', 0, 0, '2020-04-09 12:42:48', 0),
(13, '2019-12-30 07:07:47', 2, 2, 4, 'amit excell', NULL, 91, '5DLQ1qUGv09Tr3IloTdxRA==', '1970-01-01', NULL, '', '', '9596561122', 1, NULL, 'learn', 0, 0, NULL, 'new_token', 'false', 0, 0, '2020-04-09 12:42:48', 0),
(14, '2020-05-21 09:07:07', 2, 2, 3, 'Ravi', '15900520271572413905ec644bb6bcc8.', 91, 'tfAOdCLO3j+I6aMlxqWMYQ==', '2020-05-01', NULL, NULL, NULL, '9643607756', 1, 'Web Browser', 'Learn', 0, 1, '', '', 'false', 0, 0, '2020-05-21 09:07:07', 0),
(15, '2020-05-21 09:31:50', 2, 2, 5, 'Test', '159005351016699680835ec64a862f404.', 91, 'NAz0WLdsQCWzPQZVGAmzYg==', '2020-05-01', NULL, NULL, NULL, '8957992300', 1, 'Web Browser', 'Learn', 0, 1, '', '', 'false', 0, 0, '2020-05-21 09:31:50', 0),
(16, '2020-06-06 06:02:27', 2, 2, 4, 'Guest4397', '159142334715631440405edb31732f34e.', 91, 'LNRNpmAI1gH2DJtM+o6ybg==', '1970-01-01', NULL, NULL, NULL, '9968077320', 1, 'IOS', 'Edit Designation', 0, 1, '', '', 'false', 0, 0, '2020-06-06 06:02:27', 0),
(17, '2020-06-15 06:24:21', 2, 2, 5, 'Dr. Himmi Pant', '159220226118535155945ee714150a4d5.', 91, 'jleAWhxf2Nrf/jmmacXm0g==', '2020-06-09', NULL, NULL, NULL, '8929445301', 1, 'IOS', 'Learn', 0, 1, '', '', 'false', 0, 0, '2020-06-15 06:24:21', 0),
(18, '2020-06-15 12:44:33', 2, 2, 3, 'Himmi Pant', '159222507318073042605ee76d314dee7.', 91, 'kl2DojeXfngfk1D/gM8kRw==', '2020-06-03', NULL, NULL, NULL, '8929445300', 1, 'Android', 'Learn', 0, 1, '', '', 'false', 0, 0, '2020-06-15 12:44:33', 0),
(19, '2020-06-15 13:08:54', 2, 2, 2, 'Himmi Pant Dhaundiyal', '159222653420735381245ee772e6537f5.', 91, 'DwcFcSogQp7X8zIlk23jdA==', '2020-06-03', NULL, NULL, NULL, '8920881287', 1, 'IOS', 'Learn', 0, 1, '', '', 'false', 0, 0, '2020-06-15 13:08:54', 0),
(21, '2020-07-08 14:23:27', 2, 1, 4, 'AMIT APRV', '159419765715999336315f058699146d8.', 91, '5stSAbUysNPFYanVbtd7dA==', '2020-07-06', 12, NULL, NULL, NULL, 1, 'Web Browser', 'Learn', 0, 0, '', '', 'false', 0, 0, '2020-07-08 08:53:27', 0);

-- --------------------------------------------------------

--
-- Table structure for table `employeesubscription`
--

CREATE TABLE `employeesubscription` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subscription_id` int(11) DEFAULT '0',
  `no_of_days` int(11) DEFAULT '0',
  `subscription_start_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `subscription_expiry_date` date DEFAULT NULL,
  `payment_status` varchar(255) NOT NULL,
  `subscription_mode` tinyint(4) DEFAULT '1',
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employeesubscription`
--

INSERT INTO `employeesubscription` (`id`, `created_at`, `company_id`, `user_id`, `subscription_id`, `no_of_days`, `subscription_start_datetime`, `subscription_expiry_date`, `payment_status`, `subscription_mode`, `status`) VALUES
(13, '2020-03-18 19:58:35', 2, 1, 0, 0, '2020-03-18 19:58:35', '2020-08-30', 'credit', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `employee_badges`
--

CREATE TABLE `employee_badges` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `image` varchar(255) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `employee_badges`
--

INSERT INTO `employee_badges` (`id`, `created_at`, `title`, `image`, `user_id`) VALUES
(1, '2018-08-16 10:02:10', 'Miss Beautiful', '15344131639914383155b75496b8c3fc.png', 10);

-- --------------------------------------------------------

--
-- Table structure for table `employee_gossip`
--

CREATE TABLE `employee_gossip` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL,
  `answered_by` int(11) NOT NULL,
  `wheelattempt_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `employee_gossip`
--

INSERT INTO `employee_gossip` (`id`, `created_at`, `title`, `user_id`, `answered_by`, `wheelattempt_id`) VALUES
(1, '2018-08-13 10:06:25', 'rajendra prasad is my ans for wheel.', 1, 0, 1),
(2, '2018-08-13 11:56:01', 'my name is amit kumar yadav. is my ans for wheel.', 1, 0, 2),
(3, '2018-08-13 16:15:10', 'Dr Rajendra Prasad is my ans for wheel.', 3, 0, 3),
(4, '2018-08-13 16:25:35', ' is my answer for wheel.', 3, 0, 4),
(5, '2018-08-13 16:25:38', ' is my answer for wheel.', 3, 0, 5),
(6, '2018-08-13 16:25:40', ' is my answer for wheel.', 3, 0, 6),
(7, '2018-08-13 16:25:47', ' is my answer for wheel.', 3, 0, 7),
(8, '2018-08-13 16:25:49', ' is my answer for wheel.', 3, 0, 8),
(9, '2018-08-13 16:25:51', ' is my answer for wheel.', 3, 0, 9),
(10, '2018-08-13 16:25:53', ' is my answer for wheel.', 3, 0, 10),
(11, '2018-08-13 16:26:39', ' is my answer for wheel.', 4, 0, 11),
(12, '2018-08-13 16:26:48', ' is my answer for wheel.', 4, 0, 12),
(13, '2018-08-13 16:26:53', ' is my answer for wheel.', 4, 0, 13),
(14, '2018-08-13 16:27:00', ' is my answer for wheel.', 4, 0, 14),
(15, '2018-08-13 16:27:06', ' is my answer for wheel.', 4, 0, 15),
(16, '2018-08-13 16:32:48', ' is my ans for wheel.', 3, 0, 16),
(17, '2018-08-13 16:33:06', ' is my answer for wheel.', 3, 0, 17),
(18, '2018-08-13 16:34:00', ' is my ans for wheel.', 3, 0, 18),
(19, '2018-08-13 17:00:47', ' is my ans for wheel.', 6, 0, 19),
(20, '2018-08-13 17:01:55', 'Jawaharlal Nehru is another person answer for wheel.', 1, 5, 20),
(21, '2018-08-13 17:02:24', ' is my ans for wheel.', 5, 0, 21),
(22, '2018-08-13 17:34:14', 'igugugugugu another person give the ans for you.', 3, 7, 22),
(23, '2018-08-13 17:51:04', ' is my answer for wheel.', 8, 0, 23),
(24, '2018-08-13 17:52:20', ' is my ans for wheel.', 8, 0, 24),
(25, '2018-08-13 17:54:51', ' is my ans for wheel.', 8, 0, 25),
(26, '2018-08-14 09:56:43', 'Dance, I love to do in my free time.', 10, 0, 26),
(27, '2018-08-14 09:57:18', '{Value}, is the colour of my choice.', 10, 0, 27),
(28, '2018-08-14 09:57:37', ', I love to do in my free time.', 10, 0, 28),
(29, '2018-08-14 09:58:00', '{Value}, is the colour of my choice.', 10, 0, 29),
(30, '2018-08-14 10:21:43', 'The FactorE, I am mostly found here during weekdays', 10, 0, 30),
(31, '2018-08-14 10:25:11', ', I am mostly found here during weekdays', 10, 0, 31),
(32, '2018-08-14 10:27:24', 'The FactorE, I am mostly found here during weekdays', 10, 0, 32),
(33, '2018-08-14 10:32:29', ', I am mostly found here during weekdays', 10, 0, 33),
(34, '2018-08-14 10:34:06', '{Value}, is the colour of my choice.', 10, 0, 34),
(35, '2018-08-14 10:38:52', '{Value}, is the colour of my choice.', 10, 0, 35),
(36, '2018-08-14 15:32:50', 'atal vihar another person give the ans for you.', 2, 11, 36),
(37, '2018-08-15 05:21:09', 'New Answer another person give the ans for you.', 11, 12, 37),
(38, '2018-08-16 06:55:43', 'me another person give the ans for you.', 8, 4, 38),
(39, '2018-08-16 08:31:10', 'I belong to Meerut', 10, 0, 39),
(40, '2018-08-16 08:31:56', 'I belong to ', 10, 0, 40),
(41, '2018-08-16 08:32:19', '{Value} I love to do in my free time.', 10, 0, 41),
(42, '2018-08-16 08:33:10', 'I belong to ', 10, 0, 42),
(43, '2018-08-16 09:31:14', 'I belong to Meerut', 10, 0, 43),
(44, '2018-08-16 09:34:13', 'I belong to ', 10, 0, 44),
(45, '2018-08-16 11:33:18', 'I am mostly found at The FactorE during weekdays', 10, 0, 45),
(46, '2018-08-16 12:23:44', ' is my ans for wheel.', 2, 0, 46),
(47, '2018-08-16 12:24:35', 'hi another person give the ans for you.', 11, 2, 47),
(48, '2018-08-16 12:26:10', 'hello modi another person give the ans for you.', 12, 2, 48),
(49, '2018-08-16 12:37:26', ' is my ans for wheel.', 2, 0, 49),
(50, '2018-08-16 18:01:54', 'if se is my ans for wheel.', 13, 0, 50),
(51, '2018-08-16 18:02:34', 'yet another person give the ans for you.', 7, 13, 51),
(52, '2018-08-16 19:50:27', ' is my ans for wheel.', 4, 0, 52),
(53, '2018-08-16 19:51:00', ' is my ans for wheel.', 4, 0, 53),
(54, '2018-08-16 19:53:58', ' is my answer for wheel.', 4, 0, 54),
(55, '2018-08-17 12:15:56', 'Gg is my ans for wheel.', 3, 0, 55),
(56, '2018-08-17 14:56:27', 'Jgj is my ans for wheel.', 3, 0, 56),
(57, '2018-08-18 06:59:47', 'me another person give the ans for you.', 8, 4, 57),
(58, '2018-08-18 08:34:09', 'Dr Rajendra Prashad is my ans for wheel.', 16, 0, 58),
(59, '2018-08-18 08:41:57', ' is my ans for wheel.', 16, 0, 59),
(60, '2018-08-18 08:57:59', ' is my ans for wheel.', 17, 0, 60),
(61, '2018-08-18 09:08:55', 'Ethan is my ans for wheel.', 17, 0, 61),
(62, '2018-08-18 09:52:24', ' is my ans for wheel.', 4, 0, 62),
(63, '2018-08-18 09:52:37', 'Rajendra prashad is my ans for wheel.', 16, 0, 63),
(64, '2018-08-18 11:24:03', 'i dont know is my ans for wheel.', 18, 0, 64),
(65, '2018-08-18 14:37:08', ' is my ans for wheel.', 16, 0, 65),
(66, '2018-08-18 20:04:55', 'Jawaharlal Nehru is another person answer for wheel.', 8, 15, 66),
(67, '2018-08-18 20:05:56', 'Javaharlal Nehru another person give the ans for you.', 4, 15, 67),
(68, '2018-08-19 11:41:24', 'rted is my ans for wheel.', 18, 0, 68),
(69, '2018-08-20 06:00:23', ' Jawaharlal Nehru is another person answer for wheel.', 7, 23, 70),
(70, '2018-08-20 08:45:03', 'Jawaharlal Nehru is another person answer for wheel.', 20, 4, 75),
(71, '2018-08-20 08:45:36', 'Jawaharlal Nehru is another person answer for wheel.', 20, 4, 76),
(72, '2018-08-20 08:46:11', 'xyz another person give the ans for you.', 8, 4, 77),
(73, '2018-08-20 17:18:14', 'Mahatma Gandhi is another person answer for wheel.', 17, 25, 81),
(74, '2018-08-20 17:32:26', ' Jawaharlal Nehru is another person answer for wheel.', 8, 27, 82),
(75, '2018-08-20 18:51:57', 'Jawaharlal Nehru is another person answer for wheel.', 5, 28, 84),
(76, '2018-08-22 10:08:24', 'rajender prasad another person give the ans for you.', 8, 33, 86),
(77, '2018-08-22 10:19:42', 'fadsfa another person give the ans for you.', 4, 33, 87),
(78, '2018-08-22 10:58:59', 'Dbdh another person give the ans for you.', 15, 4, 88),
(79, '2018-08-22 11:00:26', 'Ecec another person give the ans for you.', 29, 4, 89),
(80, '2018-08-22 11:16:10', 'Jawaharlal Nehru is another person answer for wheel.', 20, 4, 90),
(81, '2018-08-22 11:17:38', 'Hfh another person give the ans for you.', 15, 4, 91),
(82, '2018-08-22 11:20:45', 'Fjdj another person give the ans for you.', 22, 4, 92),
(83, '2018-08-22 11:26:28', 'Ccj another person give the ans for you.', 13, 36, 93),
(84, '2018-08-22 11:29:16', 'Ueur is my ans for wheel.', 36, 0, 94),
(85, '2018-08-22 11:37:03', 'Mahatma Gandhi is another person answer for wheel.', 30, 3, 95),
(86, '2018-08-23 09:45:30', 'Hdhd is my ans for wheel.', 3, 0, 96),
(87, '2018-08-24 09:47:17', 'Jawaharlal Nehru is my answer for wheel.', 18, 0, 97);

-- --------------------------------------------------------

--
-- Table structure for table `employee_information`
--

CREATE TABLE `employee_information` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `employee_request`
--

CREATE TABLE `employee_request` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `user_role` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `avatar` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `country_code` int(5) NOT NULL DEFAULT '91',
  `employee_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `leader_id` int(11) NOT NULL,
  `custom_field_one` longtext COLLATE utf8_bin,
  `custom_field_two` longtext COLLATE utf8_bin,
  `custom_field_three` longtext COLLATE utf8_bin,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 - active, 2 - inactive',
  `device_type` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `designation` text COLLATE utf8_bin,
  `points` int(11) NOT NULL DEFAULT '0',
  `profile_flag` int(11) NOT NULL DEFAULT '0',
  `device_token` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `show_status` tinyint(4) NOT NULL DEFAULT '1',
  `approval_status` int(11) NOT NULL DEFAULT '0',
  `notification_count` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `employee_request`
--

INSERT INTO `employee_request` (`id`, `created_at`, `company_id`, `user_role`, `group_id`, `name`, `avatar`, `country_code`, `employee_id`, `date_of_birth`, `leader_id`, `custom_field_one`, `custom_field_two`, `custom_field_three`, `status`, `device_type`, `designation`, `points`, `profile_flag`, `device_token`, `show_status`, `approval_status`, `notification_count`) VALUES
(12, '2019-10-10 10:07:22', 2, 2, 2, 'amit kumar yadav', '153383081913743055535b6c66a34c3ac.jpg\r\n', 91, '9310072682', '2019-10-15', 1, NULL, NULL, NULL, 1, '', 'learn', 0, 0, NULL, 0, 2, 0),
(13, '2020-05-21 09:05:28', 2, 0, 3, 'Ravi', '15900519283190989995ec6445838983.', 91, '9643607756', '2020-05-01', 0, NULL, NULL, NULL, 1, 'null', 'Learn', 0, 1, 'null', 1, 0, 0),
(14, '2020-07-08 08:40:57', 2, 1, 4, 'AMIT APRV', '159419765715999336315f058699146d8.', 91, '5stSAbUysNPFYanVbtd7dA==', '2020-07-06', 12, NULL, NULL, NULL, 1, 'null', 'Learn', 0, 1, 'null', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `event_ticket_category`
--

CREATE TABLE `event_ticket_category` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `itinerary_data_id` int(11) NOT NULL,
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `category_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `role_base` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `user_base` varchar(244) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `venue` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `event_date_time` datetime NOT NULL,
  `ticket_count` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `include_tax_amount_status` tinyint(4) NOT NULL,
  `total_pay_amount` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `credit_amount` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `gateway_charge` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `series_status` tinyint(4) NOT NULL,
  `random_length` int(11) DEFAULT NULL,
  `gate_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `company_logo` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `event_logo` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `terms_condition` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `common_serial` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ticket_start_serial_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ticket_end_serial_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `hidden_status` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1-active, 2- deactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event_ticket_category`
--

INSERT INTO `event_ticket_category` (`id`, `created_at`, `company_id`, `itinerary_data_id`, `category_name`, `category_code`, `role_base`, `user_base`, `venue`, `event_date_time`, `ticket_count`, `amount`, `include_tax_amount_status`, `total_pay_amount`, `credit_amount`, `gateway_charge`, `series_status`, `random_length`, `gate_no`, `company_logo`, `event_logo`, `terms_condition`, `common_serial`, `ticket_start_serial_no`, `ticket_end_serial_no`, `hidden_status`, `status`) VALUES
(1, '2019-04-06 11:46:18', 2, 1, 'Associates', 'GLD', '1', '1', 'cp', '2019-04-25 14:25:00', 5, 600, 0, '614.50', '600', '14.50', 1, 8, '7', '15734723825514314415dc9487e2abee.png', '157347238218574330005dc9487e2d2f5.png', '<p>1. This is true<br>\r\n2. Hello hero<br>\r\n3. Kuch bhi karo hona wahi hia jo god chahega<br>\r\n4. Tumhe yaad karne ke wajah pata ni hai.<br>\r\n5. Pyar to ho hi jata hai.</p>', '1', '15001', '99999', 0, 1),
(3, '2019-04-06 14:43:08', 2, 1, 'Chairman', 'GLD', '2', '1', 'test', '2019-04-25 14:40:00', 7, 700, 1, '700', '683.48', '16.52', 1, 4, '7', '', '', '<p>test</p>', '1', '13001', '99999', 0, 1),
(4, '2019-04-08 15:25:04', 2, 1, 'Advisor and above', 'GLD', '1,2,3', '1', 'cp', '2019-04-26 15:20:00', 10, 500, 1, '500', '488.20', '11.80', 1, 6, '5', '157285106512977148975dbfcd792fa1c.png', '15728510658819051815dbfcd793de86.png', '<p>test</p>', '1', '10001', '99999', 0, 1),
(5, '2019-04-06 11:46:18', 2, 5, 'Associates', 'GLD', '1', '1', 'cp', '2019-04-25 14:25:00', 5, 600, 0, '614.50', '600', '14.50', 1, 4, '7', '', '', '<p>1. This is true<br>\r\n2. hello hero<br>\r\n3. kuch bhi karo hona wahi hia jo god chahega<br>\r\n4. tumhe yaad karne ke wajah pata ni hai.<br>\r\n5. Pyar to ho hi jata hai.</p>', '5', '15001', '99999', 0, 1),
(6, '2019-04-06 14:43:08', 2, 5, 'Chairman', 'GLD', '2', '1', 'test', '2019-04-25 14:40:00', 7, 700, 1, '700', '683.48', '16.52', 1, 4, '7', '', '', '<p>test</p>', '1', '13001', '99999', 0, 1),
(7, '2019-04-08 15:25:04', 2, 5, 'Advisor and above', 'GOLD', '1,2,3', '1', 'cp', '2019-04-26 15:20:00', 10, 500, 1, '500', '488.20', '11.80', 1, 6, '5', '157285106512977148975dbfcd792fa1c.png', '15728510658819051815dbfcd793de86.png', '<p>test</p>', '5', '10001', '99999', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `facedata`
--

CREATE TABLE `facedata` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `image` varchar(255) COLLATE utf8_bin NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `facedata`
--

INSERT INTO `facedata` (`id`, `created_at`, `name`, `image`, `status`) VALUES
(1, '2019-12-05 12:07:39', 'amit', '157552785919084915225de8a5b39b3e0.jpg', 2),
(2, '2019-12-05 12:43:45', 'second', '157553002510446456945de8ae295845c.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `facedetection`
--

CREATE TABLE `facedetection` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `image` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `facedetection`
--

INSERT INTO `facedetection` (`id`, `name`, `image`) VALUES
(1, 'amit', '153383081913743055535b6c66a34c3ac.jpg'),
(2, 'sumit', '15651831534595779105d4accb1489f8.JPG'),
(3, 'dimag', '157545624417409014055de78df494d47.jpeg'),
(4, 'sumit', '157546602616523190185de7b42aef47f.png'),
(5, 'aaaaa', '157546603414384333785de7b432481ba.png'),
(6, 'sumit kumar yadav', '157546612015967420835de7b4885ccb9.png'),
(7, 'fdasdfaf', '157546618811609314215de7b4cc6351a.jpg'),
(8, 'test', '157546623910529083405de7b4ffa87fd.jpg'),
(9, 'test', '157546627810491578315de7b52615422.jpg'),
(10, 'amit', '157546641016653885585de7b5aab7ce2.png'),
(11, 'test', '157546653311952866885de7b6254a451.png');

-- --------------------------------------------------------

--
-- Table structure for table `filter_settings`
--

CREATE TABLE `filter_settings` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` varchar(20) COLLATE utf8_bin NOT NULL,
  `alias` varchar(255) COLLATE utf8_bin NOT NULL,
  `start` int(11) NOT NULL DEFAULT '0',
  `length` int(11) NOT NULL DEFAULT '10',
  `order` varchar(255) COLLATE utf8_bin NOT NULL,
  `column` int(11) NOT NULL DEFAULT '0',
  `search` varchar(255) COLLATE utf8_bin NOT NULL,
  `filters` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `filter_settings`
--

INSERT INTO `filter_settings` (`id`, `created_at`, `user_id`, `alias`, `start`, `length`, `order`, `column`, `search`, `filters`) VALUES
(1, '2020-05-19 05:13:00', '2', 'employee', 0, 10, 'asc', 0, '', NULL),
(2, '2020-05-19 06:15:16', '2', 'employee_request', 0, 10, 'asc', 0, '', NULL),
(3, '2020-05-19 06:15:22', '2', 'employeevalidity', 0, 10, 'asc', 0, '', NULL),
(4, '2020-05-19 08:34:45', '2', 'adminusers', 0, 10, 'asc', 0, '', NULL),
(5, '2020-05-19 08:34:49', '2', 'company', 0, 10, 'asc', 0, '', NULL),
(6, '2020-05-24 15:33:21', '1', 'adminusers', 0, 10, 'asc', 0, '', NULL),
(7, '2020-05-24 15:33:35', '1', 'companyeventdata', 0, 10, 'asc', 4, '', NULL),
(8, '2020-06-12 14:26:15', '1', 'companylivesession', 0, 10, 'asc', 0, '', NULL),
(9, '2020-06-12 14:26:29', '1', 'employee', 0, 10, 'asc', 0, '', NULL),
(10, '2020-06-12 14:28:25', '1', 'company', 0, 10, 'asc', 0, '', NULL),
(11, '2020-06-12 14:30:16', '1', 'companylivequestion', 0, 10, 'asc', 0, '', NULL),
(12, '2020-06-12 14:30:43', '1', 'companylivepoll', 0, 10, 'asc', 0, '', NULL),
(13, '2020-06-12 14:31:04', '1', 'companylivepollquestion', 0, 10, 'asc', 0, '', NULL),
(14, '2020-06-12 14:32:29', '1', 'companylivestream', 0, 10, 'asc', 0, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `groupchannel_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `group_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `company_id` int(11) NOT NULL,
  `group_code` varchar(255) COLLATE utf8_bin NOT NULL,
  `logo` varchar(255) COLLATE utf8_bin NOT NULL,
  `group_points` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `groupchannel_id`, `created_at`, `group_name`, `company_id`, `group_code`, `logo`, `group_points`) VALUES
(1, '', '2018-08-13 20:26:01', 'The Facter E', 1, 'G001', '15341469615378879365b7139917c02a.png', 0),
(2, '5bacd2cb48bdc63211b1c0ea', '2018-08-13 20:55:41', 'Alderson', 2, 'G002', '153414874115258617915b7140855cb37.png', 0),
(3, '5bacd2d748bdc63211da89a4', '2018-08-13 20:56:19', '2nd Alderson', 2, 'G003', '153414877921448299715b7140ab100d5.png', 0),
(4, '5bacd2e248bdc632119062d9', '2018-08-13 20:57:03', '3rd Alderson', 2, 'G004', '153414882310651388165b7140d740078.png', 0),
(5, '5bade6b648bdc63211e00a04', '2018-08-13 22:13:43', '4th Alderson', 2, 'G005', '15341534239471121335b7152cf49059.png', 0),
(6, '', '2018-08-13 23:51:36', 'Recruiters', 4, 'G006', '153415929615604370845b7169c004d48.jpg', 0),
(7, '', '2018-08-13 23:55:53', 'Compensation Specialists', 4, 'G007', '153415955310691038815b716ac1319b0.jpg', 0),
(8, '', '2018-08-13 23:58:55', 'Business Partners', 4, 'G008', '1534159735394154795b716b773731e.png', 0),
(9, '', '2018-08-14 00:15:40', 'Engagement Specialist', 4, 'G009', '15341607408261171975b716f646f02f.jpg', 0),
(10, '', '2018-08-25 01:17:45', 'Melody5', 3, 'G010', '153511486514310221645b7ffe716e5ca.png', 0),
(11, '', '2018-08-25 01:24:14', 'Rythm', 3, 'G011', '15351152544191169515b7ffff6ac92e.png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `hotel`
--

CREATE TABLE `hotel` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `hotel_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `no_of_rooms` int(11) NOT NULL,
  `total_room_booked` int(11) NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8_bin NOT NULL,
  `handler_email` longtext COLLATE utf8_bin NOT NULL,
  `hotel_star` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` (`id`, `created_at`, `hotel_name`, `no_of_rooms`, `total_room_booked`, `image`, `handler_email`, `hotel_star`, `address`, `status`) VALUES
(1, '2019-09-24 00:00:00', 'The surya', 10, -2, 'abcd.jpeg', 'amit12nig96@gmail.com,prabhakar.sharma@sa-group.in', 1, 'Delhi', 2),
(2, '2019-09-24 00:00:00', 'Holiday Inn Mayur Bihar', 85, 0, 'ram.jpg', 'prabhakar.sharma@sa-group.in,vishal.sharma@sa-group.in,connect@secretsofindia.in,harin4rex@gmail.com', 4, 'Mayur Bihar Noida', 1),
(3, '2019-09-24 11:58:35', 'Crown Plaza Mayur Bihar', 65, 0, 'ram.jpg', 'prabhakar.sharma@sa-group.in,vishal.sharma@sa-group.in,connect@secretsofindia.in,harin4rex@gmail.com', 4, 'Mayur Bihar', 1),
(4, '2019-09-24 00:00:00', 'Hotel Shervani ', 20, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Delhi', 2),
(5, '2019-09-24 00:00:00', 'Mantra Amaltas', 20, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar Noida', 1),
(6, '2019-09-24 11:58:35', 'Grand Vanizia', 40, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar', 1),
(7, '2019-09-24 00:00:00', 'Park Inn Lajpat Nagar', 20, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Delhi', 2),
(8, '2019-09-24 00:00:00', 'JP Hotels', 20, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar Noida', 1),
(9, '2019-09-24 11:58:35', 'Jivitesh', 15, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar', 1),
(10, '2019-09-24 00:00:00', 'North Avenue ', 25, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Delhi', 2),
(11, '2019-09-24 00:00:00', 'Clarks Inn', 15, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar Noida', 1),
(12, '2019-09-24 11:58:35', 'Clarks Inn Nehru Place', 20, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar', 1),
(13, '2019-09-24 00:00:00', 'Visaya Nehru Place', 20, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Delhi', 2),
(14, '2019-09-24 00:00:00', 'Park Inn Sahadra', 35, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar Noida', 1),
(15, '2019-09-24 11:58:35', 'Clarks International', 5, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar', 1),
(16, '2019-09-24 00:00:00', 'Amara', 20, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Delhi', 2),
(17, '2019-09-24 00:00:00', 'Hill Palace', 20, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar Noida', 1),
(18, '2019-09-24 11:58:35', 'Saar Inn', 20, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar', 1),
(19, '2019-09-24 00:00:00', 'Hotel Ivory 32', 20, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Delhi', 2),
(20, '2019-09-24 00:00:00', 'Hotel Aster Inn', 50, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar Noida', 1),
(21, '2019-09-24 11:58:35', 'Divine Park', 40, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar', 1),
(22, '2019-09-24 00:00:00', 'Le Roi', 20, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Delhi', 2),
(23, '2019-09-24 00:00:00', 'Apra Inn', 10, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar Noida', 1),
(24, '2019-09-24 11:58:35', 'Apra International', 10, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar', 1),
(25, '2019-09-24 00:00:00', 'Good Palace', 10, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Delhi', 2),
(26, '2019-09-24 00:00:00', 'Hotel Kingston Park', 10, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar Noida', 1),
(27, '2019-09-24 11:58:35', 'High 5 land', 45, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar', 1),
(28, '2019-09-24 00:00:00', 'Hotel Park View ', 12, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Delhi', 2),
(29, '2019-09-24 00:00:00', 'Regent Continantal ', 35, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar Noida', 1),
(30, '2019-09-24 11:58:35', 'Hotel Regent Intercontinantal ', 20, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar', 1),
(31, '2019-09-24 00:00:00', 'Hotel Metropolitan', 8, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Delhi', 2),
(32, '2019-09-24 00:00:00', 'Hotel Delhi Pride ', 20, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar Noida', 1),
(33, '2019-09-24 11:58:35', 'Hotel Daanish Residency', 20, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar', 1),
(34, '2019-09-24 00:00:00', 'Hotel Delhi Grand', 20, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Delhi', 2),
(35, '2019-09-24 00:00:00', 'HOTEL GOLD SOUK', 15, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Delhi', 2),
(36, '2019-09-24 00:00:00', 'Hotel SPB 87', 15, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar Noida', 1),
(37, '2019-09-24 11:58:35', 'Hotel Maurya Heritage', 10, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar', 1),
(38, '2019-09-24 00:00:00', 'Hotel Crest Inn', 15, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Delhi', 2),
(39, '2019-09-24 00:00:00', 'The Golden Palms Hotel & Spa', 10, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar Noida', 1),
(40, '2019-09-24 11:58:35', 'Mandakini Palace ', 20, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar', 1),
(41, '2019-09-24 00:00:00', 'Comfort Zone ', 15, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Delhi', 2),
(42, '2019-09-24 00:00:00', 'The Orion Plaza ', 20, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar Noida', 1),
(43, '2019-09-24 11:58:35', 'The Grand Vikalp ', 15, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Mayur Bihar', 1),
(44, '2019-09-24 00:00:00', 'Blue Stone ', 20, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Delhi', 2),
(45, '2019-09-24 00:00:00', 'La Residenza', 15, 0, 'ram.jpg', 'sales@incredibleplanners.com,vishal.sharma@sa-group.in,prabhakar.sharma@sa-group.in,nitasha.goel@sa-group.in', 4, 'Delhi', 2);

-- --------------------------------------------------------

--
-- Table structure for table `hotel_rooms`
--

CREATE TABLE `hotel_rooms` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `hotel_id` int(11) NOT NULL,
  `room_type_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `rent` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `hotel_rooms`
--

INSERT INTO `hotel_rooms` (`id`, `created_at`, `hotel_id`, `room_type_name`, `rent`) VALUES
(1, '2019-09-24 12:02:09', 1, 'Single People', '550'),
(2, '2019-09-24 12:02:09', 1, 'Double People', '1000'),
(3, '2019-09-24 12:02:09', 2, 'Single People', '141'),
(4, '2019-09-24 12:02:09', 2, 'Double People', '150'),
(5, '2019-09-24 12:02:09', 3, 'Single People', '141'),
(6, '2019-09-24 12:02:09', 3, 'Double People', '150');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_user`
--

CREATE TABLE `hotel_user` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `mobile_no` varchar(255) COLLATE utf8_bin NOT NULL,
  `hotel_id` tinyint(4) NOT NULL,
  `passport_no` varchar(255) COLLATE utf8_bin NOT NULL,
  `check_in_datetime` datetime NOT NULL,
  `check_out_datetime` datetime NOT NULL,
  `no_of_rooms` int(11) NOT NULL,
  `single_rooms` tinyint(4) NOT NULL,
  `double_rooms` tinyint(4) NOT NULL,
  `total_members` tinyint(4) NOT NULL,
  `days` int(11) NOT NULL,
  `booking_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `total_price` int(11) NOT NULL,
  `mail_send_status` tinyint(4) NOT NULL,
  `booking_status` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `hotel_user`
--

INSERT INTO `hotel_user` (`id`, `created_at`, `name`, `email`, `mobile_no`, `hotel_id`, `passport_no`, `check_in_datetime`, `check_out_datetime`, `no_of_rooms`, `single_rooms`, `double_rooms`, `total_members`, `days`, `booking_id`, `total_price`, `mail_send_status`, `booking_status`, `status`) VALUES
(1, '2019-09-26 17:24:22', 'amit kumar yadav', 'amit12nig96@gmail.com', '9596561100', 1, 'abc5657', '2019-10-07 00:00:00', '2019-10-14 00:00:00', 5, 2, 3, 8, 7, 'BOOK6281ING1569498862', 5124, 0, 2, 0),
(2, '0000-00-00 00:00:00', 'amit kumar yadav', 'amit12nig96@gmail.com', '9596561101', 1, 'abc5657', '2019-10-07 00:00:00', '2019-10-14 00:00:00', 5, 2, 3, 8, 7, 'BOOK5025ING1569581270', 5124, 0, 0, 1),
(3, '0000-00-00 00:00:00', 'amit kumar yadav', 'amit12nig96@gmail.com', '9596561106', 1, 'abc5657', '2019-10-07 00:00:00', '2019-10-14 00:00:00', 5, 2, 3, 8, 7, 'BOOK6711ING1569581368', 5124, 0, 0, 1),
(4, '0000-00-00 00:00:00', 'amit kumar yadav', 'amit12nig96@gmail.com', '9596561116', 1, 'abc5657', '2019-10-07 00:00:00', '2019-10-14 00:00:00', 5, 2, 3, 8, 7, 'BOOK2608ING1569581455', 5124, 0, 0, 1),
(5, '0000-00-00 00:00:00', 'amit kumar yadav', 'amit12nig96@gmail.com', '9596561176', 1, 'abc5657', '2019-10-07 00:00:00', '2019-10-14 00:00:00', 5, 2, 3, 8, 7, 'BOOK3203ING1569581511', 5124, 0, 0, 1),
(6, '0000-00-00 00:00:00', 'amit kumar yadav', 'amit12nig96@gmail.com', '9596561196', 1, 'abc5657', '2019-10-07 00:00:00', '2019-10-14 00:00:00', 5, 2, 3, 8, 7, 'BOOK7000ING1569581878', 5124, 0, 0, 1),
(7, '0000-00-00 00:00:00', 'amit kumar yadav', 'amit12nig96@gmail.com', '9596561136', 1, 'abc5657', '2019-10-07 00:00:00', '2019-10-14 00:00:00', 5, 2, 3, 8, 7, 'BOOK5303ING1569582048', 5124, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `hotel_user_people`
--

CREATE TABLE `hotel_user_people` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `booking_owner_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `room_rent` varchar(255) COLLATE utf8_bin NOT NULL,
  `people` longtext COLLATE utf8_bin NOT NULL,
  `total_room_rent` int(11) NOT NULL,
  `days` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `hotel_user_people`
--

INSERT INTO `hotel_user_people` (`id`, `created_at`, `booking_owner_id`, `room_id`, `hotel_id`, `room_rent`, `people`, `total_room_rent`, `days`, `status`) VALUES
(1, '2019-09-26 17:24:22', 1, 1, 1, '141', 'amit kumar yadav0,9596561100,amit12nig960@gmail.com,ABC000', 987, 7, 1),
(2, '2019-09-26 17:24:22', 1, 1, 1, '141', 'amit kumar yadav1,9596561101,amit12nig961@gmail.com,ABC010', 987, 7, 1),
(3, '2019-09-26 17:24:22', 1, 2, 1, '150', 'amit kumar yadav2,9596561120,amit12nig962@gmail.com,ABC020~amit kumar yadav21,9596561121,amit12nig9621@gmail.com,ABC021', 1050, 7, 1),
(4, '2019-09-26 17:24:22', 1, 2, 1, '150', 'amit kumar yadav3,9596561130,amit12nig961@gmail.com,ABC030~amit kumar yadav31,9596561131,amit12nig962@gmail.com,ABC031', 1050, 7, 1),
(5, '2019-09-26 17:24:23', 1, 2, 1, '150', 'amit kumar yadav4,9596561140,amit12nig964@gmail.com,ABC040~amit kumar yadav41,9596561141,amit12nig9641@gmail.com,ABC041', 1050, 7, 1),
(6, '2019-09-27 16:17:50', 2, 1, 1, '141', 'amit kumar yadav0,9596561100,amit12nig960@gmail.com,ABC000', 987, 7, 1),
(7, '2019-09-27 16:17:50', 2, 1, 1, '141', 'amit kumar yadav1,9596561101,amit12nig961@gmail.com,ABC010', 987, 7, 1),
(8, '2019-09-27 16:17:50', 2, 2, 1, '150', 'amit kumar yadav2,9596561120,amit12nig962@gmail.com,ABC020~amit kumar yadav21,9596561121,amit12nig9621@gmail.com,ABC021', 1050, 7, 1),
(9, '2019-09-27 16:17:50', 2, 2, 1, '150', 'amit kumar yadav3,9596561130,amit12nig961@gmail.com,ABC030~amit kumar yadav31,9596561131,amit12nig962@gmail.com,ABC031', 1050, 7, 1),
(10, '2019-09-27 16:17:50', 2, 2, 1, '150', 'amit kumar yadav4,9596561140,amit12nig964@gmail.com,ABC040~amit kumar yadav41,9596561141,amit12nig9641@gmail.com,ABC041', 1050, 7, 1),
(11, '2019-09-27 16:19:28', 3, 1, 1, '141', 'amit kumar yadav0,9596561100,amit12nig960@gmail.com,ABC000', 987, 7, 1),
(12, '2019-09-27 16:19:29', 3, 1, 1, '141', 'amit kumar yadav1,9596561101,amit12nig961@gmail.com,ABC010', 987, 7, 1),
(13, '2019-09-27 16:19:29', 3, 2, 1, '150', 'amit kumar yadav2,9596561120,amit12nig962@gmail.com,ABC020~amit kumar yadav21,9596561121,amit12nig9621@gmail.com,ABC021', 1050, 7, 1),
(14, '2019-09-27 16:19:29', 3, 2, 1, '150', 'amit kumar yadav3,9596561130,amit12nig961@gmail.com,ABC030~amit kumar yadav31,9596561131,amit12nig962@gmail.com,ABC031', 1050, 7, 1),
(15, '2019-09-27 16:19:29', 3, 2, 1, '150', 'amit kumar yadav4,9596561140,amit12nig964@gmail.com,ABC040~amit kumar yadav41,9596561141,amit12nig9641@gmail.com,ABC041', 1050, 7, 1),
(16, '2019-09-27 16:20:55', 4, 1, 1, '141', 'amit kumar yadav0,9596561100,amit12nig960@gmail.com,ABC000', 987, 7, 1),
(17, '2019-09-27 16:20:55', 4, 1, 1, '141', 'amit kumar yadav1,9596561101,amit12nig961@gmail.com,ABC010', 987, 7, 1),
(18, '2019-09-27 16:20:55', 4, 2, 1, '150', 'amit kumar yadav2,9596561120,amit12nig962@gmail.com,ABC020~amit kumar yadav21,9596561121,amit12nig9621@gmail.com,ABC021', 1050, 7, 1),
(19, '2019-09-27 16:20:55', 4, 2, 1, '150', 'amit kumar yadav3,9596561130,amit12nig961@gmail.com,ABC030~amit kumar yadav31,9596561131,amit12nig962@gmail.com,ABC031', 1050, 7, 1),
(20, '2019-09-27 16:20:55', 4, 2, 1, '150', 'amit kumar yadav4,9596561140,amit12nig964@gmail.com,ABC040~amit kumar yadav41,9596561141,amit12nig9641@gmail.com,ABC041', 1050, 7, 1),
(21, '2019-09-27 16:21:51', 5, 1, 1, '141', 'amit kumar yadav0,9596561100,amit12nig960@gmail.com,ABC000', 987, 7, 1),
(22, '2019-09-27 16:21:51', 5, 1, 1, '141', 'amit kumar yadav1,9596561101,amit12nig961@gmail.com,ABC010', 987, 7, 1),
(23, '2019-09-27 16:21:51', 5, 2, 1, '150', 'amit kumar yadav2,9596561120,amit12nig962@gmail.com,ABC020~amit kumar yadav21,9596561121,amit12nig9621@gmail.com,ABC021', 1050, 7, 1),
(24, '2019-09-27 16:21:51', 5, 2, 1, '150', 'amit kumar yadav3,9596561130,amit12nig961@gmail.com,ABC030~amit kumar yadav31,9596561131,amit12nig962@gmail.com,ABC031', 1050, 7, 1),
(25, '2019-09-27 16:21:51', 5, 2, 1, '150', 'amit kumar yadav4,9596561140,amit12nig964@gmail.com,ABC040~amit kumar yadav41,9596561141,amit12nig9641@gmail.com,ABC041', 1050, 7, 1),
(26, '2019-09-27 16:27:58', 6, 1, 1, '141', 'amit kumar yadav0,9596561100,amit12nig960@gmail.com,ABC000', 987, 7, 1),
(27, '2019-09-27 16:27:58', 6, 1, 1, '141', 'amit kumar yadav1,9596561101,amit12nig961@gmail.com,ABC010', 987, 7, 1),
(28, '2019-09-27 16:27:58', 6, 2, 1, '150', 'amit kumar yadav2,9596561120,amit12nig962@gmail.com,ABC020~amit kumar yadav21,9596561121,amit12nig9621@gmail.com,ABC021', 1050, 7, 1),
(29, '2019-09-27 16:27:58', 6, 2, 1, '150', 'amit kumar yadav3,9596561130,amit12nig961@gmail.com,ABC030~amit kumar yadav31,9596561131,amit12nig962@gmail.com,ABC031', 1050, 7, 1),
(30, '2019-09-27 16:27:58', 6, 2, 1, '150', 'amit kumar yadav4,9596561140,amit12nig964@gmail.com,ABC040~amit kumar yadav41,9596561141,amit12nig9641@gmail.com,ABC041', 1050, 7, 1),
(31, '2019-09-27 16:30:48', 7, 1, 1, '141', 'amit kumar yadav0,9596561100,amit12nig960@gmail.com,ABC000', 987, 7, 1),
(32, '2019-09-27 16:30:48', 7, 1, 1, '141', 'amit kumar yadav1,9596561101,amit12nig961@gmail.com,ABC010', 987, 7, 1),
(33, '2019-09-27 16:30:48', 7, 2, 1, '150', 'amit kumar yadav2,9596561120,amit12nig962@gmail.com,ABC020~amit kumar yadav21,9596561121,amit12nig9621@gmail.com,ABC021', 1050, 7, 1),
(34, '2019-09-27 16:30:48', 7, 2, 1, '150', 'amit kumar yadav3,9596561130,amit12nig961@gmail.com,ABC030~amit kumar yadav31,9596561131,amit12nig962@gmail.com,ABC031', 1050, 7, 1),
(35, '2019-09-27 16:30:49', 7, 2, 1, '150', 'amit kumar yadav4,9596561140,amit12nig964@gmail.com,ABC040~amit kumar yadav41,9596561141,amit12nig9641@gmail.com,ABC041', 1050, 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `itinerary_buckets`
--

CREATE TABLE `itinerary_buckets` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `status` tinyint(15) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `itinerary_buckets`
--

INSERT INTO `itinerary_buckets` (`id`, `created_at`, `name`, `status`) VALUES
(1, '2018-10-09 12:58:08', 'Transport', 1),
(2, '2018-10-09 12:58:08', 'Food', 1),
(3, '2018-10-30 08:14:59', 'Generic', 1);

-- --------------------------------------------------------

--
-- Table structure for table `itinerary_ticket_bank`
--

CREATE TABLE `itinerary_ticket_bank` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `bank_title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `m_role_status` tinyint(4) NOT NULL,
  `m_role_shared` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `s_role_status` tinyint(4) NOT NULL,
  `s_role_shared` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `p_role_status` tinyint(4) NOT NULL,
  `p_role_shared` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `v_role_status` tinyint(4) NOT NULL,
  `v_role_shared` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1- active, 2- inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `itinerary_ticket_bank`
--

INSERT INTO `itinerary_ticket_bank` (`id`, `created_at`, `company_id`, `bank_title`, `m_role_status`, `m_role_shared`, `s_role_status`, `s_role_shared`, `p_role_status`, `p_role_shared`, `v_role_status`, `v_role_shared`, `status`) VALUES
(1, '2020-04-15 14:48:24', 2, 'amit', 0, '', 1, '2', 1, '3', 1, '4', 1),
(2, '2020-04-15 14:54:33', 2, 'test 2', 0, '2', 0, '2', 0, '3', 0, '1', 2);

-- --------------------------------------------------------

--
-- Table structure for table `livepollmessagedata`
--

CREATE TABLE `livepollmessagedata` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `channel_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `message_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL,
  `message_type` varchar(255) COLLATE utf8_bin NOT NULL,
  `total_response` int(11) NOT NULL,
  `poll_config` longtext COLLATE utf8_bin NOT NULL,
  `poll_type` tinyint(4) NOT NULL,
  `poll_response_by` longtext COLLATE utf8_bin NOT NULL,
  `poll_response` longtext COLLATE utf8_bin NOT NULL,
  `word_cloud_all_answer` longtext COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `livepollmessagedata`
--

INSERT INTO `livepollmessagedata` (`id`, `created_at`, `channel_id`, `message_id`, `user_id`, `message_type`, `total_response`, `poll_config`, `poll_type`, `poll_response_by`, `poll_response`, `word_cloud_all_answer`) VALUES
(6, '2020-01-06 19:03:42', '5e0d9cec0db4330001ae2589', '6618793466908372992', 1, 'text', 4, '{\"show_response\":\"1\",\"submit_anonymous\":\"1\",\"multiple_option_select_allowed\":\"0\",\"multiple_answer_allowed\":\"1\",\"answer_validation\":\"1\",\"correct_answer\":\"b\"}', 2, '[\"2\",\"2\",\"2\",\"2\"]', '[{\"user_id\":\"2\",\"submit_as_anonymous\":\"1\",\"user_response\":\"amit kumar yadav\",\"answer_weight\":\"1\"},{\"user_id\":\"2\",\"submit_as_anonymous\":\"0\",\"user_response\":\"amit kumar yadav\",\"answer_weight\":\"2\"},{\"user_id\":\"2\",\"submit_as_anonymous\":\"1\",\"user_response\":\"amit kumar yadav\",\"answer_weight\":\"3\"},{\"user_id\":\"2\",\"submit_as_anonymous\":\"1\",\"user_response\":\"amit kumar yadav\"}]', '[\"amit kumar yadav\",\"amit kumar yadav\",\"amit kumar yadav\",\"amit kumar yadav\"]'),
(7, '2020-02-19 12:52:35', '5e4bb362da71c80001f2864f', '6635470590922715136', 1, 'text', 1, '{\"show_response\":\"1\",\"submit_anonymous\":\"1\",\"multiple_option_select_allowed\":\"1\",\"multiple_answer_allowed\":\"0\",\"answer_validation\":\"1\",\"correct_answer\":\"a\"}', 1, '[\"12\"]', '[{\"user_id\":\"12\",\"submit_as_anonymous\":\"1\",\"user_response\":\"0,2\"}]', NULL),
(8, '2020-02-19 13:41:33', '5e4bb362da71c80001f2864f', '6635473461823139840', 1, 'text', 4, '{\"show_response\":\"1\",\"submit_anonymous\":\"1\",\"multiple_option_select_allowed\":\"0\",\"answer_validation\":\"0\",\"correct_answer\":\"\",\"multiple_answer_allowed\":\"0\"}', 4, '[\"3\",\"11\",\"2\"]', '[{\"user_id\":\"3\",\"submit_as_anonymous\":\"1\",\"user_response\":\"3\"}]', NULL),
(9, '2020-02-19 15:37:46', '5e4bb362da71c80001f2864f', '6635473226061312000', 1, 'text', 2, '{\"show_response\":\"1\",\"submit_anonymous\":\"1\",\"multiple_option_select_allowed\":\"0\",\"multiple_answer_allowed\":\"0\",\"answer_validation\":\"0\",\"correct_answer\":\"\"}', 3, '[\"3\",\"12\"]', '[{\"user_id\":\"3\",\"submit_as_anonymous\":\"1\",\"user_response\":\"Thank you for submission 222222222222222\"},{\"user_id\":\"12\",\"submit_as_anonymous\":\"1\",\"user_response\":\"amit\"}]', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `livepoll_groupchannel`
--

CREATE TABLE `livepoll_groupchannel` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `groupchannel_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `company_id` int(11) NOT NULL,
  `livesession_id` int(11) DEFAULT NULL,
  `channel_running_status` tinyint(4) DEFAULT '1',
  `custom_filter_channel_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `groupchannel_no` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `livepoll_groupchannel`
--

INSERT INTO `livepoll_groupchannel` (`id`, `created_at`, `groupchannel_name`, `company_id`, `livesession_id`, `channel_running_status`, `custom_filter_channel_id`, `groupchannel_no`, `status`) VALUES
(2, '2020-02-18 15:20:26', 'live poll channel', 2, 1, 1, 'ac1a2', '5e4bb362da71c80001f2864f', 1),
(3, '2020-06-12 20:00:59', 'Public Group Channel', 2, 7, 1, 'dcab3', '5ee391a4c7ecde0001114300', 1);

-- --------------------------------------------------------

--
-- Table structure for table `livepoll_questions`
--

CREATE TABLE `livepoll_questions` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `livesession_id` int(11) NOT NULL,
  `livepoll_id` int(11) NOT NULL,
  `poll_question` longtext COLLATE utf8_bin NOT NULL,
  `poll_type` tinyint(4) NOT NULL,
  `options` longtext COLLATE utf8_bin,
  `show_response` tinyint(4) NOT NULL DEFAULT '0',
  `submit_anonymous` tinyint(4) NOT NULL DEFAULT '0',
  `poll_question_chatcamp_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `channel_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `multiple_osa` tinyint(4) NOT NULL DEFAULT '0',
  `multiple_answer_allowed` tinyint(4) NOT NULL DEFAULT '0',
  `answer_validation` tinyint(4) NOT NULL DEFAULT '0',
  `correct_answer` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `poll_running_status` tinyint(4) DEFAULT '0' COMMENT '0- Running stop,1- poll running, 2- poll paused',
  `status` tinyint(4) NOT NULL COMMENT '1-active, 2- inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `livepoll_questions`
--

INSERT INTO `livepoll_questions` (`id`, `created_at`, `company_id`, `livesession_id`, `livepoll_id`, `poll_question`, `poll_type`, `options`, `show_response`, `submit_anonymous`, `poll_question_chatcamp_id`, `channel_id`, `multiple_osa`, `multiple_answer_allowed`, `answer_validation`, `correct_answer`, `poll_running_status`, `status`) VALUES
(1, '2020-01-03 13:38:28', 2, 1, 1, 'test by amit', 2, 'a,b,c,d', 1, 1, '6618793466908372992', '5e0d9cec0db4330001ae2589', 1, 1, 1, 'b', 1, 1),
(6, '2020-02-18 15:21:08', 2, 1, 2, 'multiple choice', 1, 'a,b,c', 1, 1, '6635470590922715136', '5e4bb362da71c80001f2864f', 1, 0, 1, 'a', 0, 1),
(7, '2020-02-18 15:31:37', 2, 1, 2, 'open text', 3, '', 0, 1, '6635473226061312000', '5e4bb362da71c80001f2864f', 0, 0, 0, '', 1, 1),
(8, '2020-02-18 15:32:33', 2, 1, 2, 'Rating question Please give rate??', 4, '5', 1, 1, '6635473461823139840', '5e4bb362da71c80001f2864f', 0, 0, 0, '', 0, 1),
(9, '2020-02-18 15:33:16', 2, 1, 2, 'word cloud', 2, '', 1, 1, '6635473640479518720', '5e4bb362da71c80001f2864f', 0, 1, 0, '', 0, 1),
(11, '2020-05-04 13:20:11', 2, 1, 2, 'st by amitte', 3, '', 1, 1, '6662981638785331200', '5e4bb362da71c80001f2864f', 0, 1, 0, '', 0, 1),
(13, '2020-06-12 21:10:13', 2, 7, 3, 'How is Life ??', 2, '', 1, 0, '6677233043490467840', '5ee391a4c7ecde0001114300', 0, 0, 0, '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `livepoll_word_weight`
--

CREATE TABLE `livepoll_word_weight` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `channel_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `message_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `question_type` int(11) NOT NULL,
  `word` varchar(255) COLLATE utf8_bin NOT NULL,
  `weight` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `livequestionmessagedata`
--

CREATE TABLE `livequestionmessagedata` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `channel_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `message_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL,
  `message_poster_role` int(11) NOT NULL,
  `message_type` varchar(255) NOT NULL,
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `like` int(11) NOT NULL,
  `like_members` longtext NOT NULL,
  `is_show` tinyint(4) NOT NULL DEFAULT '0',
  `reply` varchar(255) DEFAULT NULL,
  `replyVisible` tinyint(4) NOT NULL,
  `no_name` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `livequestionmessagedata`
--

INSERT INTO `livequestionmessagedata` (`id`, `created_at`, `channel_id`, `message_id`, `user_id`, `message_poster_role`, `message_type`, `message`, `like`, `like_members`, `is_show`, `reply`, `replyVisible`, `no_name`) VALUES
(6, '2019-12-16 08:23:07', '5dee4498553c8a0001c62d1a', '6610099327064862720', 1, 2, 'text', '', 0, '[{\"user_id\":\"1\"},{\"user_id\":\"2\"},{\"user_id\":\"97\"}]', 0, NULL, 0, 0),
(7, '2019-12-10 11:40:18', '5dee4498553c8a0001c62d1a', '6610099327064862720', 1, 2, 'text', '', 2, '[{\"user_id\":\"1\"},{\"user_id\":\"97\"}]', 0, NULL, 0, 0),
(8, '2019-12-16 08:15:54', '5dee4498553c8a0001c62d1a', '6610099327064862720', 1, 2, 'text', '', 1, '[{\"user_id\":\"1\"},{\"user_id\":\"97\"}]', 0, NULL, 0, 0),
(9, '2019-12-16 08:15:49', '5dee4498553c8a0001c62d1a', '6610099327064862720', 1, 2, 'text', '', 4, '[{\"user_id\":\"1\"},{\"user_id\":\"2\"},{\"user_id\":\"97\"}]', 0, NULL, 0, 0),
(10, '2019-12-10 11:40:18', '5dee4498553c8a0001c62d1a', '6610099327064862720', 1, 2, 'text', '', 3, '[{\"user_id\":\"1\"},{\"user_id\":\"2\"},{\"user_id\":\"97\"}]', 0, NULL, 0, 0),
(11, '2019-12-10 11:40:18', '5dee4498553c8a0001c62d1a', '6610099327064862720', 1, 2, 'text', '', 2, '[{\"user_id\":\"1\"},{\"user_id\":\"97\"}]', 0, NULL, 0, 0),
(12, '2019-12-10 11:40:18', '5dee4498553c8a0001c62d1a', '6610099327064862720', 1, 2, 'text', '', 2, '[{\"user_id\":\"1\"},{\"user_id\":\"97\"}]', 0, NULL, 0, 0),
(13, '2019-12-10 11:40:18', '5dee4498553c8a0001c62d1a', '6610099327064862720', 1, 2, 'text', '', 2, '[{\"user_id\":\"1\"},{\"user_id\":\"97\"}]', 0, NULL, 0, 0),
(14, '2019-12-10 11:40:18', '5dee4498553c8a0001c62d1a', '6610099327064862720', 1, 2, 'text', '', 2, '[{\"user_id\":\"1\"},{\"user_id\":\"97\"}]', 0, NULL, 0, 0),
(15, '2019-12-16 08:16:09', '5dee4498553c8a0001c62d1a', '6610099327064862720', 1, 2, 'text', '', 5, '[{\"user_id\":\"1\"},{\"user_id\":\"2\"},{\"user_id\":\"97\"}]', 0, NULL, 0, 0),
(16, '2019-12-10 11:40:18', '5dee4498553c8a0001c62d1a', '6610099327064862720', 1, 2, 'text', '', 3, '[{\"user_id\":\"1\"},{\"user_id\":\"2\"},{\"user_id\":\"97\"}]', 0, NULL, 0, 0),
(17, '2019-12-16 08:15:59', '5dee4498553c8a0001c62d1a', '6610099327064862720', 1, 2, 'text', '', 0, '[{\"user_id\":\"1\"},{\"user_id\":\"97\"}]', 0, NULL, 0, 0),
(18, '2020-06-15 07:45:51', '5ee39190c7ecde000151f698', '6678183215225630720', 14, 2, 'text', '', 0, '[]', 1, '?', 0, 1),
(19, '2020-06-15 07:46:17', '5ee39190c7ecde000151f698', '6678181756673191936', 14, 2, 'text', '', 0, '[]', 1, 'Welcome', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `livequestion_groupchannel`
--

CREATE TABLE `livequestion_groupchannel` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `groupchannel_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `company_id` int(11) NOT NULL,
  `livesession_id` int(11) DEFAULT NULL,
  `channel_running_status` tinyint(4) DEFAULT '1',
  `custom_filter_channel_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `groupchannel_no` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `livequestion_groupchannel`
--

INSERT INTO `livequestion_groupchannel` (`id`, `created_at`, `groupchannel_name`, `company_id`, `livesession_id`, `channel_running_status`, `custom_filter_channel_id`, `groupchannel_no`, `status`) VALUES
(3, '2019-12-09 18:26:56', 'Live quesiton channel', 2, 1, 1, '44883', '5dee4498553c8a0001c62d1a', 1),
(4, '2020-01-02 13:57:13', 'live question second session', 2, 2, 1, '6bc14', '5e0da9610db433000135cd96', 1),
(5, '2020-06-12 20:00:39', 'Public Group Channel', 2, 7, 1, '1a495', '5ee39190c7ecde000151f698', 1);

-- --------------------------------------------------------

--
-- Table structure for table `livesession`
--

CREATE TABLE `livesession` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `session_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `session_code` varchar(255) COLLATE utf8_bin DEFAULT 'empty',
  `connect_user_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `role_base` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `user_base` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `max_user_connection_limit` int(11) NOT NULL,
  `moderator` varchar(255) COLLATE utf8_bin NOT NULL,
  `filter_moderator_status` tinyint(4) NOT NULL,
  `start_date_time` datetime NOT NULL,
  `end_date_time` datetime NOT NULL,
  `live_question_groupchannel` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `liveqs_filter_channel` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `live_poll_groupchannel` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `live_poll_custom_filter` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `live_stream_groupchannel` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `live_stream_custom_filter` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `is_session_zoom` tinyint(4) NOT NULL DEFAULT '0',
  `zoom_user_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `zoom_password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `zoom_api_key` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `zoom_api_secret` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `zoom_meeting_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `web_url_status` tinyint(4) NOT NULL DEFAULT '0',
  `web_url` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `is_public_channel` tinyint(4) NOT NULL DEFAULT '1',
  `istobereminded` tinyint(4) NOT NULL DEFAULT '0',
  `istobereminded_time` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `notification_flag_reminder` tinyint(4) NOT NULL DEFAULT '0',
  `configuration_status` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL COMMENT '1-active,2-deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `livesession`
--

INSERT INTO `livesession` (`id`, `created_at`, `company_id`, `session_name`, `session_code`, `connect_user_id`, `role_base`, `user_base`, `max_user_connection_limit`, `moderator`, `filter_moderator_status`, `start_date_time`, `end_date_time`, `live_question_groupchannel`, `liveqs_filter_channel`, `live_poll_groupchannel`, `live_poll_custom_filter`, `live_stream_groupchannel`, `live_stream_custom_filter`, `is_session_zoom`, `zoom_user_name`, `zoom_password`, `zoom_api_key`, `zoom_api_secret`, `zoom_meeting_id`, `web_url_status`, `web_url`, `is_public_channel`, `istobereminded`, `istobereminded_time`, `notification_flag_reminder`, `configuration_status`, `status`) VALUES
(1, '2019-12-26 12:57:42', 2, 'first Session', 'tfe2', '1', '1', '1', 15, '1', 1, '2020-04-20 18:30:00', '2020-12-31 12:55:00', '5dee4498553c8a0001c62d1a', '44883', '5e4bb362da71c80001f2864f', 'ac1a2', '5e0d9cec0db4330001ae258', 'bf2a1', 0, '', '', NULL, NULL, '', 0, NULL, 1, 0, NULL, 0, 1, 1),
(2, '2019-12-26 12:57:42', 2, 'Second Session', 'amit', '1', NULL, '12', 15, '1', 1, '2020-04-20 18:46:00', '2020-12-31 12:55:00', '5e0da9610db433000135cd96', '6bc14', NULL, NULL, NULL, 'c15c4', 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 0, NULL, 0, 1, 1),
(6, '2020-04-08 22:32:22', 2, 'test for zoom', 'zoom', '', '3', '', 50, '12', 1, '2020-04-20 18:43:00', '2020-04-30 22:30:00', NULL, NULL, NULL, NULL, NULL, NULL, 1, 'abc', '123456', 'a', 'b', 'abc123', 0, NULL, 1, 0, NULL, 0, 1, 1),
(7, '2020-06-12 20:00:12', 2, 'Public Group', 'PG55', '14', '1,2,3,4', '', 100, '14', 0, '2020-06-12 20:00:00', '2020-06-30 23:55:00', '5ee39190c7ecde000151f698', '1a495', '5ee391a4c7ecde0001114300', 'dcab3', '5ee39214c7ecde000183e0f9', 'a7d28', 0, '', '', '', '', '', 0, NULL, 1, 0, NULL, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `livesession_message_like_user`
--

CREATE TABLE `livesession_message_like_user` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `like_by_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `livestream_groupchannel`
--

CREATE TABLE `livestream_groupchannel` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `groupchannel_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `company_id` int(11) NOT NULL,
  `livesession_id` int(11) DEFAULT NULL,
  `live_stream_state` tinyint(4) NOT NULL DEFAULT '0',
  `streaming_url` varchar(244) COLLATE utf8_bin DEFAULT NULL,
  `streaming_status` tinyint(4) NOT NULL DEFAULT '0',
  `channel_running_status` tinyint(4) DEFAULT '1',
  `custom_filter_channel_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `groupchannel_no` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `livestream_groupchannel`
--

INSERT INTO `livestream_groupchannel` (`id`, `created_at`, `groupchannel_name`, `company_id`, `livesession_id`, `live_stream_state`, `streaming_url`, `streaming_status`, `channel_running_status`, `custom_filter_channel_id`, `groupchannel_no`, `status`) VALUES
(7, '2020-01-01 16:04:58', 'live stream local', 2, 1, 0, '', 0, 1, '66c87', '5e0c75d10db43300013d2ffa', 1),
(8, '2020-06-12 20:02:51', 'Public Group', 2, 7, 1, 'https://www.youtube.com/embed/9mWdw-09dso', 1, 1, 'a7d28', '5ee39214c7ecde000183e0f9', 1);

-- --------------------------------------------------------

--
-- Table structure for table `live_session_join_user`
--

CREATE TABLE `live_session_join_user` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `qs_join_status` tinyint(4) NOT NULL DEFAULT '0',
  `poll_join_status` tinyint(4) NOT NULL DEFAULT '0',
  `st_join_status` tinyint(4) NOT NULL DEFAULT '0',
  `user_id` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `lottery_data`
--

CREATE TABLE `lottery_data` (
  `id` int(11) NOT NULL,
  `luckydraw_name` varchar(255) DEFAULT NULL,
  `tagline` varchar(255) DEFAULT NULL,
  `spin_screen_bg` varchar(255) DEFAULT NULL,
  `winner_screen_bg` varchar(255) DEFAULT NULL,
  `spin_img_1` varchar(255) DEFAULT NULL,
  `spin_img_2` varchar(255) DEFAULT NULL,
  `spin_img_3` varchar(255) DEFAULT NULL,
  `prize_image_1` varchar(255) DEFAULT NULL,
  `prize_image_2` varchar(255) DEFAULT NULL,
  `prize_image_3` varchar(255) DEFAULT NULL,
  `prize_image_4` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `company_logo` varchar(255) DEFAULT NULL,
  `event_logo` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lottery_data`
--

INSERT INTO `lottery_data` (`id`, `luckydraw_name`, `tagline`, `spin_screen_bg`, `winner_screen_bg`, `spin_img_1`, `spin_img_2`, `spin_img_3`, `prize_image_1`, `prize_image_2`, `prize_image_3`, `prize_image_4`, `logo`, `company_logo`, `event_logo`, `status`) VALUES
(1, 'Festival Bonanza - Rs 11000/-', '', 'https://warriors.oneteam.in/lotterydraw/images/spin_bg.jpg', 'https://warriors.oneteam.in/lotterydraw/images/winner_bg.jpg', 'https://warriors.oneteam.in/lotterydraw/images/ele1.png', 'https://warriors.oneteam.in/lotterydraw/images/ele3.png', 'https://warriors.oneteam.in/lotterydraw/images/ele2.png', 'https://warriors.oneteam.in/lotterydraw/images/prize1.png', 'https://warriors.oneteam.in/lotterydraw/images/prize2.png', 'https://warriors.oneteam.in/lotterydraw/images/prize3.png', 'https://warriors.oneteam.in/lotterydraw/images/prize4.png', '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `lottery_participants`
--

CREATE TABLE `lottery_participants` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `participant_id` varchar(4) DEFAULT NULL,
  `status` int(11) DEFAULT '1' COMMENT '0=Ineligible;1=Active;2=Winner'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lottery_participants`
--

INSERT INTO `lottery_participants` (`id`, `user_id`, `participant_id`, `status`) VALUES
(1, 31456, '2510', 2),
(2, 12001, '0197', 2),
(3, 22581, '6745', 2),
(4, 11875, '9126', 1);

-- --------------------------------------------------------

--
-- Table structure for table `lottery_winners`
--

CREATE TABLE `lottery_winners` (
  `id` int(11) NOT NULL,
  `luckydraw_name` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `participant_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lottery_winners`
--

INSERT INTO `lottery_winners` (`id`, `luckydraw_name`, `user_id`, `participant_id`, `name`, `date_time`) VALUES
(1, 'Festival Bonanza - Rs 5000/-', 22581, 6745, 'Sarita', '2020-12-03 16:43:11'),
(2, 'Festival Bonanza - Rs 5000/-', 31456, 2510, 'Lalit Shukla', '2020-12-03 16:44:55'),
(3, 'Festival Bonanza - Rs 5000/-', 12001, 197, 'MUKESH PANTH', '2020-12-03 16:46:19');

-- --------------------------------------------------------

--
-- Table structure for table `meal_type`
--

CREATE TABLE `meal_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meal_type`
--

INSERT INTO `meal_type` (`id`, `name`) VALUES
(1, 'Break Fast'),
(2, 'Lunch'),
(3, 'High Tea'),
(4, 'Dinner');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `alias` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `created_at`, `title`, `parent_id`, `alias`, `position`) VALUES
(1, '2018-06-22 00:27:37', 'Avatars', 0, '', 19),
(2, '2018-06-22 00:27:37', 'Company', 0, 'company', 2),
(3, '2018-07-04 22:32:28', 'Employee', 0, '', 9),
(4, '2018-07-04 22:34:02', 'Admin Users', 22, 'adminusers', 1),
(5, '2018-07-04 22:34:02', 'Manage Content', 22, 'content', 3),
(6, '2018-07-05 00:57:51', 'Admin Roles', 22, 'roles', 2),
(7, '2018-07-22 22:25:56', 'Itinerary', 0, '', 9),
(8, '2018-07-25 22:17:40', 'App Theme', 22, 'mobilebranding', 4),
(9, '2018-07-25 22:38:37', 'Question Category', 0, '', 6),
(10, '2018-07-25 23:24:32', 'Corporate Values', 0, '', 20),
(11, '2018-07-26 00:44:52', 'Bottom Navigation', 0, '', 5),
(12, '2018-07-26 00:57:36', 'Question Bank', 0, '', 7),
(13, '2018-07-30 00:01:10', 'Wheel Category', 0, '', 11),
(14, '2018-08-01 02:41:48', 'Activity Icons', 0, '', 13),
(15, '2018-08-01 02:46:22', 'Activities', 0, '', 15),
(16, '2018-08-03 19:37:29', 'Wheel of Discovery', 0, '', 12),
(17, '2018-08-05 03:06:28', 'Survey', 0, '', 8),
(18, '2018-08-07 02:36:53', 'One Team Splash', 22, 'oneteambranding', 5),
(19, '2018-08-07 05:02:23', 'Badges', 0, '', 14),
(20, '2018-08-07 07:55:19', 'Company Avatars', 1, 'companyavatars', 2),
(21, '2018-08-07 08:00:38', 'One Team Avatars', 1, 'avatars', 1),
(22, '2018-08-07 08:37:39', 'One Team Admin', 0, NULL, 1),
(23, '2018-08-07 10:56:07', 'One Team Bottom navigation', 11, 'mobilemenu', 1),
(24, '2018-08-07 10:56:07', 'Company Bottom Navigation', 11, 'companymobilemenu', 2),
(25, '2018-08-07 11:31:25', 'One Team Question Categories', 9, 'questioncategory', 1),
(26, '2018-08-07 11:31:25', 'Company Question Categories', 9, 'companyquestioncategory', 2),
(27, '2018-08-07 12:15:19', 'One Team Corporate Values', 10, 'corporatevalues', 1),
(28, '2018-08-07 12:15:19', 'Company Values', 10, 'companyvalues', 2),
(29, '2018-08-07 12:36:05', 'One Team Question Bank', 12, 'question', 1),
(30, '2018-08-07 12:36:05', 'Company Question Bank', 12, 'companyquestion', 2),
(31, '2018-08-07 13:52:49', 'One Team Wheel Category', 13, 'wheelcategory', 1),
(32, '2018-08-07 13:52:49', 'Company Wheel Category', 13, 'companywheelcategory', 2),
(33, '2018-08-07 15:10:52', 'Wheel Of Discovery', 16, 'wheeldiscovery', 1),
(34, '2018-08-07 15:10:52', 'Wheel Attempts', 16, 'wheelattempts', 2),
(35, '2018-08-07 15:45:59', 'Survey List', 17, 'survey', 1),
(36, '2018-08-07 15:45:59', 'Survey Response', 17, 'surveyresponse', 2),
(37, '2018-08-07 15:57:53', 'One Team Badges', 19, 'badges', 1),
(38, '2018-08-07 15:57:53', 'Company Badges', 19, 'companybadges', 2),
(39, '2018-08-07 16:38:20', 'One Team Activity Icons', 14, 'activityicons', 1),
(40, '2018-08-07 16:38:20', 'Company Activity Icons', 14, 'companyactivityicons', 2),
(41, '2018-08-15 01:06:24', 'Notification Template', 0, 'notificationtemplates', 16),
(42, '2018-08-15 08:02:43', 'Reward Rules', 0, 'rewardrules', 17),
(43, '2018-09-05 05:44:44', 'Step Counter Activitiy', 15, 'new_activity', 2),
(45, '2018-09-05 07:59:01', 'Title Fight Activitiy', 15, 'activity', 1),
(46, '2018-09-17 08:59:03', 'Device Version', 22, 'deviceversion', 6),
(47, '2018-10-09 10:46:23', 'Company Event', 7, 'companyevent', 1),
(48, '2018-10-09 10:47:18', 'Company Event Data', 7, 'companyeventdata', 2),
(49, '2018-10-10 16:09:34', 'Announcement', 0, 'announcement', 13),
(50, '2018-11-20 06:56:46', 'Sponsor', 0, '', 3),
(51, '2018-11-21 07:23:33', 'Company Sponsor Title', 50, 'companysponsortitle', 1),
(52, '2018-11-21 07:23:33', 'Company Sponsor', 50, 'companysponsor', 2),
(53, '2018-11-21 10:44:37', 'User Request', 22, 'userrequest', 7),
(54, '2018-11-21 10:44:37', 'SMS User', 22, 'webhookuser', 8),
(55, '2018-11-21 10:44:37', 'Confirmation Message', 22, 'autoreplymessage', 9),
(56, '2018-12-05 10:11:41', 'User Role', 0, '', 3),
(57, '2018-12-05 10:13:42', 'One Team User Role', 56, 'user_role', 1),
(58, '2018-12-05 10:13:42', 'Company User Role', 56, 'company_user_role', 2),
(59, '2018-12-17 06:53:44', 'Notification', 0, NULL, 10),
(60, '2018-12-17 06:54:48', 'One Team Notification', 59, 'oneteamnotifications', 1),
(61, '2018-12-17 06:55:28', 'Company Notification', 59, 'companynotifications', 1),
(62, '2019-01-14 12:39:26', 'Server Url', 0, '', 2),
(63, '2019-01-14 12:40:48', 'Oneteam Server Url', 62, 'oneteamserverurl', 1),
(64, '2019-01-14 12:40:48', 'Company server Url', 62, 'companyserverurl', 3),
(65, '2019-01-22 10:43:26', 'Push Notification', 22, 'oneteampushnotification', 3),
(66, '2019-03-13 11:27:05', 'Event Ticket', 7, 'companyeventticket', 3),
(67, '2019-04-06 06:03:35', 'Event Ticket Categroy', 7, 'eventticketcategory', 4),
(68, '2019-04-06 06:03:35', 'User Event Ticket ', 7, 'usereventticket', 7),
(69, '2019-04-29 14:18:24', 'Poll', 0, NULL, 11),
(70, '2019-04-29 14:18:24', 'Poll list', 69, 'poll', 1),
(71, '2019-05-04 10:15:24', 'Poll Response', 69, 'pollresponse', 2),
(72, '2019-05-06 06:26:53', 'Poll Theme', 22, 'oneteampolltheme', 8),
(73, '2019-07-11 07:55:08', 'Ticket Scanner', 0, NULL, 12),
(74, '2019-07-11 07:57:49', 'Scanning company', 73, 'scanningcompany', 1),
(75, '2019-07-11 07:57:49', 'Ticket Category', 73, 'scanningticketcategory', 2),
(76, '2019-07-11 07:58:50', 'Scan Ticket List', 73, 'scanticketlist', 4),
(77, '2019-08-01 13:11:05', 'Employee', 3, 'employee', 1),
(78, '2019-08-01 13:11:05', 'Employee Configuration', 3, 'companyemployeeconfiguration', 2),
(79, '2019-08-24 09:43:47', 'Employee Request', 3, 'employee_request', 2),
(80, '2019-08-27 09:54:22', 'Company Payment Gateway', 0, 'companypaymentgateway', 2),
(81, '2019-08-24 09:43:47', 'Employee Validity', 3, 'employeevalidity', 2),
(82, '2019-10-16 07:05:19', 'Ticket Configuration', 7, 'ticketconfiguration', 5),
(83, '2019-10-16 07:05:19', 'Company Referral Code', 7, 'companyreferralcode', 6),
(84, '2019-10-16 07:05:19', 'Ticket Transaction', 7, 'companyeventtickettransaction', 8),
(85, '2019-12-05 06:29:08', 'Face Data ', 22, 'facedata', 6),
(86, '2019-12-09 10:06:40', 'Company Live Session', 0, NULL, 5),
(87, '2019-12-09 10:08:21', 'Live Session ', 86, 'companylivesession', 1),
(88, '2019-12-09 10:08:21', 'Live question Channel', 86, 'companylivequestion', 2),
(89, '2019-12-09 10:08:56', 'Live Polls Channel', 86, 'companylivepoll', 3),
(90, '2019-12-24 10:28:12', 'Pri Ticket Data', 73, 'scanpriticketlist', 3),
(91, '2019-12-09 10:08:56', 'Live Stream Channel', 86, 'companylivestream', 5),
(92, '2019-12-09 10:08:56', 'Live Poll Questions', 86, 'companylivepollquestion', 4),
(93, '2018-09-17 08:59:03', 'Barcode Generator', 22, 'barcodegenerator', 7),
(94, '2020-03-06 10:06:05', 'Company Subscription', 0, 'companysubscription', 3),
(95, '2020-04-15 06:18:10', 'Event Ticket Bank', 7, 'itineraryticketbank', 9);

-- --------------------------------------------------------

--
-- Table structure for table `message_comment`
--

CREATE TABLE `message_comment` (
  `id` int(11) NOT NULL,
  `channel_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `message_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `message_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `comment` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `message_platform`
--

CREATE TABLE `message_platform` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message_platform`
--

INSERT INTO `message_platform` (`id`, `name`) VALUES
(1, 'ChatCamp'),
(2, 'Sendbird');

-- --------------------------------------------------------

--
-- Table structure for table `mobile_branding`
--

CREATE TABLE `mobile_branding` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `theme_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `normal_text_color` varchar(7) COLLATE utf8_bin NOT NULL,
  `normal_background_color` varchar(7) COLLATE utf8_bin NOT NULL,
  `button_bg_color` varchar(7) COLLATE utf8_bin NOT NULL,
  `button_text_color` varchar(7) COLLATE utf8_bin NOT NULL,
  `status_bar_color` varchar(7) COLLATE utf8_bin NOT NULL,
  `header_color` varchar(7) COLLATE utf8_bin NOT NULL,
  `splash_bg_color` varchar(7) COLLATE utf8_bin NOT NULL,
  `splash_text_color` varchar(7) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `mobile_branding`
--

INSERT INTO `mobile_branding` (`id`, `created_at`, `theme_title`, `normal_text_color`, `normal_background_color`, `button_bg_color`, `button_text_color`, `status_bar_color`, `header_color`, `splash_bg_color`, `splash_text_color`) VALUES
(1, '2018-07-20 02:53:49', 'Default Theme', '#fdfdfd', '#eeeeee', '#c62828', '#ffffff', '#a80101', '#c90000', '#e03552', '#ffffff'),
(3, '2018-08-06 20:34:17', 'Dark Theme', '#f0eaea', '#24c934', '#d91c1c', '#14171f', '', '#392727', '#46a130', '#623737'),
(4, '2018-08-19 05:04:56', 'Theme 1', '#505050', '#d40c0c', '#c24157', '#ffffff', '', '#c24157', '#ffffff', '#505050'),
(5, '2018-08-19 05:05:57', 'Theme 2', '#4cb531', '#ffffff', '#027e83', '#ffffff', '', '#027e83', '#cf1f1f', '#cf1d1d'),
(6, '2018-08-19 05:07:09', 'Theme 3', '#505050', '#ffffff', '#e4333b', '#ffffff', '', '#e4333b', '#ffffff', '#505050'),
(7, '2018-08-19 05:09:34', 'Theme 4', '#505050', '#ffffff', '#e58515', '#ffffff', '', '#e58515', '#ffffff', '#505050'),
(8, '2018-08-19 05:10:39', 'Theme 5', '#505050', '#ffffff', '#02833f', '#ffffff', '', '#02833f', '#ffffff', '#505050'),
(9, '2018-08-19 05:11:34', 'Theme 6', '#505050', '#ffffff', '#500887', '#ffffff', '', '#500887', '#ffffff', '#505050'),
(10, '2018-08-19 05:12:29', 'Theme 7', '#505050', '#ffffff', '#96093e', '#ffffff', '', '#96093e', '#ffffff', '#505050');

-- --------------------------------------------------------

--
-- Table structure for table `mobile_menu`
--

CREATE TABLE `mobile_menu` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `menu_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1 - active, 2 - inactive',
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `mobile_menu`
--

INSERT INTO `mobile_menu` (`id`, `created_at`, `menu_name`, `status`, `image`, `position`) VALUES
(1, '2018-07-21 19:29:22', 'Feed', 1, '153305915510808636865b60a053bd254.png', 1),
(2, '2018-07-21 19:29:22', 'Message', 1, '153305930214158587315b60a0e6bcb7a.png', 2),
(3, '2018-07-21 19:29:22', 'Itinerary', 1, '153305929512066980065b60a0df62da0.png', 3),
(4, '2018-07-21 19:29:22', 'Survey', 1, '153305931313729835795b60a0f17bbf9.png', 4),
(5, '2019-07-25 10:27:50', 'Directory', 1, '153305931313729835795b60a0f17bbf9.png', 5),
(13, '2019-12-09 06:26:10', 'Live', 1, '15758727706468394515dede902c9cce.png', 6),
(14, '2019-12-09 06:26:37', 'Buzz', 1, '157587279716151954045dede91d2d517.png', 7);

-- --------------------------------------------------------

--
-- Table structure for table `new_activity`
--

CREATE TABLE `new_activity` (
  `id` int(11) NOT NULL,
  `activity_global_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `companybadge_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` longtext COLLATE utf8_bin NOT NULL,
  `image` varchar(255) COLLATE utf8_bin NOT NULL,
  `dashboard_image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `start_time` time NOT NULL,
  `points` int(11) NOT NULL,
  `rules` longtext COLLATE utf8_bin NOT NULL,
  `rounds` int(11) NOT NULL,
  `activity_type` int(11) NOT NULL,
  `icons` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 - active, 2 - inactive',
  `activity_cost` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `notification_flag` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `new_activity`
--

INSERT INTO `new_activity` (`id`, `activity_global_id`, `created_at`, `company_id`, `companybadge_id`, `title`, `description`, `image`, `dashboard_image`, `start_date`, `start_time`, `points`, `rules`, `rounds`, `activity_type`, `icons`, `status`, `activity_cost`, `duration`, `notification_flag`) VALUES
(10, 'stepcounter01', '2018-09-06 08:07:57', 2, 1, 'step counter', '<p>kdslakfsajlk</p>', '153622127714131284625b90e05dd321b.png', '15362212776016152515b90e05dd4348.png', '2018-09-07 15:05:00', '00:00:00', 25, '<p>fdsafsaf</p>', 1, 1, '1,2,3', 2, 25, 55, 0),
(11, 'stepcounter01', '2018-09-07 06:14:15', 2, 1, 'step counter two', '<p>step counter two</p>', '153630085511999057455b921737ce0bb.png', '153630085516932889645b921737dd78b.png', '2018-09-13 19:50:00', '00:00:00', 35, '<p>step counter two</p>', 1, 1, '1,2', 2, 50, 50, 0),
(12, 'stepcounter01', '2018-09-13 14:16:19', 2, 1, 'step counter our team test', '<p>fdsafaf</p>', '15368481796166401455b9a71335605a.png', '15368481795337065905b9a713390498.png', '2018-09-27 12:30:00', '00:00:00', 25, '<p>fdsafdsafs</p>', 1, 1, '1,2', 1, 25, 500, 0);

-- --------------------------------------------------------

--
-- Table structure for table `new_activity_tasks`
--

CREATE TABLE `new_activity_tasks` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `task_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 - QnA',
  `start_point` varchar(255) COLLATE utf8_bin NOT NULL,
  `start_latitude` decimal(15,6) NOT NULL,
  `start_longitude` decimal(15,6) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `activity_global_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `end_point` varchar(255) COLLATE utf8_bin NOT NULL,
  `end_latitude` decimal(15,6) NOT NULL,
  `end_longitude` decimal(15,6) NOT NULL,
  `group_total_steps` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `step_length` int(11) NOT NULL,
  `answer_type` tinyint(4) NOT NULL COMMENT '1 - self group, 2 - others',
  `round` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `new_activity_tasks`
--

INSERT INTO `new_activity_tasks` (`id`, `created_at`, `title`, `task_type`, `start_point`, `start_latitude`, `start_longitude`, `activity_id`, `activity_global_id`, `end_point`, `end_latitude`, `end_longitude`, `group_total_steps`, `update_time`, `step_length`, `answer_type`, `round`, `image`) VALUES
(7, '2018-09-06 08:07:57', 'step counter', 1, 'new delhi', '30.232323', '77.232323', 10, 'stepcounter01', 'J6HH+73 New Delhi, Delhi', '30.232323', '77.232323', 55, 0, 1, 0, 1, '153622127713635300735b90e05dd6d40.png'),
(8, '2018-09-07 06:14:15', 'step counter two task', 1, 'munirka delhi', '28.553058', '77.175292', 11, 'stepcounter01', 'shiva ji stadium', '28.630341', '77.220850', 9676, 10, 10, 0, 1, '153630085512301526925b921737de3f3.png'),
(9, '2018-09-13 14:16:19', 'step counter our team', 1, 'munirka delhi', '28.553058', '77.175292', 12, 'stepcounter01', 'shiva ji stadium', '28.630341', '77.220850', 9676, 1, 10, 0, 1, '153803594817576499485bac90ec5cbe0.png');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `time_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `title` varchar(30) COLLATE utf8_bin NOT NULL,
  `message` varchar(100) COLLATE utf8_bin NOT NULL,
  `status` enum('1','2','3') COLLATE utf8_bin NOT NULL DEFAULT '1',
  `error_report` text COLLATE utf8_bin,
  `seen` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `notification_templates`
--

CREATE TABLE `notification_templates` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `notification_type` tinyint(4) NOT NULL DEFAULT '0',
  `alias` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `event` varchar(255) COLLATE utf8_bin NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `message` text COLLATE utf8_bin NOT NULL,
  `variables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `notification_templates`
--

INSERT INTO `notification_templates` (`id`, `created_at`, `notification_type`, `alias`, `event`, `title`, `message`, `variables`) VALUES
(1, '2018-08-15 00:42:21', 1, 'before_activity', '24hrs before activity/2 hrs before/30 min before activity', '{activity_title}', '{activity_title} will begin on {date} at {time}. Gear up.\r\n', '{activity_title} - Activity Title\r\n{date} - Date\r\n{time} - Time'),
(2, '2018-08-15 00:43:58', 1, 'activity_start', 'On Activity Start', '{activity_title}', '{activity_title} has started. Don’t miss your chance.........', '{activity_title} - Activity Title'),
(3, '2018-08-15 00:51:09', 2, 'announcement', 'New Announcement', '', '{announcement_title}', '{announcement_title} - Title of Announcement'),
(4, '2018-08-15 00:53:06', 3, 'survey_publish', 'New Survey', '{survey_title}', 'Your opinion matters. {survey_title} needs your participation', '{survey_title} - Survey Title'),
(5, '2018-08-15 00:53:06', 4, 'wheel_reminder', 'EveryDay (1PM & 6PM)', '', 'Try your luck and spin the wheel. You have {spin_left} spins left for today.', '{spin_left} - Spins Left'),
(6, '2018-08-15 00:53:53', 4, 'wheel_category', 'New category addition to wheel.', '', 'Hey we found new things to discover. {category_name} added to wheel. Lets spin.', '{category_name} - Category Name'),
(7, '2018-08-15 00:55:00', 3, 'pending_survey', 'pending survey { 2 days/ 5 days/ 10 days}', '', 'Your opinion is awaited for {survey_title}', '{survey_title} - Survey Title');

-- --------------------------------------------------------

--
-- Table structure for table `oneteamnotifications`
--

CREATE TABLE `oneteamnotifications` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `notification_type` tinyint(4) NOT NULL DEFAULT '0',
  `alias` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `event` varchar(255) COLLATE utf8_bin NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `message` text COLLATE utf8_bin NOT NULL,
  `time_notification` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `oneteamnotifications`
--

INSERT INTO `oneteamnotifications` (`id`, `created_at`, `notification_type`, `alias`, `event`, `title`, `message`, `time_notification`) VALUES
(1, '2018-08-15 00:42:21', 1, 'before_activity', 'Itinerary Before Start Notification', 'Reminder', 'Reminder For \"{itinerary_title}\".', '00:00:00'),
(2, '2018-08-15 00:43:58', 2, 'activity_start', 'Itinerary Survey Notification', 'Survey Reminder', 'Reminder For \"{itinerary_title}\".', '00:00:00'),
(3, '2018-08-15 00:51:09', 3, 'announcement', 'Survey Not started Notification (not started)', 'Feedback Pending ', '\"{survey_title}\", Your Feedback is Pending.', '12:00:00'),
(4, '2018-08-15 00:53:06', 4, 'survey_publish', 'Survey Running Notification ', 'Feedback Pending ', 'Only \"{left_question}\" Question left. \"{survey_title}\", Your Feedback is Pending.', '12:00:00'),
(6, '2018-08-15 00:53:53', 5, 'wheel_category', 'User Mention Notification', 'Mention', '{user_name}, Mention in a post.', '00:00:00'),
(7, '2018-12-19 06:13:14', 6, 'oneteamnotificaton', 'Itinerary update ', 'Itinerary Update', 'itinerary For \"{itinerary_title}\".', '00:00:00'),
(8, '2018-12-19 06:13:14', 7, 'feed pinned ', 'feed pinned ', 'Pinned Post Notification', 'Post by {user_name} just got pinned, Watch it now.', '00:00:00'),
(9, '2019-03-30 12:41:28', 8, 'new_text_message', 'New Text Message Notification', 'New Text Message Received', '\"{user_name}\" \"{text_message}\"', '00:00:00'),
(10, '2019-03-30 12:41:28', 9, 'new_attachment_message', 'New Attachment Message Notification', 'New Attachment Message Received1', '\"{user_name}\";\"{attachment_type}\"', '00:00:00'),
(11, '2019-04-01 13:51:26', 10, 'feed_text_post', 'Feed Post New Text Message', 'Feed Post New Text Message', 'New Feed posted by \"{user_name}\";\"{text_message}\", Watch it now.', '00:00:00'),
(12, '2019-04-01 13:51:26', 11, 'feed_attachment_post', 'Feed Post New Attachment Message', 'Feed Post New Attachment Message', 'New Feed posted by \"{user_name}\";\"{attachment_type}\", Watch it now.', '00:00:00'),
(13, '2019-04-01 13:52:23', 12, 'feed_like', 'Feed Like Notification', 'Feed Post Like', '{user_name} like your post.', '00:00:00'),
(14, '2019-04-01 13:52:23', 13, 'feed_comment', 'Feed Comment Notification', 'New Comment on Feed', '{user_name} comment on your post, {comment_message}.', '00:00:00'),
(15, '2019-04-30 09:38:46', 14, 'poll_reminder', 'Poll Reminder Notification', 'Reminder for Pending Poll', '{user_name}, Your input is awaited for \"{poll_title}\" Poll.', '00:00:00'),
(16, '2019-05-08 12:54:38', 15, 'poll_publish', 'Poll Publish Notification', 'Poll publish', '{poll_title}, company publish new Poll1.', '00:00:00'),
(17, '2020-04-23 16:53:03', 17, 'live_session_update', 'Live Session', 'Live Session', 'Live session for \"{livesession_title}\".', '00:00:00'),
(18, '2020-04-23 16:53:37', 18, 'livesession_before_reminder_notification', 'Reminder', 'Reminder', 'Reminder For \"{livesession_title}\".', '00:00:00'),
(19, '2020-04-23 16:54:02', 19, 'livesession_start_notification', 'Live Session Start Notification', 'Live Session Start Notification', '{user_name} has started {livesession_title} session', '00:00:00'),
(20, '2020-04-23 16:54:02', 20, 'livesession_join_notification', 'Live Session Join Notification', 'Live Session Join Notification', '{user_name} has joined {livesession_title} session.', '00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `oneteampolltheme`
--

CREATE TABLE `oneteampolltheme` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1-active, 2- deactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oneteampolltheme`
--

INSERT INTO `oneteampolltheme` (`id`, `created_at`, `title`, `layout_id`, `image`, `status`) VALUES
(2, '2019-05-06 12:06:04', 'layoutone', 1, '15610359123498699395d0b848889ca0.png', 1),
(3, '2019-05-06 12:47:35', 'layout two', 2, '15610335139837025235d0b7b299f638.png', 1),
(4, '2019-06-13 16:33:02', 'rating', 3, '15610335308900383205d0b7b3a7cfd8.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oneteamserverurl`
--

CREATE TABLE `oneteamserverurl` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `server_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oneteamserverurl`
--

INSERT INTO `oneteamserverurl` (`id`, `created_at`, `server_url`) VALUES
(1, '2019-01-16 07:34:22', 'http://localhost/appadmin3.6/mobile/');

-- --------------------------------------------------------

--
-- Table structure for table `oneteam_branding`
--

CREATE TABLE `oneteam_branding` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `image` varchar(255) COLLATE utf8_bin NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL COMMENT ' 1 - active, 2 - inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `oneteam_branding`
--

INSERT INTO `oneteam_branding` (`id`, `created_at`, `title`, `image`, `position`, `status`) VALUES
(1, '2018-07-26 19:50:37', 'first', 'midscreen-1.jpg', 0, 1),
(2, '2018-07-26 19:50:37', 'second', 'midscreen-2.jpg', 0, 1),
(4, '2018-07-26 19:52:19', 'four', 'midscreen-4.jpg', 0, 1),
(5, '2018-11-26 14:25:42', '', '154332633117531480155bfd4a7b1b2ed.jpg', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oneteam_buzzlive_active_users_count`
--

CREATE TABLE `oneteam_buzzlive_active_users_count` (
  `id` int(11) NOT NULL,
  `livestream_url_id` int(11) NOT NULL,
  `users_count` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `oneteam_buzzlive_active_users_list`
--

CREATE TABLE `oneteam_buzzlive_active_users_list` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `livestream_url_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `oneteam_buzzlive_join_users_count`
--

CREATE TABLE `oneteam_buzzlive_join_users_count` (
  `id` int(11) NOT NULL,
  `livestream_url_id` int(11) DEFAULT NULL,
  `users_count` int(11) DEFAULT NULL,
  `active_user_count` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oneteam_buzzlive_join_users_count`
--

INSERT INTO `oneteam_buzzlive_join_users_count` (`id`, `livestream_url_id`, `users_count`, `active_user_count`, `created_at`) VALUES
(1, 1, 919, 689, '2021-05-17 07:35:34');

-- --------------------------------------------------------

--
-- Table structure for table `oneteam_buzzlive_join_users_list`
--

CREATE TABLE `oneteam_buzzlive_join_users_list` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `livestream_url_id` int(11) DEFAULT NULL,
  `active_user_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oneteam_buzzlive_join_users_list`
--

INSERT INTO `oneteam_buzzlive_join_users_list` (`id`, `user_id`, `livestream_url_id`, `active_user_status`, `created_at`) VALUES
(39554, 51483, 1, 1, '2021-05-15 09:34:09'),
(39555, 12683, 1, 0, '2021-05-15 16:51:38'),
(39556, 12068, 1, 1, '2021-05-15 14:20:04'),
(39557, 14372, 1, 1, '2021-05-15 14:20:05'),
(39558, 52802, 1, 1, '2021-05-15 15:14:28'),
(39559, 29550, 1, 1, '2021-05-15 14:55:46'),
(39560, 52287, 1, 1, '2021-05-15 14:20:14'),
(39561, 52720, 1, 0, '2021-05-15 16:01:45'),
(39562, 13391, 1, 1, '2021-05-15 14:20:18'),
(39563, 30355, 1, 0, '2021-05-15 16:57:03'),
(39564, 52547, 1, 1, '2021-05-15 14:20:22'),
(39565, 52515, 1, 1, '2021-05-15 14:20:27'),
(39566, 49215, 1, 1, '2021-05-15 14:20:31'),
(39567, 52698, 1, 1, '2021-05-15 14:20:34'),
(39568, 11697, 1, 1, '2021-05-15 14:20:35'),
(39569, 30809, 1, 0, '2021-05-15 16:51:31'),
(39571, 28631, 1, 0, '2021-05-15 16:57:04'),
(39572, 13231, 1, 0, '2021-05-15 16:57:01'),
(39573, 13480, 1, 1, '2021-05-15 14:21:01'),
(39574, 13307, 1, 1, '2021-05-15 14:21:03'),
(39575, 9448, 1, 1, '2021-05-15 14:21:05'),
(39577, 28261, 1, 1, '2021-05-15 15:06:14'),
(39578, 51223, 1, 1, '2021-05-15 14:21:17'),
(39579, 13364, 1, 1, '2021-05-15 14:21:18'),
(39580, 9437, 1, 0, '2021-05-15 16:51:26'),
(39581, 31253, 1, 1, '2021-05-15 14:21:37'),
(39582, 11656, 1, 0, '2021-05-15 16:39:52'),
(39583, 51120, 1, 1, '2021-05-15 14:21:45'),
(39584, 52538, 1, 0, '2021-05-15 16:52:12'),
(39585, 31142, 1, 0, '2021-05-15 16:54:53'),
(39586, 9436, 1, 0, '2021-05-15 16:45:09'),
(39587, 9421, 1, 0, '2021-05-15 16:51:22'),
(39588, 9445, 1, 1, '2021-05-15 14:22:01'),
(39589, 11510, 1, 1, '2021-05-15 14:22:03'),
(39590, 46522, 1, 1, '2021-05-15 14:22:27'),
(39591, 51163, 1, 1, '2021-05-15 14:22:30'),
(39592, 11582, 1, 1, '2021-05-15 15:07:02'),
(39593, 52721, 1, 1, '2021-05-15 15:49:10'),
(39595, 11447, 1, 0, '2021-05-15 16:51:41'),
(39596, 24082, 1, 1, '2021-05-15 14:22:47'),
(39597, 52666, 1, 1, '2021-05-15 14:22:55'),
(39598, 50459, 1, 1, '2021-05-15 15:32:37'),
(39600, 44573, 1, 1, '2021-05-15 14:23:01'),
(39601, 11593, 1, 1, '2021-05-15 14:23:03'),
(39602, 50995, 1, 1, '2021-05-15 14:23:03'),
(39603, 29848, 1, 1, '2021-05-15 14:23:06'),
(39604, 14429, 1, 1, '2021-05-15 14:23:07'),
(39605, 52084, 1, 1, '2021-05-15 14:23:09'),
(39606, 52806, 1, 0, '2021-05-15 14:49:13'),
(39607, 9439, 1, 1, '2021-05-15 14:23:10'),
(39608, 52474, 1, 1, '2021-05-15 14:23:11'),
(39609, 50254, 1, 1, '2021-05-15 14:23:12'),
(39610, 11566, 1, 1, '2021-05-15 14:23:12'),
(39611, 52510, 1, 0, '2021-05-15 16:52:53'),
(39612, 52511, 1, 1, '2021-05-15 14:23:16'),
(39613, 44575, 1, 1, '2021-05-15 14:23:23'),
(39614, 50257, 1, 0, '2021-05-15 15:08:00'),
(39615, 11784, 1, 1, '2021-05-15 14:23:26'),
(39616, 14152, 1, 1, '2021-05-15 14:23:30'),
(39617, 12622, 1, 1, '2021-05-15 14:23:46'),
(39618, 12620, 1, 1, '2021-05-15 14:23:47'),
(39619, 11589, 1, 1, '2021-05-15 14:23:48'),
(39620, 11392, 1, 1, '2021-05-15 14:23:51'),
(39621, 13003, 1, 1, '2021-05-15 16:07:48'),
(39622, 13438, 1, 0, '2021-05-15 16:51:29'),
(39623, 51107, 1, 1, '2021-05-15 15:23:45'),
(39624, 13546, 1, 1, '2021-05-15 14:23:55'),
(39625, 31284, 1, 1, '2021-05-15 14:23:57'),
(39626, 52507, 1, 0, '2021-05-15 15:43:16'),
(39627, 11875, 1, 1, '2021-05-15 14:24:10'),
(39628, 49653, 1, 1, '2021-05-15 14:24:17'),
(39629, 14040, 1, 0, '2021-05-15 16:10:16'),
(39630, 51433, 1, 0, '2021-05-15 14:24:46'),
(39631, 14528, 1, 0, '2021-05-15 15:28:53'),
(39632, 12318, 1, 1, '2021-05-15 14:24:27'),
(39633, 52614, 1, 0, '2021-05-15 15:53:34'),
(39634, 51015, 1, 1, '2021-05-15 14:24:44'),
(39635, 51899, 1, 1, '2021-05-15 14:24:46'),
(39636, 28976, 1, 1, '2021-05-15 14:24:49'),
(39637, 12148, 1, 1, '2021-05-15 14:24:49'),
(39638, 52083, 1, 1, '2021-05-15 14:24:50'),
(39639, 29169, 1, 1, '2021-05-15 14:24:51'),
(39640, 12440, 1, 1, '2021-05-15 15:57:23'),
(39641, 13432, 1, 1, '2021-05-15 14:24:54'),
(39642, 31789, 1, 1, '2021-05-15 14:24:54'),
(39643, 11755, 1, 0, '2021-05-15 16:53:14'),
(39644, 12495, 1, 1, '2021-05-15 15:23:49'),
(39645, 28341, 1, 0, '2021-05-15 16:38:28'),
(39646, 28295, 1, 0, '2021-05-15 16:18:34'),
(39647, 13016, 1, 1, '2021-05-15 14:25:01'),
(39648, 24096, 1, 1, '2021-05-15 14:25:02'),
(39649, 19481, 1, 1, '2021-05-15 14:25:02'),
(39650, 11920, 1, 1, '2021-05-15 14:25:06'),
(39651, 14717, 1, 1, '2021-05-15 14:25:15'),
(39652, 52100, 1, 1, '2021-05-15 14:25:19'),
(39654, 13397, 1, 0, '2021-05-15 17:13:16'),
(39655, 52767, 1, 0, '2021-05-15 16:56:59'),
(39656, 28228, 1, 0, '2021-05-15 16:52:20'),
(39657, 51760, 1, 1, '2021-05-15 14:25:23'),
(39658, 34209, 1, 0, '2021-05-15 16:56:19'),
(39659, 50725, 1, 1, '2021-05-15 14:25:55'),
(39660, 24065, 1, 1, '2021-05-15 14:25:56'),
(39661, 44935, 1, 0, '2021-05-15 15:57:33'),
(39662, 51476, 1, 1, '2021-05-15 15:11:05'),
(39663, 12348, 1, 0, '2021-05-15 16:21:44'),
(39664, 31680, 1, 1, '2021-05-15 14:26:03'),
(39665, 12208, 1, 0, '2021-05-15 16:57:01'),
(39666, 29372, 1, 0, '2021-05-15 15:22:22'),
(39667, 50684, 1, 1, '2021-05-15 15:01:36'),
(39668, 11492, 1, 0, '2021-05-15 15:15:31'),
(39669, 13667, 1, 1, '2021-05-15 14:26:16'),
(39670, 49239, 1, 1, '2021-05-15 14:26:18'),
(39671, 52373, 1, 1, '2021-05-15 14:43:49'),
(39672, 14539, 1, 1, '2021-05-15 14:27:25'),
(39673, 11799, 1, 1, '2021-05-15 14:26:27'),
(39674, 31244, 1, 1, '2021-05-15 14:26:27'),
(39675, 17608, 1, 1, '2021-05-15 15:38:39'),
(39676, 31256, 1, 0, '2021-05-15 16:51:19'),
(39677, 12677, 1, 1, '2021-05-15 14:26:37'),
(39678, 52573, 1, 1, '2021-05-15 14:26:45'),
(39680, 50948, 1, 1, '2021-05-15 14:26:58'),
(39681, 11844, 1, 1, '2021-05-15 14:26:59'),
(39682, 30648, 1, 1, '2021-05-15 14:27:00'),
(39683, 12689, 1, 0, '2021-05-15 15:21:58'),
(39684, 29056, 1, 1, '2021-05-15 14:27:03'),
(39685, 52638, 1, 0, '2021-05-15 15:34:29'),
(39686, 12152, 1, 1, '2021-05-15 14:27:10'),
(39687, 30575, 1, 0, '2021-05-15 16:48:45'),
(39689, 14285, 1, 1, '2021-05-15 14:27:15'),
(39690, 52468, 1, 1, '2021-05-15 14:27:16'),
(39691, 51016, 1, 1, '2021-05-15 14:27:16'),
(39692, 51225, 1, 1, '2021-05-15 14:27:19'),
(39693, 44632, 1, 1, '2021-05-15 14:27:21'),
(39694, 52762, 1, 0, '2021-05-15 16:51:16'),
(39695, 51994, 1, 1, '2021-05-15 14:27:21'),
(39697, 31631, 1, 0, '2021-05-15 16:52:23'),
(39698, 13595, 1, 1, '2021-05-15 14:27:28'),
(39699, 11795, 1, 1, '2021-05-15 14:27:29'),
(39700, 28883, 1, 1, '2021-05-15 14:27:29'),
(39701, 28944, 1, 1, '2021-05-15 14:27:36'),
(39702, 14547, 1, 1, '2021-05-15 14:27:41'),
(39703, 12301, 1, 1, '2021-05-15 15:24:02'),
(39705, 13688, 1, 1, '2021-05-15 14:52:29'),
(39706, 44781, 1, 1, '2021-05-15 14:27:59'),
(39708, 14178, 1, 1, '2021-05-15 14:28:07'),
(39709, 9431, 1, 1, '2021-05-15 14:28:08'),
(39710, 51412, 1, 1, '2021-05-15 14:28:11'),
(39711, 13645, 1, 1, '2021-05-15 15:13:13'),
(39712, 52461, 1, 1, '2021-05-15 15:07:25'),
(39713, 52492, 1, 1, '2021-05-15 14:28:13'),
(39714, 11498, 1, 1, '2021-05-15 14:28:14'),
(39715, 13878, 1, 1, '2021-05-15 15:26:01'),
(39716, 44650, 1, 1, '2021-05-15 14:28:17'),
(39717, 33212, 1, 0, '2021-05-15 15:12:23'),
(39718, 31721, 1, 1, '2021-05-15 14:28:18'),
(39719, 14591, 1, 1, '2021-05-15 14:28:19'),
(39720, 12069, 1, 1, '2021-05-15 14:28:19'),
(39721, 12190, 1, 1, '2021-05-15 14:28:22'),
(39722, 44742, 1, 1, '2021-05-15 14:28:23'),
(39723, 12133, 1, 0, '2021-05-15 15:02:11'),
(39724, 28506, 1, 1, '2021-05-15 14:28:29'),
(39725, 28467, 1, 0, '2021-05-15 14:57:23'),
(39727, 29617, 1, 0, '2021-05-15 16:25:21'),
(39728, 13980, 1, 0, '2021-05-15 16:13:17'),
(39729, 51288, 1, 1, '2021-05-15 15:13:32'),
(39730, 52590, 1, 1, '2021-05-15 16:27:30'),
(39731, 31339, 1, 1, '2021-05-15 14:28:44'),
(39732, 50766, 1, 1, '2021-05-15 14:28:47'),
(39733, 11872, 1, 1, '2021-05-15 14:28:51'),
(39734, 30160, 1, 1, '2021-05-15 14:28:54'),
(39735, 52052, 1, 1, '2021-05-15 15:08:57'),
(39736, 52680, 1, 1, '2021-05-15 14:28:57'),
(39737, 13413, 1, 1, '2021-05-15 15:37:13'),
(39738, 52482, 1, 0, '2021-05-15 14:31:13'),
(39739, 11966, 1, 0, '2021-05-15 16:51:25'),
(39740, 9416, 1, 1, '2021-05-15 14:29:01'),
(39741, 14574, 1, 1, '2021-05-15 14:29:01'),
(39742, 31147, 1, 1, '2021-05-15 14:29:02'),
(39743, 51022, 1, 1, '2021-05-15 14:29:04'),
(39744, 51199, 1, 1, '2021-05-15 16:04:25'),
(39745, 50975, 1, 1, '2021-05-15 15:59:11'),
(39746, 28554, 1, 0, '2021-05-15 16:25:56'),
(39747, 11907, 1, 0, '2021-05-15 16:51:19'),
(39748, 33310, 1, 1, '2021-05-15 14:29:19'),
(39749, 29643, 1, 1, '2021-05-15 14:29:20'),
(39750, 44437, 1, 1, '2021-05-15 14:29:25'),
(39751, 12244, 1, 1, '2021-05-15 14:29:29'),
(39752, 28517, 1, 1, '2021-05-15 15:14:29'),
(39753, 19470, 1, 1, '2021-05-15 14:29:39'),
(39754, 52016, 1, 0, '2021-05-15 16:48:24'),
(39755, 52763, 1, 1, '2021-05-15 14:29:42'),
(39756, 52098, 1, 0, '2021-05-15 14:50:53'),
(39757, 44881, 1, 1, '2021-05-15 14:29:47'),
(39758, 12561, 1, 0, '2021-05-15 16:51:17'),
(39760, 28396, 1, 1, '2021-05-15 14:29:48'),
(39761, 50322, 1, 1, '2021-05-15 15:15:47'),
(39762, 30370, 1, 1, '2021-05-15 14:29:51'),
(39763, 50993, 1, 1, '2021-05-15 14:29:52'),
(39764, 9434, 1, 1, '2021-05-15 14:29:53'),
(39765, 51698, 1, 1, '2021-05-15 14:29:53'),
(39766, 51839, 1, 1, '2021-05-15 14:29:54'),
(39767, 28982, 1, 1, '2021-05-15 14:29:54'),
(39768, 31083, 1, 1, '2021-05-15 15:15:10'),
(39769, 12626, 1, 1, '2021-05-15 15:15:00'),
(39770, 52509, 1, 1, '2021-05-15 14:29:58'),
(39771, 12119, 1, 1, '2021-05-15 14:30:02'),
(39772, 31611, 1, 1, '2021-05-15 14:30:02'),
(39773, 16036, 1, 0, '2021-05-15 15:41:27'),
(39774, 21508, 1, 1, '2021-05-15 14:30:08'),
(39775, 30931, 1, 1, '2021-05-15 14:30:12'),
(39776, 11724, 1, 1, '2021-05-15 14:30:12'),
(39777, 52584, 1, 1, '2021-05-15 14:30:12'),
(39778, 13654, 1, 1, '2021-05-15 14:30:13'),
(39779, 13157, 1, 1, '2021-05-15 14:30:13'),
(39780, 9427, 1, 0, '2021-05-15 16:51:25'),
(39781, 33162, 1, 1, '2021-05-15 14:30:15'),
(39782, 29925, 1, 0, '2021-05-15 15:53:21'),
(39783, 52589, 1, 1, '2021-05-15 14:30:18'),
(39784, 44477, 1, 1, '2021-05-15 14:30:20'),
(39785, 11507, 1, 0, '2021-05-15 15:31:03'),
(39787, 24064, 1, 0, '2021-05-15 16:56:58'),
(39788, 28479, 1, 0, '2021-05-15 16:56:58'),
(39789, 13418, 1, 1, '2021-05-15 14:30:22'),
(39790, 52536, 1, 1, '2021-05-15 16:51:59'),
(39791, 14637, 1, 1, '2021-05-15 14:30:28'),
(39792, 31285, 1, 1, '2021-05-15 14:30:29'),
(39794, 16590, 1, 1, '2021-05-15 14:30:30'),
(39795, 14403, 1, 1, '2021-05-15 14:30:30'),
(39796, 13652, 1, 0, '2021-05-15 16:51:24'),
(39797, 12847, 1, 1, '2021-05-15 14:30:34'),
(39798, 30361, 1, 1, '2021-05-15 14:30:35'),
(39799, 11787, 1, 1, '2021-05-15 14:30:35'),
(39800, 52068, 1, 1, '2021-05-15 14:30:39'),
(39801, 12410, 1, 0, '2021-05-15 15:14:58'),
(39802, 12176, 1, 1, '2021-05-15 14:30:41'),
(39803, 28516, 1, 1, '2021-05-15 14:30:41'),
(39804, 51822, 1, 1, '2021-05-15 14:30:42'),
(39806, 14123, 1, 0, '2021-05-15 16:52:01'),
(39807, 52490, 1, 1, '2021-05-15 15:28:52'),
(39808, 31571, 1, 1, '2021-05-15 14:30:43'),
(39809, 13575, 1, 1, '2021-05-15 14:30:44'),
(39811, 52718, 1, 1, '2021-05-15 14:30:47'),
(39812, 52712, 1, 1, '2021-05-15 14:30:50'),
(39813, 13763, 1, 1, '2021-05-15 14:30:50'),
(39814, 33367, 1, 1, '2021-05-15 15:48:33'),
(39815, 14609, 1, 1, '2021-05-15 14:30:52'),
(39816, 52648, 1, 1, '2021-05-15 14:30:55'),
(39817, 13428, 1, 0, '2021-05-15 14:49:37'),
(39818, 13875, 1, 1, '2021-05-15 15:15:42'),
(39819, 13182, 1, 1, '2021-05-15 14:30:57'),
(39820, 12079, 1, 0, '2021-05-15 16:51:27'),
(39821, 51379, 1, 1, '2021-05-15 14:30:58'),
(39822, 14100, 1, 1, '2021-05-15 14:30:59'),
(39823, 12441, 1, 0, '2021-05-15 16:51:43'),
(39824, 29161, 1, 1, '2021-05-15 14:31:02'),
(39825, 19403, 1, 1, '2021-05-15 14:31:02'),
(39826, 31309, 1, 1, '2021-05-15 14:31:07'),
(39827, 16864, 1, 1, '2021-05-15 14:31:07'),
(39828, 52504, 1, 0, '2021-05-15 16:52:53'),
(39829, 9471, 1, 1, '2021-05-15 14:31:10'),
(39830, 51202, 1, 1, '2021-05-15 14:31:12'),
(39831, 12136, 1, 1, '2021-05-15 15:03:03'),
(39832, 11382, 1, 0, '2021-05-15 16:14:27'),
(39833, 51250, 1, 1, '2021-05-15 14:31:17'),
(39834, 12201, 1, 1, '2021-05-15 15:15:57'),
(39835, 44391, 1, 1, '2021-05-15 14:31:19'),
(39836, 14635, 1, 0, '2021-05-15 14:32:35'),
(39837, 12081, 1, 1, '2021-05-15 14:31:20'),
(39838, 49742, 1, 1, '2021-05-15 14:31:20'),
(39839, 31395, 1, 1, '2021-05-15 15:00:47'),
(39840, 30473, 1, 1, '2021-05-15 14:31:25'),
(39841, 52663, 1, 1, '2021-05-15 14:31:28'),
(39842, 31157, 1, 1, '2021-05-15 14:46:33'),
(39843, 29808, 1, 1, '2021-05-15 14:31:29'),
(39846, 28955, 1, 1, '2021-05-15 14:31:31'),
(39847, 12317, 1, 1, '2021-05-15 14:31:32'),
(39848, 12725, 1, 0, '2021-05-15 15:32:09'),
(39849, 11594, 1, 0, '2021-05-15 16:51:25'),
(39850, 29318, 1, 1, '2021-05-15 14:31:35'),
(39851, 52359, 1, 1, '2021-05-15 14:31:35'),
(39852, 49428, 1, 1, '2021-05-15 15:41:50'),
(39855, 22581, 1, 0, '2021-05-15 16:51:22'),
(39856, 19192, 1, 1, '2021-05-15 14:31:38'),
(39857, 50698, 1, 0, '2021-05-15 15:27:33'),
(39858, 12000, 1, 1, '2021-05-15 14:31:40'),
(39859, 52580, 1, 1, '2021-05-15 14:31:40'),
(39860, 33054, 1, 0, '2021-05-15 15:54:26'),
(39861, 28355, 1, 0, '2021-05-15 15:26:33'),
(39862, 11790, 1, 1, '2021-05-15 14:31:43'),
(39863, 52559, 1, 1, '2021-05-15 15:16:30'),
(39864, 13256, 1, 0, '2021-05-15 16:20:58'),
(39865, 51205, 1, 0, '2021-05-15 15:16:17'),
(39866, 12454, 1, 1, '2021-05-15 15:20:23'),
(39867, 52387, 1, 1, '2021-05-15 14:31:47'),
(39868, 51782, 1, 1, '2021-05-15 15:17:47'),
(39869, 28751, 1, 0, '2021-05-15 14:58:49'),
(39870, 14688, 1, 1, '2021-05-15 15:16:36'),
(39871, 51857, 1, 1, '2021-05-15 14:31:56'),
(39872, 52685, 1, 1, '2021-05-15 14:31:59'),
(39873, 52065, 1, 1, '2021-05-15 14:32:00'),
(39875, 12009, 1, 1, '2021-05-15 15:13:50'),
(39876, 51160, 1, 1, '2021-05-15 14:32:01'),
(39877, 12324, 1, 0, '2021-05-15 16:51:53'),
(39878, 52556, 1, 1, '2021-05-15 14:32:03'),
(39879, 31193, 1, 1, '2021-05-15 14:32:03'),
(39880, 11809, 1, 0, '2021-05-15 16:11:09'),
(39881, 51730, 1, 1, '2021-05-15 14:32:04'),
(39882, 52750, 1, 0, '2021-05-15 15:38:07'),
(39883, 11633, 1, 0, '2021-05-15 16:49:08'),
(39884, 28275, 1, 0, '2021-05-15 16:51:38'),
(39885, 52294, 1, 0, '2021-05-15 16:51:39'),
(39886, 30056, 1, 1, '2021-05-15 14:32:09'),
(39887, 20236, 1, 1, '2021-05-15 14:32:13'),
(39888, 29048, 1, 1, '2021-05-15 14:32:14'),
(39889, 9446, 1, 1, '2021-05-15 14:32:16'),
(39890, 12032, 1, 0, '2021-05-15 15:47:13'),
(39891, 31270, 1, 0, '2021-05-15 16:22:52'),
(39892, 51037, 1, 1, '2021-05-15 14:32:16'),
(39894, 50584, 1, 1, '2021-05-15 14:32:19'),
(39896, 31534, 1, 1, '2021-05-15 15:17:40'),
(39897, 14299, 1, 1, '2021-05-15 14:32:21'),
(39899, 13171, 1, 1, '2021-05-15 14:32:22'),
(39900, 31780, 1, 1, '2021-05-15 15:27:57'),
(39901, 52382, 1, 1, '2021-05-15 14:32:24'),
(39902, 19742, 1, 1, '2021-05-15 14:32:26'),
(39903, 44496, 1, 1, '2021-05-15 14:32:27'),
(39904, 24107, 1, 1, '2021-05-15 14:32:28'),
(39905, 52810, 1, 1, '2021-05-15 14:32:28'),
(39906, 13022, 1, 0, '2021-05-15 15:02:43'),
(39907, 52744, 1, 1, '2021-05-15 14:32:29'),
(39908, 12850, 1, 1, '2021-05-15 14:32:30'),
(39909, 52716, 1, 1, '2021-05-15 14:32:30'),
(39910, 14128, 1, 1, '2021-05-15 14:32:34'),
(39911, 14745, 1, 0, '2021-05-15 15:21:31'),
(39912, 50389, 1, 1, '2021-05-15 14:32:37'),
(39913, 52089, 1, 0, '2021-05-15 16:32:56'),
(39914, 51315, 1, 0, '2021-05-15 16:20:33'),
(39915, 52664, 1, 0, '2021-05-15 16:51:53'),
(39916, 14438, 1, 0, '2021-05-15 15:27:24'),
(39917, 52518, 1, 1, '2021-05-15 14:32:42'),
(39918, 49573, 1, 1, '2021-05-15 14:32:43'),
(39919, 31206, 1, 0, '2021-05-15 14:58:50'),
(39920, 46358, 1, 1, '2021-05-15 14:32:45'),
(39921, 11465, 1, 0, '2021-05-15 16:11:16'),
(39923, 51718, 1, 1, '2021-05-15 15:17:51'),
(39925, 51708, 1, 1, '2021-05-15 14:32:51'),
(39927, 32686, 1, 0, '2021-05-15 16:07:44'),
(39928, 14069, 1, 1, '2021-05-15 14:32:53'),
(39929, 52734, 1, 1, '2021-05-15 14:32:55'),
(39930, 52813, 1, 1, '2021-05-15 14:32:55'),
(39931, 52181, 1, 1, '2021-05-15 14:32:55'),
(39932, 52311, 1, 1, '2021-05-15 15:19:25'),
(39933, 30322, 1, 1, '2021-05-15 14:32:58'),
(39934, 50786, 1, 1, '2021-05-15 14:33:00'),
(39935, 29016, 1, 1, '2021-05-15 14:33:00'),
(39936, 52402, 1, 0, '2021-05-15 15:19:11'),
(39937, 52202, 1, 1, '2021-05-15 14:33:03'),
(39938, 28493, 1, 0, '2021-05-15 16:29:04'),
(39939, 13079, 1, 0, '2021-05-15 16:56:41'),
(39940, 30524, 1, 1, '2021-05-15 14:33:08'),
(39943, 9441, 1, 1, '2021-05-15 14:33:11'),
(39944, 11706, 1, 1, '2021-05-15 14:33:12'),
(39945, 28986, 1, 0, '2021-05-15 16:51:29'),
(39947, 28757, 1, 1, '2021-05-15 14:33:16'),
(39948, 30514, 1, 1, '2021-05-15 14:33:17'),
(39949, 49716, 1, 1, '2021-05-15 14:33:17'),
(39950, 14167, 1, 0, '2021-05-15 15:17:51'),
(39951, 12001, 1, 0, '2021-05-15 16:41:11'),
(39952, 30785, 1, 1, '2021-05-15 14:33:21'),
(39953, 13653, 1, 1, '2021-05-15 14:38:50'),
(39954, 11925, 1, 1, '2021-05-15 14:33:26'),
(39955, 14576, 1, 0, '2021-05-15 16:51:28'),
(39956, 28409, 1, 1, '2021-05-15 14:33:32'),
(39957, 44554, 1, 1, '2021-05-15 14:33:34'),
(39958, 50541, 1, 1, '2021-05-15 14:33:35'),
(39959, 11845, 1, 0, '2021-05-15 16:24:21'),
(39960, 13746, 1, 1, '2021-05-15 14:33:39'),
(39961, 11494, 1, 1, '2021-05-15 14:33:40'),
(39962, 13476, 1, 0, '2021-05-15 16:10:05'),
(39963, 12071, 1, 1, '2021-05-15 14:33:44'),
(39964, 52368, 1, 0, '2021-05-15 14:58:24'),
(39965, 52496, 1, 1, '2021-05-15 14:33:53'),
(39966, 14475, 1, 1, '2021-05-15 14:33:55'),
(39967, 12052, 1, 0, '2021-05-15 15:18:31'),
(39968, 51061, 1, 0, '2021-05-15 16:14:30'),
(39969, 13120, 1, 1, '2021-05-15 14:33:57'),
(39970, 52723, 1, 1, '2021-05-15 14:33:59'),
(39971, 12078, 1, 0, '2021-05-15 17:00:53'),
(39972, 33145, 1, 0, '2021-05-15 14:46:44'),
(39973, 12165, 1, 0, '2021-05-15 14:49:05'),
(39974, 49617, 1, 1, '2021-05-15 14:34:04'),
(39975, 12336, 1, 1, '2021-05-15 14:34:04'),
(39976, 34939, 1, 1, '2021-05-15 14:34:05'),
(39977, 31310, 1, 0, '2021-05-15 16:56:58'),
(39978, 11537, 1, 0, '2021-05-15 16:51:27'),
(39979, 11695, 1, 0, '2021-05-15 16:53:13'),
(39980, 12126, 1, 0, '2021-05-15 16:54:13'),
(39981, 52671, 1, 1, '2021-05-15 14:34:10'),
(39982, 9465, 1, 1, '2021-05-15 14:34:10'),
(39983, 17613, 1, 0, '2021-05-15 16:56:59'),
(39984, 29051, 1, 0, '2021-05-15 15:44:05'),
(39985, 28669, 1, 0, '2021-05-15 15:49:37'),
(39986, 50947, 1, 1, '2021-05-15 14:34:19'),
(39987, 12074, 1, 1, '2021-05-15 14:34:20'),
(39988, 13168, 1, 1, '2021-05-15 17:13:35'),
(39991, 29369, 1, 0, '2021-05-15 16:51:24'),
(39992, 50784, 1, 1, '2021-05-15 15:22:08'),
(39993, 11679, 1, 0, '2021-05-15 16:35:35'),
(39994, 30491, 1, 1, '2021-05-15 14:34:29'),
(39995, 13265, 1, 1, '2021-05-15 14:34:33'),
(39996, 52787, 1, 1, '2021-05-15 14:37:25'),
(39997, 44942, 1, 1, '2021-05-15 14:34:33'),
(39998, 51201, 1, 1, '2021-05-15 14:34:36'),
(40000, 11655, 1, 0, '2021-05-15 16:51:34'),
(40001, 52292, 1, 1, '2021-05-15 14:34:39'),
(40002, 52512, 1, 0, '2021-05-15 15:29:58'),
(40003, 52304, 1, 1, '2021-05-15 14:34:40'),
(40004, 52401, 1, 0, '2021-05-15 16:42:03'),
(40005, 12053, 1, 1, '2021-05-15 14:34:44'),
(40006, 17792, 1, 1, '2021-05-15 14:34:45'),
(40007, 30331, 1, 1, '2021-05-15 14:34:47'),
(40008, 31573, 1, 1, '2021-05-15 14:34:49'),
(40009, 52517, 1, 1, '2021-05-15 14:34:55'),
(40010, 49536, 1, 1, '2021-05-15 14:34:57'),
(40011, 29246, 1, 1, '2021-05-15 15:19:26'),
(40012, 44677, 1, 0, '2021-05-15 16:19:43'),
(40013, 30252, 1, 1, '2021-05-15 14:35:02'),
(40014, 29962, 1, 1, '2021-05-15 14:35:06'),
(40015, 28630, 1, 1, '2021-05-15 14:35:09'),
(40016, 11831, 1, 1, '2021-05-15 14:35:10'),
(40017, 50886, 1, 1, '2021-05-15 15:20:24'),
(40018, 24181, 1, 1, '2021-05-15 14:35:14'),
(40019, 31937, 1, 0, '2021-05-15 15:50:59'),
(40020, 12397, 1, 1, '2021-05-15 14:35:17'),
(40021, 52728, 1, 1, '2021-05-15 14:35:18'),
(40022, 14707, 1, 0, '2021-05-15 16:52:39'),
(40023, 11416, 1, 1, '2021-05-15 14:35:19'),
(40024, 31313, 1, 1, '2021-05-15 14:35:21'),
(40025, 11807, 1, 1, '2021-05-15 14:35:24'),
(40026, 44643, 1, 1, '2021-05-15 14:35:25'),
(40027, 52783, 1, 1, '2021-05-15 15:31:31'),
(40028, 13393, 1, 1, '2021-05-15 14:35:27'),
(40029, 24072, 1, 1, '2021-05-15 14:35:28'),
(40031, 29044, 1, 0, '2021-05-15 16:51:54'),
(40032, 51456, 1, 1, '2021-05-15 15:17:14'),
(40033, 13110, 1, 1, '2021-05-15 14:35:34'),
(40034, 12057, 1, 1, '2021-05-15 14:35:36'),
(40035, 50708, 1, 1, '2021-05-15 14:35:37'),
(40039, 51385, 1, 0, '2021-05-15 16:26:40'),
(40040, 31532, 1, 1, '2021-05-15 14:35:49'),
(40042, 52503, 1, 1, '2021-05-15 14:35:54'),
(40043, 49486, 1, 0, '2021-05-15 15:20:57'),
(40044, 31545, 1, 1, '2021-05-15 14:35:56'),
(40045, 11591, 1, 1, '2021-05-15 14:35:57'),
(40046, 24076, 1, 1, '2021-05-15 14:35:58'),
(40047, 51724, 1, 1, '2021-05-15 14:35:58'),
(40048, 50982, 1, 0, '2021-05-15 16:51:25'),
(40049, 12671, 1, 1, '2021-05-15 14:36:02'),
(40050, 44815, 1, 0, '2021-05-15 16:56:23'),
(40053, 12280, 1, 1, '2021-05-15 14:36:06'),
(40054, 51662, 1, 0, '2021-05-15 16:22:08'),
(40055, 29105, 1, 1, '2021-05-15 14:36:08'),
(40056, 9407, 1, 0, '2021-05-15 15:20:41'),
(40057, 44740, 1, 1, '2021-05-15 14:36:11'),
(40058, 13219, 1, 1, '2021-05-15 14:36:11'),
(40059, 14825, 1, 1, '2021-05-15 14:36:13'),
(40060, 50985, 1, 1, '2021-05-15 14:36:17'),
(40061, 14281, 1, 1, '2021-05-15 14:36:15'),
(40062, 12521, 1, 1, '2021-05-15 14:36:15'),
(40063, 52475, 1, 0, '2021-05-15 16:08:43'),
(40064, 52124, 1, 0, '2021-05-15 14:56:07'),
(40066, 13320, 1, 0, '2021-05-15 16:52:06'),
(40067, 30466, 1, 1, '2021-05-15 14:36:19'),
(40068, 44384, 1, 1, '2021-05-15 14:36:20'),
(40069, 31585, 1, 1, '2021-05-15 14:36:20'),
(40070, 50623, 1, 0, '2021-05-15 16:51:49'),
(40071, 51609, 1, 1, '2021-05-15 14:36:22'),
(40072, 13247, 1, 1, '2021-05-15 14:36:23'),
(40073, 13697, 1, 1, '2021-05-15 14:36:24'),
(40074, 31275, 1, 1, '2021-05-15 14:36:25'),
(40075, 52066, 1, 1, '2021-05-15 14:36:26'),
(40076, 52028, 1, 1, '2021-05-15 14:36:27'),
(40077, 13586, 1, 0, '2021-05-15 15:21:25'),
(40079, 31771, 1, 1, '2021-05-15 14:36:33'),
(40081, 44683, 1, 0, '2021-05-15 15:27:05'),
(40082, 13500, 1, 1, '2021-05-15 14:36:37'),
(40083, 30718, 1, 1, '2021-05-15 14:36:40'),
(40084, 13049, 1, 1, '2021-05-15 15:24:22'),
(40085, 13701, 1, 1, '2021-05-15 14:36:46'),
(40086, 49810, 1, 1, '2021-05-15 14:36:46'),
(40088, 13207, 1, 1, '2021-05-15 14:36:48'),
(40089, 51561, 1, 1, '2021-05-15 14:36:52'),
(40090, 52593, 1, 1, '2021-05-15 14:36:54'),
(40091, 52506, 1, 1, '2021-05-15 14:36:55'),
(40092, 12405, 1, 1, '2021-05-15 14:36:58'),
(40093, 29768, 1, 1, '2021-05-15 14:36:59'),
(40094, 51641, 1, 1, '2021-05-15 14:37:00'),
(40095, 28410, 1, 1, '2021-05-15 14:37:01'),
(40096, 52452, 1, 1, '2021-05-15 14:37:02'),
(40097, 14353, 1, 1, '2021-05-15 14:37:03'),
(40098, 29365, 1, 1, '2021-05-15 14:37:07'),
(40099, 12061, 1, 1, '2021-05-15 14:37:07'),
(40100, 29119, 1, 1, '2021-05-15 15:22:18'),
(40101, 11980, 1, 1, '2021-05-15 14:37:10'),
(40103, 52494, 1, 1, '2021-05-15 14:37:15'),
(40105, 30752, 1, 0, '2021-05-15 15:12:32'),
(40107, 28234, 1, 1, '2021-05-15 14:39:21'),
(40108, 50765, 1, 1, '2021-05-15 14:37:24'),
(40110, 14803, 1, 1, '2021-05-15 14:37:29'),
(40111, 52717, 1, 0, '2021-05-15 16:53:37'),
(40112, 12142, 1, 0, '2021-05-15 16:52:41'),
(40113, 31624, 1, 1, '2021-05-15 14:38:20'),
(40114, 51950, 1, 1, '2021-05-15 14:37:32'),
(40115, 50191, 1, 1, '2021-05-15 14:37:33'),
(40116, 11383, 1, 1, '2021-05-15 14:37:35'),
(40117, 12313, 1, 1, '2021-05-15 14:37:37'),
(40118, 49205, 1, 0, '2021-05-15 15:15:26'),
(40119, 32988, 1, 1, '2021-05-15 14:48:47'),
(40120, 44681, 1, 0, '2021-05-15 15:56:17'),
(40121, 13072, 1, 1, '2021-05-15 14:37:52'),
(40122, 28875, 1, 0, '2021-05-15 15:42:32'),
(40123, 9418, 1, 0, '2021-05-15 16:36:59'),
(40124, 29778, 1, 1, '2021-05-15 14:37:56'),
(40125, 29013, 1, 1, '2021-05-15 14:37:57'),
(40126, 44627, 1, 1, '2021-05-15 14:37:58'),
(40127, 52391, 1, 1, '2021-05-15 14:38:07'),
(40128, 50796, 1, 1, '2021-05-15 15:24:57'),
(40129, 51912, 1, 0, '2021-05-15 16:51:41'),
(40130, 51444, 1, 1, '2021-05-15 14:38:09'),
(40131, 50863, 1, 1, '2021-05-15 14:38:10'),
(40133, 13705, 1, 1, '2021-05-15 14:38:14'),
(40134, 52542, 1, 1, '2021-05-15 14:38:16'),
(40135, 50324, 1, 1, '2021-05-15 14:38:18'),
(40138, 52516, 1, 1, '2021-05-15 14:38:27'),
(40139, 44372, 1, 1, '2021-05-15 14:38:27'),
(40140, 50942, 1, 1, '2021-05-15 14:38:28'),
(40141, 33163, 1, 1, '2021-05-15 14:38:29'),
(40143, 30206, 1, 1, '2021-05-15 14:38:33'),
(40144, 51080, 1, 0, '2021-05-15 16:51:24'),
(40145, 51526, 1, 1, '2021-05-15 14:38:36'),
(40146, 13197, 1, 1, '2021-05-15 14:38:45'),
(40147, 9450, 1, 1, '2021-05-15 14:38:45'),
(40148, 49462, 1, 0, '2021-05-15 16:57:00'),
(40151, 28718, 1, 0, '2021-05-15 15:24:59'),
(40152, 44639, 1, 1, '2021-05-15 14:38:53'),
(40153, 11922, 1, 1, '2021-05-15 14:38:55'),
(40154, 13030, 1, 1, '2021-05-15 14:38:56'),
(40156, 52493, 1, 0, '2021-05-15 16:15:13'),
(40158, 11526, 1, 1, '2021-05-15 14:39:07'),
(40159, 52784, 1, 0, '2021-05-15 15:00:13'),
(40160, 31164, 1, 1, '2021-05-15 14:39:09'),
(40161, 51286, 1, 1, '2021-05-15 15:23:38'),
(40162, 9426, 1, 1, '2021-05-15 14:39:10'),
(40164, 13187, 1, 1, '2021-05-15 14:39:13'),
(40166, 14202, 1, 1, '2021-05-15 14:39:16'),
(40167, 14571, 1, 1, '2021-05-15 14:39:17'),
(40168, 50301, 1, 1, '2021-05-15 15:24:35'),
(40169, 31542, 1, 1, '2021-05-15 14:39:18'),
(40171, 52198, 1, 1, '2021-05-15 14:39:21'),
(40172, 16570, 1, 0, '2021-05-15 14:53:27'),
(40173, 11406, 1, 0, '2021-05-15 15:24:00'),
(40174, 52540, 1, 1, '2021-05-15 14:39:26'),
(40175, 15924, 1, 1, '2021-05-15 14:39:28'),
(40176, 50537, 1, 0, '2021-05-15 15:24:05'),
(40177, 52325, 1, 0, '2021-05-15 15:08:07'),
(40178, 12601, 1, 1, '2021-05-15 14:39:35'),
(40179, 12443, 1, 1, '2021-05-15 14:39:39'),
(40180, 11970, 1, 0, '2021-05-15 16:54:49'),
(40181, 30304, 1, 1, '2021-05-15 14:39:40'),
(40183, 17604, 1, 0, '2021-05-15 14:45:14'),
(40184, 12545, 1, 1, '2021-05-15 14:39:50'),
(40185, 11645, 1, 1, '2021-05-15 14:39:52'),
(40187, 30502, 1, 1, '2021-05-15 14:39:58'),
(40189, 51336, 1, 1, '2021-05-15 14:39:59'),
(40190, 13446, 1, 1, '2021-05-15 14:40:05'),
(40191, 29652, 1, 0, '2021-05-15 14:57:11'),
(40193, 13631, 1, 0, '2021-05-15 16:55:13'),
(40194, 12277, 1, 1, '2021-05-15 14:40:10'),
(40195, 50990, 1, 1, '2021-05-15 14:40:20'),
(40196, 29110, 1, 0, '2021-05-15 15:57:50'),
(40197, 28465, 1, 0, '2021-05-15 14:56:08'),
(40198, 28884, 1, 1, '2021-05-15 15:05:43'),
(40199, 50232, 1, 1, '2021-05-15 14:40:31'),
(40200, 11391, 1, 1, '2021-05-15 15:25:34'),
(40201, 49291, 1, 1, '2021-05-15 14:40:38'),
(40202, 12914, 1, 1, '2021-05-15 14:40:40'),
(40203, 11919, 1, 1, '2021-05-15 14:40:46'),
(40204, 52234, 1, 0, '2021-05-15 15:25:30'),
(40206, 52327, 1, 1, '2021-05-15 14:40:55'),
(40207, 46359, 1, 1, '2021-05-15 14:40:56'),
(40208, 49196, 1, 1, '2021-05-15 14:40:57'),
(40209, 52400, 1, 1, '2021-05-15 14:40:57'),
(40210, 14443, 1, 1, '2021-05-15 14:43:37'),
(40211, 51180, 1, 1, '2021-05-15 14:41:00'),
(40212, 11991, 1, 1, '2021-05-15 14:41:01'),
(40213, 13823, 1, 1, '2021-05-15 14:41:05'),
(40214, 31161, 1, 1, '2021-05-15 14:41:06'),
(40215, 50428, 1, 1, '2021-05-15 14:41:07'),
(40216, 30643, 1, 1, '2021-05-15 14:41:12'),
(40217, 31279, 1, 1, '2021-05-15 14:41:12'),
(40219, 51035, 1, 1, '2021-05-15 14:41:24'),
(40220, 13198, 1, 1, '2021-05-15 14:41:25'),
(40222, 51645, 1, 0, '2021-05-15 16:26:53'),
(40223, 44910, 1, 1, '2021-05-15 14:41:28'),
(40224, 12013, 1, 0, '2021-05-15 16:14:17'),
(40225, 19161, 1, 0, '2021-05-15 16:51:23'),
(40226, 9464, 1, 1, '2021-05-15 14:41:39'),
(40227, 49722, 1, 1, '2021-05-15 14:41:42'),
(40228, 14074, 1, 1, '2021-05-15 14:41:46'),
(40229, 30187, 1, 0, '2021-05-15 15:35:42'),
(40230, 52669, 1, 0, '2021-05-15 15:19:44'),
(40231, 11999, 1, 1, '2021-05-15 14:41:56'),
(40232, 13755, 1, 1, '2021-05-15 14:41:57'),
(40234, 50275, 1, 1, '2021-05-15 14:42:01'),
(40235, 46368, 1, 1, '2021-05-15 14:42:04'),
(40236, 12927, 1, 1, '2021-05-15 15:27:04'),
(40237, 52521, 1, 1, '2021-05-15 14:42:15'),
(40238, 11893, 1, 1, '2021-05-15 14:42:16'),
(40239, 28321, 1, 1, '2021-05-15 14:42:20'),
(40240, 52346, 1, 0, '2021-05-15 15:59:53'),
(40241, 11727, 1, 1, '2021-05-15 14:42:24'),
(40242, 14699, 1, 1, '2021-05-15 14:42:28'),
(40244, 9413, 1, 0, '2021-05-15 16:52:42'),
(40246, 28922, 1, 1, '2021-05-15 15:40:15'),
(40247, 52732, 1, 0, '2021-05-15 16:57:02'),
(40249, 44709, 1, 1, '2021-05-15 15:27:57'),
(40250, 51428, 1, 0, '2021-05-15 14:45:00'),
(40252, 52508, 1, 1, '2021-05-15 14:42:54'),
(40253, 31451, 1, 1, '2021-05-15 14:42:56'),
(40254, 30290, 1, 1, '2021-05-15 14:42:58'),
(40255, 52571, 1, 1, '2021-05-15 14:42:59'),
(40256, 13322, 1, 1, '2021-05-15 14:43:06'),
(40257, 24165, 1, 1, '2021-05-15 14:43:08'),
(40258, 29287, 1, 0, '2021-05-15 14:45:40'),
(40259, 9479, 1, 0, '2021-05-15 16:52:48'),
(40260, 52530, 1, 1, '2021-05-15 15:27:57'),
(40261, 13244, 1, 0, '2021-05-15 15:28:04'),
(40262, 28343, 1, 1, '2021-05-15 14:43:17'),
(40263, 44374, 1, 1, '2021-05-15 14:43:19'),
(40265, 12563, 1, 1, '2021-05-15 14:43:24'),
(40266, 12115, 1, 1, '2021-05-15 14:43:25'),
(40267, 44427, 1, 1, '2021-05-15 14:43:27'),
(40268, 13635, 1, 1, '2021-05-15 14:43:29'),
(40270, 14590, 1, 1, '2021-05-15 14:43:32'),
(40271, 13048, 1, 1, '2021-05-15 14:43:34'),
(40273, 14139, 1, 1, '2021-05-15 14:43:46'),
(40276, 29450, 1, 1, '2021-05-15 14:43:53'),
(40277, 9466, 1, 1, '2021-05-15 14:43:57'),
(40282, 44661, 1, 0, '2021-05-15 16:51:27'),
(40283, 44804, 1, 1, '2021-05-15 14:44:15'),
(40284, 31658, 1, 0, '2021-05-15 14:46:52'),
(40285, 30428, 1, 0, '2021-05-15 14:45:01'),
(40286, 44398, 1, 0, '2021-05-15 15:29:12'),
(40287, 30548, 1, 1, '2021-05-15 14:44:24'),
(40288, 44615, 1, 1, '2021-05-15 14:44:24'),
(40290, 51363, 1, 1, '2021-05-15 14:44:29'),
(40291, 49605, 1, 1, '2021-05-15 16:02:25'),
(40293, 51350, 1, 1, '2021-05-15 14:44:41'),
(40295, 14345, 1, 0, '2021-05-15 15:41:54'),
(40300, 52797, 1, 0, '2021-05-15 14:50:24'),
(40301, 30169, 1, 0, '2021-05-15 16:52:19'),
(40302, 31683, 1, 0, '2021-05-15 16:02:47'),
(40303, 49730, 1, 0, '2021-05-15 16:56:21'),
(40304, 14585, 1, 1, '2021-05-15 14:45:05'),
(40305, 44583, 1, 0, '2021-05-15 14:49:05'),
(40306, 52605, 1, 1, '2021-05-15 14:45:06'),
(40307, 52243, 1, 1, '2021-05-15 14:45:10'),
(40309, 32840, 1, 1, '2021-05-15 14:45:11'),
(40310, 52815, 1, 0, '2021-05-15 16:56:58'),
(40311, 44879, 1, 0, '2021-05-15 16:36:33'),
(40312, 12184, 1, 0, '2021-05-15 16:41:38'),
(40314, 44557, 1, 1, '2021-05-15 14:45:28'),
(40315, 29054, 1, 0, '2021-05-15 15:01:05'),
(40317, 12248, 1, 0, '2021-05-15 15:30:02'),
(40319, 13479, 1, 1, '2021-05-15 14:45:40'),
(40320, 49250, 1, 1, '2021-05-15 14:45:42'),
(40322, 29141, 1, 0, '2021-05-15 16:51:24'),
(40324, 11958, 1, 0, '2021-05-15 16:57:35'),
(40326, 52543, 1, 1, '2021-05-15 14:46:02'),
(40327, 44514, 1, 1, '2021-05-15 14:46:04'),
(40328, 51024, 1, 1, '2021-05-15 14:46:06'),
(40330, 14502, 1, 1, '2021-05-15 14:46:15'),
(40331, 52776, 1, 1, '2021-05-15 14:46:20'),
(40332, 32522, 1, 1, '2021-05-15 14:46:22'),
(40334, 50492, 1, 1, '2021-05-15 14:46:26'),
(40335, 52333, 1, 1, '2021-05-15 14:46:30'),
(40336, 52777, 1, 1, '2021-05-15 14:46:30'),
(40337, 13614, 1, 1, '2021-05-15 14:46:30'),
(40338, 24126, 1, 1, '2021-05-15 14:46:31'),
(40339, 28371, 1, 1, '2021-05-15 15:51:07'),
(40341, 51986, 1, 1, '2021-05-15 15:33:16'),
(40342, 52348, 1, 1, '2021-05-15 14:46:42'),
(40344, 31456, 1, 1, '2021-05-15 14:46:49'),
(40345, 50998, 1, 1, '2021-05-15 14:46:52'),
(40346, 13566, 1, 1, '2021-05-15 14:46:52'),
(40348, 52601, 1, 1, '2021-05-15 14:47:01'),
(40350, 13463, 1, 1, '2021-05-15 14:47:05'),
(40352, 29767, 1, 1, '2021-05-15 14:47:12'),
(40354, 12212, 1, 1, '2021-05-15 14:47:16'),
(40356, 11664, 1, 1, '2021-05-15 14:47:21'),
(40357, 51949, 1, 1, '2021-05-15 14:47:25'),
(40358, 30652, 1, 1, '2021-05-15 14:47:26'),
(40359, 14163, 1, 1, '2021-05-15 15:32:54'),
(40360, 49729, 1, 1, '2021-05-15 14:47:40'),
(40362, 52574, 1, 1, '2021-05-15 14:47:46'),
(40363, 11936, 1, 0, '2021-05-15 16:16:59'),
(40364, 28296, 1, 0, '2021-05-15 16:15:26'),
(40365, 51818, 1, 1, '2021-05-15 14:47:56'),
(40366, 12048, 1, 1, '2021-05-15 14:47:59'),
(40367, 32844, 1, 1, '2021-05-15 14:48:02'),
(40368, 28515, 1, 1, '2021-05-15 14:48:08'),
(40369, 51821, 1, 1, '2021-05-15 14:48:13'),
(40370, 28809, 1, 1, '2021-05-15 14:48:18'),
(40371, 29971, 1, 1, '2021-05-15 14:48:26'),
(40373, 13103, 1, 1, '2021-05-15 14:48:35'),
(40374, 13773, 1, 0, '2021-05-15 14:56:07'),
(40375, 9458, 1, 1, '2021-05-15 14:48:38'),
(40379, 52789, 1, 1, '2021-05-15 14:48:49'),
(40380, 28337, 1, 1, '2021-05-15 14:48:50'),
(40381, 13461, 1, 1, '2021-05-15 14:48:53'),
(40382, 52164, 1, 1, '2021-05-15 14:48:55'),
(40384, 52342, 1, 1, '2021-05-15 14:49:04'),
(40385, 11460, 1, 1, '2021-05-15 14:49:06'),
(40386, 12129, 1, 1, '2021-05-15 14:49:07'),
(40388, 12632, 1, 1, '2021-05-15 14:49:11'),
(40389, 46354, 1, 1, '2021-05-15 14:49:16'),
(40390, 12567, 1, 1, '2021-05-15 14:49:17'),
(40391, 31219, 1, 1, '2021-05-15 14:49:19'),
(40392, 52316, 1, 1, '2021-05-15 14:49:19'),
(40393, 31626, 1, 1, '2021-05-15 15:34:18'),
(40394, 52050, 1, 1, '2021-05-15 14:49:25'),
(40395, 52340, 1, 1, '2021-05-15 14:49:31'),
(40396, 51143, 1, 1, '2021-05-15 14:49:31'),
(40397, 13295, 1, 1, '2021-05-15 14:49:32'),
(40398, 12472, 1, 1, '2021-05-15 14:49:33'),
(40399, 12856, 1, 0, '2021-05-15 15:54:42'),
(40403, 52145, 1, 0, '2021-05-15 14:53:51'),
(40404, 52344, 1, 1, '2021-05-15 14:49:54'),
(40405, 52662, 1, 1, '2021-05-15 14:49:58'),
(40406, 50804, 1, 1, '2021-05-15 14:49:58'),
(40408, 12291, 1, 0, '2021-05-15 14:52:00'),
(40409, 51289, 1, 1, '2021-05-15 14:50:16'),
(40411, 13015, 1, 1, '2021-05-15 14:50:19'),
(40412, 52179, 1, 1, '2021-05-15 15:35:24'),
(40413, 28431, 1, 1, '2021-05-15 14:50:46'),
(40415, 49292, 1, 1, '2021-05-15 14:50:53'),
(40416, 13434, 1, 1, '2021-05-15 15:37:34'),
(40417, 29109, 1, 0, '2021-05-15 17:12:59'),
(40418, 16183, 1, 1, '2021-05-15 15:01:39'),
(40419, 52384, 1, 1, '2021-05-15 14:51:04'),
(40420, 51751, 1, 1, '2021-05-15 14:51:05'),
(40421, 12300, 1, 1, '2021-05-15 14:51:07'),
(40424, 14173, 1, 1, '2021-05-15 14:51:13'),
(40425, 30733, 1, 1, '2021-05-15 14:51:17'),
(40427, 44845, 1, 0, '2021-05-15 15:08:49'),
(40428, 30705, 1, 1, '2021-05-15 14:51:29'),
(40429, 28641, 1, 1, '2021-05-15 14:51:32'),
(40431, 50677, 1, 0, '2021-05-15 15:32:20'),
(40432, 29946, 1, 1, '2021-05-15 14:52:00'),
(40434, 11552, 1, 0, '2021-05-15 15:43:17'),
(40436, 44561, 1, 1, '2021-05-15 14:52:35'),
(40438, 14600, 1, 1, '2021-05-15 14:52:40'),
(40439, 12254, 1, 1, '2021-05-15 14:52:47'),
(40440, 49508, 1, 1, '2021-05-15 14:52:48'),
(40441, 51084, 1, 0, '2021-05-15 15:09:22'),
(40442, 44656, 1, 1, '2021-05-15 14:52:56'),
(40443, 30949, 1, 1, '2021-05-15 14:53:05'),
(40444, 52613, 1, 0, '2021-05-15 15:37:33'),
(40447, 28197, 1, 1, '2021-05-15 14:53:38'),
(40449, 11431, 1, 1, '2021-05-15 14:53:45'),
(40451, 9425, 1, 0, '2021-05-15 16:57:55'),
(40452, 29505, 1, 1, '2021-05-15 14:54:01'),
(40453, 29603, 1, 1, '2021-05-15 14:54:06'),
(40455, 52438, 1, 1, '2021-05-15 14:54:26'),
(40457, 49253, 1, 1, '2021-05-15 14:54:38'),
(40459, 11762, 1, 1, '2021-05-15 14:54:49'),
(40461, 31555, 1, 1, '2021-05-15 14:54:56'),
(40462, 18355, 1, 1, '2021-05-15 15:40:35'),
(40463, 52349, 1, 1, '2021-05-15 14:55:03'),
(40464, 28351, 1, 1, '2021-05-15 14:55:03'),
(40468, 13160, 1, 0, '2021-05-15 14:56:11'),
(40469, 14113, 1, 1, '2021-05-15 14:55:17'),
(40470, 14535, 1, 1, '2021-05-15 14:55:22'),
(40474, 51424, 1, 1, '2021-05-15 14:56:04'),
(40475, 52568, 1, 1, '2021-05-15 14:56:10'),
(40476, 12857, 1, 1, '2021-05-15 14:56:11'),
(40478, 12374, 1, 1, '2021-05-15 14:56:31'),
(40481, 13282, 1, 1, '2021-05-15 14:56:42'),
(40482, 44555, 1, 1, '2021-05-15 14:56:44'),
(40489, 14169, 1, 1, '2021-05-15 14:57:11'),
(40490, 30326, 1, 0, '2021-05-15 15:10:56'),
(40492, 52551, 1, 0, '2021-05-15 16:56:59'),
(40494, 44610, 1, 1, '2021-05-15 14:57:45'),
(40495, 52291, 1, 1, '2021-05-15 14:57:57'),
(40496, 31599, 1, 1, '2021-05-15 14:58:01'),
(40497, 31103, 1, 1, '2021-05-15 16:14:36'),
(40498, 14753, 1, 1, '2021-05-15 14:58:19'),
(40500, 50548, 1, 1, '2021-05-15 14:58:29'),
(40501, 9428, 1, 1, '2021-05-15 14:58:29'),
(40502, 14512, 1, 0, '2021-05-15 15:52:19'),
(40503, 12359, 1, 0, '2021-05-15 16:57:22'),
(40505, 28215, 1, 1, '2021-05-15 14:58:40'),
(40506, 12098, 1, 1, '2021-05-15 15:43:35'),
(40507, 12961, 1, 1, '2021-05-15 14:58:54'),
(40508, 44937, 1, 0, '2021-05-15 16:12:20'),
(40509, 12731, 1, 0, '2021-05-15 16:17:09'),
(40510, 19139, 1, 1, '2021-05-15 14:59:12'),
(40512, 14611, 1, 0, '2021-05-15 15:07:01'),
(40515, 13310, 1, 1, '2021-05-15 15:45:13'),
(40516, 13762, 1, 1, '2021-05-15 15:00:38'),
(40519, 50755, 1, 0, '2021-05-15 15:15:24'),
(40522, 52765, 1, 1, '2021-05-15 15:01:33'),
(40525, 28333, 1, 1, '2021-05-15 15:51:53'),
(40526, 44602, 1, 1, '2021-05-15 15:02:04'),
(40529, 28851, 1, 1, '2021-05-15 15:02:42'),
(40531, 11578, 1, 1, '2021-05-15 15:03:05'),
(40532, 44534, 1, 1, '2021-05-15 15:03:10'),
(40533, 13988, 1, 1, '2021-05-15 15:03:18'),
(40534, 13835, 1, 1, '2021-05-15 15:03:20'),
(40535, 29157, 1, 1, '2021-05-15 15:03:32'),
(40537, 50517, 1, 0, '2021-05-15 15:09:18'),
(40538, 13423, 1, 1, '2021-05-15 15:03:57'),
(40540, 51597, 1, 1, '2021-05-15 15:04:00'),
(40542, 44601, 1, 1, '2021-05-15 15:04:08'),
(40543, 11898, 1, 1, '2021-05-15 15:04:10'),
(40545, 52277, 1, 1, '2021-05-15 15:04:25'),
(40546, 50245, 1, 1, '2021-05-15 15:04:27'),
(40548, 52683, 1, 1, '2021-05-15 15:04:43'),
(40549, 49647, 1, 1, '2021-05-15 15:50:04'),
(40552, 24164, 1, 1, '2021-05-15 15:05:33'),
(40554, 11902, 1, 1, '2021-05-15 15:05:39'),
(40557, 11963, 1, 0, '2021-05-15 16:30:21'),
(40558, 52550, 1, 1, '2021-05-15 15:05:55'),
(40559, 12882, 1, 1, '2021-05-15 15:21:47'),
(40563, 29974, 1, 1, '2021-05-15 15:06:41'),
(40566, 51617, 1, 1, '2021-05-15 15:52:01'),
(40571, 28622, 1, 1, '2021-05-15 15:07:34'),
(40572, 52190, 1, 1, '2021-05-15 15:07:48'),
(40574, 13669, 1, 1, '2021-05-15 15:07:51'),
(40578, 51055, 1, 1, '2021-05-15 15:08:19'),
(40579, 28939, 1, 1, '2021-05-15 15:08:27'),
(40580, 50455, 1, 1, '2021-05-15 15:08:32'),
(40583, 14575, 1, 1, '2021-05-15 15:09:13'),
(40584, 52558, 1, 0, '2021-05-15 15:37:06'),
(40588, 33176, 1, 1, '2021-05-15 15:09:27'),
(40590, 33064, 1, 1, '2021-05-15 15:09:33'),
(40593, 29838, 1, 0, '2021-05-15 15:16:32'),
(40595, 12627, 1, 0, '2021-05-15 15:55:20'),
(40596, 29055, 1, 1, '2021-05-15 15:10:30'),
(40597, 51648, 1, 1, '2021-05-15 15:55:24'),
(40598, 9406, 1, 0, '2021-05-15 15:36:30'),
(40599, 13263, 1, 1, '2021-05-15 15:10:52'),
(40602, 12695, 1, 1, '2021-05-15 15:11:15'),
(40604, 29391, 1, 1, '2021-05-15 15:11:50'),
(40606, 11900, 1, 1, '2021-05-15 15:11:54'),
(40607, 9409, 1, 1, '2021-05-15 15:12:09'),
(40609, 13707, 1, 1, '2021-05-15 15:12:33'),
(40617, 12072, 1, 1, '2021-05-15 15:13:45'),
(40620, 31269, 1, 0, '2021-05-15 15:14:09'),
(40624, 12555, 1, 0, '2021-05-15 16:57:05'),
(40626, 11957, 1, 1, '2021-05-15 15:15:06'),
(40628, 18979, 1, 1, '2021-05-15 15:43:02'),
(40630, 15544, 1, 1, '2021-05-15 15:15:17'),
(40632, 11975, 1, 1, '2021-05-15 15:15:28'),
(40636, 14164, 1, 1, '2021-05-15 15:15:54'),
(40638, 52469, 1, 1, '2021-05-15 15:16:07'),
(40639, 11780, 1, 1, '2021-05-15 15:16:12'),
(40640, 44482, 1, 1, '2021-05-15 16:02:01'),
(40643, 50363, 1, 1, '2021-05-15 15:16:45'),
(40644, 12249, 1, 1, '2021-05-15 16:01:53'),
(40650, 28811, 1, 1, '2021-05-15 15:34:35'),
(40664, 31102, 1, 1, '2021-05-15 15:20:13'),
(40669, 52774, 1, 1, '2021-05-15 15:20:41'),
(40671, 52641, 1, 0, '2021-05-15 15:27:02'),
(40673, 44402, 1, 1, '2021-05-15 15:21:27'),
(40683, 13528, 1, 0, '2021-05-15 16:02:13'),
(40684, 29437, 1, 1, '2021-05-15 15:22:23'),
(40693, 14526, 1, 0, '2021-05-15 15:26:02'),
(40697, 52431, 1, 1, '2021-05-15 15:24:24'),
(40704, 11399, 1, 1, '2021-05-15 15:26:10'),
(40706, 11546, 1, 1, '2021-05-15 15:26:26'),
(40708, 11415, 1, 0, '2021-05-16 14:42:51'),
(40709, 12083, 1, 1, '2021-05-15 15:26:43'),
(40712, 51551, 1, 0, '2021-05-15 16:57:01'),
(40714, 11868, 1, 1, '2021-05-15 15:27:55'),
(40718, 12362, 1, 0, '2021-05-15 15:31:23'),
(40725, 13206, 1, 1, '2021-05-15 15:29:31'),
(40731, 52758, 1, 1, '2021-05-15 15:30:53'),
(40734, 14072, 1, 1, '2021-05-15 15:31:24'),
(40735, 31635, 1, 1, '2021-05-15 15:31:26'),
(40738, 28306, 1, 1, '2021-05-15 15:32:12'),
(40743, 11581, 1, 1, '2021-05-15 15:39:23'),
(40748, 50249, 1, 1, '2021-05-15 15:33:42'),
(40749, 51919, 1, 1, '2021-05-15 15:33:51'),
(40754, 52137, 1, 1, '2021-05-15 15:34:43'),
(40757, 12540, 1, 1, '2021-05-15 15:36:23'),
(40759, 12104, 1, 1, '2021-05-15 15:36:45'),
(40773, 31287, 1, 1, '2021-05-15 15:39:44'),
(40774, 44519, 1, 1, '2021-05-15 15:39:44'),
(40781, 52736, 1, 0, '2021-05-15 15:42:42'),
(40782, 12670, 1, 1, '2021-05-15 15:41:09'),
(40791, 17304, 1, 1, '2021-05-15 15:44:37'),
(40792, 49679, 1, 1, '2021-05-15 15:44:55'),
(40796, 31139, 1, 0, '2021-05-15 16:30:52'),
(40799, 12409, 1, 0, '2021-05-15 15:50:07'),
(40804, 52255, 1, 1, '2021-05-15 15:47:51'),
(40805, 51885, 1, 1, '2021-05-15 15:47:59'),
(40809, 52006, 1, 1, '2021-05-15 15:48:49'),
(40816, 12350, 1, 1, '2021-05-15 15:51:13'),
(40821, 52048, 1, 1, '2021-05-15 15:52:29'),
(40823, 11522, 1, 1, '2021-05-15 15:53:49'),
(40829, 9462, 1, 0, '2021-05-15 16:44:50'),
(40838, 13611, 1, 1, '2021-05-15 16:01:42'),
(40845, 13955, 1, 1, '2021-05-15 16:03:13'),
(40863, 11791, 1, 1, '2021-05-15 16:13:25'),
(40871, 29295, 1, 1, '2021-05-15 16:15:37'),
(40872, 51093, 1, 1, '2021-05-15 16:15:59'),
(40877, 13306, 1, 1, '2021-05-15 16:22:06'),
(40887, 52817, 1, 1, '2021-05-15 16:27:07'),
(40896, 46329, 1, 1, '2021-05-15 22:25:38'),
(40898, 52741, 1, 1, '2021-05-17 07:35:34');

-- --------------------------------------------------------

--
-- Table structure for table `oneteam_buzzlive_reactions`
--

CREATE TABLE `oneteam_buzzlive_reactions` (
  `rid` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `report_table` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oneteam_buzzlive_reactions`
--

INSERT INTO `oneteam_buzzlive_reactions` (`rid`, `id`, `name`, `icon`, `report_table`, `created_at`, `status`) VALUES
(1, 6, 'Good Morning', 'happy.png', 'o_gdmorning', '2020-07-18 01:35:20', 1),
(2, 4, 'Applaud', 'applaud.png', 'o_applaud', '2020-07-18 01:35:27', 1),
(3, 3, 'Awesome', 'awesome.png', 'o_awesome', '2020-07-18 01:35:33', 1),
(4, 2, 'Jabardast', 'jabardast.png', 'o_jabardast', '2020-07-18 01:35:38', 1),
(5, 1, 'Agree', 'agree.png', 'o_agree', '2020-07-18 01:58:43', 1),
(6, 5, 'Nice', 'nice.png', 'o_nice', '2020-07-18 01:35:48', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oneteam_buzzlive_reactions_crismas`
--

CREATE TABLE `oneteam_buzzlive_reactions_crismas` (
  `rid` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `report_table` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oneteam_buzzlive_reactions_crismas`
--

INSERT INTO `oneteam_buzzlive_reactions_crismas` (`rid`, `id`, `name`, `icon`, `report_table`, `created_at`, `status`) VALUES
(1, 6, 'Merry Christmas', 'm61.png', 'o_gdmorning', '2020-12-24 13:34:19', 1),
(2, 4, 'Jingle Bells', 'm11.png', 'o_applaud', '2020-12-24 13:34:13', 1),
(3, 3, 'Snowy Socks', 'm41.png', 'o_awesome', '2020-12-24 13:34:24', 1),
(4, 2, 'Santa Hugs', 'm31.png', 'o_jabardast', '2020-12-24 13:34:29', 1),
(5, 1, 'Happy Lights', 'm51.png', 'o_agree', '2020-12-24 13:34:40', 1),
(6, 5, 'Snow Man', 'm21.png', 'o_nice', '2020-12-24 13:39:57', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oneteam_buzzlive_reactions_diwali`
--

CREATE TABLE `oneteam_buzzlive_reactions_diwali` (
  `rid` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `report_table` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oneteam_buzzlive_reactions_diwali`
--

INSERT INTO `oneteam_buzzlive_reactions_diwali` (`rid`, `id`, `name`, `icon`, `report_table`, `created_at`, `status`) VALUES
(1, 6, 'Laddi', 'r6.png', 'o_gdmorning', '2020-11-12 10:53:35', 1),
(2, 4, 'Sky shot', 'r4.png', 'o_applaud', '2020-11-12 10:53:46', 1),
(3, 3, 'Anar', 'r8.png', 'o_awesome', '2020-11-12 12:42:54', 1),
(4, 2, 'Diya', 'r2.png', 'o_jabardast', '2020-11-12 10:54:11', 1),
(5, 1, 'Fireworks', 'r1.png', 'o_agree', '2020-11-12 10:54:20', 1),
(6, 5, 'Bomb', 'r5.png', 'o_nice', '2020-11-12 10:54:26', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oneteam_buzzlive_reactions_old`
--

CREATE TABLE `oneteam_buzzlive_reactions_old` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oneteam_buzzlive_reactions_old`
--

INSERT INTO `oneteam_buzzlive_reactions_old` (`id`, `name`, `created_at`, `status`) VALUES
(1, 'like', '2020-06-06 12:57:49', 1),
(2, 'dislike', '2020-06-06 12:57:59', 1),
(3, 'heart', '2020-06-06 14:09:12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oneteam_buzzlive_reactions_report`
--

CREATE TABLE `oneteam_buzzlive_reactions_report` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reaction_id` int(11) DEFAULT NULL,
  `livestream_url_id` int(11) DEFAULT NULL,
  `reaction_count` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oneteam_buzzlive_streaming_url`
--

CREATE TABLE `oneteam_buzzlive_streaming_url` (
  `id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `url` text,
  `session_id` int(11) DEFAULT NULL,
  `report_created_status` tinyint(4) NOT NULL DEFAULT '0',
  `event_content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `company_logo` varchar(255) NOT NULL,
  `event_logo` varchar(255) NOT NULL,
  `chatcamp_channel_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `chat_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=Active;0=Inactive',
  `notification_status` tinyint(4) NOT NULL DEFAULT '0',
  `notification_text` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `reaction_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=Enable;0=Disable',
  `live_button_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=Active;0=Inactive	',
  `viewers` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=Active;2=Total',
  `move_to_offline_content` int(11) DEFAULT NULL,
  `offline_content_category` varchar(10) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `speaker` varchar(255) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `start_date_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oneteam_buzzlive_streaming_url`
--

INSERT INTO `oneteam_buzzlive_streaming_url` (`id`, `company_id`, `url`, `session_id`, `report_created_status`, `event_content`, `company_logo`, `event_logo`, `chatcamp_channel_id`, `chat_status`, `notification_status`, `notification_text`, `reaction_status`, `live_button_status`, `viewers`, `move_to_offline_content`, `offline_content_category`, `title`, `speaker`, `host`, `start_date_time`) VALUES
(1, 32, 'https://www.youtube.com/embed/jLRG4GkCCAs', 316, 1, 'June Syllabus Lunch', 'https://dlhh07uplv9u.cloudfront.net/appadmin/uploads/companylivesession/1621067427520786828609f86a396594.png', 'https://dlhh07uplv9u.cloudfront.net/appadmin/uploads/companylivesession/1621067427895219698609f86a396a6e.png', '5f0ee5dfc7ecde0001a46700', 0, 0, 'Live session will start shortly.', 1, 0, 1, 0, '', 'June Syllabus Launch of Warriors App University, 2021\'', 'Warriors Management', 'Ms. Jyoti Jaiswal', '2021-05-15 19:50:00');

-- --------------------------------------------------------

--
-- Table structure for table `oneteam_buzzlive_streaming_url_main`
--

CREATE TABLE `oneteam_buzzlive_streaming_url_main` (
  `id` int(11) NOT NULL,
  `url` text,
  `session_id` int(11) DEFAULT NULL,
  `report_created_status` tinyint(4) NOT NULL DEFAULT '0',
  `event_content` longtext NOT NULL,
  `company_logo` varchar(255) NOT NULL,
  `event_logo` varchar(255) NOT NULL,
  `chatcamp_channel_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `chat_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=Active;0=Inactive',
  `reaction_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=Enable;0=Disable',
  `live_button_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=Active;0=Inactive	',
  `minimize_remove_participants_status` tinyint(4) NOT NULL DEFAULT '1',
  `viewers` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=Active;2=Total'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oneteam_buzzlive_streaming_url_main`
--

INSERT INTO `oneteam_buzzlive_streaming_url_main` (`id`, `url`, `session_id`, `report_created_status`, `event_content`, `company_logo`, `event_logo`, `chatcamp_channel_id`, `chat_status`, `reaction_status`, `live_button_status`, `minimize_remove_participants_status`, `viewers`) VALUES
(1, 'https://www.youtube.com/embed/4VBHHhzsJXs?playsinline=1&autoplay=1&enablejsapi=1&modestbranding=1&fs=0', 10, 1, '<h5 class=\"topic\">WHAT DOCTOR SAYS ABOUT SUPLIMENTS</h5>\r\n\r\n            <div class=\"speaker\"><i class=\"fa fa-volume-down\"></i> Dr. Pooja Thapar</div>\r\n\r\n<div class=\"host\"><i class=\"fa fa-book\"></i> Ms. Vinita Shukla</div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n', 'https://warriors.oneteam.in/live/images/w_logo.png', 'https://warriors.oneteam.in/live/images/ayusante.png', '5f0ee5dfc7ecde0001a46700', 0, 1, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oneteam_log`
--

CREATE TABLE `oneteam_log` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `text_request` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `text_request_third_api` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `text_response` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `o_agree`
--

CREATE TABLE `o_agree` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reaction_id` int(11) NOT NULL,
  `livestream_url_id` int(11) NOT NULL,
  `reaction_count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `o_agree`
--

INSERT INTO `o_agree` (`id`, `created_at`, `reaction_id`, `livestream_url_id`, `reaction_count`) VALUES
(1, '2021-05-15 16:50:39', 1, 1, 1180);

-- --------------------------------------------------------

--
-- Table structure for table `o_applaud`
--

CREATE TABLE `o_applaud` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reaction_id` int(11) NOT NULL,
  `livestream_url_id` int(11) NOT NULL,
  `reaction_count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `o_applaud`
--

INSERT INTO `o_applaud` (`id`, `created_at`, `reaction_id`, `livestream_url_id`, `reaction_count`) VALUES
(1, '2021-05-15 17:00:39', 4, 1, 4116);

-- --------------------------------------------------------

--
-- Table structure for table `o_awesome`
--

CREATE TABLE `o_awesome` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reaction_id` int(11) NOT NULL,
  `livestream_url_id` int(11) NOT NULL,
  `reaction_count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `o_awesome`
--

INSERT INTO `o_awesome` (`id`, `created_at`, `reaction_id`, `livestream_url_id`, `reaction_count`) VALUES
(1, '2021-05-15 16:54:13', 3, 1, 1487);

-- --------------------------------------------------------

--
-- Table structure for table `o_gdmorning`
--

CREATE TABLE `o_gdmorning` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reaction_id` int(11) NOT NULL,
  `livestream_url_id` int(11) NOT NULL,
  `reaction_count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `o_gdmorning`
--

INSERT INTO `o_gdmorning` (`id`, `created_at`, `reaction_id`, `livestream_url_id`, `reaction_count`) VALUES
(1, '2021-05-15 16:52:22', 6, 1, 2365);

-- --------------------------------------------------------

--
-- Table structure for table `o_jabardast`
--

CREATE TABLE `o_jabardast` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reaction_id` int(11) NOT NULL,
  `livestream_url_id` int(11) NOT NULL,
  `reaction_count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `o_jabardast`
--

INSERT INTO `o_jabardast` (`id`, `created_at`, `reaction_id`, `livestream_url_id`, `reaction_count`) VALUES
(1, '2021-05-15 16:53:44', 2, 1, 1284);

-- --------------------------------------------------------

--
-- Table structure for table `o_nice`
--

CREATE TABLE `o_nice` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reaction_id` int(11) NOT NULL,
  `livestream_url_id` int(11) NOT NULL,
  `reaction_count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `o_nice`
--

INSERT INTO `o_nice` (`id`, `created_at`, `reaction_id`, `livestream_url_id`, `reaction_count`) VALUES
(1, '2021-05-15 16:52:16', 5, 1, 983);

-- --------------------------------------------------------

--
-- Table structure for table `poll`
--

CREATE TABLE `poll` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `question_id` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `user_end_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `watchers` varchar(255) COLLATE utf8_bin NOT NULL,
  `role_base` varchar(255) COLLATE utf8_bin NOT NULL,
  `user_base` varchar(255) COLLATE utf8_bin NOT NULL,
  `layout_id` int(11) NOT NULL,
  `show_result` tinyint(4) NOT NULL DEFAULT '0',
  `is_mandatory` tinyint(4) NOT NULL DEFAULT '0',
  `reminder` tinyint(4) NOT NULL DEFAULT '0',
  `send_notification` tinyint(4) NOT NULL DEFAULT '0',
  `notification_flag` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1-Active,2-Inactive',
  `running_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1-Run,0-Stop'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `poll`
--

INSERT INTO `poll` (`id`, `created_at`, `company_id`, `title`, `question_id`, `start_date`, `user_end_date`, `end_date`, `watchers`, `role_base`, `user_base`, `layout_id`, `show_result`, `is_mandatory`, `reminder`, `send_notification`, `notification_flag`, `status`, `running_status`) VALUES
(1, '2019-05-04 09:55:40', 2, 'test poll', 2, '2019-05-04 15:20:00', '2019-07-31 15:20:00', '2019-09-30 15:20:00', '10', '1', '10', 3, 1, 1, 1, 1, 0, 2, 1),
(2, '2019-05-07 10:04:03', 2, 'test two', 3, '2019-05-07 15:30:00', '2019-05-22 21:50:00', '2019-05-31 08:25:00', '10', '3', '10', 2, 1, 1, 1, 1, 1, 2, 1),
(3, '2019-06-13 11:58:07', 2, 'test', 3, '2019-06-19 17:35:00', '2019-06-21 15:25:00', '2019-06-21 23:45:00', '10', '', '10', 2, 0, 0, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `poll_response`
--

CREATE TABLE `poll_response` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `poll_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `poll_question_id` int(11) NOT NULL,
  `response` longtext COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `question` varchar(255) COLLATE utf8_bin NOT NULL,
  `category_id` int(11) NOT NULL,
  `corporatevalue_id` int(11) NOT NULL,
  `answer_type` int(11) NOT NULL COMMENT '1 - text, 2 - multiple, 3 - dropdown',
  `options` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `correct_answer` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '1 - active, 2 - inactive 	',
  `answer_validation` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1 - yes',
  `image` varchar(255) COLLATE utf8_bin NOT NULL,
  `wheel_self_tagline` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `wheel_others_tagline` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`id`, `created_at`, `question`, `category_id`, `corporatevalue_id`, `answer_type`, `options`, `correct_answer`, `status`, `answer_validation`, `image`, `wheel_self_tagline`, `wheel_others_tagline`) VALUES
(1, '2018-08-20 01:36:37', '2.My first job was...', 2, 1, 1, '', '', 1, 0, '15369359422790496895b9bc80629738.png', 'My first job was {value}', 'I have worked as a {value}'),
(2, '2018-08-20 01:39:59', '4.Someone famous whom I have met', 2, 1, 1, '', '', 1, 0, '15369359763858045745b9bc828bc9a3.png', 'I have met {value}', 'Most famous person i have met is {value}'),
(3, '2018-08-20 01:42:33', '3.Someone at job you really admire', 2, 1, 1, '', '', 1, 0, '153693596414641778585b9bc81c3a16a.png', 'I admire {value} at office', 'You admire {value} at office.'),
(4, '2018-09-13 09:48:34', '1.hello test for facterE', 2, 1, 1, '', '', 1, 0, '153693607414799173735b9bc88a343c6.png', 'self', 'other'),
(5, '2018-09-13 12:16:19', '5.test during test', 2, 1, 1, '', '', 1, 0, '153693598910055105755b9bc835597fc.png', 'self', 'other');

-- --------------------------------------------------------

--
-- Table structure for table `question_categories`
--

CREATE TABLE `question_categories` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 - active, 2 - inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `question_categories`
--

INSERT INTO `question_categories` (`id`, `created_at`, `category_name`, `status`) VALUES
(2, '2018-08-20 01:34:45', 'Know Your Mates', 1);

-- --------------------------------------------------------

--
-- Table structure for table `reaction_live_url`
--

CREATE TABLE `reaction_live_url` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `live_url` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `reaction_live_url`
--

INSERT INTO `reaction_live_url` (`id`, `created_at`, `live_url`) VALUES
(1, '2020-06-01 13:41:04', 'https://live.oneteam.in/');

-- --------------------------------------------------------

--
-- Table structure for table `reaction_report`
--

CREATE TABLE `reaction_report` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reaction_live_url_id` int(11) NOT NULL,
  `reaction_type_id` int(11) NOT NULL,
  `hit_count` longtext COLLATE utf8_bin NOT NULL,
  `last_updatetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `reaction_report`
--

INSERT INTO `reaction_report` (`id`, `created_at`, `reaction_live_url_id`, `reaction_type_id`, `hit_count`, `last_updatetime`) VALUES
(1, '2020-06-01 14:12:17', 1, 1, '19', '2020-06-01 14:12:30'),
(2, '2020-06-01 14:12:48', 1, 2, '6', '2020-06-01 14:12:52'),
(3, '2020-06-01 14:12:57', 1, 4, '6', '2020-06-01 14:13:00'),
(4, '2020-06-01 14:13:05', 1, 3, '36', '2020-06-01 18:10:05');

-- --------------------------------------------------------

--
-- Table structure for table `reaction_type`
--

CREATE TABLE `reaction_type` (
  `id` int(11) NOT NULL,
  `reaction_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL COMMENT '1-active, 2- inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `reaction_type`
--

INSERT INTO `reaction_type` (`id`, `reaction_name`, `created_at`, `status`) VALUES
(1, 'Like', '2020-06-01 14:04:57', 1),
(2, 'Sad', '2020-06-01 14:04:57', 1),
(3, 'Love', '2020-06-01 14:04:59', 1),
(4, 'Smile', '2020-06-01 14:04:59', 1);

-- --------------------------------------------------------

--
-- Table structure for table `reward_attempts`
--

CREATE TABLE `reward_attempts` (
  `rule_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `reward_attempts`
--

INSERT INTO `reward_attempts` (`rule_id`, `employee_id`) VALUES
(1, 10),
(1, 10),
(1, 10),
(1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `reward_rules`
--

CREATE TABLE `reward_rules` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `reward_type` enum('1','2') COLLATE utf8_bin NOT NULL DEFAULT '1' COMMENT '1 - badge, 2 - points,',
  `criteria_number` int(11) NOT NULL,
  `notification_template` text COLLATE utf8_bin NOT NULL,
  `notification_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `rule_type` enum('1','2','3') COLLATE utf8_bin NOT NULL DEFAULT '1' COMMENT '1 - Announcement, 2 - Corporate Attribute, 3 - Question Category',
  `companybadge_id` int(11) NOT NULL DEFAULT '0',
  `points` int(11) NOT NULL DEFAULT '0',
  `companyvalue_id` int(11) NOT NULL DEFAULT '0',
  `companyquestioncategory_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 - active, 2 - inactive ',
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `reward_rules`
--

INSERT INTO `reward_rules` (`id`, `created_at`, `company_id`, `reward_type`, `criteria_number`, `notification_template`, `notification_title`, `rule_type`, `companybadge_id`, `points`, `companyvalue_id`, `companyquestioncategory_id`, `status`, `start_date`, `end_date`) VALUES
(1, '2018-08-16 22:24:40', 4, '1', 4, '{badge_name} has been found', 'Someone is beautiful', '1', 2, 0, 0, 0, 1, '2018-08-16', '2018-08-17');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `menu_access` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `created_at`, `role_name`, `menu_access`) VALUES
(1, '2017-12-14 03:50:09', 'Admin', '4,21,22,23,25,29,35,47,51,57,60,61,63,70,74,77,87,2,6,20,24,26,30,36,48,52,58,62,71,75,78,79,80,81,88,5,50,56,64,65,67,89,90,8,68,76,83,84,92,11,18,82,86,91,9,46,12,53,93,17,54,72,3,7,55,59,69,73,49,1,94,95');

-- --------------------------------------------------------

--
-- Table structure for table `scanningcompany`
--

CREATE TABLE `scanningcompany` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `scancompany_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `display_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `expiry_date_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `scanningcompany`
--

INSERT INTO `scanningcompany` (`id`, `created_at`, `scancompany_name`, `display_name`, `expiry_date_time`, `status`) VALUES
(1, '2020-02-07 12:49:00', 'VTEM', '9th Talkatora Event', '2020-02-10 01:00:00', 1),
(2, '2020-02-07 17:45:27', 'TEST', 'TEST COMPANY', '2020-07-31 17:45:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `scanningticketcategory`
--

CREATE TABLE `scanningticketcategory` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `scompany_id` int(11) NOT NULL,
  `category_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `sub_cat_of_id` int(11) DEFAULT '0',
  `scan_user_limit` int(11) NOT NULL DEFAULT '0',
  `gate_no` int(11) NOT NULL,
  `code_type` tinyint(4) NOT NULL,
  `barcode_start_series` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `barcode_end_series` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1- active & 2-deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `scanningticketcategory`
--

INSERT INTO `scanningticketcategory` (`id`, `created_at`, `scompany_id`, `category_name`, `sub_cat_of_id`, `scan_user_limit`, `gate_no`, `code_type`, `barcode_start_series`, `barcode_end_series`, `status`) VALUES
(1, '2020-02-07 12:50:45', 1, 'VVIP MORNING', 0, 0, 1, 1, '', '', 1),
(2, '2020-02-07 12:51:06', 1, 'VIP MORNING', 0, 0, 2, 1, '', '', 1),
(3, '2020-02-07 12:51:26', 1, 'GENERAL MORNING', 0, 0, 3, 1, '', '', 1),
(4, '2020-02-07 12:51:52', 1, 'VVIP EVENING', 0, 0, 1, 1, '', '', 1),
(5, '2020-02-07 12:52:08', 1, 'VIP EVENING', 0, 0, 2, 1, '', '', 1),
(6, '2020-02-07 12:55:44', 1, 'GENERAL EVENING', 0, 0, 3, 1, '', '', 1),
(7, '2020-02-07 17:47:10', 2, 'TEST', 0, 0, 1, 1, '1020190000', '1020191199', 1);

-- --------------------------------------------------------

--
-- Table structure for table `scanpriticketlist`
--

CREATE TABLE `scanpriticketlist` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `scompany_id` tinyint(4) NOT NULL,
  `ticket_category_id` int(11) DEFAULT NULL,
  `category_name_pri` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `barcode_no` varchar(255) COLLATE utf8_bin NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1-active, 2-deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `scanticketlist`
--

CREATE TABLE `scanticketlist` (
  `id` int(11) NOT NULL,
  `api_hit_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `m_timestamp` text COLLATE utf8_bin,
  `scompany_id` int(11) NOT NULL,
  `device_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `scanner_type` int(11) NOT NULL,
  `ticket_category_id` int(11) NOT NULL,
  `ticket_subcategory_id` int(11) NOT NULL DEFAULT '0',
  `barcode_no` varchar(255) COLLATE utf8_bin NOT NULL,
  `gate_no` varchar(255) COLLATE utf8_bin NOT NULL,
  `hit_count` tinyint(4) NOT NULL,
  `duplicate_hit_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `duplicate_hit_count` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL COMMENT '1-active, 2-deactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `scanticketlist`
--

INSERT INTO `scanticketlist` (`id`, `api_hit_timestamp`, `created_at`, `m_timestamp`, `scompany_id`, `device_id`, `scanner_type`, `ticket_category_id`, `ticket_subcategory_id`, `barcode_no`, `gate_no`, `hit_count`, `duplicate_hit_datetime`, `duplicate_hit_count`, `status`) VALUES
(11, '2020-02-17 16:55:45', '2020-02-17 16:55:45', '17-Feb-2020 4:55:45 PM@17-Feb-2020 4:55:45 PM@T2', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190449', '1', 1, '2020-02-17 16:55:45', 0, 1),
(12, '2020-02-17 16:55:54', '2020-02-17 16:55:54', '17-Feb-2020 4:55:53 PM@17-Feb-2020 4:55:53 PM@17-Feb-2020 4:55:45 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190383', '1', 1, '2020-02-17 16:55:54', 0, 1),
(13, '2020-02-17 16:56:03', '2020-02-17 16:56:03', '17-Feb-2020 4:56:02 PM@17-Feb-2020 4:56:02 PM@17-Feb-2020 4:55:54 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190127', '1', 1, '2020-02-17 16:56:03', 0, 1),
(14, '2020-02-17 16:56:08', '2020-02-17 16:56:08', '17-Feb-2020 4:56:07 PM@17-Feb-2020 4:56:07 PM@17-Feb-2020 4:56:03 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190083', '1', 1, '2020-02-17 16:56:08', 0, 1),
(15, '2020-02-17 16:56:12', '2020-02-17 16:56:12', 'Feb 17, 2020 4:56:12 PM@Feb 17, 2020 4:56:12 PM@T2', 2, '10b1183697ff0906', 2, 7, 7, '1020190416', '1', 1, '2020-02-17 16:56:12', 0, 1),
(16, '2020-02-17 16:56:13', '2020-02-17 16:56:13', '17-Feb-2020 4:56:13 PM@17-Feb-2020 4:56:13 PM@17-Feb-2020 4:56:08 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190128', '1', 1, '2020-02-17 16:56:13', 0, 1),
(17, '2020-02-17 16:56:18', '2020-02-17 16:56:18', '17-Feb-2020 4:56:17 PM@17-Feb-2020 4:56:17 PM@17-Feb-2020 4:56:13 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190126', '1', 1, '2020-02-17 16:56:18', 0, 1),
(18, '2020-02-17 16:56:22', '2020-02-17 16:56:22', '17-Feb-2020 4:56:21 PM@17-Feb-2020 4:56:21 PM@17-Feb-2020 4:56:17 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190339', '1', 1, '2020-02-17 16:56:22', 0, 1),
(19, '2020-02-17 16:56:30', '2020-02-17 16:56:30', '17-Feb-2020 4:56:29 PM@17-Feb-2020 4:56:29 PM@17-Feb-2020 4:56:22 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190134', '1', 1, '2020-02-17 16:56:30', 0, 1),
(20, '2020-02-17 16:56:30', '2020-02-17 16:56:30', 'Feb 17, 2020 4:56:29 PM@Feb 17, 2020 4:56:29 PM@Feb 17, 2020 4:56:12 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190317', '1', 1, '2020-02-17 16:56:30', 0, 1),
(21, '2020-02-17 16:56:35', '2020-02-17 16:56:35', '17-Feb-2020 4:56:34 PM@17-Feb-2020 4:56:34 PM@17-Feb-2020 4:56:30 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190074', '1', 1, '2020-02-17 16:56:35', 0, 1),
(22, '2020-02-17 16:56:37', '2020-02-17 16:56:37', 'Feb 17, 2020 4:56:37 PM@Feb 17, 2020 4:56:37 PM@Feb 17, 2020 4:56:30 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190348', '1', 1, '2020-02-17 16:56:37', 0, 1),
(23, '2020-02-17 16:56:41', '2020-02-17 16:56:41', '17-Feb-2020 4:56:40 PM@17-Feb-2020 4:56:40 PM@17-Feb-2020 4:56:35 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190118', '1', 1, '2020-02-17 16:56:41', 0, 1),
(24, '2020-02-17 16:56:43', '2020-02-17 16:56:43', 'Feb 17, 2020 4:56:42 PM@Feb 17, 2020 4:56:42 PM@Feb 17, 2020 4:56:37 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190196', '1', 1, '2020-02-17 16:56:43', 0, 1),
(25, '2020-02-17 16:56:45', '2020-02-17 16:56:45', '17-Feb-2020 4:56:44 PM@17-Feb-2020 4:56:45 PM@17-Feb-2020 4:56:41 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190119', '1', 1, '2020-02-17 16:56:45', 0, 1),
(26, '2020-02-17 16:56:46', '2020-02-17 16:56:46', 'Feb 17, 2020 4:56:45 PM@Feb 17, 2020 4:56:45 PM@Feb 17, 2020 4:56:42 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190202', '1', 1, '2020-02-17 16:56:46', 0, 1),
(27, '2020-02-17 16:56:49', '2020-02-17 16:56:49', 'Feb 17, 2020 4:56:48 PM@Feb 17, 2020 4:56:48 PM@Feb 17, 2020 4:56:45 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190182', '1', 1, '2020-02-17 16:56:49', 0, 1),
(28, '2020-02-17 16:56:50', '2020-02-17 16:56:50', '17-Feb-2020 4:56:50 PM@17-Feb-2020 4:56:50 PM@17-Feb-2020 4:56:45 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190072', '1', 1, '2020-02-17 16:56:50', 0, 1),
(29, '2020-02-17 16:56:52', '2020-02-17 16:56:52', 'Feb 17, 2020 4:56:52 PM@Feb 17, 2020 4:56:52 PM@Feb 17, 2020 4:56:49 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190185', '1', 1, '2020-02-17 16:56:52', 0, 1),
(30, '2020-02-17 16:56:55', '2020-02-17 16:56:55', '17-Feb-2020 4:56:55 PM@17-Feb-2020 4:56:55 PM@17-Feb-2020 4:56:50 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190073', '1', 1, '2020-02-17 16:56:55', 0, 1),
(31, '2020-02-17 16:56:55', '2020-02-17 16:56:55', 'Feb 17, 2020 4:56:54 PM@Feb 17, 2020 4:56:54 PM@Feb 17, 2020 4:56:52 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190203', '1', 1, '2020-02-17 16:56:55', 0, 1),
(32, '2020-02-17 16:56:58', '2020-02-17 16:56:58', 'Feb 17, 2020 4:56:57 PM@Feb 17, 2020 4:56:57 PM@Feb 17, 2020 4:56:55 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190162', '1', 1, '2020-02-17 16:56:58', 0, 1),
(33, '2020-02-17 16:56:59', '2020-02-17 16:56:59', '17-Feb-2020 4:56:58 PM@17-Feb-2020 4:56:58 PM@17-Feb-2020 4:56:55 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190115', '1', 1, '2020-02-17 16:56:59', 0, 1),
(34, '2020-02-17 16:57:01', '2020-02-17 16:57:01', 'Feb 17, 2020 4:57:00 PM@Feb 17, 2020 4:57:00 PM@Feb 17, 2020 4:56:57 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190165', '1', 1, '2020-02-17 16:57:01', 0, 1),
(35, '2020-02-17 16:57:02', '2020-02-17 16:57:02', '17-Feb-2020 4:57:02 PM@17-Feb-2020 4:57:02 PM@17-Feb-2020 4:56:59 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190084', '1', 1, '2020-02-17 16:57:02', 0, 1),
(36, '2020-02-17 16:57:03', '2020-02-17 16:57:03', 'Feb 17, 2020 4:57:03 PM@Feb 17, 2020 4:57:03 PM@Feb 17, 2020 4:57:00 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190177', '1', 1, '2020-02-17 16:57:03', 0, 1),
(37, '2020-02-17 16:57:06', '2020-02-17 16:57:06', 'Feb 17, 2020 4:57:06 PM@Feb 17, 2020 4:57:06 PM@Feb 17, 2020 4:57:03 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190163', '1', 1, '2020-02-17 16:57:06', 0, 1),
(38, '2020-02-17 16:57:07', '2020-02-17 16:57:07', '17-Feb-2020 4:57:06 PM@17-Feb-2020 4:57:06 PM@17-Feb-2020 4:57:02 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190085', '1', 1, '2020-02-17 16:57:07', 0, 1),
(39, '2020-02-17 16:57:09', '2020-02-17 16:57:09', 'Feb 17, 2020 4:57:08 PM@Feb 17, 2020 4:57:08 PM@Feb 17, 2020 4:57:06 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190166', '1', 1, '2020-02-17 16:57:09', 0, 1),
(40, '2020-02-17 16:57:11', '2020-02-17 16:57:11', '17-Feb-2020 4:57:10 PM@17-Feb-2020 4:57:11 PM@17-Feb-2020 4:57:07 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190086', '1', 1, '2020-02-17 16:57:11', 0, 1),
(41, '2020-02-17 16:57:12', '2020-02-17 16:57:12', 'Feb 17, 2020 4:57:12 PM@Feb 17, 2020 4:57:12 PM@Feb 17, 2020 4:57:09 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190178', '1', 1, '2020-02-17 16:57:12', 0, 1),
(42, '2020-02-17 16:57:15', '2020-02-17 16:57:15', 'Feb 17, 2020 4:57:14 PM@Feb 17, 2020 4:57:14 PM@Feb 17, 2020 4:57:12 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190006', '1', 1, '2020-02-17 16:57:15', 0, 1),
(43, '2020-02-17 16:57:15', '2020-02-17 16:57:15', '17-Feb-2020 4:57:15 PM@17-Feb-2020 4:57:15 PM@17-Feb-2020 4:57:11 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190130', '1', 1, '2020-02-17 16:57:15', 0, 1),
(44, '2020-02-17 16:57:18', '2020-02-17 16:57:18', 'Feb 17, 2020 4:57:17 PM@Feb 17, 2020 4:57:17 PM@Feb 17, 2020 4:57:15 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190434', '1', 1, '2020-02-17 16:57:18', 0, 1),
(45, '2020-02-17 16:57:19', '2020-02-17 16:57:19', '17-Feb-2020 4:57:19 PM@17-Feb-2020 4:57:19 PM@17-Feb-2020 4:57:15 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190131', '1', 1, '2020-02-17 16:57:19', 0, 1),
(46, '2020-02-17 16:57:20', '2020-02-17 16:57:20', 'Feb 17, 2020 4:57:20 PM@Feb 17, 2020 4:57:20 PM@Feb 17, 2020 4:57:17 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190417', '1', 1, '2020-02-17 16:57:20', 0, 1),
(47, '2020-02-17 16:57:23', '2020-02-17 16:57:23', '17-Feb-2020 4:57:22 PM@17-Feb-2020 4:57:22 PM@17-Feb-2020 4:57:19 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190129', '1', 1, '2020-02-17 16:57:23', 0, 1),
(48, '2020-02-17 16:57:23', '2020-02-17 16:57:23', 'Feb 17, 2020 4:57:22 PM@Feb 17, 2020 4:57:22 PM@Feb 17, 2020 4:57:20 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190215', '1', 1, '2020-02-17 16:57:23', 0, 1),
(49, '2020-02-17 16:57:26', '2020-02-17 16:57:26', 'Feb 17, 2020 4:57:25 PM@Feb 17, 2020 4:57:25 PM@Feb 17, 2020 4:57:22 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190408', '1', 1, '2020-02-17 16:57:26', 0, 1),
(50, '2020-02-17 16:57:26', '2020-02-17 16:57:26', '17-Feb-2020 4:57:26 PM@17-Feb-2020 4:57:26 PM@17-Feb-2020 4:57:22 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190058', '1', 1, '2020-02-17 16:57:26', 0, 1),
(51, '2020-02-17 16:57:29', '2020-02-17 16:57:29', 'Feb 17, 2020 4:57:28 PM@Feb 17, 2020 4:57:28 PM@Feb 17, 2020 4:57:26 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190273', '1', 1, '2020-02-17 16:57:29', 0, 1),
(52, '2020-02-17 16:57:30', '2020-02-17 16:57:30', '17-Feb-2020 4:57:29 PM@17-Feb-2020 4:57:29 PM@17-Feb-2020 4:57:26 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190079', '1', 1, '2020-02-17 16:57:30', 0, 1),
(53, '2020-02-17 16:57:32', '2020-02-17 16:57:32', 'Feb 17, 2020 4:57:32 PM@Feb 17, 2020 4:57:32 PM@Feb 17, 2020 4:57:29 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190414', '1', 1, '2020-02-17 16:57:32', 0, 1),
(54, '2020-02-17 16:57:34', '2020-02-17 16:57:34', '17-Feb-2020 4:57:34 PM@17-Feb-2020 4:57:34 PM@17-Feb-2020 4:57:30 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190352', '1', 1, '2020-02-17 16:57:34', 0, 1),
(55, '2020-02-17 16:57:35', '2020-02-17 16:57:35', 'Feb 17, 2020 4:57:34 PM@Feb 17, 2020 4:57:34 PM@Feb 17, 2020 4:57:32 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190422', '1', 1, '2020-02-17 16:57:35', 0, 1),
(56, '2020-02-17 16:57:38', '2020-02-17 16:57:38', 'Feb 17, 2020 4:57:37 PM@Feb 17, 2020 4:57:37 PM@Feb 17, 2020 4:57:35 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190403', '1', 1, '2020-02-17 16:57:38', 0, 1),
(57, '2020-02-17 16:57:39', '2020-02-17 16:57:39', '17-Feb-2020 4:57:38 PM@17-Feb-2020 4:57:38 PM@17-Feb-2020 4:57:34 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190056', '1', 1, '2020-02-17 16:57:39', 0, 1),
(58, '2020-02-17 16:57:40', '2020-02-17 16:57:40', 'Feb 17, 2020 4:57:40 PM@Feb 17, 2020 4:57:40 PM@Feb 17, 2020 4:57:37 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190413', '1', 1, '2020-02-17 16:57:40', 0, 1),
(59, '2020-02-17 16:57:42', '2020-02-17 16:57:42', '17-Feb-2020 4:57:42 PM@17-Feb-2020 4:57:42 PM@17-Feb-2020 4:57:39 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190062', '1', 1, '2020-02-17 16:57:42', 0, 1),
(60, '2020-02-17 16:57:46', '2020-02-17 16:57:46', '17-Feb-2020 4:57:46 PM@17-Feb-2020 4:57:46 PM@17-Feb-2020 4:57:42 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190055', '1', 1, '2020-02-17 16:57:46', 0, 1),
(61, '2020-02-17 16:57:52', '2020-02-17 16:57:52', '17-Feb-2020 4:57:51 PM@17-Feb-2020 4:57:51 PM@17-Feb-2020 4:57:46 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190057', '1', 1, '2020-02-17 16:57:52', 0, 1),
(62, '2020-02-17 16:57:56', '2020-02-17 16:57:56', 'Feb 17, 2020 4:57:56 PM@Feb 17, 2020 4:57:56 PM@Feb 17, 2020 4:57:40 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190415', '1', 1, '2020-02-17 16:57:56', 0, 1),
(63, '2020-02-17 16:57:56', '2020-02-17 16:57:56', '17-Feb-2020 4:57:56 PM@17-Feb-2020 4:57:56 PM@17-Feb-2020 4:57:52 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190104', '1', 1, '2020-02-17 16:57:56', 0, 1),
(64, '2020-02-17 16:58:02', '2020-02-17 16:58:02', '17-Feb-2020 4:58:02 PM@17-Feb-2020 4:58:02 PM@17-Feb-2020 4:57:56 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190103', '1', 1, '2020-02-17 16:58:02', 0, 1),
(65, '2020-02-17 16:58:06', '2020-02-17 16:58:06', 'Feb 17, 2020 4:58:06 PM@Feb 17, 2020 4:58:06 PM@Feb 17, 2020 4:57:56 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190418', '1', 1, '2020-02-17 16:58:06', 0, 1),
(66, '2020-02-17 16:58:07', '2020-02-17 16:58:07', '17-Feb-2020 4:58:06 PM@17-Feb-2020 4:58:06 PM@17-Feb-2020 4:58:02 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190102', '1', 1, '2020-02-17 16:58:07', 0, 1),
(67, '2020-02-17 16:58:10', '2020-02-17 16:58:10', 'Feb 17, 2020 4:58:09 PM@Feb 17, 2020 4:58:09 PM@Feb 17, 2020 4:58:06 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190287', '1', 1, '2020-02-17 16:58:10', 0, 1),
(68, '2020-02-17 16:58:10', '2020-02-17 16:58:10', '17-Feb-2020 4:58:10 PM@17-Feb-2020 4:58:10 PM@17-Feb-2020 4:58:07 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190149', '1', 1, '2020-02-17 16:58:10', 0, 1),
(69, '2020-02-17 16:58:13', '2020-02-17 16:58:13', 'Feb 17, 2020 4:58:12 PM@Feb 17, 2020 4:58:12 PM@Feb 17, 2020 4:58:09 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190175', '1', 1, '2020-02-17 16:58:13', 0, 1),
(70, '2020-02-17 16:58:15', '2020-02-17 16:58:15', '17-Feb-2020 4:58:15 PM@17-Feb-2020 4:58:15 PM@17-Feb-2020 4:58:10 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190147', '1', 1, '2020-02-17 16:58:15', 0, 1),
(71, '2020-02-17 16:58:16', '2020-02-17 16:58:16', 'Feb 17, 2020 4:58:15 PM@Feb 17, 2020 4:58:15 PM@Feb 17, 2020 4:58:12 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190409', '1', 1, '2020-02-17 16:58:16', 0, 1),
(72, '2020-02-17 16:58:18', '2020-02-17 16:58:18', 'Feb 17, 2020 4:58:18 PM@Feb 17, 2020 4:58:18 PM@Feb 17, 2020 4:58:15 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190172', '1', 1, '2020-02-17 16:58:18', 0, 1),
(73, '2020-02-17 16:58:19', '2020-02-17 16:58:19', '17-Feb-2020 4:58:19 PM@17-Feb-2020 4:58:19 PM@17-Feb-2020 4:58:15 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190148', '1', 1, '2020-02-17 16:58:19', 0, 1),
(74, '2020-02-17 16:58:21', '2020-02-17 16:58:21', 'Feb 17, 2020 4:58:21 PM@Feb 17, 2020 4:58:21 PM@Feb 17, 2020 4:58:18 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190430', '1', 1, '2020-02-17 16:58:21', 0, 1),
(75, '2020-02-17 16:58:23', '2020-02-17 16:58:23', '17-Feb-2020 4:58:23 PM@17-Feb-2020 4:58:23 PM@17-Feb-2020 4:58:19 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190394', '1', 1, '2020-02-17 16:58:23', 0, 1),
(76, '2020-02-17 16:58:25', '2020-02-17 16:58:25', 'Feb 17, 2020 4:58:25 PM@Feb 17, 2020 4:58:25 PM@Feb 17, 2020 4:58:21 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190286', '1', 1, '2020-02-17 16:58:25', 0, 1),
(77, '2020-02-17 16:58:27', '2020-02-17 16:58:27', '17-Feb-2020 4:58:26 PM@17-Feb-2020 4:58:27 PM@17-Feb-2020 4:58:23 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190426', '1', 1, '2020-02-17 16:58:27', 0, 1),
(78, '2020-02-17 16:58:28', '2020-02-17 16:58:28', 'Feb 17, 2020 4:58:28 PM@Feb 17, 2020 4:58:28 PM@Feb 17, 2020 4:58:25 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190395', '1', 1, '2020-02-17 16:58:31', 1, 1),
(79, '2020-02-17 16:58:30', '2020-02-17 16:58:31', '17-Feb-2020 4:58:30 PM@17-Feb-2020 4:58:30 PM@17-Feb-2020 4:58:27 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190412', '1', 1, '2020-02-17 16:58:31', 0, 1),
(80, '2020-02-17 16:58:35', '2020-02-17 16:58:35', 'Feb 17, 2020 4:58:34 PM@Feb 17, 2020 4:58:34 PM@Feb 17, 2020 4:58:31 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190241', '1', 1, '2020-02-17 16:58:35', 0, 1),
(81, '2020-02-17 16:58:40', '2020-02-17 16:58:40', '17-Feb-2020 4:58:39 PM@17-Feb-2020 4:58:39 PM@17-Feb-2020 4:58:30 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190059', '1', 1, '2020-02-17 16:58:40', 0, 1),
(82, '2020-02-17 16:58:40', '2020-02-17 16:58:40', 'Feb 17, 2020 4:58:39 PM@Feb 17, 2020 4:58:39 PM@Feb 17, 2020 4:58:34 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190252', '1', 1, '2020-02-17 16:58:40', 0, 1),
(83, '2020-02-17 16:58:43', '2020-02-17 16:58:43', 'Feb 17, 2020 4:58:42 PM@Feb 17, 2020 4:58:42 PM@Feb 17, 2020 4:58:39 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190206', '1', 1, '2020-02-17 16:58:43', 0, 1),
(84, '2020-02-17 16:58:46', '2020-02-17 16:58:46', 'Feb 17, 2020 4:58:45 PM@Feb 17, 2020 4:58:45 PM@Feb 17, 2020 4:58:42 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190223', '1', 1, '2020-02-17 16:58:46', 0, 1),
(85, '2020-02-17 16:58:50', '2020-02-17 16:58:50', '17-Feb-2020 4:58:50 PM@17-Feb-2020 4:58:50 PM@17-Feb-2020 4:58:44 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190364', '1', 1, '2020-02-17 16:58:50', 0, 1),
(86, '2020-02-17 16:58:52', '2020-02-17 16:58:52', 'Feb 17, 2020 4:58:51 PM@Feb 17, 2020 4:58:51 PM@Feb 17, 2020 4:58:46 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190159', '1', 1, '2020-02-17 16:58:52', 0, 1),
(87, '2020-02-17 16:58:55', '2020-02-17 16:58:55', '17-Feb-2020 4:58:54 PM@17-Feb-2020 4:58:54 PM@17-Feb-2020 4:58:50 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190374', '1', 1, '2020-02-17 16:58:55', 0, 1),
(88, '2020-02-17 16:58:55', '2020-02-17 16:58:55', 'Feb 17, 2020 4:58:54 PM@Feb 17, 2020 4:58:54 PM@Feb 17, 2020 4:58:52 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190249', '1', 1, '2020-02-17 16:58:55', 0, 1),
(89, '2020-02-17 16:58:59', '2020-02-17 16:58:59', 'Feb 17, 2020 4:58:58 PM@Feb 17, 2020 4:58:58 PM@Feb 17, 2020 4:58:55 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190256', '1', 1, '2020-02-17 16:58:59', 0, 1),
(90, '2020-02-17 16:58:59', '2020-02-17 16:58:59', '17-Feb-2020 4:58:59 PM@17-Feb-2020 4:58:59 PM@17-Feb-2020 4:58:55 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190404', '1', 1, '2020-02-17 16:58:59', 0, 1),
(91, '2020-02-17 16:59:02', '2020-02-17 16:59:02', 'Feb 17, 2020 4:59:01 PM@Feb 17, 2020 4:59:01 PM@Feb 17, 2020 4:58:58 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190313', '1', 1, '2020-02-17 16:59:02', 0, 1),
(92, '2020-02-17 16:59:03', '2020-02-17 16:59:03', '17-Feb-2020 4:59:02 PM@17-Feb-2020 4:59:02 PM@17-Feb-2020 4:58:59 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190425', '1', 1, '2020-02-17 16:59:03', 0, 1),
(93, '2020-02-17 16:59:05', '2020-02-17 16:59:05', 'Feb 17, 2020 4:59:04 PM@Feb 17, 2020 4:59:04 PM@Feb 17, 2020 4:59:02 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190390', '1', 1, '2020-02-17 16:59:05', 0, 1),
(94, '2020-02-17 16:59:07', '2020-02-17 16:59:07', '17-Feb-2020 4:59:07 PM@17-Feb-2020 4:59:07 PM@17-Feb-2020 4:59:03 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190401', '1', 1, '2020-02-17 16:59:07', 0, 1),
(95, '2020-02-17 16:59:08', '2020-02-17 16:59:08', 'Feb 17, 2020 4:59:07 PM@Feb 17, 2020 4:59:07 PM@Feb 17, 2020 4:59:05 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190309', '1', 1, '2020-02-17 16:59:08', 0, 1),
(96, '2020-02-17 16:59:11', '2020-02-17 16:59:11', '17-Feb-2020 4:59:11 PM@17-Feb-2020 4:59:11 PM@17-Feb-2020 4:59:07 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190078', '1', 1, '2020-02-17 16:59:11', 0, 1),
(97, '2020-02-17 16:59:12', '2020-02-17 16:59:12', 'Feb 17, 2020 4:59:11 PM@Feb 17, 2020 4:59:11 PM@Feb 17, 2020 4:59:08 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190189', '1', 1, '2020-02-17 16:59:12', 0, 1),
(98, '2020-02-17 16:59:14', '2020-02-17 16:59:14', '17-Feb-2020 4:59:14 PM@17-Feb-2020 4:59:14 PM@17-Feb-2020 4:59:11 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190155', '1', 1, '2020-02-17 16:59:14', 0, 1),
(99, '2020-02-17 16:59:14', '2020-02-17 16:59:15', 'Feb 17, 2020 4:59:14 PM@Feb 17, 2020 4:59:14 PM@Feb 17, 2020 4:59:11 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190032', '1', 1, '2020-02-17 16:59:15', 0, 1),
(100, '2020-02-17 16:59:18', '2020-02-17 16:59:18', 'Feb 17, 2020 4:59:17 PM@Feb 17, 2020 4:59:17 PM@Feb 17, 2020 4:59:14 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190292', '1', 1, '2020-02-17 16:59:18', 0, 1),
(101, '2020-02-17 16:59:18', '2020-02-17 16:59:18', '17-Feb-2020 4:59:18 PM@17-Feb-2020 4:59:18 PM@17-Feb-2020 4:59:14 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190053', '1', 1, '2020-02-17 16:59:18', 0, 1),
(102, '2020-02-17 16:59:21', '2020-02-17 16:59:21', 'Feb 17, 2020 4:59:21 PM@Feb 17, 2020 4:59:21 PM@Feb 17, 2020 4:59:18 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190266', '1', 1, '2020-02-17 16:59:21', 0, 1),
(103, '2020-02-17 16:59:22', '2020-02-17 16:59:22', '17-Feb-2020 4:59:21 PM@17-Feb-2020 4:59:21 PM@17-Feb-2020 4:59:18 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190039', '1', 1, '2020-02-17 16:59:22', 0, 1),
(104, '2020-02-17 16:59:24', '2020-02-17 16:59:24', 'Feb 17, 2020 4:59:23 PM@Feb 17, 2020 4:59:23 PM@Feb 17, 2020 4:59:21 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190291', '1', 1, '2020-02-17 16:59:24', 0, 1),
(105, '2020-02-17 16:59:27', '2020-02-17 16:59:27', 'Feb 17, 2020 4:59:27 PM@Feb 17, 2020 4:59:27 PM@Feb 17, 2020 4:59:24 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190186', '1', 1, '2020-02-17 16:59:27', 0, 1),
(106, '2020-02-17 16:59:31', '2020-02-17 16:59:31', '17-Feb-2020 4:59:30 PM@17-Feb-2020 4:59:30 PM@17-Feb-2020 4:59:26 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190052', '1', 1, '2020-02-17 16:59:31', 0, 1),
(107, '2020-02-17 16:59:31', '2020-02-17 16:59:31', 'Feb 17, 2020 4:59:30 PM@Feb 17, 2020 4:59:30 PM@Feb 17, 2020 4:59:27 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190222', '1', 1, '2020-02-17 16:59:31', 0, 1),
(108, '2020-02-17 16:59:35', '2020-02-17 16:59:35', 'Feb 17, 2020 4:59:34 PM@Feb 17, 2020 4:59:34 PM@Feb 17, 2020 4:59:31 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190205', '1', 1, '2020-02-17 16:59:35', 0, 1),
(109, '2020-02-17 16:59:35', '2020-02-17 16:59:35', '17-Feb-2020 4:59:34 PM@17-Feb-2020 4:59:34 PM@17-Feb-2020 4:59:31 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190051', '1', 1, '2020-02-17 16:59:35', 0, 1),
(110, '2020-02-17 16:59:38', '2020-02-17 16:59:38', 'Feb 17, 2020 4:59:37 PM@Feb 17, 2020 4:59:37 PM@Feb 17, 2020 4:59:34 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190231', '1', 1, '2020-02-17 16:59:38', 0, 1),
(111, '2020-02-17 16:59:40', '2020-02-17 16:59:40', '17-Feb-2020 4:59:40 PM@17-Feb-2020 4:59:40 PM@17-Feb-2020 4:59:35 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190112', '1', 1, '2020-02-17 16:59:40', 0, 1),
(112, '2020-02-17 16:59:42', '2020-02-17 16:59:42', 'Feb 17, 2020 4:59:41 PM@Feb 17, 2020 4:59:41 PM@Feb 17, 2020 4:59:37 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190031', '1', 1, '2020-02-17 16:59:42', 0, 1),
(113, '2020-02-17 16:59:45', '2020-02-17 16:59:45', 'Feb 17, 2020 4:59:44 PM@Feb 17, 2020 4:59:44 PM@Feb 17, 2020 4:59:41 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190243', '1', 1, '2020-02-17 16:59:45', 0, 1),
(114, '2020-02-17 16:59:45', '2020-02-17 16:59:45', '17-Feb-2020 4:59:45 PM@17-Feb-2020 4:59:45 PM@17-Feb-2020 4:59:40 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190111', '1', 1, '2020-02-17 16:59:45', 0, 1),
(115, '2020-02-17 16:59:49', '2020-02-17 16:59:49', '17-Feb-2020 4:59:49 PM@17-Feb-2020 4:59:49 PM@17-Feb-2020 4:59:45 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190066', '1', 1, '2020-02-17 16:59:49', 0, 1),
(116, '2020-02-17 16:59:49', '2020-02-17 16:59:49', 'Feb 17, 2020 4:59:49 PM@Feb 17, 2020 4:59:49 PM@Feb 17, 2020 4:59:45 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190319', '1', 1, '2020-02-17 16:59:49', 0, 1),
(117, '2020-02-17 16:59:53', '2020-02-17 16:59:53', '17-Feb-2020 4:59:52 PM@17-Feb-2020 4:59:52 PM@17-Feb-2020 4:59:49 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190113', '1', 1, '2020-02-17 16:59:53', 0, 1),
(118, '2020-02-17 16:59:54', '2020-02-17 16:59:54', 'Feb 17, 2020 4:59:53 PM@Feb 17, 2020 4:59:53 PM@Feb 17, 2020 4:59:49 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190301', '1', 1, '2020-02-17 16:59:54', 0, 1),
(119, '2020-02-17 16:59:57', '2020-02-17 16:59:57', '17-Feb-2020 4:59:57 PM@17-Feb-2020 4:59:57 PM@17-Feb-2020 4:59:53 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190067', '1', 1, '2020-02-17 16:59:57', 0, 1),
(120, '2020-02-17 16:59:57', '2020-02-17 16:59:57', 'Feb 17, 2020 4:59:57 PM@Feb 17, 2020 4:59:57 PM@Feb 17, 2020 4:59:54 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190323', '1', 1, '2020-02-17 16:59:57', 0, 1),
(121, '2020-02-17 17:00:01', '2020-02-17 17:00:01', 'Feb 17, 2020 5:00:00 PM@Feb 17, 2020 5:00:00 PM@Feb 17, 2020 4:59:57 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190230', '1', 1, '2020-02-17 17:00:01', 0, 1),
(122, '2020-02-17 17:00:01', '2020-02-17 17:00:01', '17-Feb-2020 5:00:01 PM@17-Feb-2020 5:00:01 PM@17-Feb-2020 4:59:57 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190068', '1', 1, '2020-02-17 17:00:01', 0, 1),
(123, '2020-02-17 17:00:04', '2020-02-17 17:00:04', 'Feb 17, 2020 5:00:03 PM@Feb 17, 2020 5:00:03 PM@Feb 17, 2020 5:00:01 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190225', '1', 1, '2020-02-17 17:00:04', 0, 1),
(124, '2020-02-17 17:00:05', '2020-02-17 17:00:05', '17-Feb-2020 5:00:04 PM@17-Feb-2020 5:00:04 PM@17-Feb-2020 5:00:01 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190153', '1', 1, '2020-02-17 17:00:05', 0, 1),
(125, '2020-02-17 17:00:07', '2020-02-17 17:00:07', 'Feb 17, 2020 5:00:06 PM@Feb 17, 2020 5:00:06 PM@Feb 17, 2020 5:00:03 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190248', '1', 1, '2020-02-17 17:00:07', 0, 1),
(126, '2020-02-17 17:00:09', '2020-02-17 17:00:09', '17-Feb-2020 5:00:08 PM@17-Feb-2020 5:00:08 PM@17-Feb-2020 5:00:05 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190098', '1', 1, '2020-02-17 17:00:09', 0, 1),
(127, '2020-02-17 17:00:11', '2020-02-17 17:00:11', 'Feb 17, 2020 5:00:10 PM@Feb 17, 2020 5:00:10 PM@Feb 17, 2020 5:00:07 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190210', '1', 1, '2020-02-17 17:00:11', 0, 1),
(128, '2020-02-17 17:00:14', '2020-02-17 17:00:14', 'Feb 17, 2020 5:00:13 PM@Feb 17, 2020 5:00:13 PM@Feb 17, 2020 5:00:11 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190219', '1', 1, '2020-02-17 17:00:14', 0, 1),
(129, '2020-02-17 17:00:14', '2020-02-17 17:00:14', '17-Feb-2020 5:00:14 PM@17-Feb-2020 5:00:14 PM@17-Feb-2020 5:00:08 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190435', '1', 1, '2020-02-17 17:00:14', 0, 1),
(130, '2020-02-17 17:00:18', '2020-02-17 17:00:18', 'Feb 17, 2020 5:00:17 PM@Feb 17, 2020 5:00:17 PM@Feb 17, 2020 5:00:14 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190236', '1', 1, '2020-02-17 17:00:18', 0, 1),
(131, '2020-02-17 17:00:18', '2020-02-17 17:00:18', '17-Feb-2020 5:00:17 PM@17-Feb-2020 5:00:17 PM@17-Feb-2020 5:00:14 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190096', '1', 1, '2020-02-17 17:00:18', 0, 1),
(132, '2020-02-17 17:00:21', '2020-02-17 17:00:21', 'Feb 17, 2020 5:00:21 PM@Feb 17, 2020 5:00:21 PM@Feb 17, 2020 5:00:17 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190228', '1', 1, '2020-02-17 17:00:21', 0, 1),
(133, '2020-02-17 17:00:22', '2020-02-17 17:00:22', '17-Feb-2020 5:00:21 PM@17-Feb-2020 5:00:21 PM@17-Feb-2020 5:00:18 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190117', '1', 1, '2020-02-17 17:00:22', 0, 1),
(134, '2020-02-17 17:00:24', '2020-02-17 17:00:24', 'Feb 17, 2020 5:00:24 PM@Feb 17, 2020 5:00:24 PM@Feb 17, 2020 5:00:21 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190092', '1', 1, '2020-02-17 17:00:24', 0, 1),
(135, '2020-02-17 17:00:28', '2020-02-17 17:00:28', 'Feb 17, 2020 5:00:27 PM@Feb 17, 2020 5:00:27 PM@Feb 17, 2020 5:00:24 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190070', '1', 1, '2020-02-17 17:00:28', 0, 1),
(136, '2020-02-17 17:00:28', '2020-02-17 17:00:28', '17-Feb-2020 5:00:28 PM@17-Feb-2020 5:00:28 PM@17-Feb-2020 5:00:22 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190356', '1', 1, '2020-02-17 17:00:28', 0, 1),
(137, '2020-02-17 17:00:31', '2020-02-17 17:00:31', 'Feb 17, 2020 5:00:30 PM@Feb 17, 2020 5:00:30 PM@Feb 17, 2020 5:00:28 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190251', '1', 1, '2020-02-17 17:08:28', 3, 1),
(138, '2020-02-17 17:00:33', '2020-02-17 17:00:33', '17-Feb-2020 5:00:32 PM@17-Feb-2020 5:00:32 PM@17-Feb-2020 5:00:28 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190353', '1', 1, '2020-02-17 17:00:33', 0, 1),
(139, '2020-02-17 17:00:33', '2020-02-17 17:00:33', 'Feb 17, 2020 5:00:33 PM@Feb 17, 2020 5:00:33 PM@Feb 17, 2020 5:00:31 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190232', '1', 1, '2020-02-17 17:00:37', 1, 1),
(140, '2020-02-17 17:00:38', '2020-02-17 17:00:38', '17-Feb-2020 5:00:37 PM@17-Feb-2020 5:00:37 PM@17-Feb-2020 5:00:33 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190396', '1', 1, '2020-02-17 17:00:38', 0, 1),
(141, '2020-02-17 17:00:39', '2020-02-17 17:00:39', 'Feb 17, 2020 5:00:39 PM@Feb 17, 2020 5:00:39 PM@Feb 17, 2020 5:00:36 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190183', '1', 1, '2020-02-17 17:00:39', 0, 1),
(142, '2020-02-17 17:00:41', '2020-02-17 17:00:41', '17-Feb-2020 5:00:41 PM@17-Feb-2020 5:00:41 PM@17-Feb-2020 5:00:37 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190399', '1', 1, '2020-02-17 17:00:41', 0, 1),
(143, '2020-02-17 17:00:43', '2020-02-17 17:00:43', 'Feb 17, 2020 5:00:42 PM@Feb 17, 2020 5:00:42 PM@Feb 17, 2020 5:00:39 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190268', '1', 1, '2020-02-17 17:00:43', 0, 1),
(144, '2020-02-17 17:00:45', '2020-02-17 17:00:45', '17-Feb-2020 5:00:45 PM@17-Feb-2020 5:00:45 PM@17-Feb-2020 5:00:41 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190402', '1', 1, '2020-02-17 17:00:45', 0, 1),
(145, '2020-02-17 17:00:45', '2020-02-17 17:00:45', 'Feb 17, 2020 5:00:45 PM@Feb 17, 2020 5:00:45 PM@Feb 17, 2020 5:00:42 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190263', '1', 1, '2020-02-17 17:00:45', 0, 1),
(146, '2020-02-17 17:00:48', '2020-02-17 17:00:48', 'Feb 17, 2020 5:00:48 PM@Feb 17, 2020 5:00:48 PM@Feb 17, 2020 5:00:45 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190254', '1', 1, '2020-02-17 17:00:48', 0, 1),
(147, '2020-02-17 17:00:49', '2020-02-17 17:00:49', '17-Feb-2020 5:00:49 PM@17-Feb-2020 5:00:49 PM@17-Feb-2020 5:00:45 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190087', '1', 1, '2020-02-17 17:00:49', 0, 1),
(148, '2020-02-17 17:00:51', '2020-02-17 17:00:51', 'Feb 17, 2020 5:00:51 PM@Feb 17, 2020 5:00:51 PM@Feb 17, 2020 5:00:48 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190310', '1', 1, '2020-02-17 17:00:51', 0, 1),
(149, '2020-02-17 17:00:53', '2020-02-17 17:00:53', '17-Feb-2020 5:00:52 PM@17-Feb-2020 5:00:52 PM@17-Feb-2020 5:00:49 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190088', '1', 1, '2020-02-17 17:00:53', 0, 1),
(150, '2020-02-17 17:00:55', '2020-02-17 17:00:55', 'Feb 17, 2020 5:00:54 PM@Feb 17, 2020 5:00:54 PM@Feb 17, 2020 5:00:51 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190238', '1', 1, '2020-02-17 17:00:55', 0, 1),
(151, '2020-02-17 17:00:57', '2020-02-17 17:00:57', '17-Feb-2020 5:00:56 PM@17-Feb-2020 5:00:56 PM@17-Feb-2020 5:00:53 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190089', '1', 1, '2020-02-17 17:00:57', 0, 1),
(152, '2020-02-17 17:01:00', '2020-02-17 17:01:00', '17-Feb-2020 5:00:59 PM@17-Feb-2020 5:00:59 PM@17-Feb-2020 5:00:56 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190133', '1', 1, '2020-02-17 17:01:00', 0, 1),
(153, '2020-02-17 17:01:03', '2020-02-17 17:01:03', '17-Feb-2020 5:01:03 PM@17-Feb-2020 5:01:03 PM@17-Feb-2020 5:01:00 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190384', '1', 1, '2020-02-17 17:01:03', 0, 1),
(154, '2020-02-17 17:01:05', '2020-02-17 17:01:05', 'Feb 17, 2020 5:01:04 PM@Feb 17, 2020 5:01:04 PM@Feb 17, 2020 5:01:01 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190071', '1', 1, '2020-02-17 17:01:05', 0, 1),
(155, '2020-02-17 17:01:07', '2020-02-17 17:01:07', '17-Feb-2020 5:01:07 PM@17-Feb-2020 5:01:07 PM@17-Feb-2020 5:01:03 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190293', '1', 1, '2020-02-17 17:01:07', 0, 1),
(156, '2020-02-17 17:01:08', '2020-02-17 17:01:08', 'Feb 17, 2020 5:01:08 PM@Feb 17, 2020 5:01:08 PM@Feb 17, 2020 5:01:05 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190255', '1', 1, '2020-02-17 17:01:08', 0, 1),
(157, '2020-02-17 17:01:11', '2020-02-17 17:01:11', '17-Feb-2020 5:01:10 PM@17-Feb-2020 5:01:10 PM@17-Feb-2020 5:01:07 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190380', '1', 1, '2020-02-17 17:01:11', 0, 1),
(158, '2020-02-17 17:01:12', '2020-02-17 17:01:12', 'Feb 17, 2020 5:01:12 PM@Feb 17, 2020 5:01:12 PM@Feb 17, 2020 5:01:08 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190391', '1', 1, '2020-02-17 17:01:12', 0, 1),
(159, '2020-02-17 17:01:14', '2020-02-17 17:01:14', '17-Feb-2020 5:01:14 PM@17-Feb-2020 5:01:14 PM@17-Feb-2020 5:01:11 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190368', '1', 1, '2020-02-17 17:01:14', 0, 1),
(160, '2020-02-17 17:01:16', '2020-02-17 17:01:16', 'Feb 17, 2020 5:01:15 PM@Feb 17, 2020 5:01:15 PM@Feb 17, 2020 5:01:12 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190269', '1', 1, '2020-02-17 17:01:16', 0, 1),
(161, '2020-02-17 17:01:18', '2020-02-17 17:01:18', '17-Feb-2020 5:01:18 PM@17-Feb-2020 5:01:18 PM@17-Feb-2020 5:01:14 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190377', '1', 1, '2020-02-17 17:01:18', 0, 1),
(162, '2020-02-17 17:01:19', '2020-02-17 17:01:19', 'Feb 17, 2020 5:01:18 PM@Feb 17, 2020 5:01:18 PM@Feb 17, 2020 5:01:15 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190234', '1', 1, '2020-02-17 17:01:19', 0, 1),
(163, '2020-02-17 17:01:22', '2020-02-17 17:01:22', 'Feb 17, 2020 5:01:21 PM@Feb 17, 2020 5:01:21 PM@Feb 17, 2020 5:01:19 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190258', '1', 1, '2020-02-17 17:01:22', 0, 1),
(164, '2020-02-17 17:01:23', '2020-02-17 17:01:23', '17-Feb-2020 5:01:22 PM@17-Feb-2020 5:01:22 PM@17-Feb-2020 5:01:18 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190357', '1', 1, '2020-02-17 17:01:23', 0, 1),
(165, '2020-02-17 17:01:25', '2020-02-17 17:01:25', 'Feb 17, 2020 5:01:25 PM@Feb 17, 2020 5:01:25 PM@Feb 17, 2020 5:01:22 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190209', '1', 1, '2020-02-17 17:01:25', 0, 1),
(166, '2020-02-17 17:01:27', '2020-02-17 17:01:27', '17-Feb-2020 5:01:26 PM@17-Feb-2020 5:01:26 PM@17-Feb-2020 5:01:23 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190365', '1', 1, '2020-02-17 17:01:27', 0, 1),
(167, '2020-02-17 17:01:29', '2020-02-17 17:01:29', 'Feb 17, 2020 5:01:28 PM@Feb 17, 2020 5:01:28 PM@Feb 17, 2020 5:01:25 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190004', '1', 1, '2020-02-17 17:01:29', 0, 1),
(168, '2020-02-17 17:01:31', '2020-02-17 17:01:31', '17-Feb-2020 5:01:30 PM@17-Feb-2020 5:01:30 PM@17-Feb-2020 5:01:27 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190350', '1', 1, '2020-02-17 17:01:31', 0, 1),
(169, '2020-02-17 17:01:32', '2020-02-17 17:01:32', 'Feb 17, 2020 5:01:31 PM@Feb 17, 2020 5:01:31 PM@Feb 17, 2020 5:01:28 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190271', '1', 1, '2020-02-17 17:01:32', 0, 1),
(170, '2020-02-17 17:01:35', '2020-02-17 17:01:35', '17-Feb-2020 5:01:35 PM@17-Feb-2020 5:01:35 PM@17-Feb-2020 5:01:31 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190354', '1', 1, '2020-02-17 17:01:35', 0, 1),
(171, '2020-02-17 17:01:35', '2020-02-17 17:01:35', 'Feb 17, 2020 5:01:35 PM@Feb 17, 2020 5:01:35 PM@Feb 17, 2020 5:01:32 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190220', '1', 1, '2020-02-17 17:01:35', 0, 1),
(172, '2020-02-17 17:01:39', '2020-02-17 17:01:39', 'Feb 17, 2020 5:01:38 PM@Feb 17, 2020 5:01:38 PM@Feb 17, 2020 5:01:35 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190285', '1', 1, '2020-02-17 17:01:39', 0, 1),
(173, '2020-02-17 17:01:40', '2020-02-17 17:01:40', '17-Feb-2020 5:01:40 PM@17-Feb-2020 5:01:40 PM@17-Feb-2020 5:01:35 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190345', '1', 1, '2020-02-17 17:01:40', 0, 1),
(174, '2020-02-17 17:01:42', '2020-02-17 17:01:42', 'Feb 17, 2020 5:01:41 PM@Feb 17, 2020 5:01:41 PM@Feb 17, 2020 5:01:38 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190260', '1', 1, '2020-02-17 17:01:42', 0, 1),
(175, '2020-02-17 17:01:43', '2020-02-17 17:01:43', '17-Feb-2020 5:01:43 PM@17-Feb-2020 5:01:43 PM@17-Feb-2020 5:01:40 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190370', '1', 1, '2020-02-17 17:01:43', 0, 1),
(176, '2020-02-17 17:01:45', '2020-02-17 17:01:45', 'Feb 17, 2020 5:01:44 PM@Feb 17, 2020 5:01:44 PM@Feb 17, 2020 5:01:41 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190237', '1', 1, '2020-02-17 17:01:45', 0, 1),
(177, '2020-02-17 17:01:48', '2020-02-17 17:01:48', '17-Feb-2020 5:01:47 PM@17-Feb-2020 5:01:47 PM@17-Feb-2020 5:01:43 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190382', '1', 1, '2020-02-17 17:01:48', 0, 1),
(178, '2020-02-17 17:01:48', '2020-02-17 17:01:48', 'Feb 17, 2020 5:01:48 PM@Feb 17, 2020 5:01:48 PM@Feb 17, 2020 5:01:45 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190257', '1', 1, '2020-02-17 17:01:48', 0, 1),
(179, '2020-02-17 17:01:51', '2020-02-17 17:01:51', '17-Feb-2020 5:01:51 PM@17-Feb-2020 5:01:51 PM@17-Feb-2020 5:01:48 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190367', '1', 1, '2020-02-17 17:01:51', 0, 1),
(180, '2020-02-17 17:01:52', '2020-02-17 17:01:52', 'Feb 17, 2020 5:01:51 PM@Feb 17, 2020 5:01:51 PM@Feb 17, 2020 5:01:48 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190246', '1', 1, '2020-02-17 17:01:52', 0, 1),
(181, '2020-02-17 17:01:55', '2020-02-17 17:01:55', '17-Feb-2020 5:01:54 PM@17-Feb-2020 5:01:54 PM@17-Feb-2020 5:01:51 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190351', '1', 1, '2020-02-17 17:01:55', 0, 1),
(182, '2020-02-17 17:01:55', '2020-02-17 17:01:56', 'Feb 17, 2020 5:01:55 PM@Feb 17, 2020 5:01:55 PM@Feb 17, 2020 5:01:52 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190169', '1', 1, '2020-02-17 17:01:56', 0, 1),
(183, '2020-02-17 17:01:59', '2020-02-17 17:01:59', '17-Feb-2020 5:01:58 PM@17-Feb-2020 5:01:58 PM@17-Feb-2020 5:01:54 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190344', '1', 1, '2020-02-17 17:01:59', 0, 1),
(184, '2020-02-17 17:01:59', '2020-02-17 17:01:59', 'Feb 17, 2020 5:01:58 PM@Feb 17, 2020 5:01:58 PM@Feb 17, 2020 5:01:55 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190343', '1', 1, '2020-02-17 17:01:59', 0, 1),
(185, '2020-02-17 17:02:02', '2020-02-17 17:02:02', 'Feb 17, 2020 5:02:02 PM@Feb 17, 2020 5:02:02 PM@Feb 17, 2020 5:01:59 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190240', '1', 1, '2020-02-17 17:02:02', 0, 1),
(186, '2020-02-17 17:02:03', '2020-02-17 17:02:03', '17-Feb-2020 5:02:03 PM@17-Feb-2020 5:02:03 PM@17-Feb-2020 5:01:59 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190341', '1', 1, '2020-02-17 17:02:03', 0, 1),
(187, '2020-02-17 17:02:07', '2020-02-17 17:02:07', '17-Feb-2020 5:02:06 PM@17-Feb-2020 5:02:06 PM@17-Feb-2020 5:02:03 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190372', '1', 1, '2020-02-17 17:02:07', 0, 1),
(188, '2020-02-17 17:02:09', '2020-02-17 17:02:09', 'Feb 17, 2020 5:02:08 PM@Feb 17, 2020 5:02:08 PM@Feb 17, 2020 5:02:02 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190244', '1', 1, '2020-02-17 17:02:09', 0, 1),
(189, '2020-02-17 17:02:11', '2020-02-17 17:02:11', '17-Feb-2020 5:02:11 PM@17-Feb-2020 5:02:11 PM@17-Feb-2020 5:02:07 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190358', '1', 1, '2020-02-17 17:02:11', 0, 1),
(190, '2020-02-17 17:02:12', '2020-02-17 17:02:12', 'Feb 17, 2020 5:02:11 PM@Feb 17, 2020 5:02:11 PM@Feb 17, 2020 5:02:08 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190235', '1', 1, '2020-02-17 17:02:12', 0, 1),
(191, '2020-02-17 17:02:15', '2020-02-17 17:02:15', '17-Feb-2020 5:02:14 PM@17-Feb-2020 5:02:14 PM@17-Feb-2020 5:02:11 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190355', '1', 1, '2020-02-17 17:02:15', 0, 1),
(192, '2020-02-17 17:02:15', '2020-02-17 17:02:15', 'Feb 17, 2020 5:02:14 PM@Feb 17, 2020 5:02:14 PM@Feb 17, 2020 5:02:11 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190283', '1', 1, '2020-02-17 17:02:15', 0, 1),
(193, '2020-02-17 17:02:18', '2020-02-17 17:02:19', '17-Feb-2020 5:02:18 PM@17-Feb-2020 5:02:18 PM@17-Feb-2020 5:02:15 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190346', '1', 1, '2020-02-17 17:02:19', 0, 1),
(194, '2020-02-17 17:02:21', '2020-02-17 17:02:21', 'Feb 17, 2020 5:02:21 PM@Feb 17, 2020 5:02:21 PM@Feb 17, 2020 5:02:18 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190274', '1', 1, '2020-02-17 17:02:21', 0, 1),
(195, '2020-02-17 17:02:22', '2020-02-17 17:02:22', '17-Feb-2020 5:02:21 PM@17-Feb-2020 5:02:21 PM@17-Feb-2020 5:02:18 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190349', '1', 1, '2020-02-17 17:02:22', 0, 1),
(196, '2020-02-17 17:02:25', '2020-02-17 17:02:25', 'Feb 17, 2020 5:02:24 PM@Feb 17, 2020 5:02:24 PM@Feb 17, 2020 5:02:21 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190197', '1', 1, '2020-02-17 17:02:25', 0, 1),
(197, '2020-02-17 17:02:26', '2020-02-17 17:02:26', '17-Feb-2020 5:02:25 PM@17-Feb-2020 5:02:25 PM@17-Feb-2020 5:02:22 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190340', '1', 1, '2020-02-17 17:02:26', 0, 1),
(198, '2020-02-17 17:02:28', '2020-02-17 17:02:28', 'Feb 17, 2020 5:02:28 PM@Feb 17, 2020 5:02:28 PM@Feb 17, 2020 5:02:25 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190024', '1', 1, '2020-02-17 17:02:28', 0, 1),
(199, '2020-02-17 17:02:30', '2020-02-17 17:02:30', '17-Feb-2020 5:02:29 PM@17-Feb-2020 5:02:29 PM@17-Feb-2020 5:02:26 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190400', '1', 1, '2020-02-17 17:02:30', 0, 1),
(200, '2020-02-17 17:02:34', '2020-02-17 17:02:34', '17-Feb-2020 5:02:33 PM@17-Feb-2020 5:02:33 PM@17-Feb-2020 5:02:30 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190362', '1', 1, '2020-02-17 17:02:34', 0, 1),
(201, '2020-02-17 17:02:35', '2020-02-17 17:02:35', 'Feb 17, 2020 5:02:34 PM@Feb 17, 2020 5:02:35 PM@Feb 17, 2020 5:02:31 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190190', '1', 1, '2020-02-17 17:02:35', 0, 1),
(202, '2020-02-17 17:02:38', '2020-02-17 17:02:38', '17-Feb-2020 5:02:37 PM@17-Feb-2020 5:02:37 PM@17-Feb-2020 5:02:33 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190371', '1', 1, '2020-02-17 17:02:38', 0, 1),
(203, '2020-02-17 17:02:39', '2020-02-17 17:02:39', 'Feb 17, 2020 5:02:38 PM@Feb 17, 2020 5:02:38 PM@Feb 17, 2020 5:02:35 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190025', '1', 1, '2020-02-17 17:02:39', 0, 1),
(204, '2020-02-17 17:02:42', '2020-02-17 17:02:42', '17-Feb-2020 5:02:41 PM@17-Feb-2020 5:02:41 PM@17-Feb-2020 5:02:37 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190361', '1', 1, '2020-02-17 17:02:42', 0, 1),
(205, '2020-02-17 17:02:42', '2020-02-17 17:02:42', 'Feb 17, 2020 5:02:42 PM@Feb 17, 2020 5:02:42 PM@Feb 17, 2020 5:02:39 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190201', '1', 1, '2020-02-17 17:02:42', 0, 1),
(206, '2020-02-17 17:02:46', '2020-02-17 17:02:46', '17-Feb-2020 5:02:45 PM@17-Feb-2020 5:02:45 PM@17-Feb-2020 5:02:41 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190398', '1', 1, '2020-02-17 17:02:46', 0, 1),
(207, '2020-02-17 17:02:46', '2020-02-17 17:02:46', 'Feb 17, 2020 5:02:46 PM@Feb 17, 2020 5:02:46 PM@Feb 17, 2020 5:02:42 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190192', '1', 1, '2020-02-17 17:02:46', 0, 1),
(208, '2020-02-17 17:02:50', '2020-02-17 17:02:50', '17-Feb-2020 5:02:49 PM@17-Feb-2020 5:02:49 PM@17-Feb-2020 5:02:46 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190385', '1', 1, '2020-02-17 17:02:50', 0, 1),
(209, '2020-02-17 17:02:51', '2020-02-17 17:02:51', 'Feb 17, 2020 5:02:51 PM@Feb 17, 2020 5:02:51 PM@Feb 17, 2020 5:02:46 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190180', '1', 1, '2020-02-17 17:02:51', 0, 1),
(210, '2020-02-17 17:02:53', '2020-02-17 17:02:53', '17-Feb-2020 5:02:53 PM@17-Feb-2020 5:02:53 PM@17-Feb-2020 5:02:49 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190406', '1', 1, '2020-02-17 17:02:53', 0, 1),
(211, '2020-02-17 17:02:55', '2020-02-17 17:02:55', 'Feb 17, 2020 5:02:55 PM@Feb 17, 2020 5:02:55 PM@Feb 17, 2020 5:02:51 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190218', '1', 1, '2020-02-17 17:02:55', 0, 1),
(212, '2020-02-17 17:02:57', '2020-02-17 17:02:57', '17-Feb-2020 5:02:57 PM@17-Feb-2020 5:02:57 PM@17-Feb-2020 5:02:53 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190427', '1', 1, '2020-02-17 17:02:57', 0, 1),
(213, '2020-02-17 17:02:59', '2020-02-17 17:02:59', 'Feb 17, 2020 5:02:58 PM@Feb 17, 2020 5:02:58 PM@Feb 17, 2020 5:02:55 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190198', '1', 1, '2020-02-17 17:02:59', 0, 1),
(214, '2020-02-17 17:03:01', '2020-02-17 17:03:01', '17-Feb-2020 5:03:01 PM@17-Feb-2020 5:03:01 PM@17-Feb-2020 5:02:57 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190381', '1', 1, '2020-02-17 17:03:01', 0, 1),
(215, '2020-02-17 17:03:02', '2020-02-17 17:03:02', 'Feb 17, 2020 5:03:01 PM@Feb 17, 2020 5:03:01 PM@Feb 17, 2020 5:02:58 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190212', '1', 1, '2020-02-17 17:03:02', 0, 1),
(216, '2020-02-17 17:03:05', '2020-02-17 17:03:05', 'Feb 17, 2020 5:03:05 PM@Feb 17, 2020 5:03:05 PM@Feb 17, 2020 5:03:02 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190211', '1', 1, '2020-02-17 17:03:05', 0, 1),
(217, '2020-02-17 17:03:05', '2020-02-17 17:03:05', '17-Feb-2020 5:03:05 PM@17-Feb-2020 5:03:05 PM@17-Feb-2020 5:03:01 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190360', '1', 1, '2020-02-17 17:03:05', 0, 1),
(218, '2020-02-17 17:03:08', '2020-02-17 17:03:08', 'Feb 17, 2020 5:03:08 PM@Feb 17, 2020 5:03:08 PM@Feb 17, 2020 5:03:05 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190226', '1', 1, '2020-02-17 17:03:08', 0, 1),
(219, '2020-02-17 17:03:09', '2020-02-17 17:03:09', '17-Feb-2020 5:03:08 PM@17-Feb-2020 5:03:08 PM@17-Feb-2020 5:03:05 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190369', '1', 1, '2020-02-17 17:03:09', 0, 1),
(220, '2020-02-17 17:03:12', '2020-02-17 17:03:12', 'Feb 17, 2020 5:03:11 PM@Feb 17, 2020 5:03:11 PM@Feb 17, 2020 5:03:08 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190217', '1', 1, '2020-02-17 17:03:12', 0, 1),
(221, '2020-02-17 17:03:13', '2020-02-17 17:03:13', '17-Feb-2020 5:03:12 PM@17-Feb-2020 5:03:12 PM@17-Feb-2020 5:03:09 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190424', '1', 1, '2020-02-17 17:03:13', 0, 1),
(222, '2020-02-17 17:03:15', '2020-02-17 17:03:15', 'Feb 17, 2020 5:03:14 PM@Feb 17, 2020 5:03:14 PM@Feb 17, 2020 5:03:12 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190214', '1', 1, '2020-02-17 17:03:15', 0, 1),
(223, '2020-02-17 17:03:17', '2020-02-17 17:03:17', '17-Feb-2020 5:03:17 PM@17-Feb-2020 5:03:17 PM@17-Feb-2020 5:03:13 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190366', '1', 1, '2020-02-17 17:03:17', 0, 1),
(224, '2020-02-17 17:03:19', '2020-02-17 17:03:19', 'Feb 17, 2020 5:03:18 PM@Feb 17, 2020 5:03:18 PM@Feb 17, 2020 5:03:15 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190239', '1', 1, '2020-02-17 17:03:19', 0, 1),
(225, '2020-02-17 17:03:21', '2020-02-17 17:03:21', '17-Feb-2020 5:03:20 PM@17-Feb-2020 5:03:20 PM@17-Feb-2020 5:03:17 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190428', '1', 1, '2020-02-17 17:03:21', 0, 1),
(226, '2020-02-17 17:03:22', '2020-02-17 17:03:22', 'Feb 17, 2020 5:03:21 PM@Feb 17, 2020 5:03:21 PM@Feb 17, 2020 5:03:18 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190208', '1', 1, '2020-02-17 17:03:22', 0, 1),
(227, '2020-02-17 17:03:25', '2020-02-17 17:03:25', '17-Feb-2020 5:03:24 PM@17-Feb-2020 5:03:24 PM@17-Feb-2020 5:03:21 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190388', '1', 1, '2020-02-17 17:03:25', 0, 1),
(228, '2020-02-17 17:03:25', '2020-02-17 17:03:25', 'Feb 17, 2020 5:03:25 PM@Feb 17, 2020 5:03:25 PM@Feb 17, 2020 5:03:22 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190207', '1', 1, '2020-02-17 17:03:25', 0, 1),
(229, '2020-02-17 17:03:28', '2020-02-17 17:03:29', 'Feb 17, 2020 5:03:28 PM@Feb 17, 2020 5:03:28 PM@Feb 17, 2020 5:03:25 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190233', '1', 1, '2020-02-17 17:03:29', 0, 1),
(230, '2020-02-17 17:03:29', '2020-02-17 17:03:29', '17-Feb-2020 5:03:28 PM@17-Feb-2020 5:03:28 PM@17-Feb-2020 5:03:25 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190407', '1', 1, '2020-02-17 17:03:29', 0, 1),
(231, '2020-02-17 17:03:32', '2020-02-17 17:03:32', 'Feb 17, 2020 5:03:32 PM@Feb 17, 2020 5:03:32 PM@Feb 17, 2020 5:03:28 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190247', '1', 1, '2020-02-17 17:03:32', 0, 1),
(232, '2020-02-17 17:03:33', '2020-02-17 17:03:33', '17-Feb-2020 5:03:32 PM@17-Feb-2020 5:03:32 PM@17-Feb-2020 5:03:29 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190363', '1', 1, '2020-02-17 17:03:33', 0, 1),
(233, '2020-02-17 17:03:37', '2020-02-17 17:03:37', '17-Feb-2020 5:03:37 PM@17-Feb-2020 5:03:37 PM@17-Feb-2020 5:03:33 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190397', '1', 1, '2020-02-17 17:03:37', 0, 1),
(234, '2020-02-17 17:03:38', '2020-02-17 17:03:38', 'Feb 17, 2020 5:03:37 PM@Feb 17, 2020 5:03:37 PM@Feb 17, 2020 5:03:32 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190037', '1', 1, '2020-02-17 17:03:38', 0, 1),
(235, '2020-02-17 17:03:41', '2020-02-17 17:03:41', 'Feb 17, 2020 5:03:40 PM@Feb 17, 2020 5:03:40 PM@Feb 17, 2020 5:03:37 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190389', '1', 1, '2020-02-17 17:03:41', 0, 1),
(236, '2020-02-17 17:03:42', '2020-02-17 17:03:42', '17-Feb-2020 5:03:42 PM@17-Feb-2020 5:03:42 PM@17-Feb-2020 5:03:37 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190270', '1', 1, '2020-02-17 17:03:42', 0, 1),
(237, '2020-02-17 17:03:45', '2020-02-17 17:03:45', 'Feb 17, 2020 5:03:44 PM@Feb 17, 2020 5:03:44 PM@Feb 17, 2020 5:03:41 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190411', '1', 1, '2020-02-17 17:03:45', 0, 1),
(238, '2020-02-17 17:03:46', '2020-02-17 17:03:46', '17-Feb-2020 5:03:46 PM@17-Feb-2020 5:03:46 PM@17-Feb-2020 5:03:42 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190023', '1', 1, '2020-02-17 17:03:46', 0, 1),
(239, '2020-02-17 17:03:48', '2020-02-17 17:03:48', 'Feb 17, 2020 5:03:48 PM@Feb 17, 2020 5:03:48 PM@Feb 17, 2020 5:03:45 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190303', '1', 1, '2020-02-17 17:03:48', 0, 1),
(240, '2020-02-17 17:03:50', '2020-02-17 17:03:50', '17-Feb-2020 5:03:50 PM@17-Feb-2020 5:03:50 PM@17-Feb-2020 5:03:46 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190027', '1', 1, '2020-02-17 17:03:50', 0, 1),
(241, '2020-02-17 17:03:52', '2020-02-17 17:03:52', 'Feb 17, 2020 5:03:51 PM@Feb 17, 2020 5:03:51 PM@Feb 17, 2020 5:03:48 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190288', '1', 1, '2020-02-17 17:03:52', 0, 1),
(242, '2020-02-17 17:03:54', '2020-02-17 17:03:54', '17-Feb-2020 5:03:54 PM@17-Feb-2020 5:03:54 PM@17-Feb-2020 5:03:50 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190034', '1', 1, '2020-02-17 17:03:54', 0, 1),
(243, '2020-02-17 17:03:55', '2020-02-17 17:03:55', 'Feb 17, 2020 5:03:54 PM@Feb 17, 2020 5:03:55 PM@Feb 17, 2020 5:03:52 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190213', '1', 1, '2020-02-17 17:03:55', 0, 1),
(244, '2020-02-17 17:03:58', '2020-02-17 17:03:58', '17-Feb-2020 5:03:58 PM@17-Feb-2020 5:03:58 PM@17-Feb-2020 5:03:54 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190145', '1', 1, '2020-02-17 17:03:58', 0, 1),
(245, '2020-02-17 17:04:00', '2020-02-17 17:04:00', 'Feb 17, 2020 5:03:59 PM@Feb 17, 2020 5:03:59 PM@Feb 17, 2020 5:03:55 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190410', '1', 1, '2020-02-17 17:04:00', 0, 1),
(246, '2020-02-17 17:04:02', '2020-02-17 17:04:02', '17-Feb-2020 5:04:02 PM@17-Feb-2020 5:04:02 PM@17-Feb-2020 5:03:58 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190221', '1', 1, '2020-02-17 17:04:02', 0, 1),
(247, '2020-02-17 17:04:03', '2020-02-17 17:04:04', 'Feb 17, 2020 5:04:03 PM@Feb 17, 2020 5:04:03 PM@Feb 17, 2020 5:04:00 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190311', '1', 1, '2020-02-17 17:04:04', 0, 1),
(248, '2020-02-17 17:04:06', '2020-02-17 17:04:06', '17-Feb-2020 5:04:05 PM@17-Feb-2020 5:04:05 PM@17-Feb-2020 5:04:02 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190224', '1', 1, '2020-02-17 17:04:06', 0, 1),
(249, '2020-02-17 17:04:07', '2020-02-17 17:04:07', 'Feb 17, 2020 5:04:06 PM@Feb 17, 2020 5:04:06 PM@Feb 17, 2020 5:04:03 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190373', '1', 1, '2020-02-17 17:04:07', 0, 1),
(250, '2020-02-17 17:04:10', '2020-02-17 17:04:10', '17-Feb-2020 5:04:10 PM@17-Feb-2020 5:04:10 PM@17-Feb-2020 5:04:06 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190125', '1', 1, '2020-02-17 17:04:10', 0, 1);
INSERT INTO `scanticketlist` (`id`, `api_hit_timestamp`, `created_at`, `m_timestamp`, `scompany_id`, `device_id`, `scanner_type`, `ticket_category_id`, `ticket_subcategory_id`, `barcode_no`, `gate_no`, `hit_count`, `duplicate_hit_datetime`, `duplicate_hit_count`, `status`) VALUES
(251, '2020-02-17 17:04:10', '2020-02-17 17:04:10', 'Feb 17, 2020 5:04:10 PM@Feb 17, 2020 5:04:10 PM@Feb 17, 2020 5:04:07 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190314', '1', 1, '2020-02-17 17:04:10', 0, 1),
(252, '2020-02-17 17:04:14', '2020-02-17 17:04:14', 'Feb 17, 2020 5:04:13 PM@Feb 17, 2020 5:04:14 PM@Feb 17, 2020 5:04:10 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190316', '1', 1, '2020-02-17 17:04:14', 0, 1),
(253, '2020-02-17 17:04:15', '2020-02-17 17:04:15', '17-Feb-2020 5:04:14 PM@17-Feb-2020 5:04:14 PM@17-Feb-2020 5:04:10 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190124', '1', 1, '2020-02-17 17:04:15', 0, 1),
(254, '2020-02-17 17:04:18', '2020-02-17 17:04:18', 'Feb 17, 2020 5:04:17 PM@Feb 17, 2020 5:04:17 PM@Feb 17, 2020 5:04:14 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190420', '1', 1, '2020-02-17 17:04:18', 0, 1),
(255, '2020-02-17 17:04:19', '2020-02-17 17:04:19', '17-Feb-2020 5:04:19 PM@17-Feb-2020 5:04:19 PM@17-Feb-2020 5:04:15 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190200', '1', 1, '2020-02-17 17:04:19', 0, 1),
(256, '2020-02-17 17:04:21', '2020-02-17 17:04:21', 'Feb 17, 2020 5:04:20 PM@Feb 17, 2020 5:04:21 PM@Feb 17, 2020 5:04:17 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190181', '1', 1, '2020-02-17 17:04:21', 0, 1),
(257, '2020-02-17 17:04:23', '2020-02-17 17:04:23', '17-Feb-2020 5:04:23 PM@17-Feb-2020 5:04:23 PM@17-Feb-2020 5:04:19 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190194', '1', 1, '2020-02-17 17:04:23', 0, 1),
(258, '2020-02-17 17:04:25', '2020-02-17 17:04:25', 'Feb 17, 2020 5:04:24 PM@Feb 17, 2020 5:04:24 PM@Feb 17, 2020 5:04:21 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190295', '1', 1, '2020-02-17 17:04:25', 0, 1),
(259, '2020-02-17 17:04:28', '2020-02-17 17:04:28', '17-Feb-2020 5:04:27 PM@17-Feb-2020 5:04:27 PM@17-Feb-2020 5:04:23 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190191', '1', 1, '2020-02-17 17:04:28', 0, 1),
(260, '2020-02-17 17:04:28', '2020-02-17 17:04:28', 'Feb 17, 2020 5:04:28 PM@Feb 17, 2020 5:04:28 PM@Feb 17, 2020 5:04:24 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190296', '1', 1, '2020-02-17 17:04:28', 0, 1),
(261, '2020-02-17 17:04:32', '2020-02-17 17:04:32', 'Feb 17, 2020 5:04:31 PM@Feb 17, 2020 5:04:31 PM@Feb 17, 2020 5:04:28 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190378', '1', 1, '2020-02-17 17:04:32', 0, 1),
(262, '2020-02-17 17:04:32', '2020-02-17 17:04:32', '17-Feb-2020 5:04:31 PM@17-Feb-2020 5:04:31 PM@17-Feb-2020 5:04:27 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190216', '1', 1, '2020-02-17 17:04:32', 0, 1),
(263, '2020-02-17 17:04:35', '2020-02-17 17:04:35', 'Feb 17, 2020 5:04:35 PM@Feb 17, 2020 5:04:35 PM@Feb 17, 2020 5:04:32 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190387', '1', 1, '2020-02-17 17:04:35', 0, 1),
(264, '2020-02-17 17:04:36', '2020-02-17 17:04:36', '17-Feb-2020 5:04:35 PM@17-Feb-2020 5:04:35 PM@17-Feb-2020 5:04:32 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190245', '1', 1, '2020-02-17 17:04:36', 0, 1),
(265, '2020-02-17 17:04:39', '2020-02-17 17:04:39', 'Feb 17, 2020 5:04:39 PM@Feb 17, 2020 5:04:39 PM@Feb 17, 2020 5:04:35 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190423', '1', 1, '2020-02-17 17:04:39', 0, 1),
(266, '2020-02-17 17:04:40', '2020-02-17 17:04:40', '17-Feb-2020 5:04:39 PM@17-Feb-2020 5:04:39 PM@17-Feb-2020 5:04:36 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190204', '1', 1, '2020-02-17 17:04:40', 0, 1),
(267, '2020-02-17 17:04:44', '2020-02-17 17:04:44', '17-Feb-2020 5:04:43 PM@17-Feb-2020 5:04:43 PM@17-Feb-2020 5:04:40 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190242', '1', 1, '2020-02-17 17:04:44', 0, 1),
(268, '2020-02-17 17:04:44', '2020-02-17 17:04:44', 'Feb 17, 2020 5:04:44 PM@Feb 17, 2020 5:04:44 PM@Feb 17, 2020 5:04:39 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190276', '1', 1, '2020-02-17 17:04:44', 0, 1),
(269, '2020-02-17 17:04:48', '2020-02-17 17:04:48', 'Feb 17, 2020 5:04:47 PM@Feb 17, 2020 5:04:47 PM@Feb 17, 2020 5:04:44 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190279', '1', 1, '2020-02-17 17:04:48', 0, 1),
(270, '2020-02-17 17:04:48', '2020-02-17 17:04:48', '17-Feb-2020 5:04:47 PM@17-Feb-2020 5:04:47 PM@17-Feb-2020 5:04:44 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190272', '1', 1, '2020-02-17 17:04:48', 0, 1),
(271, '2020-02-17 17:04:52', '2020-02-17 17:04:52', '17-Feb-2020 5:04:51 PM@17-Feb-2020 5:04:51 PM@17-Feb-2020 5:04:48 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190137', '1', 1, '2020-02-17 17:04:52', 0, 1),
(272, '2020-02-17 17:04:52', '2020-02-17 17:04:52', 'Feb 17, 2020 5:04:51 PM@Feb 17, 2020 5:04:51 PM@Feb 17, 2020 5:04:47 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190007', '1', 1, '2020-02-17 17:04:52', 0, 1),
(273, '2020-02-17 17:04:56', '2020-02-17 17:04:56', '17-Feb-2020 5:04:55 PM@17-Feb-2020 5:04:56 PM@17-Feb-2020 5:04:52 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190090', '1', 1, '2020-02-17 17:04:56', 0, 1),
(274, '2020-02-17 17:04:56', '2020-02-17 17:04:56', 'Feb 17, 2020 5:04:56 PM@Feb 17, 2020 5:04:56 PM@Feb 17, 2020 5:04:52 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190253', '1', 1, '2020-02-17 17:04:56', 0, 1),
(275, '2020-02-17 17:05:00', '2020-02-17 17:05:00', 'Feb 17, 2020 5:04:59 PM@Feb 17, 2020 5:04:59 PM@Feb 17, 2020 5:04:56 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190227', '1', 1, '2020-02-17 17:05:00', 0, 1),
(276, '2020-02-17 17:05:01', '2020-02-17 17:05:01', '17-Feb-2020 5:05:00 PM@17-Feb-2020 5:05:00 PM@17-Feb-2020 5:04:56 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190091', '1', 1, '2020-02-17 17:05:01', 0, 1),
(277, '2020-02-17 17:05:04', '2020-02-17 17:05:04', 'Feb 17, 2020 5:05:03 PM@Feb 17, 2020 5:05:03 PM@Feb 17, 2020 5:05:00 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190431', '1', 1, '2020-02-17 17:05:04', 0, 1),
(278, '2020-02-17 17:05:05', '2020-02-17 17:05:05', '17-Feb-2020 5:05:04 PM@17-Feb-2020 5:05:04 PM@17-Feb-2020 5:05:00 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190265', '1', 1, '2020-02-17 17:05:05', 0, 1),
(279, '2020-02-17 17:05:08', '2020-02-17 17:05:08', 'Feb 17, 2020 5:05:07 PM@Feb 17, 2020 5:05:07 PM@Feb 17, 2020 5:05:04 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190327', '1', 1, '2020-02-17 17:05:08', 0, 1),
(280, '2020-02-17 17:05:09', '2020-02-17 17:05:09', '17-Feb-2020 5:05:08 PM@17-Feb-2020 5:05:08 PM@17-Feb-2020 5:05:05 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190262', '1', 1, '2020-02-17 17:05:09', 0, 1),
(281, '2020-02-17 17:05:11', '2020-02-17 17:05:11', 'Feb 17, 2020 5:05:11 PM@Feb 17, 2020 5:05:11 PM@Feb 17, 2020 5:05:07 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190442', '1', 1, '2020-02-17 17:05:11', 0, 1),
(282, '2020-02-17 17:05:13', '2020-02-17 17:05:13', '17-Feb-2020 5:05:13 PM@17-Feb-2020 5:05:13 PM@17-Feb-2020 5:05:09 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190259', '1', 1, '2020-02-17 17:05:13', 0, 1),
(283, '2020-02-17 17:05:15', '2020-02-17 17:05:15', 'Feb 17, 2020 5:05:15 PM@Feb 17, 2020 5:05:15 PM@Feb 17, 2020 5:05:11 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190429', '1', 1, '2020-02-17 17:05:15', 0, 1),
(284, '2020-02-17 17:05:17', '2020-02-17 17:05:17', '17-Feb-2020 5:05:17 PM@17-Feb-2020 5:05:17 PM@17-Feb-2020 5:05:13 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190321', '1', 1, '2020-02-17 17:05:17', 0, 1),
(285, '2020-02-17 17:05:19', '2020-02-17 17:05:19', 'Feb 17, 2020 5:05:19 PM@Feb 17, 2020 5:05:19 PM@Feb 17, 2020 5:05:15 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190312', '1', 1, '2020-02-17 17:05:19', 0, 1),
(286, '2020-02-17 17:05:22', '2020-02-17 17:05:22', '17-Feb-2020 5:05:21 PM@17-Feb-2020 5:05:21 PM@17-Feb-2020 5:05:17 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190267', '1', 1, '2020-02-17 17:05:22', 0, 1),
(287, '2020-02-17 17:05:23', '2020-02-17 17:05:23', 'Feb 17, 2020 5:05:22 PM@Feb 17, 2020 5:05:22 PM@Feb 17, 2020 5:05:19 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190003', '1', 1, '2020-02-17 17:05:23', 0, 1),
(288, '2020-02-17 17:05:26', '2020-02-17 17:05:26', '17-Feb-2020 5:05:25 PM@17-Feb-2020 5:05:25 PM@17-Feb-2020 5:05:22 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190250', '1', 1, '2020-02-17 17:05:26', 0, 1),
(289, '2020-02-17 17:05:27', '2020-02-17 17:05:27', 'Feb 17, 2020 5:05:26 PM@Feb 17, 2020 5:05:26 PM@Feb 17, 2020 5:05:23 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190047', '1', 1, '2020-02-17 17:05:27', 0, 1),
(290, '2020-02-17 17:05:30', '2020-02-17 17:05:30', '17-Feb-2020 5:05:30 PM@17-Feb-2020 5:05:30 PM@17-Feb-2020 5:05:26 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190275', '1', 1, '2020-02-17 17:05:30', 0, 1),
(291, '2020-02-17 17:05:30', '2020-02-17 17:05:30', 'Feb 17, 2020 5:05:30 PM@Feb 17, 2020 5:05:30 PM@Feb 17, 2020 5:05:26 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190000', '1', 1, '2020-02-17 17:05:30', 0, 1),
(292, '2020-02-17 17:05:34', '2020-02-17 17:05:34', 'Feb 17, 2020 5:05:34 PM@Feb 17, 2020 5:05:34 PM@Feb 17, 2020 5:05:30 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190446', '1', 1, '2020-02-17 17:05:34', 0, 1),
(293, '2020-02-17 17:05:34', '2020-02-17 17:05:34', '17-Feb-2020 5:05:34 PM@17-Feb-2020 5:05:34 PM@17-Feb-2020 5:05:30 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190284', '1', 1, '2020-02-17 17:05:34', 0, 1),
(294, '2020-02-17 17:05:38', '2020-02-17 17:05:38', 'Feb 17, 2020 5:05:37 PM@Feb 17, 2020 5:05:37 PM@Feb 17, 2020 5:05:34 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190097', '1', 1, '2020-02-17 17:05:38', 0, 1),
(295, '2020-02-17 17:05:38', '2020-02-17 17:05:38', '17-Feb-2020 5:05:38 PM@17-Feb-2020 5:05:38 PM@17-Feb-2020 5:05:34 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190264', '1', 1, '2020-02-17 17:05:38', 0, 1),
(296, '2020-02-17 17:05:42', '2020-02-17 17:05:42', 'Feb 17, 2020 5:05:41 PM@Feb 17, 2020 5:05:41 PM@Feb 17, 2020 5:05:38 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190160', '1', 1, '2020-02-17 17:05:42', 0, 1),
(297, '2020-02-17 17:05:43', '2020-02-17 17:05:43', '17-Feb-2020 5:05:42 PM@17-Feb-2020 5:05:42 PM@17-Feb-2020 5:05:38 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190308', '1', 1, '2020-02-17 17:05:43', 0, 1),
(298, '2020-02-17 17:05:46', '2020-02-17 17:05:46', 'Feb 17, 2020 5:05:45 PM@Feb 17, 2020 5:05:45 PM@Feb 17, 2020 5:05:41 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190433', '1', 1, '2020-02-17 17:05:46', 0, 1),
(299, '2020-02-17 17:05:47', '2020-02-17 17:05:47', '17-Feb-2020 5:05:46 PM@17-Feb-2020 5:05:46 PM@17-Feb-2020 5:05:42 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190289', '1', 1, '2020-02-17 17:05:47', 0, 1),
(300, '2020-02-17 17:05:49', '2020-02-17 17:05:49', 'Feb 17, 2020 5:05:49 PM@Feb 17, 2020 5:05:49 PM@Feb 17, 2020 5:05:45 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190342', '1', 1, '2020-02-17 17:05:49', 0, 1),
(301, '2020-02-17 17:05:51', '2020-02-17 17:05:51', '17-Feb-2020 5:05:50 PM@17-Feb-2020 5:05:50 PM@17-Feb-2020 5:05:46 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190281', '1', 1, '2020-02-17 17:05:51', 0, 1),
(302, '2020-02-17 17:05:53', '2020-02-17 17:05:53', 'Feb 17, 2020 5:05:53 PM@Feb 17, 2020 5:05:53 PM@Feb 17, 2020 5:05:49 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190436', '1', 1, '2020-02-17 17:05:53', 0, 1),
(303, '2020-02-17 17:05:56', '2020-02-17 17:05:56', '17-Feb-2020 5:05:56 PM@17-Feb-2020 5:05:56 PM@17-Feb-2020 5:05:50 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190305', '1', 1, '2020-02-17 17:05:56', 0, 1),
(304, '2020-02-17 17:05:57', '2020-02-17 17:05:57', 'Feb 17, 2020 5:05:56 PM@Feb 17, 2020 5:05:56 PM@Feb 17, 2020 5:05:53 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190114', '1', 1, '2020-02-17 17:05:57', 0, 1),
(305, '2020-02-17 17:06:00', '2020-02-17 17:06:01', '17-Feb-2020 5:06:00 PM@17-Feb-2020 5:06:00 PM@17-Feb-2020 5:05:56 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190280', '1', 1, '2020-02-17 17:06:01', 0, 1),
(306, '2020-02-17 17:06:01', '2020-02-17 17:06:01', 'Feb 17, 2020 5:06:00 PM@Feb 17, 2020 5:06:00 PM@Feb 17, 2020 5:05:57 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190146', '1', 1, '2020-02-17 17:06:01', 0, 1),
(307, '2020-02-17 17:06:05', '2020-02-17 17:06:05', '17-Feb-2020 5:06:04 PM@17-Feb-2020 5:06:04 PM@17-Feb-2020 5:06:00 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190277', '1', 1, '2020-02-17 17:06:05', 0, 1),
(308, '2020-02-17 17:06:06', '2020-02-17 17:06:06', 'Feb 17, 2020 5:06:05 PM@Feb 17, 2020 5:06:05 PM@Feb 17, 2020 5:06:00 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190261', '1', 1, '2020-02-17 17:06:06', 0, 1),
(309, '2020-02-17 17:06:10', '2020-02-17 17:06:10', '17-Feb-2020 5:06:09 PM@17-Feb-2020 5:06:09 PM@17-Feb-2020 5:06:05 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190282', '1', 1, '2020-02-17 17:06:10', 0, 1),
(310, '2020-02-17 17:06:10', '2020-02-17 17:06:10', 'Feb 17, 2020 5:06:09 PM@Feb 17, 2020 5:06:09 PM@Feb 17, 2020 5:06:05 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190026', '1', 1, '2020-02-17 17:06:10', 0, 1),
(311, '2020-02-17 17:06:14', '2020-02-17 17:06:14', 'Feb 17, 2020 5:06:13 PM@Feb 17, 2020 5:06:13 PM@Feb 17, 2020 5:06:09 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190069', '1', 1, '2020-02-17 17:06:14', 0, 1),
(312, '2020-02-17 17:06:14', '2020-02-17 17:06:14', '17-Feb-2020 5:06:13 PM@17-Feb-2020 5:06:13 PM@17-Feb-2020 5:06:09 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190302', '1', 1, '2020-02-17 17:06:14', 0, 1),
(313, '2020-02-17 17:06:18', '2020-02-17 17:06:18', '17-Feb-2020 5:06:18 PM@17-Feb-2020 5:06:18 PM@17-Feb-2020 5:06:14 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190299', '1', 1, '2020-02-17 17:06:18', 0, 1),
(314, '2020-02-17 17:06:19', '2020-02-17 17:06:19', 'Feb 17, 2020 5:06:18 PM@Feb 17, 2020 5:06:18 PM@Feb 17, 2020 5:06:13 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190195', '1', 1, '2020-02-17 17:06:19', 0, 1),
(315, '2020-02-17 17:06:23', '2020-02-17 17:06:23', 'Feb 17, 2020 5:06:23 PM@Feb 17, 2020 5:06:23 PM@Feb 17, 2020 5:06:19 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190199', '1', 1, '2020-02-17 17:06:23', 0, 1),
(316, '2020-02-17 17:06:23', '2020-02-17 17:06:23', '17-Feb-2020 5:06:23 PM@17-Feb-2020 5:06:23 PM@17-Feb-2020 5:06:18 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190298', '1', 1, '2020-02-17 17:06:23', 0, 1),
(317, '2020-02-17 17:06:28', '2020-02-17 17:06:28', 'Feb 17, 2020 5:06:27 PM@Feb 17, 2020 5:06:27 PM@Feb 17, 2020 5:06:23 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190193', '1', 1, '2020-02-17 17:06:28', 0, 1),
(318, '2020-02-17 17:06:28', '2020-02-17 17:06:28', '17-Feb-2020 5:06:27 PM@17-Feb-2020 5:06:27 PM@17-Feb-2020 5:06:23 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190294', '1', 1, '2020-02-17 17:06:28', 0, 1),
(319, '2020-02-17 17:06:32', '2020-02-17 17:06:32', '17-Feb-2020 5:06:31 PM@17-Feb-2020 5:06:31 PM@17-Feb-2020 5:06:27 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190329', '1', 1, '2020-02-17 17:06:32', 0, 1),
(320, '2020-02-17 17:06:32', '2020-02-17 17:06:33', 'Feb 17, 2020 5:06:32 PM@Feb 17, 2020 5:06:32 PM@Feb 17, 2020 5:06:27 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190168', '1', 1, '2020-02-17 17:06:33', 0, 1),
(321, '2020-02-17 17:06:36', '2020-02-17 17:06:36', '17-Feb-2020 5:06:36 PM@17-Feb-2020 5:06:36 PM@17-Feb-2020 5:06:32 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190306', '1', 1, '2020-02-17 17:06:36', 0, 1),
(322, '2020-02-17 17:06:38', '2020-02-17 17:06:38', 'Feb 17, 2020 5:06:38 PM@Feb 17, 2020 5:06:38 PM@Feb 17, 2020 5:06:32 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190135', '1', 1, '2020-02-17 17:06:38', 0, 1),
(323, '2020-02-17 17:06:42', '2020-02-17 17:06:42', '17-Feb-2020 5:06:41 PM@17-Feb-2020 5:06:41 PM@17-Feb-2020 5:06:36 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190278', '1', 1, '2020-02-17 17:06:42', 0, 1),
(324, '2020-02-17 17:06:46', '2020-02-17 17:06:46', '17-Feb-2020 5:06:45 PM@17-Feb-2020 5:06:45 PM@17-Feb-2020 5:06:41 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190438', '1', 1, '2020-02-17 17:06:46', 0, 1),
(325, '2020-02-17 17:06:47', '2020-02-17 17:06:47', 'Feb 17, 2020 5:06:46 PM@Feb 17, 2020 5:06:46 PM@Feb 17, 2020 5:06:42 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190101', '1', 1, '2020-02-17 17:06:47', 0, 1),
(326, '2020-02-17 17:06:50', '2020-02-17 17:06:50', '17-Feb-2020 5:06:50 PM@17-Feb-2020 5:06:50 PM@17-Feb-2020 5:06:46 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190432', '1', 1, '2020-02-17 17:06:50', 0, 1),
(327, '2020-02-17 17:06:54', '2020-02-17 17:06:54', 'Feb 17, 2020 5:06:53 PM@Feb 17, 2020 5:06:53 PM@Feb 17, 2020 5:06:47 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190100', '1', 1, '2020-02-17 17:06:54', 0, 1),
(328, '2020-02-17 17:06:55', '2020-02-17 17:06:55', '17-Feb-2020 5:06:54 PM@17-Feb-2020 5:06:54 PM@17-Feb-2020 5:06:50 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190440', '1', 1, '2020-02-17 17:06:55', 0, 1),
(329, '2020-02-17 17:06:58', '2020-02-17 17:06:58', 'Feb 17, 2020 5:06:58 PM@Feb 17, 2020 5:06:58 PM@Feb 17, 2020 5:06:54 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190099', '1', 1, '2020-02-17 17:06:58', 0, 1),
(330, '2020-02-17 17:06:59', '2020-02-17 17:06:59', '17-Feb-2020 5:06:58 PM@17-Feb-2020 5:06:58 PM@17-Feb-2020 5:06:54 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190437', '1', 1, '2020-02-17 17:06:59', 0, 1),
(331, '2020-02-17 17:07:03', '2020-02-17 17:07:03', 'Feb 17, 2020 5:07:03 PM@Feb 17, 2020 5:07:03 PM@Feb 17, 2020 5:06:58 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190075', '1', 1, '2020-02-17 17:07:03', 0, 1),
(332, '2020-02-17 17:07:04', '2020-02-17 17:07:04', '17-Feb-2020 5:07:03 PM@17-Feb-2020 5:07:03 PM@17-Feb-2020 5:06:59 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190439', '1', 1, '2020-02-17 17:07:04', 0, 1),
(333, '2020-02-17 17:07:08', '2020-02-17 17:07:08', 'Feb 17, 2020 5:07:07 PM@Feb 17, 2020 5:07:07 PM@Feb 17, 2020 5:07:03 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190076', '1', 1, '2020-02-17 17:07:08', 0, 1),
(334, '2020-02-17 17:07:08', '2020-02-17 17:07:08', '17-Feb-2020 5:07:07 PM@17-Feb-2020 5:07:07 PM@17-Feb-2020 5:07:03 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190320', '1', 1, '2020-02-17 17:07:08', 0, 1),
(335, '2020-02-17 17:07:12', '2020-02-17 17:07:12', 'Feb 17, 2020 5:07:12 PM@Feb 17, 2020 5:07:12 PM@Feb 17, 2020 5:07:07 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190077', '1', 1, '2020-02-17 17:07:12', 0, 1),
(336, '2020-02-17 17:07:12', '2020-02-17 17:07:12', '17-Feb-2020 5:07:12 PM@17-Feb-2020 5:07:12 PM@17-Feb-2020 5:07:08 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190334', '1', 1, '2020-02-17 17:07:12', 0, 1),
(337, '2020-02-17 17:07:16', '2020-02-17 17:07:16', 'Feb 17, 2020 5:07:16 PM@Feb 17, 2020 5:07:16 PM@Feb 17, 2020 5:07:12 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190121', '1', 1, '2020-02-17 17:07:16', 0, 1),
(338, '2020-02-17 17:07:17', '2020-02-17 17:07:17', '17-Feb-2020 5:07:16 PM@17-Feb-2020 5:07:16 PM@17-Feb-2020 5:07:12 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190331', '1', 1, '2020-02-17 17:07:17', 0, 1),
(339, '2020-02-17 17:07:21', '2020-02-17 17:07:21', 'Feb 17, 2020 5:07:20 PM@Feb 17, 2020 5:07:20 PM@Feb 17, 2020 5:07:16 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190122', '1', 1, '2020-02-17 17:07:21', 0, 1),
(340, '2020-02-17 17:07:21', '2020-02-17 17:07:21', '17-Feb-2020 5:07:21 PM@17-Feb-2020 5:07:21 PM@17-Feb-2020 5:07:16 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190328', '1', 1, '2020-02-17 17:07:21', 0, 1),
(341, '2020-02-17 17:07:25', '2020-02-17 17:07:25', 'Feb 17, 2020 5:07:24 PM@Feb 17, 2020 5:07:24 PM@Feb 17, 2020 5:07:20 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190120', '1', 1, '2020-02-17 17:07:25', 0, 1),
(342, '2020-02-17 17:07:27', '2020-02-17 17:07:27', '17-Feb-2020 5:07:26 PM@17-Feb-2020 5:07:26 PM@17-Feb-2020 5:07:21 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190325', '1', 1, '2020-02-17 17:07:27', 0, 1),
(343, '2020-02-17 17:07:30', '2020-02-17 17:07:30', 'Feb 17, 2020 5:07:29 PM@Feb 17, 2020 5:07:29 PM@Feb 17, 2020 5:07:25 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190144', '1', 1, '2020-02-17 17:07:30', 0, 1),
(344, '2020-02-17 17:07:32', '2020-02-17 17:07:32', '17-Feb-2020 5:07:31 PM@17-Feb-2020 5:07:31 PM@17-Feb-2020 5:07:27 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190322', '1', 1, '2020-02-17 17:07:32', 0, 1),
(345, '2020-02-17 17:07:35', '2020-02-17 17:07:35', 'Feb 17, 2020 5:07:34 PM@Feb 17, 2020 5:07:34 PM@Feb 17, 2020 5:07:29 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190187', '1', 1, '2020-02-17 17:07:35', 0, 1),
(346, '2020-02-17 17:07:37', '2020-02-17 17:07:37', '17-Feb-2020 5:07:36 PM@17-Feb-2020 5:07:36 PM@17-Feb-2020 5:07:32 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190315', '1', 1, '2020-02-17 17:07:37', 0, 1),
(347, '2020-02-17 17:07:41', '2020-02-17 17:07:41', 'Feb 17, 2020 5:07:40 PM@Feb 17, 2020 5:07:40 PM@Feb 17, 2020 5:07:34 PM', 2, '10b1183697ff0906', 2, 7, 7, '1020190184', '1', 1, '2020-02-17 17:07:41', 0, 1),
(348, '2020-02-17 17:07:42', '2020-02-17 17:07:42', '17-Feb-2020 5:07:42 PM@17-Feb-2020 5:07:42 PM@17-Feb-2020 5:07:37 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190333', '1', 1, '2020-02-17 17:07:42', 0, 1),
(349, '2020-02-17 17:07:47', '2020-02-17 17:07:47', '17-Feb-2020 5:07:46 PM@17-Feb-2020 5:07:46 PM@17-Feb-2020 5:07:42 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190330', '1', 1, '2020-02-17 17:07:47', 0, 1),
(350, '2020-02-17 17:07:52', '2020-02-17 17:07:52', '17-Feb-2020 5:07:52 PM@17-Feb-2020 5:07:52 PM@17-Feb-2020 5:07:47 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190324', '1', 1, '2020-02-17 17:07:52', 0, 1),
(351, '2020-02-17 17:07:57', '2020-02-17 17:07:57', '17-Feb-2020 5:07:57 PM@17-Feb-2020 5:07:57 PM@17-Feb-2020 5:07:52 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190347', '1', 1, '2020-02-17 17:07:57', 0, 1),
(352, '2020-02-17 17:08:02', '2020-02-17 17:08:02', '17-Feb-2020 5:08:02 PM@17-Feb-2020 5:08:02 PM@17-Feb-2020 5:07:57 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190359', '1', 1, '2020-02-17 17:08:02', 0, 1),
(353, '2020-02-17 17:08:08', '2020-02-17 17:08:08', '17-Feb-2020 5:08:07 PM@17-Feb-2020 5:08:07 PM@17-Feb-2020 5:08:02 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190337', '1', 1, '2020-02-17 17:08:08', 0, 1),
(354, '2020-02-17 17:08:12', '2020-02-17 17:08:12', '17-Feb-2020 5:08:12 PM@17-Feb-2020 5:08:12 PM@17-Feb-2020 5:08:07 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190336', '1', 1, '2020-02-17 17:08:12', 0, 1),
(355, '2020-02-17 17:08:17', '2020-02-17 17:08:18', '17-Feb-2020 5:08:17 PM@17-Feb-2020 5:08:17 PM@17-Feb-2020 5:08:12 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190443', '1', 1, '2020-02-17 17:08:18', 0, 1),
(356, '2020-02-17 17:08:23', '2020-02-17 17:08:23', '17-Feb-2020 5:08:22 PM@17-Feb-2020 5:08:22 PM@17-Feb-2020 5:08:17 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190393', '1', 1, '2020-02-17 17:08:23', 0, 1),
(357, '2020-02-17 17:08:28', '2020-02-17 17:08:28', '17-Feb-2020 5:08:27 PM@17-Feb-2020 5:08:27 PM@17-Feb-2020 5:08:23 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190405', '1', 1, '2020-02-17 17:08:28', 0, 1),
(358, '2020-02-17 17:08:33', '2020-02-17 17:08:33', '17-Feb-2020 5:08:32 PM@17-Feb-2020 5:08:33 PM@17-Feb-2020 5:08:27 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190448', '1', 1, '2020-02-17 17:08:33', 0, 1),
(359, '2020-02-17 17:08:39', '2020-02-17 17:08:39', '17-Feb-2020 5:08:38 PM@17-Feb-2020 5:08:38 PM@17-Feb-2020 5:08:33 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190158', '1', 1, '2020-02-17 17:08:39', 0, 1),
(360, '2020-02-17 17:08:44', '2020-02-17 17:08:44', '17-Feb-2020 5:08:43 PM@17-Feb-2020 5:08:43 PM@17-Feb-2020 5:08:38 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190080', '1', 1, '2020-02-17 17:08:44', 0, 1),
(361, '2020-02-17 17:08:49', '2020-02-17 17:08:49', '17-Feb-2020 5:08:49 PM@17-Feb-2020 5:08:49 PM@17-Feb-2020 5:08:44 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190123', '1', 1, '2020-02-17 17:08:49', 0, 1),
(362, '2020-02-17 17:08:55', '2020-02-17 17:08:55', '17-Feb-2020 5:08:54 PM@17-Feb-2020 5:08:54 PM@17-Feb-2020 5:08:49 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190049', '1', 1, '2020-02-17 17:08:55', 0, 1),
(363, '2020-02-17 17:09:00', '2020-02-17 17:09:00', '17-Feb-2020 5:09:00 PM@17-Feb-2020 5:09:00 PM@17-Feb-2020 5:08:55 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190022', '1', 1, '2020-02-17 17:09:00', 0, 1),
(364, '2020-02-17 17:09:06', '2020-02-17 17:09:06', '17-Feb-2020 5:09:05 PM@17-Feb-2020 5:09:05 PM@17-Feb-2020 5:09:00 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190081', '1', 1, '2020-02-17 17:09:06', 0, 1),
(365, '2020-02-17 17:09:12', '2020-02-17 17:09:12', '17-Feb-2020 5:09:11 PM@17-Feb-2020 5:09:11 PM@17-Feb-2020 5:09:06 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190082', '1', 1, '2020-02-17 17:09:12', 0, 1),
(366, '2020-02-17 17:09:18', '2020-02-17 17:09:18', '17-Feb-2020 5:09:17 PM@17-Feb-2020 5:09:17 PM@17-Feb-2020 5:09:11 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190042', '1', 1, '2020-02-17 17:09:18', 0, 1),
(367, '2020-02-17 17:09:23', '2020-02-17 17:09:23', '17-Feb-2020 5:09:22 PM@17-Feb-2020 5:09:22 PM@17-Feb-2020 5:09:17 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190167', '1', 1, '2020-02-17 17:09:23', 0, 1),
(368, '2020-02-17 17:09:29', '2020-02-17 17:09:29', '17-Feb-2020 5:09:28 PM@17-Feb-2020 5:09:28 PM@17-Feb-2020 5:09:23 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190164', '1', 1, '2020-02-17 17:09:29', 0, 1),
(369, '2020-02-17 17:09:35', '2020-02-17 17:09:35', '17-Feb-2020 5:09:34 PM@17-Feb-2020 5:09:34 PM@17-Feb-2020 5:09:29 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190001', '1', 1, '2020-02-17 17:09:35', 0, 1),
(370, '2020-02-17 17:09:41', '2020-02-17 17:09:41', '17-Feb-2020 5:09:40 PM@17-Feb-2020 5:09:40 PM@17-Feb-2020 5:09:34 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190033', '1', 1, '2020-02-17 17:09:41', 0, 1),
(371, '2020-02-17 17:09:47', '2020-02-17 17:09:47', '17-Feb-2020 5:09:46 PM@17-Feb-2020 5:09:46 PM@17-Feb-2020 5:09:41 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190174', '1', 1, '2020-02-17 17:09:47', 0, 1),
(372, '2020-02-17 17:09:53', '2020-02-17 17:09:53', '17-Feb-2020 5:09:52 PM@17-Feb-2020 5:09:52 PM@17-Feb-2020 5:09:46 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190171', '1', 1, '2020-02-17 17:09:53', 0, 1),
(373, '2020-02-17 17:09:59', '2020-02-17 17:09:59', '17-Feb-2020 5:09:59 PM@17-Feb-2020 5:09:59 PM@17-Feb-2020 5:09:53 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190188', '1', 1, '2020-02-17 17:09:59', 0, 1),
(374, '2020-02-17 17:10:06', '2020-02-17 17:10:06', '17-Feb-2020 5:10:05 PM@17-Feb-2020 5:10:05 PM@17-Feb-2020 5:09:59 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190375', '1', 1, '2020-02-17 17:10:06', 0, 1),
(375, '2020-02-17 17:10:12', '2020-02-17 17:10:12', '17-Feb-2020 5:10:11 PM@17-Feb-2020 5:10:11 PM@17-Feb-2020 5:10:05 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190116', '1', 1, '2020-02-17 17:10:12', 0, 1),
(376, '2020-02-17 17:10:19', '2020-02-17 17:10:19', '17-Feb-2020 5:10:18 PM@17-Feb-2020 5:10:18 PM@17-Feb-2020 5:10:12 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190036', '1', 1, '2020-02-17 17:10:19', 0, 1),
(377, '2020-02-17 17:10:25', '2020-02-17 17:10:25', '17-Feb-2020 5:10:24 PM@17-Feb-2020 5:10:24 PM@17-Feb-2020 5:10:18 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190048', '1', 1, '2020-02-17 17:10:25', 0, 1),
(378, '2020-02-17 17:10:32', '2020-02-17 17:10:32', '17-Feb-2020 5:10:31 PM@17-Feb-2020 5:10:31 PM@17-Feb-2020 5:10:24 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190095', '1', 1, '2020-02-17 17:10:32', 0, 1),
(379, '2020-02-17 17:10:38', '2020-02-17 17:10:38', '17-Feb-2020 5:10:37 PM@17-Feb-2020 5:10:37 PM@17-Feb-2020 5:10:31 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190094', '1', 1, '2020-02-17 17:10:38', 0, 1),
(380, '2020-02-17 17:10:44', '2020-02-17 17:10:44', '17-Feb-2020 5:10:44 PM@17-Feb-2020 5:10:44 PM@17-Feb-2020 5:10:38 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190093', '1', 1, '2020-02-17 17:10:44', 0, 1),
(381, '2020-02-17 17:10:51', '2020-02-17 17:10:51', '17-Feb-2020 5:10:50 PM@17-Feb-2020 5:10:50 PM@17-Feb-2020 5:10:44 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190140', '1', 1, '2020-02-17 17:10:51', 0, 1),
(382, '2020-02-17 17:10:56', '2020-02-17 17:10:56', '17-Feb-2020 5:10:56 PM@17-Feb-2020 5:10:56 PM@17-Feb-2020 5:10:50 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190138', '1', 1, '2020-02-17 17:10:56', 0, 1),
(383, '2020-02-17 17:11:03', '2020-02-17 17:11:03', '17-Feb-2020 5:11:02 PM@17-Feb-2020 5:11:02 PM@17-Feb-2020 5:10:56 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190139', '1', 1, '2020-02-17 17:11:03', 0, 1),
(384, '2020-02-17 17:11:09', '2020-02-17 17:11:09', '17-Feb-2020 5:11:08 PM@17-Feb-2020 5:11:08 PM@17-Feb-2020 5:11:02 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190050', '1', 1, '2020-02-17 17:11:09', 0, 1),
(385, '2020-02-17 17:11:14', '2020-02-17 17:11:14', '17-Feb-2020 5:11:14 PM@17-Feb-2020 5:11:14 PM@17-Feb-2020 5:11:09 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190379', '1', 1, '2020-02-17 17:11:14', 0, 1),
(386, '2020-02-17 17:11:20', '2020-02-17 17:11:20', '17-Feb-2020 5:11:19 PM@17-Feb-2020 5:11:19 PM@17-Feb-2020 5:11:14 PM', 2, '16ba37f1fe37d640', 2, 7, 7, '1020190376', '1', 1, '2020-02-17 17:11:20', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `scanticketlistinvalid`
--

CREATE TABLE `scanticketlistinvalid` (
  `id` int(11) NOT NULL,
  `scanner_type` int(11) NOT NULL,
  `api_hit_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `m_timestamp` text COLLATE utf8_bin,
  `scompany_id` int(11) NOT NULL,
  `device_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ticket_category_id` int(11) NOT NULL,
  `ticket_subcategory_id` int(11) NOT NULL DEFAULT '0',
  `barcode_no` varchar(255) COLLATE utf8_bin NOT NULL,
  `gate_no` tinyint(4) NOT NULL,
  `message` text COLLATE utf8_bin,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `scanticketlistinvalid`
--

INSERT INTO `scanticketlistinvalid` (`id`, `scanner_type`, `api_hit_timestamp`, `created_at`, `m_timestamp`, `scompany_id`, `device_id`, `ticket_category_id`, `ticket_subcategory_id`, `barcode_no`, `gate_no`, `message`, `status`) VALUES
(1, 2, '2020-02-17 13:43:44', '2020-02-17 13:43:44', 'Feb 17, 2020 1:43:42 PM@Feb 17, 2020 1:43:42 PM@T2', 2, '4694d752de928254', 7, 7, '06682440', 1, 'Series Invalid', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sendbird_data`
--

CREATE TABLE `sendbird_data` (
  `id` int(11) NOT NULL,
  `channel_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `message_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL,
  `message_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `like` int(11) NOT NULL,
  `comment` int(11) NOT NULL,
  `comment_by` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sendbird_data`
--

INSERT INTO `sendbird_data` (`id`, `channel_url`, `message_id`, `user_id`, `message_type`, `like`, `comment`, `comment_by`) VALUES
(2, 'sendbird_group_channel_43842_d2dda10d811129e1d2112803a5bcd166b73d5e98', '1910226302', 45, 'FILE', 61, 2, '[{\"user_id\":\"45\",\"comment_message\":\"amit\"},{\"user_id\":\"44\",\"comment_message\":\"ankesh\"}]'),
(3, 'sendbird_group_channel_43842_d2dda10d811129e1d2112803a5bcd166b73d5e98', '1910226301', 44, 'FILE', 27, 2, '[{\"user_id\":\"44\",\"comment_message\":\"amit\"},{\"user_id\":\"45\",\"comment_message\":\"ankesh\"}]'),
(5, 'sendbird_group_channel_43842_d2dda10d811129e1d2112803a5bcd166b73d5e98', '1910226305', 45, 'FILE', 5, 2, '[{\"user_id\":\"4\",\"comment_message\":\"amit\"},{\"user_id\":\"45\",\"comment_message\":\"ankesh\"}]'),
(15, 'sendbird_group_channel_43842_d2dda10d811129e1d2112803a5bcd166b73d5e98', '1924386048', 47, 'MESG', 17, 0, '[]'),
(16, 'sendbird_group_channel_43842_d2dda10d811129e1d2112803a5bcd166b73d5e98', '1922508380', 47, 'MESG', 3, 1, '[{\"user_id\":\"45\",\"comment_message\":\"amit\"}]'),
(17, 'sendbird_group_channel_43842_254a52f724c8c95a81b35745d882191bfdcba51c', '1934213227', 56, 'MESG', 3, 2, '[{\"user_id\":\"55\",\"comment_message\":\"Hi micky\"},{\"user_id\":\"56\",\"comment_message\":\" Hello brother\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `signup_payment`
--

CREATE TABLE `signup_payment` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `payment_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `payment_initiated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `subscription_id` int(11) DEFAULT '0',
  `no_of_days` int(11) DEFAULT '0',
  `user_expiry_date` date NOT NULL,
  `amount` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `credit_amount` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `transaction_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `payment_gateway` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `bank_txn_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `payment_mode` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `gateway_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `gateway_txn_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `gateway_fee` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `signup_payment`
--

INSERT INTO `signup_payment` (`id`, `created_at`, `user_id`, `company_id`, `payment_status`, `payment_initiated_date`, `subscription_id`, `no_of_days`, `user_expiry_date`, `amount`, `credit_amount`, `transaction_id`, `payment_gateway`, `bank_name`, `bank_txn_id`, `payment_mode`, `gateway_name`, `gateway_txn_id`, `gateway_fee`, `status`) VALUES
(185, '2019-09-16 15:41:58', 1, 2, 'waiting', '2019-09-16 15:41:58', 0, 0, '2020-09-16', '1.02', '1', 'ORDS95403315X1568628718', '1', NULL, NULL, NULL, NULL, NULL, '0.02', 2),
(186, '2019-09-16 15:45:48', 1, 2, 'credit', '2019-09-16 15:45:48', 0, 0, '2020-09-16', '1.02', '1', 'ORDS82011856X1568628948', '1', 'SBI', '15580011697', 'NB', 'SBI', '20190916111212800110168442800835533', '0.02', 2),
(187, '2019-09-16 16:09:34', 1, 2, 'credit', '2019-09-16 16:09:34', 0, 0, '2020-09-16', '1.02', '0.98', 'ORDS19335499X1568630374', '1', 'SBI', '13637336061', 'NB', 'SBI', '20190916111212800110168088701868679', '0.02', 2),
(188, '2019-09-16 16:10:29', 1, 2, 'TXN_FAILURE', '2019-09-16 16:10:29', 0, 0, '2020-09-16', '1', '0.98', 'ORDS38650851X1568630429', '1', 'SBI', '19980085460', 'NB', 'SBI', '20190916111212800110168902000837111', '0.02', 2),
(189, '2019-09-16 16:13:55', 1, 2, 'waiting', '2019-09-16 16:13:55', 0, 0, '2020-09-16', '1', '0.98', 'ORDS85664457X1568630635', '1', NULL, NULL, NULL, NULL, NULL, '0.02', 2),
(190, '2019-09-16 16:14:23', 1, 2, 'TXN_FAILURE', '2019-09-16 16:14:23', 0, 0, '2020-09-16', '1', '0.98', 'ORDS94678829X1568630663', '1', NULL, '', NULL, NULL, '20190916111212800110168194800845615', '0.02', 2),
(191, '2019-09-17 12:42:49', 1, 2, 'waiting', '2019-09-17 12:42:49', 0, 0, '2020-09-17', '1', '0.98', 'ORDS17482095X1568704369', '1', NULL, NULL, NULL, NULL, NULL, '0.02', 2),
(192, '2019-09-17 12:45:26', 1, 2, 'waiting', '2019-09-17 12:45:26', 0, 0, '2020-09-17', '1', '0.98', 'ORDS47396256X1568704526', '1', NULL, NULL, NULL, NULL, NULL, '0.02', 2),
(193, '2019-09-17 12:46:50', 1, 2, 'waiting', '2019-09-17 12:46:50', 0, 0, '2020-09-17', '1', '0.98', 'ORDS51890414X1568704610', '1', NULL, NULL, NULL, NULL, NULL, '0.02', 2),
(194, '2019-09-17 13:08:25', 1, 2, 'waiting', '2019-09-17 13:08:25', 0, 0, '2020-09-17', '1', '0.98', 'ORDS23970895X1568705905', '1', NULL, NULL, NULL, NULL, NULL, '0.02', 2),
(195, '2019-09-17 13:09:06', 1, 2, 'waiting', '2019-09-17 13:09:06', 0, 0, '2020-09-17', '1', '0.98', 'ORDS24814094X1568705946', '1', NULL, NULL, NULL, NULL, NULL, '0.02', 2),
(196, '2019-09-17 13:09:24', 1, 2, 'waiting', '2019-09-17 13:09:24', 0, 0, '2020-09-17', '1', '0.98', 'ORDS71844586X1568705964', '1', NULL, NULL, NULL, NULL, NULL, '0.02', 2),
(197, '2019-09-17 13:11:30', 1, 2, 'waiting', '2019-09-17 13:11:30', 0, 0, '2020-09-17', '1', '0.98', 'ORDS43970784X1568706090', '1', NULL, NULL, NULL, NULL, NULL, '0.02', 2),
(198, '2019-09-17 13:15:03', 1, 2, 'credit', '2019-09-17 13:15:03', 0, 0, '2020-09-17', '1', '0.98', 'ORDS80026624X1568706303', '1', 'SBI', '10197457714', 'NB', 'SBI', '20190917111212800110168034200848228', '0.02', 2),
(199, '2019-09-17 14:10:58', 1, 2, 'TXN_FAILURE', '2019-09-17 14:10:58', 0, 0, '2020-09-17', '1', '0.98', 'ORDS32982298X1568709658', '1', 'SBI', '15808561472', 'NB', 'SBI', '20190917111212800110168220900851846', '0.02', 2),
(200, '2019-09-24 17:23:10', 1, 2, 'credit', '2019-09-24 17:23:10', 0, 0, '2020-09-24', '1', '0.98', 'ORDS32334193X1569325990', '1', 'SBI', '16911142601', 'NB', 'SBI', '20190924111212800110168596100875606', '0.02', 2),
(201, '2019-09-24 17:26:26', 1, 2, 'TXN_FAILURE', '2019-09-24 17:26:26', 0, 0, '2020-09-24', '1', '0.98', 'ORDS93443945X1569326186', '1', 'SBI', '13779482875', 'NB', 'SBI', '20190924111212800110168848600861503', '0.02', 2),
(202, '2019-09-24 17:30:57', 1, 2, 'credit', '2019-09-24 17:30:57', 0, 0, '2020-09-24', '255', '248.98', 'ORDS8479367X1569326457', '1', 'SBI', '11679859554', 'NB', 'SBI', '20190924111212800110168748400871296', '6.02', 2),
(203, '2019-09-24 17:34:38', 1, 2, 'credit', '2019-09-24 17:34:38', 0, 0, '2020-09-24', '255', '248.98', 'ORDS81321988X1569326678', '1', 'SBI', '11392024640', 'NB', 'SBI', '20190924111212800110168635500864423', '6.02', 2),
(204, '2019-09-24 17:36:35', 1, 2, 'TXN_FAILURE', '2019-09-24 17:36:35', 0, 0, '2020-09-24', '255', '248.98', 'ORDS84804311X1569326795', '1', 'SBI', '10255379340', 'NB', 'SBI', '20190924111212800110168589800854941', '6.02', 2),
(205, '2019-09-24 17:36:52', 1, 2, 'TXN_FAILURE', '2019-09-24 17:36:52', 0, 0, '2020-09-24', '255', '248.98', 'ORDS48498915X1569326812', '1', 'SBI', '15998521142', 'NB', 'SBI', '20190924111212800110168973600879802', '6.02', 2),
(206, '2019-09-24 17:37:09', 1, 2, 'credit', '2019-09-24 17:37:09', 0, 0, '2020-09-24', '255', '248.98', 'ORDS51418105X1569326829', '1', 'SBI', '17767120322', 'NB', 'SBI', '20190924111212800110168705300866231', '6.02', 2),
(207, '2019-09-24 17:38:29', 1, 2, 'credit', '2019-09-24 17:38:29', 0, 0, '2020-09-24', '255', '248.98', 'ORDS74448978X1569326909', '1', 'SBI', '17657883416', 'NB', 'SBI', '20190924111212800110168634300866055', '6.02', 2);

-- --------------------------------------------------------

--
-- Table structure for table `step_counter_data`
--

CREATE TABLE `step_counter_data` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `activity_id` int(11) NOT NULL,
  `activity_global_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `emp_steps` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `start_date_time` datetime NOT NULL,
  `end_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `step_counter_data`
--

INSERT INTO `step_counter_data` (`id`, `created_at`, `activity_id`, `activity_global_id`, `emp_steps`, `user_id`, `group_id`, `start_date_time`, `end_date_time`) VALUES
(2, '2018-09-10 10:20:58', 11, 'stepcounter01', 9656, 18, 5, '2018-09-10 17:27:00', '2018-09-10 20:27:00'),
(3, '2018-09-10 13:51:54', 11, 'stepcounter01', 13, 2, 5, '2018-09-10 03:14:18', '2018-09-10 18:17:31'),
(4, '2018-09-11 11:22:12', 11, 'stepcounter01', 11, 35, 5, '2018-09-10 17:27:00', '2018-09-11 18:28:00'),
(6, '2018-09-14 10:32:32', 12, 'stepcounter01', 15, 18, 5, '2018-09-14 12:09:06', '2018-09-14 18:36:47'),
(7, '2018-09-14 11:05:12', 12, 'stepcounter01', 10, 2, 5, '2018-09-14 12:09:06', '2018-09-14 18:36:47'),
(9, '2018-09-20 08:35:18', 12, 'stepcounter01', 10, 38, 5, '2018-09-14 12:09:06', '2018-09-14 18:36:47'),
(10, '2018-09-20 08:37:01', 12, 'stepcounter01', 12, 12, 5, '2018-09-14 12:09:06', '2018-09-14 18:36:47'),
(11, '2018-09-27 09:51:49', 12, 'stepcounter01', 35, 33, 4, '2018-09-14 12:09:06', '2018-09-14 18:36:47'),
(12, '2018-09-27 09:52:36', 12, 'stepcounter01', 45, 4, 4, '2018-09-14 12:09:06', '2018-09-14 18:36:47');

-- --------------------------------------------------------

--
-- Table structure for table `step_counter_like`
--

CREATE TABLE `step_counter_like` (
  `id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `activity_global_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `like_for` int(11) NOT NULL,
  `like_by` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `step_counter_like`
--

INSERT INTO `step_counter_like` (`id`, `activity_id`, `activity_global_id`, `like_for`, `like_by`, `group_id`, `created_at`) VALUES
(2, 12, 'stepcounter01', 18, 12, 5, '2018-09-21 14:05:47'),
(3, 12, 'stepcounter01', 2, 12, 5, '2018-09-21 14:06:20'),
(5, 12, 'stepcounter01', 18, 2, 5, '2018-09-26 12:14:44'),
(6, 12, 'stepcounter01', 18, 38, 5, '2018-09-26 12:16:03'),
(7, 12, 'stepcounter01', 12, 38, 5, '2018-09-26 12:17:05'),
(8, 12, 'stepcounter01', 12, 12, 5, '2018-09-26 12:17:13'),
(9, 12, 'stepcounter01', 12, 18, 5, '2018-09-26 12:17:20'),
(10, 12, 'stepcounter01', 2, 38, 5, '2018-09-27 07:29:13'),
(11, 12, 'stepcounter01', 2, 2, 5, '2018-09-27 07:30:10'),
(13, 12, 'stepcounter01', 2, 2, 5, '2018-09-27 07:35:20'),
(14, 12, 'stepcounter01', 2, 2, 5, '2018-09-27 07:36:52'),
(15, 12, 'stepcounter01', 2, 2, 5, '2018-09-27 07:36:52'),
(16, 12, 'stepcounter01', 18, 18, 5, '2018-09-27 07:41:30'),
(17, 12, 'stepcounter01', 18, 18, 5, '2018-09-27 07:41:33'),
(18, 12, 'stepcounter01', 18, 18, 5, '2018-09-27 07:41:35'),
(19, 12, 'stepcounter01', 18, 18, 5, '2018-09-27 07:41:36'),
(20, 12, 'stepcounter01', 18, 18, 5, '2018-09-27 07:41:37'),
(21, 12, 'stepcounter01', 18, 18, 5, '2018-09-27 07:41:37'),
(22, 12, 'stepcounter01', 18, 18, 5, '2018-09-27 07:41:38'),
(23, 12, 'stepcounter01', 18, 18, 5, '2018-09-27 07:41:39'),
(24, 12, 'stepcounter01', 18, 38, 5, '2018-09-27 07:43:24'),
(25, 12, 'stepcounter01', 18, 38, 5, '2018-09-27 07:43:25'),
(26, 12, 'stepcounter01', 18, 38, 5, '2018-09-27 07:43:26'),
(27, 12, 'stepcounter01', 18, 38, 5, '2018-09-27 07:43:27'),
(34, 12, 'stepcounter01', 33, 4, 4, '2018-09-27 09:57:07'),
(35, 12, 'stepcounter01', 33, 4, 4, '2018-09-27 09:57:08'),
(36, 12, 'stepcounter01', 33, 4, 4, '2018-09-27 09:57:09'),
(37, 12, 'stepcounter01', 33, 4, 4, '2018-09-27 09:57:10'),
(38, 12, 'stepcounter01', 4, 4, 4, '2018-09-27 09:58:48'),
(39, 12, 'stepcounter01', 4, 4, 4, '2018-09-27 09:58:49'),
(40, 12, 'stepcounter01', 4, 4, 4, '2018-09-27 09:58:49'),
(41, 12, 'stepcounter01', 4, 4, 4, '2018-09-27 09:58:50'),
(42, 12, 'stepcounter01', 4, 4, 4, '2018-09-27 09:58:51'),
(43, 12, 'stepcounter01', 4, 33, 4, '2018-09-27 09:59:00'),
(44, 12, 'stepcounter01', 4, 33, 4, '2018-09-27 09:59:01');

-- --------------------------------------------------------

--
-- Table structure for table `survey`
--

CREATE TABLE `survey` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `duration` varchar(20) COLLATE utf8_bin NOT NULL,
  `expiry_date` date NOT NULL,
  `information` text COLLATE utf8_bin NOT NULL,
  `total_questions` int(11) NOT NULL,
  `anonymous` tinyint(4) DEFAULT '0',
  `publish_date` datetime DEFAULT NULL,
  `watchers` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `role_base` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `user_base` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `document` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `in_itinerary` int(11) NOT NULL DEFAULT '0' COMMENT '1 - survey add in itinerary, 0 - survey not add in itinerary 	',
  `notification_flag` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `survey`
--

INSERT INTO `survey` (`id`, `created_at`, `company_id`, `title`, `duration`, `expiry_date`, `information`, `total_questions`, `anonymous`, `publish_date`, `watchers`, `role_base`, `user_base`, `document`, `in_itinerary`, `notification_flag`) VALUES
(1, '2018-08-14 04:40:54', 2, 'sample survey12356', '5', '2020-01-31', '<p>1. his is survey information.</p>\r\n\r\n<p>2. This is survey information.</p>\r\n\r\n<p>3. This is survey information.</p>\r\n\r\n<p>4. This is survey information.</p>\r\n\r\n<p>5. This is survey information.</p>\r\n\r\n<p>6. This is survey information.</p>\r\n\r\n<p>7. This is survey information.</p>\r\n\r\n<p>8. This is survey information.</p>\r\n\r\n<p>9. This is survey information.</p>\r\n\r\n<p>10. his is survey information.</p>\r\n\r\n<p>11. This is survey information.</p>', 1, 1, '2018-12-29 20:30:00', '1', '', '1', '154529365519853382305c1b4f57096ac.pdf', 1, 4),
(7, '2018-12-14 15:38:49', 2, 'second feed back', '14', '2018-12-31', '<p>ffffff</p>', 2, 0, '2018-12-24 20:05:00', NULL, '1', '10,63,1', '154529365519853382305c1b4f57096ac.pdf', 0, 0),
(26, '2019-01-08 09:19:51', 2, 'ff', '10', '2019-01-29', '<p>fffffff</p>', 3, 0, '2019-01-09 14:45:00', '1', '1', '10', NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `survey_question`
--

CREATE TABLE `survey_question` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `survey_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_type` int(11) NOT NULL COMMENT '1 - text, 2 - multiple, 3 - dropdown',
  `question` text COLLATE utf8_bin NOT NULL,
  `options` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `survey_question`
--

INSERT INTO `survey_question` (`id`, `created_at`, `survey_id`, `company_id`, `question_id`, `answer_type`, `question`, `options`) VALUES
(1, '2018-09-04 07:30:00', 1, 2, 2, 2, 'First Prime Ministers of India?', 'Mahatma Gandhi, Jawaharlal Nehru, Indra Gandhi,None of these.'),
(42, '2018-12-14 15:38:06', 7, 2, 3, 1, 'First president of India.', ''),
(43, '2018-12-14 15:38:49', 7, 2, 3, 1, 'First president of India.\r\n\'D1\' being the chosen cell.\r\n\r\nInstead of using this code for every cell I need wrapped, is there a way to make the entire Excel Worksheet automatically wrap everything?\r\n\r\nOr is there a better practice technique to use for specified columns', ''),
(57, '2019-01-08 09:19:51', 26, 2, 2, 2, 'First Prime Ministers of India?', 'Mahatma Gandhi, Jawaharlal Nehru, Indra Gandhi, None of these.'),
(58, '2019-01-08 09:19:51', 26, 2, 3, 1, 'First president of India.', ''),
(59, '2019-01-08 09:19:51', 26, 2, 4, 1, 'First Prime Ministers of India1?', '');

-- --------------------------------------------------------

--
-- Table structure for table `survey_response`
--

CREATE TABLE `survey_response` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `survey_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `survey_question_id` int(11) NOT NULL,
  `response` longtext COLLATE utf8_bin NOT NULL,
  `anonymous` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `survey_response`
--

INSERT INTO `survey_response` (`id`, `created_at`, `survey_id`, `employee_id`, `survey_question_id`, `response`, `anonymous`) VALUES
(754, '2019-01-22 15:28:15', 26, 10, 57, 'Mahatma Gandhi', 0),
(755, '2019-08-31 14:36:59', 1, 1, 1, 'Mahatma Gandhi', 0),
(756, '2019-08-31 14:37:20', 1, 3, 2, 'hiii', 0),
(757, '2019-09-02 07:50:00', 1, 2, 1, 'Jawaharlal Nehru', 0),
(758, '2019-09-02 07:50:07', 1, 2, 2, 'Test', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ticketconfiguration`
--

CREATE TABLE `ticketconfiguration` (
  `id` int(4) NOT NULL,
  `company_id` int(4) NOT NULL,
  `configuration_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `validation_with` tinyint(4) NOT NULL,
  `payment_gateway` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `ticketconfiguration`
--

INSERT INTO `ticketconfiguration` (`id`, `company_id`, `configuration_name`, `validation_with`, `payment_gateway`, `status`, `created_at`) VALUES
(1, 2, 'QR Config', 2, 1, 1, '2019-10-16 17:17:39'),
(3, 2, 'Normal Config', 3, 1, 2, '2019-10-17 17:22:44'),
(4, 7, 'Barcode Config', 1, 2, 1, '2019-10-18 11:54:28');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_report`
--

CREATE TABLE `ticket_report` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `employee_id` varchar(255) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `itinerary_id` int(11) NOT NULL,
  `ticket_count` int(11) DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ticket_report`
--

INSERT INTO `ticket_report` (`id`, `user_id`, `employee_id`, `company_id`, `category_id`, `itinerary_id`, `ticket_count`, `created_at`) VALUES
(6684, 173, '8872617531', 2, 4, 1, 4, '2019-04-11 18:54:45'),
(6685, 166, '8558860098', 2, 3, 1, 2, '2019-04-11 19:05:44'),
(6686, 172, '8872617532', 2, 4, 1, 4, '2019-04-11 18:54:45'),
(6687, 173, '8872617531', 2, 4, 1, 0, '2019-04-11 18:54:45'),
(6688, 166, '8558860098', 2, 3, 1, 0, '2019-04-11 19:05:44'),
(6689, 172, '8872617532', 2, 4, 1, 0, '2019-04-11 18:54:45');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_scan_result`
--

CREATE TABLE `ticket_scan_result` (
  `id` int(11) NOT NULL,
  `barcode_no` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_transactions`
--

CREATE TABLE `ticket_transactions` (
  `id` int(11) NOT NULL,
  `random_identifire` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `itinerary_id` int(11) NOT NULL,
  `payment_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `amount` varchar(255) NOT NULL,
  `gateway_fees` varchar(255) NOT NULL,
  `payment_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_user`
--

CREATE TABLE `ticket_user` (
  `id` int(11) NOT NULL,
  `random_identifire` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `itinerary_id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `price` int(11) DEFAULT '0',
  `total_ticket` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL DEFAULT '0',
  `payment_status` varchar(255) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `titlefight_votes`
--

CREATE TABLE `titlefight_votes` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `activity_id` int(11) NOT NULL,
  `voted_by` int(11) NOT NULL,
  `voted_for` int(11) NOT NULL,
  `round` int(11) NOT NULL,
  `winner` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `titlefight_votes`
--

INSERT INTO `titlefight_votes` (`id`, `created_at`, `activity_id`, `voted_by`, `voted_for`, `round`, `winner`) VALUES
(1, '2018-08-13 16:38:22', 1, 3, 3, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `transport_type`
--

CREATE TABLE `transport_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transport_type`
--

INSERT INTO `transport_type` (`id`, `name`) VALUES
(1, 'Train'),
(2, 'Bus'),
(3, 'Plane'),
(4, 'Personal Vehicle'),
(5, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `userrequest`
--

CREATE TABLE `userrequest` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `mobile_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `country_code` int(5) NOT NULL DEFAULT '91',
  `activation_code` longtext NOT NULL,
  `information` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `reply_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT ' 1 - true, 0 - false'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userrequest`
--

INSERT INTO `userrequest` (`id`, `created_at`, `name`, `email`, `mobile_no`, `country_code`, `activation_code`, `information`, `reply_status`) VALUES
(33, '2018-11-22 15:13:30', 'amit yadav', 'amit12nig96@gmail.com', '9596561100', 91, 'BUZZ', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(200) COLLATE utf8_bin NOT NULL,
  `email` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `second_password` varchar(255) COLLATE utf8_bin NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users table will hold records for storing application users';

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `second_password`, `remember_token`, `created_at`, `role`) VALUES
(1, 'Admin', 'info@oneteam.in', 'eaebaffc18e30987b0ff3a907f83fcfba7ca0115c08253e12f0832dc83eb7043', '', '', '2017-03-12 22:18:46', 1),
(2, 'Amit Kumar Yadav', 'info@oneteam.in', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '', '', '2018-04-10 21:42:14', 1),
(3, 'Prabhakar Sharma', 'prabhakar.sharma@sa-group.in', 'eaebaffc18e30987b0ff3a907f83fcfba7ca0115c08253e12f0832dc83eb7043', '', NULL, '2018-07-20 07:53:08', 1),
(4, 'main admin', 'info@oneteam.in', '1fec7401b302e76333629c9f68415b7718769e072f0613e54726143cda06d789', '', NULL, '2019-04-01 11:50:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `created_at`, `name`, `status`) VALUES
(1, '2018-12-05 11:14:18', 'Viewer', 1),
(2, '2018-12-05 11:14:18', 'Participant', 1),
(3, '2018-12-05 11:14:18', 'Event Manager', 1),
(4, '2018-12-05 11:14:18', 'Spoc', 1);

-- --------------------------------------------------------

--
-- Table structure for table `warriors_buzzlive_active_users_count`
--

CREATE TABLE `warriors_buzzlive_active_users_count` (
  `id` int(11) NOT NULL,
  `livestream_url_id` int(11) NOT NULL,
  `users_count` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `warriors_buzzlive_active_users_list`
--

CREATE TABLE `warriors_buzzlive_active_users_list` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `livestream_url_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `warriors_buzzlive_join_users_count`
--

CREATE TABLE `warriors_buzzlive_join_users_count` (
  `id` int(11) NOT NULL,
  `livestream_url_id` int(11) DEFAULT NULL,
  `users_count` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `warriors_buzzlive_join_users_list`
--

CREATE TABLE `warriors_buzzlive_join_users_list` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `livestream_url_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `warriors_buzzlive_reactions`
--

CREATE TABLE `warriors_buzzlive_reactions` (
  `rid` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `warriors_buzzlive_reactions`
--

INSERT INTO `warriors_buzzlive_reactions` (`rid`, `id`, `name`, `icon`, `created_at`, `status`) VALUES
(1, 6, 'Good Morning', 'happy.png', '2020-06-30 15:43:08', 1),
(2, 4, 'Applaud', 'applaud.png', '2020-06-30 15:43:08', 1),
(3, 3, 'Awesome', 'awesome.png', '2020-06-30 15:43:08', 1),
(4, 2, 'Jabardast', 'jabardast.png', '2020-06-30 15:43:08', 1),
(5, 1, 'Agree', 'agree.png', '2020-06-30 15:43:08', 1),
(6, 5, 'Nice', 'nice.png', '2020-06-30 15:43:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `warriors_buzzlive_reactions_report`
--

CREATE TABLE `warriors_buzzlive_reactions_report` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reaction_id` int(11) DEFAULT NULL,
  `livestream_url_id` int(11) DEFAULT NULL,
  `reaction_count` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `warriors_buzzlive_streaming_url`
--

CREATE TABLE `warriors_buzzlive_streaming_url` (
  `id` int(11) NOT NULL,
  `url` text,
  `session_id` int(11) DEFAULT NULL,
  `report_created_status` tinyint(4) NOT NULL DEFAULT '0',
  `event_content` longtext NOT NULL,
  `company_logo` varchar(255) NOT NULL,
  `event_logo` varchar(255) NOT NULL,
  `chatcamp_channel_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `chat_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=Active;0=Inactive',
  `live_button_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=Active;0=Inactive	'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `warriors_buzzlive_streaming_url`
--

INSERT INTO `warriors_buzzlive_streaming_url` (`id`, `url`, `session_id`, `report_created_status`, `event_content`, `company_logo`, `event_logo`, `chatcamp_channel_id`, `chat_status`, `live_button_status`) VALUES
(1, 'https://www.youtube.com/embed/Q9ij1MUCXVg?playsinline=1&autoplay=1&enablejsapi=1&modestbranding=1&fs=0', 5, 1, '<h5 class=\"topic\"> BUSINESS OPPORTUNITY SEMINAR 2020</h5>\r\n             <div class=\"speaker\"><i class=\"fa fa-volume-down\"></i> Ms Deepa Pathak</div>\r\n             <div class=\"host\"><i class=\"fa fa-book\"></i> Mr. Niraj Tyagi</div>', 'https://warriors.oneteam.in/live/images/logo2.png', 'https://warriors.oneteam.in/live/images/logo1.png', '5f0ee5dfc7ecde0001a46700', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `warriors_lottery_data`
--

CREATE TABLE `warriors_lottery_data` (
  `id` int(11) NOT NULL,
  `luckydraw_name` varchar(255) DEFAULT NULL,
  `tagline` varchar(255) DEFAULT NULL,
  `spin_screen_bg` varchar(255) DEFAULT NULL,
  `winner_screen_bg` varchar(255) DEFAULT NULL,
  `spin_img_1` varchar(255) DEFAULT NULL,
  `spin_img_2` varchar(255) DEFAULT NULL,
  `spin_img_3` varchar(255) DEFAULT NULL,
  `prize_image_1` varchar(255) DEFAULT NULL,
  `prize_image_2` varchar(255) DEFAULT NULL,
  `prize_image_3` varchar(255) DEFAULT NULL,
  `prize_image_4` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `company_logo` varchar(255) DEFAULT NULL,
  `event_logo` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `warriors_lottery_data`
--

INSERT INTO `warriors_lottery_data` (`id`, `luckydraw_name`, `tagline`, `spin_screen_bg`, `winner_screen_bg`, `spin_img_1`, `spin_img_2`, `spin_img_3`, `prize_image_1`, `prize_image_2`, `prize_image_3`, `prize_image_4`, `logo`, `company_logo`, `event_logo`, `status`) VALUES
(1, 'iPhone 11 Pro', 'Are You Ready', 'https://warriors.oneteam.in/lotterydraw/images/spin_bg.jpg', 'https://warriors.oneteam.in/lotterydraw/images/winner_bg.jpg', 'https://warriors.oneteam.in/lotterydraw/images/ele1.png', 'https://warriors.oneteam.in/lotterydraw/images/ele3.png', 'https://warriors.oneteam.in/lotterydraw/images/ele2.png', 'https://warriors.oneteam.in/lotterydraw/images/prize1.png', 'https://warriors.oneteam.in/lotterydraw/images/prize2.png', 'https://warriors.oneteam.in/lotterydraw/images/prize3.png', 'https://warriors.oneteam.in/lotterydraw/images/prize4.png', 'https://warriors.oneteam.in/lotterydraw/images/Biosash.png', 'https://warriors.oneteam.in/lotterydraw/images/SOL-White.png', 'https://warriors.oneteam.in/lotterydraw/images/Genesis.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `warriors_lottery_participants`
--

CREATE TABLE `warriors_lottery_participants` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `participant_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '1' COMMENT '0=Ineligible;1=Active;2=Winner'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `warriors_lottery_winners`
--

CREATE TABLE `warriors_lottery_winners` (
  `id` int(11) NOT NULL,
  `luckydraw_name` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `participant_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `webhookuser`
--

CREATE TABLE `webhookuser` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mobile_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `number` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wheel_attempts`
--

CREATE TABLE `wheel_attempts` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `wheel_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `others_id` int(11) NOT NULL,
  `answer` varchar(255) COLLATE utf8_bin NOT NULL,
  `question` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `qcategory_id` int(11) NOT NULL DEFAULT '0',
  `qcompanyvalue_id` int(11) NOT NULL DEFAULT '0',
  `qanswer_type` int(11) NOT NULL,
  `qoptions` int(11) DEFAULT NULL,
  `qcorrect_answer` varchar(255) COLLATE utf8_bin NOT NULL,
  `qanswer_validation` tinyint(4) NOT NULL DEFAULT '0',
  `qtagline` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `wheel_attempts`
--

INSERT INTO `wheel_attempts` (`id`, `created_at`, `user_id`, `wheel_id`, `question_id`, `others_id`, `answer`, `question`, `qcategory_id`, `qcompanyvalue_id`, `qanswer_type`, `qoptions`, `qcorrect_answer`, `qanswer_validation`, `qtagline`) VALUES
(1, '2018-08-13 10:06:25', 1, 1, 3, 0, 'rajendra prasad', 'First president of India.', 3, 0, 1, 0, '', 0, 'rajendra prasad is my ans for wheel.'),
(2, '2018-08-13 11:56:01', 1, 1, 3, 0, 'my name is amit kumar yadav.', 'First president of India.', 3, 0, 1, 0, '', 0, 'my name is amit kumar yadav. is my ans for wheel.'),
(3, '2018-08-13 16:15:10', 3, 1, 3, 0, 'Dr Rajendra Prasad', 'First president of India.', 3, 0, 1, 0, '', 0, 'Dr Rajendra Prasad is my ans for wheel.'),
(4, '2018-08-13 16:25:35', 3, 1, 2, 0, '', 'First Prime Ministers of India?', 2, 0, 2, 0, '', 0, ' is my answer for wheel.'),
(5, '2018-08-13 16:25:38', 3, 1, 2, 0, '', 'First Prime Ministers of India?', 2, 0, 2, 0, '', 0, ' is my answer for wheel.'),
(6, '2018-08-13 16:25:40', 3, 1, 2, 0, '', 'First Prime Ministers of India?', 2, 0, 2, 0, '', 0, ' is my answer for wheel.'),
(7, '2018-08-13 16:25:47', 3, 1, 2, 0, '', 'First Prime Ministers of India?', 2, 0, 2, 0, '', 0, ' is my answer for wheel.'),
(8, '2018-08-13 16:25:49', 3, 1, 2, 0, '', 'First Prime Ministers of India?', 2, 0, 2, 0, '', 0, ' is my answer for wheel.'),
(9, '2018-08-13 16:25:51', 3, 1, 2, 0, 'Mahatma Gandhi', 'First Prime Ministers of India?', 2, 0, 2, 0, '', 0, ' is my answer for wheel.'),
(10, '2018-08-13 16:25:53', 3, 1, 2, 0, 'Mahatma Gandhi', 'First Prime Ministers of India?', 2, 0, 2, 0, '', 0, ' is my answer for wheel.'),
(11, '2018-08-13 16:26:39', 4, 1, 2, 0, 'Mahatma Gandhi', 'First Prime Ministers of India?', 2, 0, 2, 0, '', 0, ' is my answer for wheel.'),
(12, '2018-08-13 16:26:48', 4, 1, 2, 0, '', 'First Prime Ministers of India?', 2, 0, 2, 0, '', 0, ' is my answer for wheel.'),
(13, '2018-08-13 16:26:53', 4, 1, 2, 0, '', 'First Prime Ministers of India?', 2, 0, 2, 0, '', 0, ' is my answer for wheel.'),
(14, '2018-08-13 16:27:00', 4, 1, 2, 0, '', 'First Prime Ministers of India?', 2, 0, 2, 0, '', 0, ' is my answer for wheel.'),
(15, '2018-08-13 16:27:06', 4, 1, 2, 0, '', 'First Prime Ministers of India?', 2, 0, 2, 0, '', 0, ' is my answer for wheel.'),
(16, '2018-08-13 16:32:48', 3, 1, 3, 0, '', 'First president of India.', 3, 0, 1, 0, '', 0, ' is my ans for wheel.'),
(17, '2018-08-13 16:33:06', 3, 1, 2, 0, '', 'First Prime Ministers of India?', 2, 0, 2, 0, '', 0, ' is my answer for wheel.'),
(18, '2018-08-13 16:34:00', 3, 1, 3, 0, '', 'First president of India.', 3, 0, 1, 0, '', 0, ' is my ans for wheel.'),
(19, '2018-08-13 17:00:47', 6, 1, 3, 0, '', 'First president of India.', 3, 0, 1, 0, '', 0, ' is my ans for wheel.'),
(20, '2018-08-13 17:01:55', 5, 1, 2, 1, 'Jawaharlal Nehru', 'First Prime Ministers of India?', 2, 0, 2, 0, '', 0, 'Jawaharlal Nehru is another person answer for wheel.'),
(21, '2018-08-13 17:02:24', 5, 1, 3, 0, '', 'First president of India.', 3, 0, 1, 0, '', 0, ' is my ans for wheel.'),
(22, '2018-08-13 17:34:14', 7, 1, 3, 3, 'igugugugugu', 'First president of India.', 3, 0, 1, 0, '', 0, 'igugugugugu another person give the ans for you.'),
(23, '2018-08-13 17:51:04', 8, 1, 2, 0, '', 'First Prime Ministers of India?', 2, 0, 2, 0, '', 0, ' is my answer for wheel.'),
(24, '2018-08-13 17:52:20', 8, 1, 3, 0, '', 'First president of India.', 3, 0, 1, 0, '', 0, ' is my ans for wheel.'),
(25, '2018-08-13 17:54:51', 8, 1, 3, 0, '', 'First president of India.', 3, 0, 1, 0, '', 0, ' is my ans for wheel.'),
(26, '2018-08-14 09:56:43', 10, 2, 5, 0, 'Dance', 'Hobbies', 4, 0, 2, 0, '', 0, 'Dance, I love to do in my free time.'),
(27, '2018-08-14 09:57:18', 10, 2, 4, 0, '', 'Your favourite colour', 4, 0, 2, 0, '', 0, '{Value}, is the colour of my choice.'),
(28, '2018-08-14 09:57:37', 10, 2, 5, 0, '', 'Hobbies', 4, 0, 2, 0, '', 0, ', I love to do in my free time.'),
(29, '2018-08-14 09:58:00', 10, 2, 4, 0, 'Pink', 'Your favourite colour', 4, 0, 2, 0, '', 0, '{Value}, is the colour of my choice.'),
(30, '2018-08-14 10:21:43', 10, 2, 7, 0, 'The FactorE', 'Current Organisation', 4, 0, 1, 0, '', 0, 'The FactorE, I am mostly found here during weekdays'),
(31, '2018-08-14 10:25:11', 10, 2, 7, 0, '', 'Current Organisation', 4, 0, 1, 0, '', 0, ', I am mostly found here during weekdays'),
(32, '2018-08-14 10:27:24', 10, 2, 7, 0, 'The FactorE', 'Current Organisation', 4, 0, 1, 0, '', 0, 'The FactorE, I am mostly found here during weekdays'),
(33, '2018-08-14 10:32:29', 10, 2, 7, 0, '', 'Current Organisation', 4, 0, 1, 0, '', 0, ', I am mostly found here during weekdays'),
(34, '2018-08-14 10:34:06', 10, 2, 4, 0, '', 'Your favourite colour', 4, 0, 2, 0, '', 0, '{Value}, is the colour of my choice.'),
(35, '2018-08-14 10:38:52', 10, 2, 4, 0, '', 'Your favourite colour', 4, 0, 2, 0, '', 0, '{Value}, is the colour of my choice.'),
(36, '2018-08-14 15:32:50', 11, 1, 3, 2, 'atal vihar', 'First president of India.', 3, 0, 1, 0, '', 0, 'atal vihar another person give the ans for you.'),
(37, '2018-08-15 17:51:09', 12, 1, 3, 11, 'New Answer', 'First president of India.', 3, 0, 1, 0, '', 0, 'New Answer another person give the ans for you.'),
(38, '2018-08-16 19:25:43', 4, 1, 3, 8, 'me', 'First president of India.', 3, 0, 1, 0, '', 0, 'me another person give the ans for you.'),
(39, '2018-08-16 21:01:10', 10, 2, 6, 0, 'Meerut', 'Where is hometown', 4, 0, 1, 0, '', 0, 'I belong to Meerut'),
(40, '2018-08-16 21:01:56', 10, 2, 6, 0, '', 'Where is hometown', 4, 0, 1, 0, '', 0, 'I belong to '),
(41, '2018-08-16 21:02:19', 10, 2, 5, 0, 'Entertainment', 'Hobbies', 4, 0, 2, 0, '', 0, '{Value} I love to do in my free time.'),
(42, '2018-08-16 21:03:10', 10, 2, 6, 0, '', 'Where is hometown', 4, 0, 1, 0, '', 0, 'I belong to '),
(43, '2018-08-16 22:01:14', 10, 2, 6, 0, 'Meerut', 'Where is hometown', 4, 0, 1, 0, '', 0, 'I belong to Meerut'),
(44, '2018-08-16 22:04:13', 10, 2, 6, 0, '', 'Where is hometown', 4, 0, 1, 0, '', 0, 'I belong to '),
(45, '2018-08-17 00:03:18', 10, 2, 7, 0, 'The FactorE', 'Current Organisation', 4, 0, 1, 0, '', 0, 'I am mostly found at The FactorE during weekdays'),
(46, '2018-08-17 00:53:44', 2, 1, 3, 0, '', 'First president of India.', 3, 0, 1, 0, '', 0, ' is my ans for wheel.'),
(47, '2018-08-17 00:54:35', 2, 1, 3, 11, 'hi', 'First president of India.', 3, 0, 1, 0, '', 0, 'hi another person give the ans for you.'),
(48, '2018-08-17 00:56:10', 2, 1, 3, 12, 'hello modi', 'First president of India.', 3, 0, 1, 0, '', 0, 'hello modi another person give the ans for you.'),
(49, '2018-08-17 01:07:26', 2, 1, 3, 0, '', 'First president of India.', 3, 0, 1, 0, '', 0, ' is my ans for wheel.'),
(50, '2018-08-17 06:31:54', 13, 1, 3, 0, 'if se', 'First president of India.', 3, 0, 1, 0, '', 0, 'if se is my ans for wheel.'),
(51, '2018-08-17 06:32:34', 13, 1, 3, 7, 'yet', 'First president of India.', 3, 0, 1, 0, '', 0, 'yet another person give the ans for you.'),
(52, '2018-08-17 08:20:27', 4, 1, 3, 0, '', 'First president of India.', 3, 0, 1, 0, '', 0, ' is my ans for wheel.'),
(53, '2018-08-17 08:21:00', 4, 1, 3, 0, '', 'First president of India.', 3, 0, 1, 0, '', 0, ' is my ans for wheel.'),
(54, '2018-08-17 08:23:58', 4, 1, 2, 0, '', 'First Prime Ministers of India?', 2, 0, 2, 0, '', 0, ' is my answer for wheel.'),
(55, '2018-08-18 00:45:56', 3, 1, 3, 0, 'Gg', 'First president of India.', 3, 0, 1, 0, '', 0, 'Gg is my ans for wheel.'),
(56, '2018-08-18 03:26:27', 3, 1, 3, 0, 'Jgj', 'First president of India.', 3, 0, 1, 0, '', 0, 'Jgj is my ans for wheel.'),
(57, '2018-08-18 19:29:47', 4, 1, 3, 8, 'me', 'First president of India.', 3, 0, 1, 0, '', 0, 'me another person give the ans for you.'),
(58, '2018-08-18 21:04:09', 16, 1, 3, 0, 'Dr Rajendra Prashad', 'First president of India.', 3, 0, 1, 0, '', 0, 'Dr Rajendra Prashad is my ans for wheel.'),
(59, '2018-08-18 21:11:57', 16, 1, 3, 0, '', 'First president of India.', 3, 0, 1, 0, '', 0, ' is my ans for wheel.'),
(60, '2018-08-18 21:27:59', 17, 1, 3, 0, '', 'First president of India.', 3, 0, 1, 0, '', 0, ' is my ans for wheel.'),
(61, '2018-08-18 21:38:55', 17, 1, 3, 0, 'Ethan', 'First president of India.', 3, 0, 1, 0, '', 0, 'Ethan is my ans for wheel.'),
(62, '2018-08-18 22:22:24', 4, 1, 3, 0, '', 'First president of India.', 3, 0, 1, 0, '', 0, ' is my ans for wheel.'),
(63, '2018-08-18 22:22:37', 16, 1, 3, 0, 'Rajendra prashad', 'First president of India.', 3, 0, 1, 0, '', 0, 'Rajendra prashad is my ans for wheel.'),
(64, '2018-08-18 23:54:03', 18, 1, 3, 0, 'i dont know', 'First president of India.', 3, 0, 1, 0, '', 0, 'i dont know is my ans for wheel.'),
(65, '2018-08-19 03:07:08', 16, 1, 3, 0, '', 'First president of India.', 3, 0, 1, 0, '', 0, ' is my ans for wheel.'),
(66, '2018-08-19 08:34:55', 15, 1, 2, 8, 'Jawaharlal Nehru', 'First Prime Ministers of India?', 2, 0, 2, 0, '', 0, 'Jawaharlal Nehru is another person answer for wheel.'),
(67, '2018-08-19 08:35:56', 15, 1, 3, 4, 'Javaharlal Nehru', 'First president of India.', 3, 0, 1, 0, '', 0, 'Javaharlal Nehru another person give the ans for you.'),
(68, '2018-08-20 00:11:24', 18, 1, 3, 0, 'rted', 'First president of India.', 3, 0, 1, 0, '', 0, 'rted is my ans for wheel.'),
(69, '2018-08-20 08:32:58', 15, 1, 3, 0, '', 'First president of India.', 3, 0, 1, 0, '', 0, ''),
(70, '2018-08-20 18:30:23', 23, 1, 2, 7, ' Jawaharlal Nehru', 'First Prime Ministers of India?', 2, 0, 2, 0, '', 0, ' Jawaharlal Nehru is another person answer for wheel.'),
(71, '2018-08-20 20:21:13', 18, 1, 3, 0, '', 'First president of India.', 3, 0, 1, 0, '', 0, ''),
(72, '2018-08-20 20:23:25', 18, 1, 3, 0, '', 'First president of India.', 3, 0, 1, 0, '', 0, ''),
(73, '2018-08-20 20:23:29', 18, 1, 3, 0, '', 'First president of India.', 3, 0, 1, 0, '', 0, ''),
(74, '2018-08-20 20:23:31', 18, 1, 3, 0, '', 'First president of India.', 3, 0, 1, 0, '', 0, ''),
(75, '2018-08-20 21:15:03', 4, 1, 2, 20, 'Jawaharlal Nehru', 'First Prime Ministers of India?', 2, 0, 2, 0, '', 0, 'Jawaharlal Nehru is another person answer for wheel.'),
(76, '2018-08-20 21:15:36', 4, 1, 2, 20, 'Jawaharlal Nehru', 'First Prime Ministers of India?', 2, 0, 2, 0, '', 0, 'Jawaharlal Nehru is another person answer for wheel.'),
(77, '2018-08-20 21:16:11', 4, 1, 3, 8, 'xyz', 'First president of India.', 3, 0, 1, 0, '', 0, 'xyz another person give the ans for you.'),
(78, '2018-08-20 21:17:02', 4, 1, 3, 0, '', 'First president of India.', 3, 0, 1, 0, '', 0, ''),
(79, '2018-08-20 21:38:01', 4, 1, 2, 0, '', 'First Prime Ministers of India?', 2, 0, 2, 0, '', 0, ''),
(80, '2018-08-20 21:38:19', 4, 1, 3, 0, '', 'First president of India.', 3, 0, 1, 0, '', 0, ''),
(81, '2018-08-21 05:48:14', 25, 1, 2, 17, 'Mahatma Gandhi', 'First Prime Ministers of India?', 4, 0, 2, 0, '', 0, 'Mahatma Gandhi is another person answer for wheel.'),
(82, '2018-08-21 06:02:26', 27, 1, 2, 8, ' Jawaharlal Nehru', 'First Prime Ministers of India?', 4, 0, 2, 0, '', 0, ' Jawaharlal Nehru is another person answer for wheel.'),
(83, '2018-08-21 06:08:10', 25, 1, 3, 0, '', 'First president of India.', 1, 0, 1, 0, '', 0, ''),
(84, '2018-08-21 07:21:57', 28, 1, 2, 5, 'Jawaharlal Nehru', 'First Prime Ministers of India?', 4, 0, 2, 0, '', 0, 'Jawaharlal Nehru is another person answer for wheel.'),
(85, '2018-08-21 09:16:23', 29, 1, 3, 0, '', 'First president of India.', 1, 0, 1, 0, '', 0, ''),
(86, '2018-08-22 22:38:24', 33, 1, 3, 8, 'rajender prasad', 'First president of India.', 1, 0, 1, 0, '', 0, 'rajender prasad another person give the ans for you.'),
(87, '2018-08-22 22:49:42', 33, 1, 3, 4, 'fadsfa', 'First president of India.', 1, 0, 1, 0, '', 0, 'fadsfa another person give the ans for you.'),
(88, '2018-08-22 23:28:59', 4, 1, 3, 15, 'Dbdh', 'First president of India.', 1, 0, 1, 0, '', 0, 'Dbdh another person give the ans for you.'),
(89, '2018-08-22 23:30:26', 4, 1, 3, 29, 'Ecec', 'First president of India.', 1, 0, 1, 0, '', 0, 'Ecec another person give the ans for you.'),
(90, '2018-08-22 23:46:10', 4, 1, 2, 20, 'Jawaharlal Nehru', 'First Prime Ministers of India?', 4, 0, 2, 0, '', 0, 'Jawaharlal Nehru is another person answer for wheel.'),
(91, '2018-08-22 23:47:38', 4, 1, 3, 15, 'Hfh', 'First president of India.', 1, 0, 1, 0, '', 0, 'Hfh another person give the ans for you.'),
(92, '2018-08-22 23:50:45', 4, 1, 3, 22, 'Fjdj', 'First president of India.', 1, 0, 1, 0, '', 0, 'Fjdj another person give the ans for you.'),
(93, '2018-08-22 23:56:28', 36, 1, 3, 13, 'Ccj', 'First president of India.', 1, 0, 1, 0, '', 0, 'Ccj another person give the ans for you.'),
(94, '2018-08-22 23:59:16', 36, 1, 3, 0, 'Ueur', 'First president of India.', 1, 0, 1, 0, '', 0, 'Ueur is my ans for wheel.'),
(95, '2018-08-23 00:07:03', 3, 1, 2, 30, 'Mahatma Gandhi', 'First Prime Ministers of India?', 4, 0, 2, 0, '', 0, 'Mahatma Gandhi is another person answer for wheel.'),
(96, '2018-08-23 22:15:30', 3, 1, 3, 0, 'Hdhd', 'First president of India.', 1, 0, 1, 0, '', 0, 'Hdhd is my ans for wheel.'),
(97, '2018-08-24 22:17:17', 18, 1, 2, 0, 'Jawaharlal Nehru', 'First Prime Ministers of India?', 4, 0, 2, 0, '', 0, 'Jawaharlal Nehru is my answer for wheel.'),
(98, '2018-08-25 13:46:22', 4, 1, 3, 0, '', 'First president of India.', 1, 0, 1, 0, '', 0, ''),
(99, '2018-08-28 01:34:04', 18, 1, 3, 0, '', 'First president of India.', 1, 0, 1, 0, '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `wheel_categories`
--

CREATE TABLE `wheel_categories` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 - active, 2 - inactive',
  `image` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `wheel_categories`
--

INSERT INTO `wheel_categories` (`id`, `created_at`, `category_name`, `status`, `image`) VALUES
(1, '2018-07-30 03:18:31', 'Collaboration', 1, '153287982815656248895b5de3d4a145f.png'),
(2, '2018-07-30 03:18:41', 'Commitment', 1, '153287032113431526265b5dbeb1a928e.png'),
(3, '2018-07-30 03:18:52', 'Fear', 1, '153287033213906535715b5dbebc55e5c.png'),
(4, '2018-07-30 03:19:03', 'Happiness', 1, '15328703434904361405b5dbec75c5da.png'),
(5, '2018-07-30 03:19:13', 'Innovation', 1, '15328703538804205495b5dbed12a9f0.png'),
(6, '2018-07-30 03:19:20', 'Personal', 1, '15328703601495302025b5dbed8b2a30.png'),
(7, '2018-07-30 03:19:48', 'Social', 1, '153287038818884420085b5dbef404e80.png'),
(8, '2018-07-30 03:19:59', 'Strength', 1, '153287039916578172565b5dbeff0533c.png');

-- --------------------------------------------------------

--
-- Table structure for table `wheel_discovery`
--

CREATE TABLE `wheel_discovery` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `company_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1 - active, 2-inactive',
  `self_points` int(11) NOT NULL,
  `others_points` int(11) NOT NULL,
  `attempts` int(11) NOT NULL,
  `information` longtext COLLATE utf8_bin,
  `cantdo_points` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `wheel_discovery`
--

INSERT INTO `wheel_discovery` (`id`, `created_at`, `title`, `company_id`, `status`, `self_points`, `others_points`, `attempts`, `information`, `cantdo_points`) VALUES
(1, '2018-08-13 21:10:52', 'The Facter E wheel', 2, 1, 20, 25, 5, 'It is wheel please try.', -5),
(2, '2018-08-14 00:49:08', 'Know Your Mates', 4, 1, 20, 10, 10, 'Wheel of discovery allows you to discover about your self and others in your organisation.', -5);

-- --------------------------------------------------------

--
-- Table structure for table `wheel_questions`
--

CREATE TABLE `wheel_questions` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity_icons`
--
ALTER TABLE `activity_icons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity_tasks`
--
ALTER TABLE `activity_tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `autoreplymessage`
--
ALTER TABLE `autoreplymessage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `avatars`
--
ALTER TABLE `avatars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `badges`
--
ALTER TABLE `badges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barcodescanner`
--
ALTER TABLE `barcodescanner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barcode_no`
--
ALTER TABLE `barcode_no`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barcode_ticket_category`
--
ALTER TABLE `barcode_ticket_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chatcamp_data`
--
ALTER TABLE `chatcamp_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comment` (`comment`);

--
-- Indexes for table `ci_session`
--
ALTER TABLE `ci_session`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companyeventdata`
--
ALTER TABLE `companyeventdata`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companyeventtickettransaction`
--
ALTER TABLE `companyeventtickettransaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companyreferralcode`
--
ALTER TABLE `companyreferralcode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companyserverurl`
--
ALTER TABLE `companyserverurl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companysponsor`
--
ALTER TABLE `companysponsor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companysponsortitle`
--
ALTER TABLE `companysponsortitle`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companysubscriptiontype`
--
ALTER TABLE `companysubscriptiontype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_activity_icons`
--
ALTER TABLE `company_activity_icons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_avatars`
--
ALTER TABLE `company_avatars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_badges`
--
ALTER TABLE `company_badges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_employee_configuration`
--
ALTER TABLE `company_employee_configuration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_event`
--
ALTER TABLE `company_event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_event_data`
--
ALTER TABLE `company_event_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_modules`
--
ALTER TABLE `company_modules`
  ADD UNIQUE KEY `company_id` (`company_id`);

--
-- Indexes for table `company_notifications`
--
ALTER TABLE `company_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_payment_gateway`
--
ALTER TABLE `company_payment_gateway`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_question`
--
ALTER TABLE `company_question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_question_categories`
--
ALTER TABLE `company_question_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_signupconf`
--
ALTER TABLE `company_signupconf`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_sponsor`
--
ALTER TABLE `company_sponsor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_user_event_shared_ticket`
--
ALTER TABLE `company_user_event_shared_ticket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_user_event_ticket`
--
ALTER TABLE `company_user_event_ticket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_user_role`
--
ALTER TABLE `company_user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_values`
--
ALTER TABLE `company_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_wheel_categories`
--
ALTER TABLE `company_wheel_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_zoom_accounts`
--
ALTER TABLE `company_zoom_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `corporate_menu`
--
ALTER TABLE `corporate_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `corporate_values`
--
ALTER TABLE `corporate_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `device_version`
--
ALTER TABLE `device_version`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employeesubscription`
--
ALTER TABLE `employeesubscription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_badges`
--
ALTER TABLE `employee_badges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_gossip`
--
ALTER TABLE `employee_gossip`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_information`
--
ALTER TABLE `employee_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_request`
--
ALTER TABLE `employee_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_ticket_category`
--
ALTER TABLE `event_ticket_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facedata`
--
ALTER TABLE `facedata`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facedetection`
--
ALTER TABLE `facedetection`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `filter_settings`
--
ALTER TABLE `filter_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotel_rooms`
--
ALTER TABLE `hotel_rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotel_user`
--
ALTER TABLE `hotel_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotel_user_people`
--
ALTER TABLE `hotel_user_people`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `itinerary_buckets`
--
ALTER TABLE `itinerary_buckets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `itinerary_ticket_bank`
--
ALTER TABLE `itinerary_ticket_bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `livepollmessagedata`
--
ALTER TABLE `livepollmessagedata`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `livepoll_groupchannel`
--
ALTER TABLE `livepoll_groupchannel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `livepoll_questions`
--
ALTER TABLE `livepoll_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `livepoll_word_weight`
--
ALTER TABLE `livepoll_word_weight`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `livequestionmessagedata`
--
ALTER TABLE `livequestionmessagedata`
  ADD PRIMARY KEY (`id`),
  ADD KEY `like` (`like`);

--
-- Indexes for table `livequestion_groupchannel`
--
ALTER TABLE `livequestion_groupchannel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `livesession`
--
ALTER TABLE `livesession`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `livesession_message_like_user`
--
ALTER TABLE `livesession_message_like_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `livestream_groupchannel`
--
ALTER TABLE `livestream_groupchannel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `live_session_join_user`
--
ALTER TABLE `live_session_join_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lottery_data`
--
ALTER TABLE `lottery_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lottery_participants`
--
ALTER TABLE `lottery_participants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `participant_id` (`participant_id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `lottery_winners`
--
ALTER TABLE `lottery_winners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meal_type`
--
ALTER TABLE `meal_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message_comment`
--
ALTER TABLE `message_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message_platform`
--
ALTER TABLE `message_platform`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobile_branding`
--
ALTER TABLE `mobile_branding`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobile_menu`
--
ALTER TABLE `mobile_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `new_activity`
--
ALTER TABLE `new_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `new_activity_tasks`
--
ALTER TABLE `new_activity_tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_templates`
--
ALTER TABLE `notification_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oneteamnotifications`
--
ALTER TABLE `oneteamnotifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oneteampolltheme`
--
ALTER TABLE `oneteampolltheme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oneteamserverurl`
--
ALTER TABLE `oneteamserverurl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oneteam_branding`
--
ALTER TABLE `oneteam_branding`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oneteam_buzzlive_active_users_count`
--
ALTER TABLE `oneteam_buzzlive_active_users_count`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `livestream_url_id` (`livestream_url_id`);

--
-- Indexes for table `oneteam_buzzlive_active_users_list`
--
ALTER TABLE `oneteam_buzzlive_active_users_list`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`livestream_url_id`);

--
-- Indexes for table `oneteam_buzzlive_join_users_count`
--
ALTER TABLE `oneteam_buzzlive_join_users_count`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `livestream_url_id` (`livestream_url_id`);

--
-- Indexes for table `oneteam_buzzlive_join_users_list`
--
ALTER TABLE `oneteam_buzzlive_join_users_list`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`livestream_url_id`);

--
-- Indexes for table `oneteam_buzzlive_reactions`
--
ALTER TABLE `oneteam_buzzlive_reactions`
  ADD PRIMARY KEY (`rid`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `report_table` (`report_table`);

--
-- Indexes for table `oneteam_buzzlive_reactions_crismas`
--
ALTER TABLE `oneteam_buzzlive_reactions_crismas`
  ADD PRIMARY KEY (`rid`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `report_table` (`report_table`);

--
-- Indexes for table `oneteam_buzzlive_reactions_diwali`
--
ALTER TABLE `oneteam_buzzlive_reactions_diwali`
  ADD PRIMARY KEY (`rid`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `report_table` (`report_table`);

--
-- Indexes for table `oneteam_buzzlive_reactions_old`
--
ALTER TABLE `oneteam_buzzlive_reactions_old`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oneteam_buzzlive_reactions_report`
--
ALTER TABLE `oneteam_buzzlive_reactions_report`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reaction_id` (`reaction_id`,`livestream_url_id`);

--
-- Indexes for table `oneteam_buzzlive_streaming_url`
--
ALTER TABLE `oneteam_buzzlive_streaming_url`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oneteam_buzzlive_streaming_url_main`
--
ALTER TABLE `oneteam_buzzlive_streaming_url_main`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oneteam_log`
--
ALTER TABLE `oneteam_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `o_agree`
--
ALTER TABLE `o_agree`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reaction_id` (`reaction_id`),
  ADD UNIQUE KEY `livestream_url_id` (`livestream_url_id`);

--
-- Indexes for table `o_applaud`
--
ALTER TABLE `o_applaud`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reaction_id` (`reaction_id`),
  ADD UNIQUE KEY `livestream_url_id` (`livestream_url_id`);

--
-- Indexes for table `o_awesome`
--
ALTER TABLE `o_awesome`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reaction_id` (`reaction_id`),
  ADD UNIQUE KEY `livestream_url_id` (`livestream_url_id`);

--
-- Indexes for table `o_gdmorning`
--
ALTER TABLE `o_gdmorning`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reaction_id` (`reaction_id`),
  ADD UNIQUE KEY `livestream_url_id` (`livestream_url_id`);

--
-- Indexes for table `o_jabardast`
--
ALTER TABLE `o_jabardast`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reaction_id` (`reaction_id`),
  ADD UNIQUE KEY `livestream_url_id` (`livestream_url_id`);

--
-- Indexes for table `o_nice`
--
ALTER TABLE `o_nice`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reaction_id` (`reaction_id`),
  ADD UNIQUE KEY `livestream_url_id` (`livestream_url_id`);

--
-- Indexes for table `poll`
--
ALTER TABLE `poll`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `poll_response`
--
ALTER TABLE `poll_response`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_categories`
--
ALTER TABLE `question_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reaction_live_url`
--
ALTER TABLE `reaction_live_url`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reaction_report`
--
ALTER TABLE `reaction_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reaction_type`
--
ALTER TABLE `reaction_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reward_rules`
--
ALTER TABLE `reward_rules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scanningcompany`
--
ALTER TABLE `scanningcompany`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scanningticketcategory`
--
ALTER TABLE `scanningticketcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scanpriticketlist`
--
ALTER TABLE `scanpriticketlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scanticketlist`
--
ALTER TABLE `scanticketlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scanticketlistinvalid`
--
ALTER TABLE `scanticketlistinvalid`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sendbird_data`
--
ALTER TABLE `sendbird_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `signup_payment`
--
ALTER TABLE `signup_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `step_counter_data`
--
ALTER TABLE `step_counter_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `step_counter_like`
--
ALTER TABLE `step_counter_like`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `survey`
--
ALTER TABLE `survey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `survey_question`
--
ALTER TABLE `survey_question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `survey_response`
--
ALTER TABLE `survey_response`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ticketconfiguration`
--
ALTER TABLE `ticketconfiguration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ticket_report`
--
ALTER TABLE `ticket_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ticket_scan_result`
--
ALTER TABLE `ticket_scan_result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ticket_transactions`
--
ALTER TABLE `ticket_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ticket_user`
--
ALTER TABLE `ticket_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `titlefight_votes`
--
ALTER TABLE `titlefight_votes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transport_type`
--
ALTER TABLE `transport_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userrequest`
--
ALTER TABLE `userrequest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warriors_buzzlive_active_users_count`
--
ALTER TABLE `warriors_buzzlive_active_users_count`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `livestream_url_id` (`livestream_url_id`);

--
-- Indexes for table `warriors_buzzlive_active_users_list`
--
ALTER TABLE `warriors_buzzlive_active_users_list`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`livestream_url_id`);

--
-- Indexes for table `warriors_buzzlive_join_users_count`
--
ALTER TABLE `warriors_buzzlive_join_users_count`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `livestream_url_id` (`livestream_url_id`);

--
-- Indexes for table `warriors_buzzlive_join_users_list`
--
ALTER TABLE `warriors_buzzlive_join_users_list`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`livestream_url_id`);

--
-- Indexes for table `warriors_buzzlive_reactions`
--
ALTER TABLE `warriors_buzzlive_reactions`
  ADD PRIMARY KEY (`rid`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `warriors_buzzlive_reactions_report`
--
ALTER TABLE `warriors_buzzlive_reactions_report`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reaction_id` (`reaction_id`,`livestream_url_id`);

--
-- Indexes for table `warriors_buzzlive_streaming_url`
--
ALTER TABLE `warriors_buzzlive_streaming_url`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warriors_lottery_data`
--
ALTER TABLE `warriors_lottery_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warriors_lottery_participants`
--
ALTER TABLE `warriors_lottery_participants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `participant_id` (`participant_id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `warriors_lottery_winners`
--
ALTER TABLE `warriors_lottery_winners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `webhookuser`
--
ALTER TABLE `webhookuser`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wheel_attempts`
--
ALTER TABLE `wheel_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wheel_categories`
--
ALTER TABLE `wheel_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wheel_discovery`
--
ALTER TABLE `wheel_discovery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wheel_questions`
--
ALTER TABLE `wheel_questions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity`
--
ALTER TABLE `activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `activity_icons`
--
ALTER TABLE `activity_icons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `activity_tasks`
--
ALTER TABLE `activity_tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `autoreplymessage`
--
ALTER TABLE `autoreplymessage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `avatars`
--
ALTER TABLE `avatars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `badges`
--
ALTER TABLE `badges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `barcodescanner`
--
ALTER TABLE `barcodescanner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `barcode_no`
--
ALTER TABLE `barcode_no`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `barcode_ticket_category`
--
ALTER TABLE `barcode_ticket_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `chatcamp_data`
--
ALTER TABLE `chatcamp_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `companyeventdata`
--
ALTER TABLE `companyeventdata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `companyeventtickettransaction`
--
ALTER TABLE `companyeventtickettransaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `companyreferralcode`
--
ALTER TABLE `companyreferralcode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `companyserverurl`
--
ALTER TABLE `companyserverurl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `companysponsor`
--
ALTER TABLE `companysponsor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `companysponsortitle`
--
ALTER TABLE `companysponsortitle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `companysubscriptiontype`
--
ALTER TABLE `companysubscriptiontype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `company_activity_icons`
--
ALTER TABLE `company_activity_icons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `company_avatars`
--
ALTER TABLE `company_avatars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `company_badges`
--
ALTER TABLE `company_badges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `company_employee_configuration`
--
ALTER TABLE `company_employee_configuration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `company_event`
--
ALTER TABLE `company_event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `company_event_data`
--
ALTER TABLE `company_event_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `company_notifications`
--
ALTER TABLE `company_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `company_payment_gateway`
--
ALTER TABLE `company_payment_gateway`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `company_question`
--
ALTER TABLE `company_question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `company_question_categories`
--
ALTER TABLE `company_question_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `company_signupconf`
--
ALTER TABLE `company_signupconf`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `company_sponsor`
--
ALTER TABLE `company_sponsor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `company_user_event_shared_ticket`
--
ALTER TABLE `company_user_event_shared_ticket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `company_user_event_ticket`
--
ALTER TABLE `company_user_event_ticket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `company_user_role`
--
ALTER TABLE `company_user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `company_values`
--
ALTER TABLE `company_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `company_wheel_categories`
--
ALTER TABLE `company_wheel_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `company_zoom_accounts`
--
ALTER TABLE `company_zoom_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `corporate_menu`
--
ALTER TABLE `corporate_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `corporate_values`
--
ALTER TABLE `corporate_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `device_version`
--
ALTER TABLE `device_version`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `employeesubscription`
--
ALTER TABLE `employeesubscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `employee_badges`
--
ALTER TABLE `employee_badges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employee_gossip`
--
ALTER TABLE `employee_gossip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `employee_information`
--
ALTER TABLE `employee_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee_request`
--
ALTER TABLE `employee_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `event_ticket_category`
--
ALTER TABLE `event_ticket_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `facedata`
--
ALTER TABLE `facedata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `facedetection`
--
ALTER TABLE `facedetection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `filter_settings`
--
ALTER TABLE `filter_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `hotel`
--
ALTER TABLE `hotel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `hotel_rooms`
--
ALTER TABLE `hotel_rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `hotel_user`
--
ALTER TABLE `hotel_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `hotel_user_people`
--
ALTER TABLE `hotel_user_people`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `itinerary_buckets`
--
ALTER TABLE `itinerary_buckets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `itinerary_ticket_bank`
--
ALTER TABLE `itinerary_ticket_bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `livepollmessagedata`
--
ALTER TABLE `livepollmessagedata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `livepoll_groupchannel`
--
ALTER TABLE `livepoll_groupchannel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `livepoll_questions`
--
ALTER TABLE `livepoll_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `livepoll_word_weight`
--
ALTER TABLE `livepoll_word_weight`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `livequestionmessagedata`
--
ALTER TABLE `livequestionmessagedata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `livequestion_groupchannel`
--
ALTER TABLE `livequestion_groupchannel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `livesession`
--
ALTER TABLE `livesession`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `livesession_message_like_user`
--
ALTER TABLE `livesession_message_like_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `livestream_groupchannel`
--
ALTER TABLE `livestream_groupchannel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `live_session_join_user`
--
ALTER TABLE `live_session_join_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lottery_data`
--
ALTER TABLE `lottery_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lottery_participants`
--
ALTER TABLE `lottery_participants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lottery_winners`
--
ALTER TABLE `lottery_winners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `meal_type`
--
ALTER TABLE `meal_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `message_comment`
--
ALTER TABLE `message_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `message_platform`
--
ALTER TABLE `message_platform`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mobile_branding`
--
ALTER TABLE `mobile_branding`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `mobile_menu`
--
ALTER TABLE `mobile_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `new_activity`
--
ALTER TABLE `new_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `new_activity_tasks`
--
ALTER TABLE `new_activity_tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notification_templates`
--
ALTER TABLE `notification_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `oneteamnotifications`
--
ALTER TABLE `oneteamnotifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `oneteampolltheme`
--
ALTER TABLE `oneteampolltheme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oneteamserverurl`
--
ALTER TABLE `oneteamserverurl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oneteam_branding`
--
ALTER TABLE `oneteam_branding`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `oneteam_buzzlive_active_users_count`
--
ALTER TABLE `oneteam_buzzlive_active_users_count`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oneteam_buzzlive_active_users_list`
--
ALTER TABLE `oneteam_buzzlive_active_users_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oneteam_buzzlive_join_users_count`
--
ALTER TABLE `oneteam_buzzlive_join_users_count`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oneteam_buzzlive_join_users_list`
--
ALTER TABLE `oneteam_buzzlive_join_users_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40906;

--
-- AUTO_INCREMENT for table `oneteam_buzzlive_reactions_old`
--
ALTER TABLE `oneteam_buzzlive_reactions_old`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `oneteam_buzzlive_reactions_report`
--
ALTER TABLE `oneteam_buzzlive_reactions_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oneteam_buzzlive_streaming_url`
--
ALTER TABLE `oneteam_buzzlive_streaming_url`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oneteam_buzzlive_streaming_url_main`
--
ALTER TABLE `oneteam_buzzlive_streaming_url_main`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oneteam_log`
--
ALTER TABLE `oneteam_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `o_agree`
--
ALTER TABLE `o_agree`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `o_applaud`
--
ALTER TABLE `o_applaud`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `o_awesome`
--
ALTER TABLE `o_awesome`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `o_gdmorning`
--
ALTER TABLE `o_gdmorning`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `o_jabardast`
--
ALTER TABLE `o_jabardast`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `o_nice`
--
ALTER TABLE `o_nice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `poll`
--
ALTER TABLE `poll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `poll_response`
--
ALTER TABLE `poll_response`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `question_categories`
--
ALTER TABLE `question_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `reaction_live_url`
--
ALTER TABLE `reaction_live_url`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `reaction_report`
--
ALTER TABLE `reaction_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `reaction_type`
--
ALTER TABLE `reaction_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `reward_rules`
--
ALTER TABLE `reward_rules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scanningcompany`
--
ALTER TABLE `scanningcompany`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scanningticketcategory`
--
ALTER TABLE `scanningticketcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `scanpriticketlist`
--
ALTER TABLE `scanpriticketlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scanticketlist`
--
ALTER TABLE `scanticketlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=387;

--
-- AUTO_INCREMENT for table `scanticketlistinvalid`
--
ALTER TABLE `scanticketlistinvalid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sendbird_data`
--
ALTER TABLE `sendbird_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `signup_payment`
--
ALTER TABLE `signup_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=208;

--
-- AUTO_INCREMENT for table `step_counter_data`
--
ALTER TABLE `step_counter_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `step_counter_like`
--
ALTER TABLE `step_counter_like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `survey`
--
ALTER TABLE `survey`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `survey_question`
--
ALTER TABLE `survey_question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `survey_response`
--
ALTER TABLE `survey_response`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=759;

--
-- AUTO_INCREMENT for table `ticketconfiguration`
--
ALTER TABLE `ticketconfiguration`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ticket_report`
--
ALTER TABLE `ticket_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6690;

--
-- AUTO_INCREMENT for table `ticket_scan_result`
--
ALTER TABLE `ticket_scan_result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_transactions`
--
ALTER TABLE `ticket_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_user`
--
ALTER TABLE `ticket_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `titlefight_votes`
--
ALTER TABLE `titlefight_votes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transport_type`
--
ALTER TABLE `transport_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `userrequest`
--
ALTER TABLE `userrequest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `warriors_buzzlive_active_users_count`
--
ALTER TABLE `warriors_buzzlive_active_users_count`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `warriors_buzzlive_active_users_list`
--
ALTER TABLE `warriors_buzzlive_active_users_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `warriors_buzzlive_join_users_count`
--
ALTER TABLE `warriors_buzzlive_join_users_count`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `warriors_buzzlive_join_users_list`
--
ALTER TABLE `warriors_buzzlive_join_users_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `warriors_buzzlive_reactions_report`
--
ALTER TABLE `warriors_buzzlive_reactions_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `warriors_buzzlive_streaming_url`
--
ALTER TABLE `warriors_buzzlive_streaming_url`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `warriors_lottery_data`
--
ALTER TABLE `warriors_lottery_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `warriors_lottery_participants`
--
ALTER TABLE `warriors_lottery_participants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `warriors_lottery_winners`
--
ALTER TABLE `warriors_lottery_winners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `webhookuser`
--
ALTER TABLE `webhookuser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wheel_attempts`
--
ALTER TABLE `wheel_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `wheel_categories`
--
ALTER TABLE `wheel_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `wheel_discovery`
--
ALTER TABLE `wheel_discovery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wheel_questions`
--
ALTER TABLE `wheel_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
